import React, { Component } from 'react';
import { Text, StyleSheet} from 'react-native';
import validatejs from 'validate.js';
import ValidationRule from './Rules';

var isValid = true;

// Mengambil Value Inputan
function onInputChange({ id, value }) {
	const { inputs } = this.state;
	this.setState(
	{
		inputs: {
			...inputs,
			[id]: getValidationState({
				input: inputs[id],
				value
			})
		}
	});
};

// Mengembalikan state terbaru yang sudah di validasi
function getValidationState({ input, value }) {
	return {
		...input,
		value,
		error: input.optional ? null : validate({ type: input.type, value })
	}

};

// Cek validasi inputan
function validate({ type, value }) {

	const result = validatejs(
		{
			[type]: value
		},
		{
			[type]: ValidationRule[type]
		});

	if (result) {
		return result[type][0];
	}

	return null;
};

function isValidateInput(){
	return isValid;
}

// Cek validasi inputan dan lakukan update state
function validateInput({ id, value }) {
	const { inputs } = this.state;
	let validationState = getValidationState({
		input: inputs[id],
		value : value
	});

	this.setState(
	{
		inputs: {
			...inputs,
			[id]: validationState
		}
	});

	if(validationState.error == null){
		return true;
	}

	return false;

};

// Cek seluruh inputan yang dites
function isValidate() {

//	console.log(this.state);
	 const { inputs } = this.state;
	 const updatedInputs = {};
   var counterFail = 0;

	 /* https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/entries */
	 for (const [key, input] of Object.entries(inputs)) {
     let validationState = getValidationState({
 		  input,
 		  value: input.value
 		});

    if((input.optional === undefined || input.optional === false) && validationState.error !== null){
      counterFail+= 1;
    }

		updatedInputs[key] = validationState;
	 }

	this.setState({
		inputs: updatedInputs
	});

  if(counterFail>0) return false;

  return true;
};

function validateError(id) {
  const { inputs } = this.state;
  if (inputs[id].error) {
    return <Text style={styles.errorLabel}>{inputs[id].error}</Text>;
  }
  return null;
};

const styles = StyleSheet.create({
  errorLabel: {
    color: "red",
    marginLeft : 15,
    fontSize: 10
  }
});

export const ValidationHelper = {
	onInputChange,
	validateInput,
	getValidationState,
	validate,
	isValidate,
	validateError,
	isValidateInput
}
