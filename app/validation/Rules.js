export default ValidationRule = {
  email: {
	presence: {
		allowEmpty: false,
		message: '^Email tidak boleh kosong'
	},
	email: {
		message: '^Email tidak valid'
	}
},
general: {
	presence: {
		allowEmpty: false,
		message: '^Tidak boleh kosong'
	}
},
phoneNumber: {
	presence: {
		allowEmpty: false,
		message: '^Nomor handhpone tidak boleh kosong'
	},
	format: {
		pattern: /^\d+$/,
    message: '^Nomor handhpone tidak valid'
	},
  length: {
	  minimum: 10,
	  message: "^Nomor handphone minimal 10 digit"
	}
},
password: {
	presence: {
	  allowEmpty: false,
	  message: "^Password tidak boleh kosong"
	},
	format:{
		pattern: /^((?=.*[A-Za-z])(?=.*\d)|(?=.*[A-Za-z])(?=.*[@$!%*#?&])|(?=.*\d)(?=.*[@$!%*#?&])).{8,12}$/,
		message: "^Panjang password minimal 8 karakter, terdiri dari minimal 2 tipe karakter (huruf, angka atau simbol)"
	}
},
name: {
	presence: {
		allowEmpty: false,
		message: '^Nama tidak boleh kosong'
	},
  format: {
		pattern: /^[a-zA-Z\s]*$/,
    message: '^Nama harus mengandung alfabet'
	}
},
pin: {
	presence: {
		allowEmpty: false,
		message: '^Pin tidak boleh kosong'
	},
	format: {
		pattern: /^\d+$/,
    message: '^Pin tidak valid'
	}
},
otp: {
	presence: {
		allowEmpty: false,
		message: '^Otp tidak boleh kosong'
	},
	format: {
		pattern: /^\d+$/,
    message: '^Otp tidak valid'
	}
},
noMeter: {
	presence: {
		allowEmpty: false,
		message: '^Nomor meter tidak boleh kosong'
	},
	format: {
		pattern: /^\d+$/,
    message: '^Nomor meter tidak valid'
	},
  length: {
	  minimum: 11,
	  message: "^Nomor meter minimal 11 digit"
	}
},
idPelanggan: {
	presence: {
		allowEmpty: false,
		message: '^Id pelanggan tidak boleh kosong'
	},
	format: {
		pattern: /^\d+$/,
    message: '^Id pelanggan tidak valid'
	},
  length: {
	  minimum: 11,
	  message: "^Id pelanggan minimal 11 digit"
	}
},
nominal: {
	presence: {
		allowEmpty: false,
		message: '^Nominal tidak boleh kosong'
	},
	format: {
		pattern: /^\d+$/,
    message: '^Nominal tidak valid'
	}
},
nik: {
	presence: {
		allowEmpty: false,
		message: '^NIK tidak boleh kosong'
	},
	format: {
		pattern: /^\d+$/,
    message: '^NIK tidak valid'
	},
  length: {
	  minimum: 16,
	  message: "^NIK 16 digit"
	}
},
cashOut: {
	presence: {
		allowEmpty: false,
		message: '^Jumlah penarikan tidak boleh kosong'
	},
	/*format: {
		pattern: /^\d+$/,
    message: '^Jumlah penarikan tidak valid'
	},*/
  length: {
	  minimum: 8,//tambahan 2 digit 'Rp' dan 1 delimeter '.'
	  message: "^Jumlah penarikan minimal 10.000"
	}
},
accountNum: {
	presence: {
		allowEmpty: false,
		message: '^Nomor rekening tidak boleh kosong'
	},
	format: {
		pattern: /^\d+$/,
    message: '^Nomor rekening tidak valid'
	}
},
currencyIDR: {
	presence: {
	  allowEmpty: false,
	  message: "^Nominal tidak boleh kosong"
	},
	/*format:{
		pattern: /^\d+(\.\d{1,2})?$/,
		message: "^Nominal tidak valid"
	}*/
},
}
