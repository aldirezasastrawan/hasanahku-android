
import { StyleSheet, Dimensions } from 'react-native';

const size = Dimensions.get('window');

export default StyleSheet.create({
    btnBack: {
        marginLeft: 20,
        marginTop: 5
    },
    navBox: {
        flexDirection: 'row',
        marginTop: 20,
        flex: 1
    },
    navBoxTitle: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginLeft: 10
    },
    navTitle: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 18,
    },
    bannerBox: {
        margin : 20
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    bannerContent: {
        backgroundColor: "#FFF2DF",
        height: size.height / 7,
        width: '100%',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    bannerInfo: {
        flex: 1
    },
    bannerLogo: {
        height: 55,
        width: 42,
        margin : 20
    },
    bannerTitle: {
        fontFamily: 'Roboto',
        color: '#783F2E',
        fontSize: 20,
        paddingTop:10,
        paddingBottom : 10,
    },
    bannerDesc: {
        fontFamily: 'Roboto',
        color: '#131A22',
        fontSize: 10,
        lineHeight : 12,
        paddingBottom : 10,
    },
    tabBox : {
      marginLeft : 20,
      marginRight : 20
    },
    tabContent : {
      marginTop : 10
    },
    formBox: {
        marginTop: 20,
        marginBottom:20,
        marginLeft:10,
        marginRight:20
    },
    itemTrx : {
      borderColor: 'transparent',
      marginBottom: 15
    },
    itemHeaderTrx: {
        marginBottom: 10
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    labelHeaderTrx: {
        fontSize: 14
    },
    labelTrx: {
        fontSize: 13,
    },
    input: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    labelInput: {
        fontWeight: 'bold',
        fontSize: 13,
        paddingLeft: 10
    },
    labelTrxValue: {
        flex: 1,
        textAlign : 'right',
        fontSize: 13
    },
    btnDefaultBox: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnDefault: {
        width: size.width - 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#4caf50',
        justifyContent: 'center'
    },
    btnDefaultText: {
        color: '#FFFFFF',
        fontFamily: 'Roboto',
        fontWeight: 'bold'
    },
    boxTotalConfirm: {
      backgroundColor: '#F3EFFF',
      height : 35,
      justifyContent : 'center',
      marginBottom : 20
    },
    sheetTitle: {
        fontWeight: 'bold',
        fontSize: 24,
        paddingLeft: 20
    },
    boxPinReset: {
        marginTop: 10
    },
    txtPinReset: {
        color: '#2a8b40',
        fontWeight: 'bold'
    },
    headerTrxSuccess: {
        height: 100,
        backgroundColor: '#e7fcfa',
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkedHeaderTrxSuccessContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkedHeaderTrxSuccessMasking: {
        marginTop: -20,
        borderWidth: 20,
        borderColor: '#fff',
        borderRadius: 50,
        backgroundColor: '#fff'
    },
    txtTrxSuccess:{
      fontWeight:'bold',
      fontSize:18
    },
    btnheaderTrxSuccessSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -30
    },
    btnShareTrxSuccess: {
        backgroundColor: '#FFF1F0',
        padding: 10,
        height:30,
        borderRadius: 20,
        marginRight: 10
    },
    txtShareTrxSuccess: {
        marginLeft: 5,
        color: '#FF4133'
    },
    btnDownloadTrxSuccess: {
        backgroundColor: '#e7fcfa',
        padding: 10,
        height: 30,
        borderRadius: 20
    },
    txtBtnDownloadTrxSuccess: {
        marginLeft: 5,
        color: '#21c4a0'
    },
    titleContainerTrxSuccess: {
        margin: 10,
        backgroundColor: '#f9f8ff',
        borderRadius: 10,
        justifyContent: 'center'
    },
    txtTitleContainerTrxSuccess: {
        fontSize: 14,
        color: '#283757',
        padding: 10,
        fontWeight: 'bold',
    },
    btnBackContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    phoneContactContainer: {
        flex: 1,
    },
    phoneContactTitleContainer: {
        marginLeft: 25
    },
    phoneContactTitle: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    phoneContactHeader: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    phoneContactViewHeader: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#f7f7f7',
        borderRadius: 30,
        width: Dimensions.get('window').width - 50,
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 5,
    },
    phoneContactInputIcon: {
        marginLeft: 15
    },
    phoneContactInputHeader: {
        height: 40,
        marginLeft: 5,
        borderBottomColor: '#FFFFFF',
        flex: 1,
        fontSize: 13
    },
    phoneContactBody: {
        flex: 2,
        margin: 20
    },
    phoneContactListContainer: {
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        margin: 5,
    },
    phoneContactAvaContainer: {
        backgroundColor: '#fff',
        borderWidth: 2,
        borderColor: '#dedede',
        borderRadius: 50,
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    phoneContactAvaTxt: {
        fontSize: 24
    },
    phoneContactInfo: {
        marginBottom: 10,
        marginLeft: 15,
        width: '70%'
        // justifyContent: 'center',
    },
    phoneContactName: {
        fontWeight: 'bold',
        fontSize: 18
    },
    phoneContactItem: {
        marginTop: 5,
        marginBottom: 5,
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f7f7f7',
        width: '100%'
    },
    phoneContactNumber: {
        color: '#666',
        fontSize: 13,
        padding: 3,
        flex: 2
    },
    phoneContactChooseItem: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    phoneContactbtnItem: {
        height: 30,
        padding: 5,
        backgroundColor: '#02aced',
    },
    phoneContactTxtItem: {
        color: '#fff',
        fontWeight: 'bold'
    },
    labelFootNote: {
      color:'#C76050',
      fontSize: 10
    }
})
