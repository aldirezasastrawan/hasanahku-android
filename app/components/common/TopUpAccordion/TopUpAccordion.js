import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, FlatList, LayoutAnimation, UIManager, Platform } from 'react-native';
import { Icon } from 'react-native-elements';

class TopUpAccordion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            expanded: false,
        };

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    toggleExpand = () => {
        this.setState({ expanded: !this.state.expanded })
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    }

    render() {
        return (
            <View style={styles.listItemContainer}>
                <TouchableOpacity style={styles.row} onPress={() => this.toggleExpand()}>
                    <Icon containerStyle={styles.listIcon} name={this.props.icon} type={this.props.iconType} size={this.props.iconSize} />
                    <Text style={styles.listItemName}>{this.props.name}</Text>
                    <Icon name={this.state.expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'} size={30} color="#b8b8b8" />
                </TouchableOpacity>
                <View style={styles.parentHr} />
                {
                    this.state.expanded &&
                    <View style={{}}>
                        <FlatList
                            data={this.state.data}
                            numColumns={1}
                            scrollEnabled={false}
                            renderItem={({ item, index }) =>
                                <View>
                                    <View style={styles.dataListContainer}>
                                        <Text style={styles.pageTxt}>{item.step}.</Text>
                                        <Text style={[styles.font, styles.itemInActive]} >{item.nama}.</Text>
                                    </View>
                                    <View style={styles.childHr} />
                                </View>
                            } />
                    </View>
                }
            </View>
        );
    }
}

const size = Dimensions.get('window')


const styles = StyleSheet.create({
    listItemContainer: {
        width: size.width - 50,
        // backgroundColor: '#dedede',
        margin: 5
    },
    row: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        margin: 10,
        borderBottomWidth: 0.5,
        borderBottomColor: '#efefef',
        // padding: 10
    },
    listItemName: {
        flex: 1,
        color: '#000',
        fontWeight: '700'
    },
    dataListContainer: {
        flexDirection: 'row'
    },
    pageTxt: {
        marginRight: 5
    },
    listIcon: {
        marginRight: 20,
        backgroundColor: '#fcfbfc',
        borderRadius: 20,
        height: 60,
        width: 60,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default TopUpAccordion;
