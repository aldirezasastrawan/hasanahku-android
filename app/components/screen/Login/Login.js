import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Container, Content, Button, Form, Item, Input } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { post } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import { getValue, saveData } from '../../../module/LocalData/Storage';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';
import DeviceInfo from 'react-native-device-info';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            showPassword: false,
            fcmKey: '',
            versionName: '',
            inputs: {
                password: {
                    type: 'password',
                    value: ''
                }
            }
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
    }

    componentDidMount() {
        this.getFcmKey()
        this.getVersionName()
    }

    getVersionName(){
      this.setState({
          versionName: DeviceInfo.getVersion()
      })
    }

    getFcmKey() {
        getValue('fcm').then(fcmKey => {
            this.setState({
                fcmKey: fcmKey
            })
        })
    }

    doLogin() {

        if(this.isValidate() === false) return;

        this.setState({
            showSpinner: true,
        });

        let params = {
            "phone": this.props.navigation.getParam('phone'),
            "password": this.state.inputs.password.value,
            "fcmKey" : this.state.fcmKey,
            "versionName" : this.state.versionName
        }

        post(Env.base_url + Api.loginUserChannel, params).then(response => {

          this.setState({showSpinner: false});

          if(response.code == ResponseCode.OK){
            saveData('userLogin', response.okContent);
            saveData('hasLogin', true);
            saveData('tokenLogin', response.okContent.token);
            this.props.navigation.navigate('Home2');
          }else{
            dialog.alertFail(response.failContent, this);
          }
        }).catch(err => {
          this.setState({showSpinner: false});
          dialog.alertException(err);
        });
    }

    hubungiKami() {
        this.props.navigation.navigate('Hubungikami')
    }

    lupaPassword() {
        this.props.navigation.navigate('VerifikasiLupaPassword', {
            "phone": this.props.navigation.getParam('phone')
        })
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>Login</Text>
                        </View>
                    </View>

                    <View style={styles.infoContainer}>
                        <Text style={styles.infoTitle}>Masukkan password Anda</Text>
                        <Text style={styles.infoSubTitle}>Jaga Kerahasiaan Password Anda</Text>
                    </View>
                    <View style={styles.inputContainer}>

                        <Form>
                            <Item stackedLabel last>
                                <View style={{ flexDirection: 'row' }}>
                                    <Input
                                        secureTextEntry={this.state.showPassword == false ? true : false}
                                        style={styles.input} maxLength={12} keyboardType='default'onChangeText={value => this.onInputChange({id: 'password', value})}
                                    />
                                    <Icon
                                        onPress={() => this.setState({ showPassword: !this.state.showPassword })}
                                        active
                                        name={this.state.showPassword == true ? "eye" : "eye-slash"} type="font-awesome" />
                                </View>
                            </Item>
                            {this.validateError('password')}
                        </Form>
                    </View>

                    <View style={styles.btnRegisterContainer}>
                        <Button onPress={() => this.doLogin()} style={styles.btnRegister}>
                            <Text style={styles.RegisterText}>Mulai</Text>
                        </Button>
                    </View>

                    <View style={styles.btnContainer}>
                        <Button onPress={() => this.lupaPassword()} style={styles.btnLupa} transparent>
                            <Text style={styles.btnText}>Lupa Password ?</Text>
                        </Button>
                        {/* <Button onPress={() => this.hubungiKami()} style={styles.btnCallCentre} transparent>
                            <Text style={styles.btnText}>Hubungi Kami</Text>
                        </Button> */}
                    </View>

                    <Spinner
                        cancelable={true}
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        // justifyContent: 'flex-end',
        // alignItems: 'flex-end',
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    infoContainer: {
        marginTop: 30,
        marginLeft: 20,
        marginRight: 20
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    infoTitle: {
        color: '#000',
        fontSize: 24,
        fontWeight: 'bold',
    },
    infoSubTitle: {
        color: '#B8B8B8',
        fontSize: 18,
        marginTop: 20,
        fontWeight: '500',
        marginRight: 5,
    },
    inputContainer: {
        marginTop: 20,
        marginBottom: 1,
        marginLeft: 30,
        marginRight: 30
    },
    btnContainer: {
        flex: 1,
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnLupa: {
        flex: 2,
        // marginLeft: 40
    },
    btnCallCentre: {
        marginRight: 40
    },
    btnText: {
        color: '#2a8b40',
        fontWeight: 'bold'
    },
    borderStyleHighLighted: {
        borderColor: "#2a8b40",
    },
    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 2,
    },
    underlineStyleHighLighted: {
        borderColor: "#2a8b40",
    },
    borderStyleBase: {
        width: 30,
        height: 45
    },
    input: {
        fontSize: 24,
        fontWeight: 'bold',
        width: 200,
        textAlign: 'center'
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#26C165',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 24
    },
})

export default Login;
