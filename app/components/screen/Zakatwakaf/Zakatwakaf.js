import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, UIManager, LayoutAnimation, Platform } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label} from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import RBSheet from 'react-native-raw-bottom-sheet';

class Zakatwakaf extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            dataPLN: [],
            nominal: '',
            tipe: '',
            detail: '',
            prosesName: '',

        };

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentDidMount() {
        this.getDataPLN();
    }

    getDataPLN() {
        let data = [
            { typeId: 1, type: 'Zakat_Fitrah', detail: ' Zakat Fitrah' },
            { typeId: 2, type: 'Zakat_Mal', detail: 'Zakat Mal' },
            { typeId: 3, type: 'Zakat_Profesi', detail: 'Zakat Profesi' },
            { typeId: 4, type: 'Donasi', detail: 'Donasi' },
        ]

        this.setState({
            dataPLN: data
        })
    }

    selectTipe(item) {
        this.setState({ tipe: item.detail, detail: item.detail })
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);

    }

    renderPLN = ({ item }) => {
        return (
            <Button onPress={() => this.selectTipe(item)} style={styles.gridListContainer}>
                <Text style={styles.txtGrid}>{item.detail}</Text>
            </Button>
        )
    }

    openZakatModal() {
        this.setState({
            showSpinner: true,
            prosesName: "Form Zakat"
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
            });

            // this.props.navigation.navigate('ZakatwakafConfirmation')
            // this.RBSheet.open()


        }, 1000);
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    }

    openWakafModal() {
        this.setState({
            showSpinner: true,
            prosesName: "Form Wakaf"
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
            });

            // this.props.navigation.navigate('ZakatwakafConfirmation')
            // this.RBSheet.open()


        }, 1000);
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    }

    proses() {
        this.setState({
            showSpinner: true,
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
            });

            // this.props.navigation.navigate('ZakatwakafConfirmation')
            this.RBSheet.open()


        }, 1000);
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.headerContainer}>
                        <View style={styles.imageLogoContainer}>
                            <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                                <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                            </Button>
                            <View style={styles.titleNavContainer}>
                                <Text style={styles.titleNav}>Zakat & Wakaf</Text>
                            </View>
                        </View>

                        <View style={{ justifyContent: 'center', alignItems: 'center', marginRight: 20 }}>
                            <Image style={styles.bgImage} source={require('../../../assets/image/zakatBG.png')} />
                        </View>


                        <View>
                            {/* <View style={styles.formContainer}>
                                <Text style={styles.infoFormTitle}>Salurkan Zakat & Wakaf Anda melalui HasanahKu</Text>
                            </View> */}

                            <View style={styles.btnContainer}>
                                <Button onPress={() => this.openZakatModal()} style={styles.btnMenu}>
                                    <Text style={{ paddingLeft: 10 }}>Pembayaran Zakat</Text>
                                    <Icon containerStyle={{ paddingRight: 10 }} size={16} name={this.state.prosesName == "Form Zakat" ? "chevron-down" : "chevron-right"} color="#c4c4c4" type="font-awesome" />
                                </Button>

                                <Button onPress={() => this.openWakafModal()} style={styles.btnMenu}>
                                    <Text style={{ paddingLeft: 10 }}>Pembayaran Wakaf</Text>
                                    <Icon containerStyle={{ paddingRight: 10 }} size={16} name={this.state.prosesName == "Form Wakaf" ? "chevron-down" : "chevron-right"} color="#c4c4c4" type="font-awesome" />
                                </Button>
                            </View>

                            {/* {this.state.prosesName != "" ?

                                <View style={{ alignItems: 'flex-end', }}>
                                    <View style={styles.btnRegisterContainer}>
                                        <Button onPress={() => this.proses()} style={styles.btnRegister}>
                                            <Text style={styles.RegisterText}>{this.state.prosesName}</Text>
                                        </Button>
                                    </View>
                                </View>
                                : null} */}


                        </View>
                    </View>

                    {this.state.prosesName != "" ?

                        <View style={styles.formContainer}>
                            <View>
                                <Text style={styles.titleform}>{this.state.prosesName}</Text>
                            </View>

                            <View style={styles.inputContainer}>
                                <Form>
                                    <Item inlineLabel style={{ backgroundColor: "#FCFBFC", borderBottomWidth: 0 }}>
                                        <Label style={styles.label1}>Rp</Label>
                                        <Input style={styles.input1} keyboardType='number-pad' value={this.state.nominal} onChangeText={(nominal) => this.setState({ nominal: nominal })} />
                                    </Item>

                                    <Item stackedLabel>
                                        <Label style={styles.label}>Nama Penerima</Label>
                                        <Input style={styles.input} disabled value={this.state.nama} onChangeText={(nama) => this.setState({ nama: nama })} />
                                    </Item>

                                    <Item stackedLabel>
                                        <Label style={styles.label}>Saldo HasanahKu Anda</Label>
                                        <Input style={styles.input} keyboardType='numbers-and-punctuation' value={this.state.nominal} onChangeText={(nominal) => this.setState({ nominal: nominal })} />
                                    </Item>
                                </Form>

                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={styles.btnRegisterContainer}>
                                        <Button onPress={() => this.proses()} style={styles.btnRegister}>
                                            <Text style={styles.RegisterText}>Lanjutkan</Text>
                                        </Button>
                                    </View>
                                </View>
                            </View>
                        </View>

                        : null}


                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />

                    <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown
                        height={Platform.OS == 'android' ? 400 : 500}
                        // animationType="fade"
                        duration={300}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 15,
                                borderTopLeftRadius: 15,
                            },
                        }}>

                        <View>

                            <Text>aa</Text>

                        </View>
                    </RBSheet>
                </Content>
            </Container>
        );
    }
}

const size = Dimensions.get('window')

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    titleContainer: {
        marginTop: 10,
    },
    title: {
        fontSize: 24,
        color: '#e35200',
        paddingLeft: 30,
        paddingTop: 20,
        fontWeight: 'bold'
    },
    formContainer: {
        marginTop: 20,
        marginLeft: 30,
        marginRight: 30,
    },
    infoFormTitle: {
        color: '#000',
        fontSize: 16,
        fontWeight: '500'
    },
    infoFormSubTitle: {
        color: '#385757',
        fontSize: 13,
        marginTop: 20,
        fontWeight: 'bold',
    },
    inputContainer: {
        margin: 20
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    label1: {
        fontWeight: 'bold',
        fontSize: 16,
        paddingLeft: 10
    },
    input1: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#636364'
    },
    labelInput: {
        fontWeight: 'bold',
        fontSize: 13,
        paddingLeft: 10
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
    },
    btnRegister: {
        marginRight: 15,
        width: 250,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#26C165',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: '500'
    },
    gridContainer: {
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    gridListContainer: {
        height: 50,
        // width: Dimensions.get('window').width / 4,
        backgroundColor: '#2a8b40',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation: 5,
    },
    txtGrid: {
        color: '#fff',
        fontSize: 13,
        fontWeight: 'bold',
        padding: 10
    },
    checkContainer: {
        marginTop: -20,
    },
    checkTextContainer: {
        flexDirection: 'row',
        marginRight: 20
    },
    txtSetuju: {
        marginTop: 14,
        fontSize: 12
    },
    txtSyarat: {
        fontSize: 12,
        marginTop: 2,
        color: '#385757',
        fontWeight: 'bold'
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 21
    },
    bgImage: {
        height: Dimensions.get('window').height / 5,
        width: size.width - 50,
        resizeMode: 'contain'
    },
    btnContainer: {
        margin: 20,
    },
    btnMenu: {
        margin: 10,
        backgroundColor: '#FCFBFC',
        borderRadius: 10
    },
    titleform: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    headerContainer: {
        height: size.height / 2,
        // backgroundColor: '#24DB6F'
    }
})

export default Zakatwakaf;
