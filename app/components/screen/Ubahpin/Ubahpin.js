import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, BackHandler } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { getValue, saveData } from '../../../module/LocalData/Storage';
import {post, postWithToken} from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {Error, ResponseCode} from '../../../supports/Constants';
import moment from 'moment';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';


class Ubahpin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            hasVerify: false,
            isScreenNewPin: false,
            isScreenNewPinConfirm: false,
            wrongCodeCount: 0,
            currentExpDate: "",
            isBanned: false,
            inputs: {
              code: {
                type: 'pin',
                value: ''
              },
              newCode: {
                type: 'pin',
                value: ''
              },
              confirmationNewCode: {
                type: 'pin',
                value: ''
              }
            }
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
        this.handleBackButton = this.handleBackButton.bind(this);
    }

    componentWillMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
      this.goBack();
      return true;
    }

    resetInputOldPin(){
      const { inputs } = this.state;
        this.setState({
            showSpinner: false,
            inputs: {
              ...inputs,
              ['code']: {
                ...inputs['code'],
                value : '',
                error:  null
              }
            }
        });
    }

    resetInputNewPinConfirm(){
      const { inputs } = this.state;
        this.setState({
            showSpinner: false,
            inputs: {
              ...inputs,
              ['confirmationNewCode']: {
                ...inputs['confirmationNewCode'],
                value : '',
                error:  null
              }
            }
        });
    }

    resetInputNewPin(){
        const { inputs } = this.state;
        this.setState({
            showSpinner: false,
            inputs: {
                ...inputs,
                ['newCode']: {
                    ...inputs['newCode'],
                    value : '',
                    error:  null
                }
            }
        });
    }

    onFilledOldPin(oldPin) {

      const valid = ValidationHelper.validate({ type: 'pin', value : oldPin });

      if(valid !== null) return;

        this.setState({
            showSpinner: true
        });

        let params =
        {
          "phone": this.props.navigation.getParam('phone'),
          "pin": oldPin
        }

        post(Env.base_url + Api.sakuPinMatch, params).then(response => {
          if(response.code == ResponseCode.OK){
              setTimeout(() => {
                  this.setState({
                      showSpinner: false,
                      isScreenNewPin: true,
                  });
              }, 500);
          } else {
              setTimeout(() => {
                  this.setState({
                      showSpinner: false,
                      wrongCodeCount: this.state.wrongCodeCount + 1
                  });

                  this.resetInputOldPin();
                  dialog.alertFail(response.failContent, this);

                  if (this.state.wrongCodeCount == 3) {
                      const currentDate = moment().format("YYYY-MM-DD hh:mm:ss")

                      saveData('timeWrongCode', currentDate)
                      this.setState({
                          currentExpDate: currentDate
                      })
                  }
              }, 500);
          }
        }).catch(err => {
          setTimeout(() => {
              this.setState({
                  showSpinner: false
              });
              this.resetInputOldPin();
              dialog.alertException(err);
          }, 500);
        });
    }

    onFilledNewPin(newPin) {

      const valid = ValidationHelper.validate({ type: 'pin', value : newPin });

      if(valid !== null) return;

        this.setState({
            showSpinner: true
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
                isScreenNewPinConfirm: true
            });

        }, 1000);
    }

    onFilledNewPinConfirm(newPinConfirm) {

      const valid = ValidationHelper.validate({ type: 'pin', value : newPinConfirm });

      if(valid !== null) return;

        this.setState({
            showSpinner: true
        });

        getValue('tokenLogin').then(tokenLogin => {
            let params = {
                "phone": this.props.navigation.getParam('phone'),
                "oldPin": this.state.inputs.code.value,
                "newPin": this.state.inputs.newCode.value,
                "reEnterNewPin": newPinConfirm
            }

            postWithToken(Env.base_url + Api.sakuPinChange, params, tokenLogin).then(response => {
              if(response.code == ResponseCode.OK){
                setTimeout(() => {
                    this.setState({
                      showSpinner: false,
                      hasVerify: true
                    });
                }, 500);
              }else{
                setTimeout(() => {
                  this.setState({
                      showSpinner: false
                  });

                  this.resetInputNewPinConfirm();
                  dialog.alertFail(response.failContent, this);

                  if(response.failContent.failCode == "USER_ERR_16"){
                      setTimeout(() => {
                          this.setState({
                              isScreenNewPinConfirm: false
                          });
                          this.resetInputNewPin();
                      }, 500);
                  }
                }, 1000);
              }
            }).catch(err => {
              setTimeout(() => {
                this.setState({
                    showSpinner: false
                });
                this.resetInputNewPinConfirm();
                dialog.alertException(err);
              }, 500);
            });
          })
    }

    forgotPIN() {
        this.props.navigation.navigate('VerifikasiLupaPin', {
            phone: this.props.navigation.getParam('phone'),
            screenCallback : "Akun"
        })
    }

    goBack() {
      if(this.state.hasVerify == true){
        this.setState({
            showSpinner: true,
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false
            });

            this.props.navigation.navigate('Akun')

        }, 1000);
      }else{
        this.props.navigation.goBack()
      }
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' size={35} type='antdesign' />
                        </Button>
                        <Image style={styles.imageLogo} source={require('../../../assets/image/bnisyariahlogo.png')} />
                    </View>

                    {this.state.hasVerify == false ?
                        (
                            <View>
                                <View style={styles.infoContainer}>
                                    <Text style={styles.infoTitle}>{this.state.isScreenNewPin == false ? 'Masukkan PIN lama Anda' : this.state.isScreenNewPinConfirm == false ? ' Masukkan PIN baru Anda' : 'Ulangi PIN baru Anda'}</Text>
                                    <Text style={styles.infoSubTitle}>PIN digunakan untuk login ke akun Anda dan digunakan untuk bertransaksi</Text>

                                </View>

                                {this.state.isScreenNewPin == false ? (

                                    <View style={styles.inputContainer}>
                                        <OTPInputView
                                            style={{ width: '80%', height: 200, }}
                                            pinCount={6}
                                            code={this.state.inputs.code.value} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                            onCodeChanged={value => { this.onInputChange({id: 'code', value})}}
                                            autoFocusOnLoad
                                            secureTextEntry
                                            codeInputFieldStyle={styles.underlineStyleBase}
                                            codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                            onCodeFilled={(code => {this.onFilledOldPin(code)})}
                                        />
                                        <Text style={styles.jagaPIN}>Jaga kerahasiaan PIN Anda</Text>
                                        {this.validateError('code')}
                                        <View style={styles.forgotPINcontainer}>
                                            <Button onPress={() => this.forgotPIN()} transparent style={styles.btnForgotPIN}>
                                                <Text style={styles.txtForgotPin}>Lupa PIN</Text>
                                            </Button>
                                        </View>
                                    </View>


                                ) : this.state.isScreenNewPinConfirm == false ?

                                        <View style={styles.inputContainer}>
                                            <OTPInputView
                                                style={{ width: '80%', height: 200, }}
                                                pinCount={6}
                                                code={this.state.inputs.newCode.value} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                                onCodeChanged={value => { this.onInputChange({id: 'newCode', value})}}
                                                autoFocusOnLoad
                                                secureTextEntry
                                                codeInputFieldStyle={styles.underlineStyleBase}
                                                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                                onCodeFilled={(code => {this.onFilledNewPin(code)})}
                                            />
                                            <Text style={styles.jagaPIN}>Jaga kerahasiaan PIN Anda</Text>
                                            {this.validateError('newCode')}
                                        </View>


                                        :

                                        <View style={styles.inputContainer}>
                                            <OTPInputView
                                                style={{ width: '80%', height: 200, }}
                                                pinCount={6}
                                                code={this.state.inputs.confirmationNewCode.value} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                                onCodeChanged={value => { this.onInputChange({id: 'confirmationNewCode', value})}}
                                                autoFocusOnLoad
                                                secureTextEntry
                                                codeInputFieldStyle={styles.underlineStyleBase}
                                                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                                onCodeFilled={code  => this.onFilledNewPinConfirm(code)}
                                            />
                                            <Text style={styles.jagaPIN}>Jaga kerahasiaan PIN Anda</Text>
                                            {this.validateError('confirmationNewCode')}
                                        </View>
                                }

                            </View>
                        ) :
                        <View>
                            <View style={styles.titleContainer}>
                                <Text style={styles.infoTitle}>Selamat yaa ...</Text>
                                <Text style={styles.infoSubTitle1}>PIN baru Anda berhasil dibuat, tetap jaga kerahasiaan PIN anda.</Text>
                                <Image style={styles.imageBig} source={require('../../../assets/image/Credit-card.jpg')} />
                            </View>

                            <View style={styles.btnRegisterContainer}>
                                <Button onPress={() => this.goBack()} style={styles.btnRegister}>
                                    <Text style={styles.RegisterText}>Selesai</Text>
                                </Button>
                            </View>
                        </View>

                    }

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    infoContainer: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    infoTitle: {
        color: '#2a8b40',
        fontSize: 16,
        fontWeight: 'bold'
    },
    infoSubTitle: {
        color: '#385757',
        fontSize: 13,
        marginTop: 20,
        fontWeight: '900',
        textAlign: 'center'
    },
    infoSubTitle1: {
        color: '#385757',
        fontSize: 13,
        marginTop: 10,
        fontWeight: 'bold',
        textAlign: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    borderStyleHighLighted: {
        borderColor: "#2a8b40",
    },
    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 2,
    },
    underlineStyleHighLighted: {
        borderColor: "#2a8b40",
    },
    borderStyleBase: {
        width: 30,
        height: 45
    },
    titleContainer: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        // fontFamily: 'Varela',
        fontSize: 16,
        fontWeight: 'bold'
    },
    imageBig: {
        height: Dimensions.get('window').height / 2 - 50,
        width: Dimensions.get('window').width - 50
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#2a8b40',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    infoBottomContainer: {
        margin: 20
    },
    infoBtmText: {
        fontSize: 12,
        color: '#385757'
    },
    jagaPIN: {
        textAlign: 'left',
        marginTop: -50,
        fontWeight: '800',
        fontSize: 13
    },
    forgotPINcontainer: {
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnForgotPIN: {
        marginTop: 10
    },
    txtForgotPin: {
        color: '#2a8b40',
        fontWeight: 'bold'
    }
})

export default Ubahpin;
