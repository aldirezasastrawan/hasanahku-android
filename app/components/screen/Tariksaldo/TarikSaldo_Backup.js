import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, Modal, ScrollView, FlatList, TouchableOpacity, LayoutAnimation, UIManager } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import RBSheet from "react-native-raw-bottom-sheet";
import { getValue } from '../../../module/LocalData/Storage';
import { post, postWithToken, get } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';

const size = Dimensions.get('window')

class Tariksaldo_Backup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            namaBank: 'BNI',
            jumlahPenarikan: '',
            nomorRekening: '',
            keterangan: '',
            checked: true,
            checkedFav: true,
            dataBank: [],
            isModalOpen: false,
            userInput: '',
            dataInquiryWithdraw: {},
            dataUser: {},
            hasConfirmation: false,
            hasSuccessConfirm: false,
            hasChooseType: false,
            cashType: 'Cabang'
        };

        this.arrayholderBank = [];

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }

    }

    componentDidMount() {
        this.getDataUser()
        this.getDataBank()
    }

    getDataUser(){
      getValue('userLogin').then(userLogin => {
        this.setState({
            dataUser: userLogin
        });
      })
    }

    tarik() {
        if (this.state.cashType == "Cabang") {
            if (this.state.jumlahPenarikan != "") {
                this.getInquiryWithdraw()
                // this.RBSheet.open()
            } else {
                // Toast.show({
                //     text: 'Silahkan isi data-data yang diperlukan',
                //     buttonStyle: 'Okay'
                // })
                alert('Silahkan isi data-data yang diperlukan')
            }
        } else {
            if (this.state.jumlahPenarikan != "" && this.state.nomorRekening != "") {
                this.getInquiryWithdraw()
                // this.RBSheet.open()
            } else {
                // Toast.show({
                //     text: 'Silahkan isi data-data yang diperlukan',
                //     buttonStyle: 'Okay'
                // })
                alert('Silahkan isi data-data yang diperlukan')

            }
        }


    }

    transferConfirmation() {
        this.setState({
            hasConfirmation: true
        })

        setTimeout(() => {
            if (this.state.cashType == "Cabang") {
                this.processTarikCabang()
            } else {
                this.processWithdraw()
            }
        }, 5000);
    }

    processTarikCabang() {
        console.log('process tf');

        this.setState({
            // hasConfirmation: false,
            hasSuccessConfirm: true
        })

        setTimeout(() => {
            this.RBSheet.close();
        }, 2000);


        setTimeout(() => {
            const dataInquiry = this.state.dataInquiryWithdraw
            const dataUser = this.state.dataUser
            let dataSlip = {
                "phone": this.props.navigation.getParam('phone'),
                "va": dataUser.vaContents[0].va,
                "toAccount": dataInquiry.toAccount,
                "destinationBank": dataInquiry.destinationBank,
                "nominal": this.state.jumlahPenarikan,
                "note": this.state.keterangan,
                "fee": dataInquiry.fee,
            }
            this.props.navigation.navigate('TarikSaldoCabangSlip', {
                "dataSlip": dataSlip,
            })
        }, 500);
    }

    processWithdraw() {
        console.log('process tf');

        this.setState({
            // hasConfirmation: false,
            hasSuccessConfirm: true
        })

        setTimeout(() => {
            this.RBSheet.close();
        }, 2000);


        setTimeout(() => {
            const dataInquiry = this.state.dataInquiryWithdraw
            const dataUser = this.state.dataUser
            this.props.navigation.navigate('TarikSaldoPin', {
                "phone": dataUser.noHP,
                "va": dataUser.vaContents[0].va,
                "toAccount": dataInquiry.toAccount,
                "destinationBank": dataInquiry.destinationBank,
                "nominal": this.state.jumlahPenarikan,
                "note": this.state.keterangan,
                "fee": dataInquiry.fee,
            })
        }, 500);
    }

    getInquiryWithdraw() {
        this.setState({
            showSpinner: true,
        });

        getValue('token').then(token => {
            let params = {
                "access_token": token,
                "phone": this.props.navigation.getParam('phone'),
                "toAccount": this.state.nomorRekening,
                "destinationBank": this.state.namaBank,
                "nominal": this.state.jumlahPenarikan,
                "fee": ""
            }

            console.log(params);

            postData(Env.base_url + '/sakuInqWithdraw', params).then(response => {
                console.log(response);
                if (response.data.code == 'OK') {
                    setTimeout(() => {
                        this.setState({
                            dataInquiryWithdraw: response.data.okContent,
                            showSpinner: false,
                        })

                        setTimeout(() => {
                            this.RBSheet.open()

                        }, 500);
                    }, 1000);

                } else {
                    setTimeout(() => {
                        this.setState({
                            showSpinner: false,
                        });

                        setTimeout(() => {
                            // Toast.show({
                            //     text: response.data.failContent.failDescription,
                            //     buttonStyle: 'Okay'
                            // })
                            alert(response.data.failContent.failDescription)

                        }, 500);
                    }, 1000);
                }

            })
        })
    }

    getDataBank() {

        let dataBank = [
            {
                "name": "BANK BRI",
                "code": "002"
            },
            {
                "name": "BANK EKSPOR INDONESIA",
                "code": "003"
            },
            {
                "name": "BANK MANDIRI",
                "code": "008"
            },
            {
                "name": "BANK BNI",
                "code": "009"
            },
            {
                "name": "BANK DANAMON",
                "code": "011"
            },
            {
                "name": "PERMATA BANK",
                "code": "013"
            },
            {
                "name": "BANK BCA",
                "code": "014"
            },
            {
                "name": "BANK BII",
                "code": "016"
            },
            {
                "name": "BANK PANIN",
                "code": "019"
            },
            {
                "name": "BANK ARTA NIAGA KENCANA",
                "code": "020"
            },
            {
                "name": "BANK NIAGA",
                "code": "022"
            },
            {
                "name": "BANK BUANA IND",
                "code": "023"
            },
            {
                "name": "BANK LIPPO",
                "code": "026"
            },
            {
                "name": "BANK NISP",
                "code": "028"
            },
            {
                "name": "AMERICAN EXPRESS BANK LTD",
                "code": "030"
            },
            {
                "name": "CITIBANK N.A.",
                "code": "031"
            },
            {
                "name": "JP. MORGAN CHASE BANK, N.A.",
                "code": "032"
            },
            {
                "name": "BANK OF AMERICA, N.A",
                "code": "033"
            },
            {
                "name": "ING INDONESIA BANK",
                "code": "034"
            },
            {
                "name": "BANK MULTICOR TBK.",
                "code": "036"
            },
            {
                "name": "BANK ARTHA GRAHA",
                "code": "037"
            },
            {
                "name": "BANK CREDIT AGRICOLE INDOSUEZ",
                "code": "039"
            },
            {
                "name": "THE BANGKOK BANK COMP. LTD",
                "code": "040"
            },
            {
                "name": "THE HONGKONG & SHANGHAI B.C.",
                "code": "041"
            },
            {
                "name": "THE BANK OF TOKYO MITSUBISHI UFJ LTD",
                "code": "042"
            },
            {
                "name": "BANK SUMITOMO MITSUI INDONESIA",
                "code": "045"
            },
            {
                "name": "BANK DBS INDONESIA",
                "code": "046"
            },
            {
                "name": "BANK RESONA PERDANIA",
                "code": "047"
            },
            {
                "name": "BANK MIZUHO INDONESIA",
                "code": "048"
            },
            {
                "name": "STANDARD CHARTERED BANK",
                "code": "050"
            },
            {
                "name": "BANK ABN AMRO",
                "code": "052"
            },
            {
                "name": "BANK KEPPEL TATLEE BUANA",
                "code": "053"
            },
            {
                "name": "BANK CAPITAL INDONESIA, TBK.",
                "code": "054"
            },
            {
                "name": "BANK BNP PARIBAS INDONESIA",
                "code": "057"
            },
            {
                "name": "BANK UOB INDONESIA",
                "code": "058"
            },
            {
                "name": "KOREA EXCHANGE BANK DANAMON",
                "code": "059"
            },
            {
                "name": "RABOBANK INTERNASIONAL INDONESIA",
                "code": "060"
            },
            {
                "name": "ANZ PANIN BANK",
                "code": "061"
            },
            {
                "name": "DEUTSCHE BANK AG.",
                "code": "067"
            },
            {
                "name": "BANK WOORI INDONESIA",
                "code": "068"
            },
            {
                "name": "BANK OF CHINA LIMITED",
                "code": "069"
            },
            {
                "name": "BANK BUMI ARTA",
                "code": "076"
            },
            {
                "name": "BANK EKONOMI",
                "code": "087"
            },
            {
                "name": "BANK ANTARDAERAH",
                "code": "088"
            },
            {
                "name": "BANK HAGA",
                "code": "089"
            },
            {
                "name": "BANK IFI",
                "code": "093"
            },
            {
                "name": "BANK CENTURY, TBK.",
                "code": "095"
            },
            {
                "name": "BANK MAYAPADA",
                "code": "097"
            },
            {
                "name": "BANK JABAR",
                "code": "110"
            },
            {
                "name": "BANK DKI",
                "code": "111"
            },
            {
                "name": "BPD DIY",
                "code": "112"
            },
            {
                "name": "BANK JATENG",
                "code": "113"
            },
            {
                "name": "BANK JATIM",
                "code": "114"
            },
            {
                "name": "BPD JAMBI",
                "code": "115"
            },
            {
                "name": "BPD ACEH",
                "code": "116"
            },
            {
                "name": "BANK SUMUT",
                "code": "117"
            },
            {
                "name": "BANK NAGARI",
                "code": "118"
            },
            {
                "name": "BANK RIAU",
                "code": "119"
            },
            {
                "name": "BANK SUMSEL",
                "code": "120"
            },
            {
                "name": "BANK LAMPUNG",
                "code": "121"
            },
            {
                "name": "BPD KALSEL",
                "code": "122"
            },
            {
                "name": "BPD KALIMANTAN BARAT",
                "code": "123"
            },
            {
                "name": "BPD KALTIM",
                "code": "124"
            },
            {
                "name": "BPD KALTENG",
                "code": "125"
            },
            {
                "name": "BPD SULSEL",
                "code": "126"
            },
            {
                "name": "BANK SULUT",
                "code": "127"
            },
            {
                "name": "BPD NTB",
                "code": "128"
            },
            {
                "name": "BPD BALI",
                "code": "129"
            },
            {
                "name": "BANK NTT",
                "code": "130"
            },
            {
                "name": "BANK MALUKU",
                "code": "131"
            },
            {
                "name": "BPD PAPUA",
                "code": "132"
            },
            {
                "name": "BANK BENGKULU",
                "code": "133"
            },
            {
                "name": "BPD SULAWESI TENGAH",
                "code": "134"
            },
            {
                "name": "BANK SULTRA",
                "code": "135"
            },
            {
                "name": "BANK NUSANTARA PARAHYANGAN",
                "code": "145"
            },
            {
                "name": "BANK SWADESI",
                "code": "146"
            },
            {
                "name": "BANK MUAMALAT",
                "code": "147"
            },
            {
                "name": "BANK MESTIKA",
                "code": "151"
            },
            {
                "name": "BANK METRO EXPRESS",
                "code": "152"
            },
            {
                "name": "BANK SHINTA INDONESIA",
                "code": "153"
            },
            {
                "name": "BANK MASPION",
                "code": "157"
            },
            {
                "name": "BANK HAGAKITA",
                "code": "159"
            },
            {
                "name": "BANK GANESHA",
                "code": "161"
            },
            {
                "name": "BANK WINDU KENTJANA",
                "code": "162"
            },
            {
                "name": "HALIM INDONESIA BANK",
                "code": "164"
            },
            {
                "name": "BANK HARMONI INTERNATIONAL",
                "code": "166"
            },
            {
                "name": "BANK KESAWAN",
                "code": "167"
            },
            {
                "name": "BANK TABUNGAN NEGARA (PERSERO)",
                "code": "200"
            },
            {
                "name": "BANK HIMPUNAN SAUDARA 1906, TBK .",
                "code": "212"
            },
            {
                "name": "BANK TABUNGAN PENSIUNAN NASIONAL",
                "code": "213"
            },
            {
                "name": "BANK SWAGUNA",
                "code": "405"
            },
            {
                "name": "BANK JASA ARTA",
                "code": "422"
            },
            {
                "name": "BANK MEGA",
                "code": "426"
            },
            {
                "name": "BANK JASA JAKARTA",
                "code": "427"
            },
            {
                "name": "BANK BUKOPIN",
                "code": "441"
            },
            {
                "name": "BANK SYARIAH MANDIRI",
                "code": "451"
            },
            {
                "name": "BANK BISNIS INTERNASIONAL",
                "code": "459"
            },
            {
                "name": "BANK SRI PARTHA",
                "code": "466"
            },
            {
                "name": "BANK JASA JAKARTA",
                "code": "472"
            },
            {
                "name": "BANK BINTANG MANUNGGAL",
                "code": "484"
            },
            {
                "name": "BANK BUMIPUTERA",
                "code": "485"
            },
            {
                "name": "BANK YUDHA BHAKTI",
                "code": "490"
            },
            {
                "name": "BANK MITRANIAGA",
                "code": "491"
            },
            {
                "name": "BANK AGRO NIAGA",
                "code": "494"
            },
            {
                "name": "BANK INDOMONEX",
                "code": "498"
            },
            {
                "name": "BANK ROYAL INDONESIA",
                "code": "501"
            },
            {
                "name": "BANK ALFINDO",
                "code": "503"
            },
            {
                "name": "BANK SYARIAH MEGA",
                "code": "506"
            },
            {
                "name": "BANK INA PERDANA",
                "code": "513"
            },
            {
                "name": "BANK HARFA",
                "code": "517"
            },
            {
                "name": "PRIMA MASTER BANK",
                "code": "520"
            },
            {
                "name": "BANK PERSYARIKATAN INDONESIA",
                "code": "521"
            },
            {
                "name": "BANK AKITA",
                "code": "525"
            },
            {
                "name": "LIMAN INTERNATIONAL BANK",
                "code": "526"
            },
            {
                "name": "ANGLOMAS INTERNASIONAL BANK",
                "code": "531"
            },
            {
                "name": "BANK DIPO INTERNATIONAL",
                "code": "523"
            },
            {
                "name": "BANK KESEJAHTERAAN EKONOMI",
                "code": "535"
            },
            {
                "name": "BANK UIB",
                "code": "536"
            },
            {
                "name": "BANK ARTOS IND",
                "code": "542"
            },
            {
                "name": "BANK PURBA DANARTA",
                "code": "547"
            },
            {
                "name": "BANK MULTI ARTA SENTOSA",
                "code": "548"
            },
            {
                "name": "BANK MAYORA",
                "code": "553"
            },
            {
                "name": "BANK INDEX SELINDO",
                "code": "555"
            },
            {
                "name": "BANK VICTORIA INTERNATIONAL",
                "code": "566"
            },
            {
                "name": "BANK EKSEKUTIF",
                "code": "558"
            },
            {
                "name": "CENTRATAMA NASIONAL BANK",
                "code": "559"
            },
            {
                "name": "BANK FAMA INTERNASIONAL",
                "code": "562"
            },
            {
                "name": "BANK SINAR HARAPAN BALI",
                "code": "564"
            },
            {
                "name": "BANK HARDA",
                "code": "567"
            },
            {
                "name": "BANK FINCONESIA",
                "code": "945"
            },
            {
                "name": "BANK MERINCORP",
                "code": "946"
            },
            {
                "name": "BANK MAYBANK INDOCORP",
                "code": "947"
            },
            {
                "name": "BANK OCBC – INDONESIA",
                "code": "948"
            },
            {
                "name": "BANK CHINA TRUST INDONESIA",
                "code": "949"
            },
            {
                "name": "BANK COMMONWEALTH",
                "code": "950"
            }
        ]

        this.setState({
            dataBank: dataBank
        },
            function () {
                this.arrayholderBank = dataBank;
            }
        )
    }

    loadModal() {
        this.setState({
            isModalOpen: true
        })
    }

    closeModal() {
        this.setState({
            isModalOpen: false
        })
    }

    selectBank(bank) {
        this.setState({
            isModalOpen: false,
            namaBank: bank
        })
    }

    searching(userInput) {
        const newDataBank = this.arrayholderBank.filter(function (item) {
            const itemData = item.name
                ? item.name.toUpperCase()
                : ''.toUpperCase();
            const textData = userInput.toUpperCase();
            console.log(userInput);

            return itemData.indexOf(textData) > -1;
        });


        this.setState({
            dataBank: newDataBank,
            userInput: userInput
        });
    }

    selectCashType(type) {
      switch (type) {
        case 1:
          alert("Menu not available")
          break;
        default:
          this.setState({
              cashType: 'Transfer',
              hasChooseType: true
            })
      }
      LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);

    }

    render() {
        const datauser = this.state.dataUser
        const dataInquiry = this.state.dataInquiryWithdraw
        const size = Dimensions.get('window')
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' size={35} type='antdesign' />
                        </Button>
                        <Image style={styles.profileImage} source={require('../../../assets/image/man.png')} />
                    </View>

                    <View styles={styles.titleContainer}>
                        <Text style={styles.title}>
                            Tarik Tunai
                        </Text>
                    </View>

                    {this.state.hasChooseType == true ?
                        <View style={styles.animContainer}>
                            <LottieView
                                style={{ height: 80, width: 300, marginLeft: 3, transform: [{ scale: 1.1 }] }}
                                // autoSize={true}
                                resizeMode="cover"
                                source={require('../../../assets/animation/tarikanim.json')}
                                autoPlay
                            />
                        </View>

                        : null}

                    <TouchableOpacity onPress={() => this.selectCashType(1)} activeOpacity={0.8} style={styles.cashOutContainer}>
                        <View style={styles.iconCashOutContainer}>
                            <Icon name="home" type="entypo" color="#26C165" size={35} />
                        </View>
                        <View style={styles.titleCashContainer}>
                            <Text style={styles.titleCash}>
                                Tarik Tunai Via Cabang
                            </Text>
                            <Text style={styles.subTitleCash}>
                                Lakukan tarik tunai dan ambil uang kamu di cabang BNI Syariah terdekat
                            </Text>
                        </View>
                    </TouchableOpacity>


                    <TouchableOpacity onPress={() => this.selectCashType(2)} activeOpacity={0.8} style={styles.cashOutContainer2}>
                        <View style={styles.iconCashOutContainer}>
                            <Icon name="credit-card-alt" type="font-awesome" color="#FF4133" />
                        </View>
                        <View style={styles.titleCashContainer}>
                            <Text style={styles.titleCash}>
                                Transfer ke Rekening BNI Syariah
                            </Text>
                            <Text style={styles.subTitleCash}>
                                Kirim saldo HasanahKu ke Rekening BNI Syariah Milikmu
                            </Text>
                        </View>
                    </TouchableOpacity>

                    {this.state.hasChooseType == false ?
                        <View style={styles.animContainer2}>
                            <LottieView
                                style={{ height: 80, width: 300, marginLeft: 3, transform: [{ scale: 1.1 }] }}
                                // autoSize={true}
                                resizeMode="cover"
                                source={require('../../../assets/animation/tarikanim.json')}
                                autoPlay
                            />
                        </View>

                        : null}

                    {this.state.hasChooseType == true ?

                        <View style={styles.inputContainer}>
                            <Form>
                                <Item stackedLabel>
                                    <Label style={styles.label}>Tipe Penarikan</Label>
                                    <Input disabled style={styles.input} value={this.state.cashType} onChangeText={(cashType) => this.setState({ cashType: cashType })} />
                                </Item>
                                <Item stackedLabel>
                                    <Label style={styles.label}>Jumlah Penarikan</Label>
                                    <Input style={styles.input} value={this.state.jumlahPenarikan} onChangeText={(jumlahPenarikan) => this.setState({ jumlahPenarikan: jumlahPenarikan })} />
                                </Item>
                                <Item stackedLabel>
                                    <Label style={styles.label}>Nama Bank</Label>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Input disabled style={styles.input} value={this.state.namaBank} onChangeText={(namaBank) => this.setState({ namaBank: namaBank })} />
                                        <Icon
                                            //  onPress={() => this.loadModal()}
                                            active name='bank' type="material-community" />
                                    </View>
                                </Item>

                                {this.state.cashType == 'Transfer' ?

                                    <Item stackedLabel last>
                                        <Label style={styles.label}>Nomor Rekening</Label>
                                        <Input style={styles.input} keyboardType='number-pad' value={this.state.nomorRekening} onChangeText={(nomorRekening) => this.setState({ nomorRekening: nomorRekening })} />
                                    </Item>
                                    : null}
                            </Form>
                        </View>

                        : null}

                    {this.state.hasChooseType == true ?

                        <View style={styles.noteContainer}>
                            <Text style={styles.noteTitle}>Catatan:</Text>
                            <Text style={styles.noteDetail}>- Saldo yang ditarik minimal Rp 10.000</Text>
                        </View>
                        : null}

                    {this.state.hasChooseType == true ?

                        <View style={styles.btnRegisterContainer}>
                            <Button onPress={() => this.tarik()} style={styles.btnRegister}>
                                <Text style={styles.RegisterText}>Proses Tarik Tunai</Text>
                            </Button>
                        </View>
                        : null}

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                    />

                    <Modal animationType={"slide"} transparent={false}
                        visible={this.state.isModalOpen}
                        // presentationStyle='pageSheet'
                        onRequestClose={() => { console.log("Modal has been closed.") }}>

                        <Container style={styles.modal}>
                            <Content>

                                <View style={styles.backModalContainer}>
                                    <Button onPress={() => this.closeModal()} androidRippleColor='#000' transparent style={styles.btnBack}>
                                        <Icon name='arrowleft' size={30} type='antdesign' />
                                    </Button>
                                    <Image style={styles.profileImage} source={require('../../../assets/image/man.png')} />
                                </View>

                                <View style={styles.modalTitleContainer}>
                                    <Text style={styles.titleModal}>List Bank</Text>
                                    <Text>{this.state.dataBank.length} available</Text>
                                </View>

                                <View style={styles.modalHeader}>

                                    <View style={styles.modalInputHeader}>
                                        <Icon
                                            name="search"
                                            type="font-awesome"
                                            size={20}
                                            color="#02aced"
                                            containerStyle={styles.inputIcon}
                                        />
                                        <Input
                                            style={styles.inputHeader}
                                            placeholder="Search by name"
                                            autoCapitalize="none"
                                            onChangeText={text => this.searching(text)}
                                            onClear={text => this.SearchFilterFunction('')}
                                            underlineColorAndroid="transparent"
                                            value={this.state.userInput}
                                        />
                                    </View>
                                    {/*
                                    <Button onPress={() => this.closeModal()} style={styles.btnCloseHeader}>
                                        <Text style={styles.btnCloseText}>Tutup</Text>
                                    </Button> */}

                                </View>

                                <View style={styles.listContainer}>
                                    <View style={styles.kirimKeContainer}>
                                        <Text>Pilih Bank tujuan: </Text>
                                    </View>
                                    <ScrollView >


                                        <FlatList
                                            data={this.state.dataBank}
                                            renderItem={({ item }) => (
                                                <View style={styles.contactListContainer}>
                                                    <View style={styles.infoContact}>
                                                        <Text style={styles.contactName}>{item.name}</Text>
                                                        <View style={styles.contactContainer}>
                                                            <Text style={styles.contactNumber}>{item.code}</Text>
                                                            <View style={styles.btnPilihContainer}>
                                                                <Button onPress={() => this.selectBank(item.name)} style={styles.btnPilih}>
                                                                    <Text style={styles.txtPilih}>pilih</Text>
                                                                </Button>
                                                            </View>
                                                        </View>
                                                    </View>

                                                </View>
                                            )}
                                            extraData={this.state}
                                        />

                                        {/* {this.state.dataBank.map(bank => {
                                            return (
                                                <View style={styles.contactListContainer}>
                                                    <View style={styles.infoContact}>
                                                        <Text style={styles.contactName}>{bank.name}</Text>
                                                        <View style={styles.contactContainer}>
                                                            <Text style={styles.contactNumber}>{bank.code}</Text>
                                                            <View style={styles.btnPilihContainer}>
                                                                <Button onPress={() => this.selectBank(bank.name)} style={styles.btnPilih}>
                                                                    <Text style={styles.txtPilih}>pilih</Text>
                                                                </Button>
                                                            </View>
                                                        </View>
                                                    </View>

                                                </View>
                                            )
                                        })} */}


                                    </ScrollView>
                                </View>

                            </Content>

                        </Container>
                    </Modal>

                    <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown
                        height={Platform.OS == 'android' ? size.height - 50 : size.height - 50}
                        // animationType="fade"
                        duration={300}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 15,
                                borderTopLeftRadius: 15,
                            },
                        }}>
                        <View>
                            <View style={styles.sheetHeader}>
                                <Text style={styles.sheetTitle}>
                                    Withdraw Confirmation
                                </Text>
                            </View>
                            <ScrollView style={styles.inputContainer}>
                                {this.state.cashType != "Cabang" ?
                                    <Form>
                                        <Item stackedLabel stackedLabel>
                                            <Label style={styles.label}>Nama Pengirim</Label>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Input style={styles.input1} disabled value={datauser.nama} />
                                                <View style={styles.todayContainer}>
                                                    <Icon active name='calendar-today' size={20} type="material-community" />
                                                    <Text>Today</Text>
                                                </View>
                                            </View>
                                        </Item>

                                        <Item stackedLabel>
                                            <Label style={styles.label}>Nama Penerima</Label>
                                            <Input style={styles.input1} disabled value={dataInquiry.nasabahName} />
                                        </Item>

                                        <Item stackedLabel>
                                            <Label style={styles.label}>Bank Tujuan</Label>
                                            <Input style={styles.input1} disabled value={dataInquiry.destinationBank} />
                                        </Item>

                                        <Item stackedLabel>
                                            <Label style={styles.label}>Rekening Tujuan</Label>
                                            <Input style={styles.input1} disabled value={dataInquiry.toAccount} />
                                        </Item>

                                        <Item stackedLabel>
                                            <Label style={styles.label}>Nominal</Label>
                                            <Input style={styles.input1} disabled keyboardType='numbers-and-punctuation' value={dataInquiry.nominal + ""} />
                                        </Item>

                                        <Item stackedLabel last>
                                            <Label style={styles.label}>Fee</Label>
                                            <Input numberOfLines={1} disabled style={styles.input1} value={dataInquiry.fee + ""} />
                                        </Item>
                                    </Form>

                                    :
                                    <Form>
                                        <Item stackedLabel stackedLabel>
                                            <Label style={styles.label}>Nama Pengirim</Label>
                                            <View style={{ flexDirection: 'row' }}>
                                                <Input style={styles.input1} disabled value={datauser.namaDepan} />
                                                <View style={styles.todayContainer}>
                                                    <Icon active name='calendar-today' size={20} type="material-community" />
                                                    <Text>Today</Text>
                                                </View>
                                            </View>
                                        </Item>

                                        <Item stackedLabel>
                                            <Label style={styles.label}>Bank Tujuan</Label>
                                            <Input style={styles.input1} disabled value={dataInquiry.destinationBank} />
                                        </Item>

                                        <Item stackedLabel>
                                            <Label style={styles.label}>Nominal</Label>
                                            <Input style={styles.input1} disabled keyboardType='numbers-and-punctuation' value={dataInquiry.nominal + ""} />
                                        </Item>

                                        <Item stackedLabel last>
                                            <Label style={styles.label}>Fee</Label>
                                            <Input numberOfLines={1} disabled style={styles.input1} value={dataInquiry.fee + ""} />
                                        </Item>
                                    </Form>
                                }

                            </ScrollView>

                            <View style={styles.btnConfirmationContainer}>
                                {this.state.hasConfirmation == false ?
                                    <Button onPress={() => this.transferConfirmation()} style={styles.btnConfirmation}>
                                        <Text style={styles.txtConfirmation}>
                                            Konfirmasi Tarik Tunai
                                </Text>
                                    </Button>

                                    :
                                    this.state.hasSuccessConfirm == false ?
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                                            <LottieView
                                                style={{ height: 40, transform: [{ scale: 1.5 }] }}
                                                autoSize={true}
                                                resizeMode="cover"
                                                source={require('../../../assets/animation/searching1.json')}
                                                autoPlay
                                            />

                                            <Text style={styles.txtOnProcess}>On process ...</Text>

                                        </View>

                                        :

                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                                            <LottieView
                                                style={{ height: 40, transform: [{ scale: 1.5 }] }}
                                                autoSize={true}
                                                resizeMode="cover"
                                                source={require('../../../assets/animation/checked.json')}
                                                autoPlay
                                            />

                                            <Text style={styles.txtOnProcess}>Data terverifikasi ...</Text>

                                        </View>
                                }
                            </View>

                        </View>
                    </RBSheet>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    titleContainer: {
        marginTop: 5,
    },
    title: {
        fontSize: 24,
        color: '#2a8b40',
        paddingLeft: 30,
        paddingTop: 20,
        fontWeight: 'bold'
    },
    animContainer: {
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    animContainer2: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: Dimensions.get('window').height / 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardContainer: {
        margin: 20
    },
    cardWrapper: {
        height: 200,
        borderRadius: 10,
        backgroundColor: '#e35200'
    },
    cardTitleContainer: {
        margin: 20
    },
    cardTitle: {
        fontSize: 24,
        color: '#fff'
    },
    cardChipContainer: {
        height: 40,
        width: 70,
        borderWidth: 0.5,
        borderColor: '#dedede',
        marginLeft: 20,
        borderRadius: 5,
        backgroundColor: '#f5cf1a'
    },
    cardNumberContainer: {
        margin: 10,
        flexDirection: 'row'
    },
    cardNumber: {
        color: '#fff',
        padding: 10,
        fontSize: 20
    },
    cardOwnerContainer: {
        marginLeft: 25
    },
    cardOwner: {
        color: '#fff'
    },
    inputContainer: {
        margin: 20
    },
    checkContainer: {
        marginTop: 20,
    },
    checkTextContainer: {
        flexDirection: 'row',
        marginRight: 20
    },
    txtSetuju: {
        marginTop: 14,
        fontSize: 12
    },
    txtSyarat: {
        fontSize: 12,
        marginTop: 2,
        color: '#385757',
        fontWeight: 'bold'
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#4caf50',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    noteContainer: {
        marginLeft: 20
    },
    noteTitle: {
        fontWeight: 'bold'
    },
    noteDetailContainer: {
        flexDirection: 'row'
    },
    noteDetail: {
        fontSize: 12,
        color: '#273554',
        fontWeight: 'bold',
        marginTop: 5
    },
    modal: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        // backgroundColor: 'rgba(0,0,0,0.7)',
        // padding: 100
    },
    modalHeader: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    modalInputHeader: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#f7f7f7',
        borderRadius: 30,
        width: Dimensions.get('window').width - 50,
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 5,
    },
    inputIcon: {
        marginLeft: 15
    },
    inputHeader: {
        height: 40,
        marginLeft: 5,
        borderBottomColor: '#FFFFFF',
        flex: 1,
        fontSize: 13
    },
    btnCloseHeader: {
        backgroundColor: '#02aced',
        marginLeft: 10,
        marginRight: 10,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnCloseText: {
        color: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        fontWeight: 'bold'
    },
    listContainer: {
        flex: 2,
        margin: 20
    },
    contactListContainer: {
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        margin: 5,
    },
    backModalContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    contactName: {
        fontWeight: 'bold',
        fontSize: 18
    },
    avaContainer: {
        backgroundColor: '#fff',
        borderWidth: 2,
        borderColor: '#dedede',
        borderRadius: 50,
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    txtAva: {
        fontSize: 24
    },
    infoContact: {
        marginBottom: 10,
        marginLeft: 15,
        width: '90%'
        // justifyContent: 'center',
    },
    contactNumber: {
        color: '#666',
        fontSize: 13,
        padding: 3,
        flex: 2
    },
    contactContainer: {
        marginTop: 5,
        marginBottom: 5,
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f7f7f7',
        width: '100%'
    },
    modalTitleContainer: {
        marginLeft: 25
    },
    titleModal: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    kirimKeContainer: {
        margin: 10,
        justifyContent: 'center',
    },
    btnPilih: {
        height: 30,
        padding: 5,
        backgroundColor: '#02aced',
    },
    txtPilih: {
        color: '#fff',
        fontWeight: 'bold'
    },
    btnPilihContainer: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    input: {
        fontSize: 13,
        textTransform: 'uppercase'
    },
    sheetHeader: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    sheetTitle: {
        flex: 2,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    todayContainer: {
        flexDirection: 'row',
        backgroundColor: '#eee',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 10
    },
    btnConfirmationContainer: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnConfirmation: {
        backgroundColor: '#2a8b40',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        width: 300
    },
    txtConfirmation: {
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 14
    },
    txtOnProcess: {
        textAlign: 'center',
        paddingTop: 15,
        color: '#666'
    },
    input1: {
        fontSize: 14,
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
    cashOutContainer: {
        margin: 20,
        height: Dimensions.get('window').height / 8,
        backgroundColor: '#C0FDD8',
        borderRadius: 20,
        flexDirection: 'row'
    },
    cashOutContainer2: {
        margin: 20,
        height: Dimensions.get('window').height / 8,
        backgroundColor: '#FCE5B3',
        borderRadius: 20,
        flexDirection: 'row'
    },
    iconCashOutContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    titleCashContainer: {
        flex: 3,
        justifyContent: 'center',
        // alignItems: 'center',
    },
    titleCash: {
        fontSize: 16,
        fontWeight: '500',
        paddingRight: 10,
    },
    subTitleCash: {
        fontSize: 13,
        paddingRight: 10,
        paddingTop: 5
    }
})

export default Tariksaldo_Backup;
