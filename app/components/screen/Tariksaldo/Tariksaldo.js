import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, ScrollView, FlatList, TouchableOpacity, LayoutAnimation, UIManager } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label } from 'native-base';
import { Icon } from 'react-native-elements';
import LottieView from 'lottie-react-native';
import { getValue } from '../../../module/LocalData/Storage';
import appStyle from '../../styles/AppStyle';

const size = Dimensions.get('window')

class Tariksaldo extends Component {
    constructor(props) {
        super(props);

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    selectCashType(type) {
      switch (type) {
        case 1:
          alert("Menu not available")
          break;
        default:
          this.props.navigation.navigate('TarikSaldoViaTransfer')
      }
      LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);

    }

    render() {
        return (
            <Container>
                <Content>
                   <View style={appStyle.navBox}>
                      <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={appStyle.btnBack}>
                          <Icon name='arrowleft' color="#26C165" size={25} type='antdesign' />
                          <View style={appStyle.navBoxTitle}>
                            <Text style={appStyle.navTitle}>Tarik Tunai</Text>
                          </View>
                      </Button>
                    </View>
                    <TouchableOpacity onPress={() => this.selectCashType(1)} activeOpacity={0.8} style={styles.cashOutContainer}>
                        <View style={styles.iconCashOutContainer}>
                            <Icon name="home" type="entypo" color="#26C165" size={35} />
                        </View>
                        <View style={styles.titleCashContainer}>
                            <Text style={styles.titleCash}>
                                Tarik Tunai Via Cabang
                            </Text>
                            <Text style={styles.subTitleCash}>
                                Lakukan tarik tunai dan ambil uang kamu di cabang BNI Syariah terdekat
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.selectCashType(2)} activeOpacity={0.8} style={styles.cashOutContainer2}>
                        <View style={styles.iconCashOutContainer}>
                            <Icon name="credit-card-alt" type="font-awesome" color="#FF4133" />
                        </View>
                        <View style={styles.titleCashContainer}>
                            <Text style={styles.titleCash}>
                                Transfer ke Rekening BNI Syariah
                            </Text>
                            <Text style={styles.subTitleCash}>
                                Kirim saldo HasanahKu ke Rekening BNI Syariah Milikmu
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.animContainer2}>
                        <LottieView
                            style={{ height: 80, width: 300, marginLeft: 3, transform: [{ scale: 1.1 }] }}
                            // autoSize={true}
                            resizeMode="cover"
                            source={require('../../../assets/animation/tarikanim.json')}
                            autoPlay
                        />
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    titleContainer: {
        marginTop: 5,
    },
    title: {
        fontSize: 24,
        color: '#2a8b40',
        paddingLeft: 30,
        paddingTop: 20,
        fontWeight: 'bold'
    },
    animContainer: {
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    animContainer2: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: Dimensions.get('window').height / 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardContainer: {
        margin: 20
    },
    cardWrapper: {
        height: 200,
        borderRadius: 10,
        backgroundColor: '#e35200'
    },
    cardTitleContainer: {
        margin: 20
    },
    cardTitle: {
        fontSize: 24,
        color: '#fff'
    },
    cardChipContainer: {
        height: 40,
        width: 70,
        borderWidth: 0.5,
        borderColor: '#dedede',
        marginLeft: 20,
        borderRadius: 5,
        backgroundColor: '#f5cf1a'
    },
    cardNumberContainer: {
        margin: 10,
        flexDirection: 'row'
    },
    cardNumber: {
        color: '#fff',
        padding: 10,
        fontSize: 20
    },
    cardOwnerContainer: {
        marginLeft: 25
    },
    cardOwner: {
        color: '#fff'
    },
    inputContainer: {
        margin: 20
    },
    checkContainer: {
        marginTop: 20,
    },
    checkTextContainer: {
        flexDirection: 'row',
        marginRight: 20
    },
    txtSetuju: {
        marginTop: 14,
        fontSize: 12
    },
    txtSyarat: {
        fontSize: 12,
        marginTop: 2,
        color: '#385757',
        fontWeight: 'bold'
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#4caf50',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    noteContainer: {
        marginLeft: 20
    },
    noteTitle: {
        fontWeight: 'bold'
    },
    noteDetailContainer: {
        flexDirection: 'row'
    },
    noteDetail: {
        fontSize: 12,
        color: '#273554',
        fontWeight: 'bold',
        marginTop: 5
    },
    modal: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        // backgroundColor: 'rgba(0,0,0,0.7)',
        // padding: 100
    },
    modalHeader: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    modalInputHeader: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#f7f7f7',
        borderRadius: 30,
        width: Dimensions.get('window').width - 50,
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 5,
    },
    inputIcon: {
        marginLeft: 15
    },
    inputHeader: {
        height: 40,
        marginLeft: 5,
        borderBottomColor: '#FFFFFF',
        flex: 1,
        fontSize: 13
    },
    btnCloseHeader: {
        backgroundColor: '#02aced',
        marginLeft: 10,
        marginRight: 10,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnCloseText: {
        color: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        fontWeight: 'bold'
    },
    listContainer: {
        flex: 2,
        margin: 20
    },
    contactListContainer: {
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        margin: 5,
    },
    backModalContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    contactName: {
        fontWeight: 'bold',
        fontSize: 18
    },
    avaContainer: {
        backgroundColor: '#fff',
        borderWidth: 2,
        borderColor: '#dedede',
        borderRadius: 50,
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    txtAva: {
        fontSize: 24
    },
    infoContact: {
        marginBottom: 10,
        marginLeft: 15,
        width: '90%'
        // justifyContent: 'center',
    },
    contactNumber: {
        color: '#666',
        fontSize: 13,
        padding: 3,
        flex: 2
    },
    contactContainer: {
        marginTop: 5,
        marginBottom: 5,
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f7f7f7',
        width: '100%'
    },
    modalTitleContainer: {
        marginLeft: 25
    },
    titleModal: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    kirimKeContainer: {
        margin: 10,
        justifyContent: 'center',
    },
    btnPilih: {
        height: 30,
        padding: 5,
        backgroundColor: '#02aced',
    },
    txtPilih: {
        color: '#fff',
        fontWeight: 'bold'
    },
    btnPilihContainer: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    input: {
        fontSize: 13,
        textTransform: 'uppercase'
    },
    sheetHeader: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    sheetTitle: {
        flex: 2,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    todayContainer: {
        flexDirection: 'row',
        backgroundColor: '#eee',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 10
    },
    btnConfirmationContainer: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnConfirmation: {
        backgroundColor: '#2a8b40',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        width: 300
    },
    txtConfirmation: {
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 14
    },
    txtOnProcess: {
        textAlign: 'center',
        paddingTop: 15,
        color: '#666'
    },
    input1: {
        fontSize: 14,
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
    cashOutContainer: {
        margin: 20,
        height: Dimensions.get('window').height / 8,
        backgroundColor: '#C0FDD8',
        borderRadius: 20,
        flexDirection: 'row'
    },
    cashOutContainer2: {
        margin: 20,
        height: Dimensions.get('window').height / 8,
        backgroundColor: '#FCE5B3',
        borderRadius: 20,
        flexDirection: 'row'
    },
    iconCashOutContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    titleCashContainer: {
        flex: 3,
        justifyContent: 'center',
        // alignItems: 'center',
    },
    titleCash: {
        fontSize: 16,
        fontWeight: '500',
        paddingRight: 10,
    },
    subTitleCash: {
        fontSize: 13,
        paddingRight: 10,
        paddingTop: 5
    }
})

export default Tariksaldo;
