import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, Modal, ScrollView, FlatList, TouchableOpacity, LayoutAnimation, UIManager } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import RBSheet from "react-native-raw-bottom-sheet";
import { getValue } from '../../../module/LocalData/Storage';
import { postWithToken} from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';
import appStyle from '../../styles/AppStyle';
import { TextInputMask } from 'react-native-masked-text'

const size = Dimensions.get('window')

class TarikSaldoViaTransfer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            namaBank: 'BNI Syariah',
            keterangan: '',
            dataInquiryWithdraw: {},
            dataUser: {},
            hasConfirmation: false,
            hasSuccessConfirm: false,
            inputs: {
              cashOut: {
                type: 'cashOut',
                value: ''
              },
              accountNum: {
                type: 'accountNum',
                value: ''
              }
            }
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentDidMount() {
        this.getDataUser()
    }

    getDataUser(){
      getValue('userLogin').then(userLogin => {
        this.setState({
            dataUser: userLogin
        });
      })
    }

    onChangeNominal(nominal){
      const { inputs } = this.state;
      this.setState({
          inputs: {
            ...inputs,
            ['cashOut']: {
              ...inputs['cashOut'],
              value : nominal,
              error:  null
            }
          }
      });
    }

    doInquiryCashOut() {

      if(this.isValidate() === false) return;

      this.setState({
          showSpinner: true
      });

      getValue('tokenLogin').then(tokenLogin => {
        const nominal = this.cashOutField.getRawValue();
          let params = {
              "phone": this.state.dataUser.noHP,
              "toAccount": this.state.inputs.accountNum.value,
              "destinationBank": this.state.namaBank,
              "nominal": nominal,
              "fee": ""
          }

          postWithToken(Env.base_url + Api.sakuInqWithdraw, params, tokenLogin).then(response => {
              if(response.code == ResponseCode.OK){
                this.setState({
                    dataInquiryWithdraw: response.okContent,
                    showSpinner: false
                })
                this.RBSheet.open()
              } else {
                this.setState({showSpinner: false});
                dialog.alertFail(response.failContent, this);
              }

          }).catch(err => {
            this.setState({showSpinner: false})
            dialog.alertException(err);
          })
      })
    }

    cashOutConfirmation() {
        this.setState({
            hasConfirmation: true
        })

        setTimeout(() => {
          this.setState({
              // hasConfirmation: false,
              hasSuccessConfirm: true
          })

          setTimeout(() => {
              this.RBSheet.close();
          }, 600);

          setTimeout(() => {
              const dataInquiry = this.state.dataInquiryWithdraw;
              const nominal = this.cashOutField.getRawValue();
              console.log("nominal raw confirm :: "+nominal);
              this.props.navigation.navigate('TarikSaldoViaTransferConfirm', {
                  "phone": this.state.dataUser.noHP,
                  "va": this.state.dataUser.vaContents[0].va,
                  "toAccount": dataInquiry.toAccount,
                  "destinationBank": dataInquiry.destinationBank,
                  "senderName" : this.state.dataUser.nama,
                  "receiverName" : dataInquiry.nasabahName,
                  "nominal": nominal,
                  "note": this.state.keterangan,
                  "fee": dataInquiry.fee
              })
          }, 1000);
        }, 500);

        setTimeout(() => {
          this.setState({
              hasConfirmation: false,
              hasSuccessConfirm: false
          })
        }, 1200);
    }

    render() {
        const datauser = this.state.dataUser
        const dataInquiry = this.state.dataInquiryWithdraw
        const size = Dimensions.get('window')
        return (
            <Container>
                <Content>
                <View style={appStyle.navBox}>
                   <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={appStyle.btnBack}>
                       <Icon name='arrowleft' color="#26C165" size={25} type='antdesign' />
                       <View style={appStyle.navBoxTitle}>
                         <Text style={appStyle.navTitle}>Tarik Tunai</Text>
                       </View>
                   </Button>
                 </View>
                    {/*<View style={styles.animContainer}>
                        <LottieView
                            style={{ height: 80, width: 300, marginLeft: 3, transform: [{ scale: 1.1 }] }}
                            // autoSize={true}
                            resizeMode="cover"
                            source={require('../../../assets/animation/tarikanim.json')}
                            autoPlay
                        />
                    </View> */}
                    <View style={styles.cashOutContainer2}>
                        <View style={styles.iconCashOutContainer}>
                            <Icon name="credit-card-alt" type="font-awesome" color="#FF4133" />
                        </View>
                        <View style={styles.titleCashContainer}>
                            <Text style={styles.titleCash}>
                                Transfer ke Rekening BNI Syariah
                            </Text>
                            <Text style={styles.subTitleCash}>
                                Kirim saldo HasanahKu ke Rekening BNI Syariah Milikmu
                            </Text>
                        </View>
                    </View>

                    <View style={styles.inputContainer}>
                        <Form>
                            <View style={{marginLeft: 15}}>
                              <Label style={{fontWeight: 'bold', fontSize: 13, marginTop:10, color: '#575757'}}>Jumlah Penarikan</Label>
                              <TextInputMask
                                type={'money'}
                                options={{
                                  precision: 0,
                                  separator: ',',
                                  delimiter: '.',
                                  unit: 'Rp',
                                  suffixUnit: ''
                                }}
                                value={this.state.inputs.cashOut.value}
                                onChangeText={value => this.onChangeNominal(value)}
                                ref={(ref) => this.cashOutField = ref}
                              />
                            <View style={{borderBottomColor: '#D9D5DC',borderBottomWidth: 1,}}/>
                            </View>
                            {this.validateError('cashOut')}
                            <Item stackedLabel>
                                <Label style={styles.label}>Nama Bank</Label>
                                <View style={{ flexDirection: 'row' }}>
                                    <Input disabled style={styles.input} value={this.state.namaBank} onChangeText={(namaBank) => this.setState({ namaBank: namaBank })} />
                                    <Icon active name='bank' type="material-community" />
                                </View>
                            </Item>
                            <Item stackedLabel>
                                <Label style={styles.label}>Nomor Rekening</Label>
                                <Input style={styles.input} keyboardType='number-pad' value={this.state.inputs.accountNum.value} onChangeText={value => this.onInputChange({id: 'accountNum', value})} />
                            </Item>
                            {this.validateError('accountNum')}
                            <Item stackedLabel>
                                <Label style={styles.label}>Keterangan</Label>
                                <Input style={styles.input} value={this.state.keterangan} onChangeText={(keterangan) => this.setState({ keterangan: keterangan })} />
                            </Item>
                        </Form>
                    </View>
                    <View style={styles.btncontainer}>
                        <View style={styles.btnBerandaContainer}>
                            <Text>Tidak ada rekening BNI Syariah?</Text>
                            <View>
                            <Button onPress={() => this.props.navigation.navigate('WebViewRekeningOnline')} transparent>
                                <Text style={styles.rekeningOnlineTxt}>Buka Disini</Text>
                            </Button>
                            </View>
                        </View>
                    </View>
                    <View style={styles.noteContainer}>
                        <Text style={styles.noteTitle}>Catatan:</Text>
                        <Text style={styles.noteDetail}>- Saldo yang ditarik minimal Rp 10.000</Text>
                    </View>
                    <View style={styles.btnRegisterContainer}>
                        <Button onPress={() => this.doInquiryCashOut()} style={styles.btnRegister}>
                            <Text style={styles.RegisterText}>Proses Tarik Tunai</Text>
                        </Button>
                    </View>
                    <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown
                        height={Platform.OS == 'android' ? size.height - 50 : size.height - 50}
                        // animationType="fade"
                        duration={300}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 15,
                                borderTopLeftRadius: 15,
                            },
                        }}>
                        <View>
                            <View style={styles.sheetHeader}>
                                <Text style={styles.sheetTitle}>
                                    Konfirmasi Tarik Tunai
                                </Text>
                            </View>
                            <ScrollView style={styles.inputContainer}>
                            <Form>
                                <Item stackedLabel stackedLabel>
                                    <Label style={styles.label}>Nama Pengirim</Label>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Input style={styles.input1} disabled value={datauser.nama} />
                                    </View>
                                </Item>
                                <Item stackedLabel>
                                    <Label style={styles.label}>Nama Penerima</Label>
                                    <Input style={styles.input1} disabled value={dataInquiry.nasabahName} />
                                </Item>
                                <Item stackedLabel>
                                    <Label style={styles.label}>Bank Tujuan</Label>
                                    <Input style={styles.input1} disabled value={dataInquiry.destinationBank} />
                                </Item>
                                <Item stackedLabel>
                                    <Label style={styles.label}>Rekening Tujuan</Label>
                                    <Input style={styles.input1} disabled value={dataInquiry.toAccount} />
                                </Item>
                                <Item stackedLabel>
                                    <Label style={styles.label}>Nominal</Label>
                                    <Input style={styles.input1} disabled value={dataInquiry.nominalFormat} />
                                </Item>
                                <Item stackedLabel last>
                                    <Label style={styles.label}>Fee</Label>
                                    <Input style={styles.input1} disabled value={dataInquiry.feeFormat} />
                                </Item>
                            </Form>
                            </ScrollView>
                            <View style={styles.btnConfirmationContainer}>
                                {this.state.hasConfirmation == false ?
                                    <Button onPress={() => this.cashOutConfirmation()} style={styles.btnConfirmation}>
                                        <Text style={styles.txtConfirmation}>
                                            Konfirmasi Tarik Tunai
                                        </Text>
                                    </Button>
                                    :
                                    this.state.hasSuccessConfirm == false ?
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                            <LottieView
                                                style={{ height: 40, transform: [{ scale: 1.5 }] }}
                                                autoSize={true}
                                                resizeMode="cover"
                                                source={require('../../../assets/animation/searching1.json')}
                                                autoPlay
                                            />
                                            <Text style={styles.txtOnProcess}>On process ...</Text>
                                        </View>
                                        :
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                            <LottieView
                                                style={{ height: 40, transform: [{ scale: 1.5 }] }}
                                                autoSize={true}
                                                resizeMode="cover"
                                                source={require('../../../assets/animation/checked.json')}
                                                autoPlay
                                            />
                                            <Text style={styles.txtOnProcess}>Data terverifikasi ...</Text>
                                        </View>
                                }
                            </View>
                        </View>
                    </RBSheet>
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    titleContainer: {
        marginTop: 5,
    },
    title: {
        fontSize: 24,
        color: '#2a8b40',
        paddingLeft: 30,
        paddingTop: 20,
        fontWeight: 'bold'
    },
    animContainer: {
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    animContainer2: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: Dimensions.get('window').height / 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardContainer: {
        margin: 20
    },
    cardWrapper: {
        height: 200,
        borderRadius: 10,
        backgroundColor: '#e35200'
    },
    cardTitleContainer: {
        margin: 20
    },
    cardTitle: {
        fontSize: 24,
        color: '#fff'
    },
    cardChipContainer: {
        height: 40,
        width: 70,
        borderWidth: 0.5,
        borderColor: '#dedede',
        marginLeft: 20,
        borderRadius: 5,
        backgroundColor: '#f5cf1a'
    },
    cardNumberContainer: {
        margin: 10,
        flexDirection: 'row'
    },
    cardNumber: {
        color: '#fff',
        padding: 10,
        fontSize: 20
    },
    cardOwnerContainer: {
        marginLeft: 25
    },
    cardOwner: {
        color: '#fff'
    },
    inputContainer: {
        margin: 20
    },
    checkContainer: {
        marginTop: 20,
    },
    checkTextContainer: {
        flexDirection: 'row',
        marginRight: 20
    },
    txtSetuju: {
        marginTop: 14,
        fontSize: 12
    },
    txtSyarat: {
        fontSize: 12,
        marginTop: 2,
        color: '#385757',
        fontWeight: 'bold'
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#4caf50',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    noteContainer: {
        marginLeft: 20
    },
    noteTitle: {
        fontWeight: 'bold'
    },
    noteDetailContainer: {
        flexDirection: 'row'
    },
    noteDetail: {
        fontSize: 12,
        color: '#273554',
        fontWeight: 'bold',
        marginTop: 5
    },
    modal: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        // backgroundColor: 'rgba(0,0,0,0.7)',
        // padding: 100
    },
    modalHeader: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    modalInputHeader: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#f7f7f7',
        borderRadius: 30,
        width: Dimensions.get('window').width - 50,
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 5,
    },
    inputIcon: {
        marginLeft: 15
    },
    inputHeader: {
        height: 40,
        marginLeft: 5,
        borderBottomColor: '#FFFFFF',
        flex: 1,
        fontSize: 13
    },
    btnCloseHeader: {
        backgroundColor: '#02aced',
        marginLeft: 10,
        marginRight: 10,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnCloseText: {
        color: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        fontWeight: 'bold'
    },
    listContainer: {
        flex: 2,
        margin: 20
    },
    contactListContainer: {
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        margin: 5,
    },
    backModalContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    contactName: {
        fontWeight: 'bold',
        fontSize: 18
    },
    avaContainer: {
        backgroundColor: '#fff',
        borderWidth: 2,
        borderColor: '#dedede',
        borderRadius: 50,
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    txtAva: {
        fontSize: 24
    },
    infoContact: {
        marginBottom: 10,
        marginLeft: 15,
        width: '90%'
        // justifyContent: 'center',
    },
    contactNumber: {
        color: '#666',
        fontSize: 13,
        padding: 3,
        flex: 2
    },
    contactContainer: {
        marginTop: 5,
        marginBottom: 5,
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f7f7f7',
        width: '100%'
    },
    modalTitleContainer: {
        marginLeft: 25
    },
    titleModal: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    kirimKeContainer: {
        margin: 10,
        justifyContent: 'center',
    },
    btnPilih: {
        height: 30,
        padding: 5,
        backgroundColor: '#02aced',
    },
    txtPilih: {
        color: '#fff',
        fontWeight: 'bold'
    },
    btnPilihContainer: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    input: {
        fontSize: 13
    },
    sheetHeader: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    sheetTitle: {
        flex: 2,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    todayContainer: {
        flexDirection: 'row',
        backgroundColor: '#eee',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 10
    },
    btnConfirmationContainer: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnConfirmation: {
        backgroundColor: '#2a8b40',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        width: 300
    },
    txtConfirmation: {
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 14
    },
    txtOnProcess: {
        textAlign: 'center',
        paddingTop: 15,
        color: '#666'
    },
    input1: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    cashOutContainer: {
        margin: 20,
        height: Dimensions.get('window').height / 8,
        backgroundColor: '#C0FDD8',
        borderRadius: 20,
        flexDirection: 'row'
    },
    cashOutContainer2: {
        margin: 20,
        height: Dimensions.get('window').height / 8,
        backgroundColor: '#FCE5B3',
        borderRadius: 20,
        flexDirection: 'row'
    },
    iconCashOutContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    titleCashContainer: {
        flex: 3,
        justifyContent: 'center',
        // alignItems: 'center',
    },
    titleCash: {
        fontSize: 16,
        fontWeight: '500',
        paddingRight: 10,
    },
    subTitleCash: {
        fontSize: 13,
        paddingRight: 10,
        paddingTop: 5
    },
    btncontainer: {
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnBerandaContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    rekeningOnlineTxt: {
        color: '#2a8b40',
        paddingLeft: 5,
        fontWeight: 'bold'
    },
})

export default TarikSaldoViaTransfer;
