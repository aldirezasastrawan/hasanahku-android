import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import { Container, Content, Button, Form,Item, Label, Input } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import { PinInput, PinKeyboard   } from 'react-native-awesome-pin';
import appStyle from '../../styles/AppStyle';
import PinScreen from '../../../supports/PinScreen';
import { postWithToken } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {Error, ResponseCode} from '../../../supports/Constants';
import { getValue } from '../../../module/LocalData/Storage';
import {dialog} from '../../../supports/Dialog';

const size = Dimensions.get('window');

class PlnPascaBayarConfirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            inqRes: this.props.navigation.getParam('inqRes'),
            colorSpinner: '#e35200',
        };
    }

    paymentConfirmation() {
      this.RBSheet.open();
    }

    pinReset(){
      this.RBSheet.close();
      setTimeout(() => {
          this.props.navigation.navigate('VerifikasiLupaPin', {
              phone: this.props.navigation.getParam('phone'),
              screenCallback : "PlnPascaBayarConfirm"
          });
      }, 500);
    }

    doPayment(pin){
      this.setState({
        showSpinner: true
      });

      let params = {
        "phone": this.props.navigation.getParam('phone'),
        "pin": pin,
        "traceId": this.state.inqRes.traceId,
        "idpel": this.state.inqRes.idpel,
        "tagihan": this.state.inqRes.tagihan,
        "totalTagihan": this.state.inqRes.totalTagihan
      }

      getValue('tokenLogin').then(tokenLogin => {
        postWithToken(Env.base_url + Api.payPLNPostpaid, params, tokenLogin).then(response => {
          if(response.code == ResponseCode.OK){
            setTimeout(() => {
                this.setState({showSpinner: false});
                this.RBSheet.close();
                this.props.navigation.navigate('PlnPascaBayarPayment',{
                  "payRes": response.okContent,
                  "phone": this.props.navigation.getParam('phone')
                });
            }, 1000);
          } else {
            let failCode = response.failContent.failCode;
            if(failCode ==="USER_ERR_06"){
              setTimeout(() => {
                  this.setState({showSpinner: false});
                  this.pinScreen.clearError();
                  this.pinScreen.clearPin();
                  this.pinScreen.throwError(response.failContent.failDescription);
              }, 1000);
            }else{
              if(failCode === "USER_ERR_14" || failCode === "USER_ERR_15"){
                this.pinScreen.clearError();
                this.pinScreen.clearPin();
              }
              this.setState({showSpinner: false});
              this.pinScreen.clearPin();
              dialog.alertFail(response.failContent, this);
            }
          }
        }).catch(err => {
          this.setState({showSpinner: false});
          this.pinScreen.clearPin();
          dialog.alertException(err);
        });
      });
    }

    render() {

        return (
            <Container>
                <Content>
                    <View style={appStyle.navBox}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={appStyle.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={25} type='antdesign' />
                            <View style={appStyle.navBoxTitle}>
                              <Text style={appStyle.navTitle}>Listrik Pasca Bayar</Text>
                            </View>
                        </Button>
                    </View>
                    <View style={appStyle.formBox}>
                      <Form>
                        <Item fixedLabel style={appStyle.itemHeaderTrx}>
                            <Label style={appStyle.labelHeaderTrx}>Detail Transaksi</Label>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>ID Pelanggan</Label>
                          <Text style={appStyle.labelTrxValue}>{this.state.inqRes.idpel}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Nama Pelanggan</Label>
                          <Text style={appStyle.labelTrxValue}>{this.state.inqRes.nama}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Bulan/ Tahun</Label>
                          <Text style={appStyle.labelTrxValue}>{this.state.inqRes.periodeFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Total Lembar Tagihan</Label>
                          <Text style={appStyle.labelTrxValue}>{this.state.inqRes.lembar}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemHeaderTrx}>
                            <Label style={appStyle.labelHeaderTrx}>Detail Pembayaran</Label>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Denda</Label>
                          <Text style={[appStyle.labelTrxValue, {fontWeight : 'bold'}]}>{this.state.inqRes.dendaFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Jumlah Tagihan PLN</Label>
                          <Text style={[appStyle.labelTrxValue, {fontWeight : 'bold'}]}>{this.state.inqRes.tagihanFormatted}</Text>
                        </Item>
                        <View style={{marginTop:-20, marginBottom:10, marginLeft:15}}>
                          <Text style={appStyle.labelFootNote}>*Termasuk denda</Text>
                        </View>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Biaya Transaksi</Label>
                          <Text style={[appStyle.labelTrxValue, {fontWeight : 'bold'}]}>{this.state.inqRes.adminFeeFormatted}</Text>
                        </Item>
                      </Form>
                    </View>
                    <View style={appStyle.boxTotalConfirm}>
                      <View style={appStyle.formBox}>
                        <Form>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Total Pembayaran</Label>
                          <Text style={[appStyle.labelTrxValue, {fontWeight : 'bold'}]}>{this.state.inqRes.totalTagihanFormatted}</Text>
                        </Item>
                        </Form>
                      </View>
                    </View>
                    <View style={appStyle.btnDefaultBox}>
                        <Button onPress={() => this.paymentConfirmation()} style={appStyle.btnDefault}>
                            <Text style={appStyle.btnDefaultText}>Konfirmasi</Text>
                        </Button>
                    </View>
                    <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown
                        height={Platform.OS == 'android' ? size.height / 1.5 : (size.height / 1.5) + 100}
                        // animationType="fade"
                        duration={300}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 10,
                                borderTopLeftRadius: 10,
                            },
                        }}>
                          <PinScreen
                            onRef={ ref => (this.pinScreen = ref) }
                            tagline='Masukkan PIN HasanahKu'
                            numberOfPins={6}
                            keyDown={ this.doPayment.bind(this) }
                            pinReset={this.pinReset.bind(this)}
                            />
                      </RBSheet>
                      <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                      />
                </Content>
            </Container>
        );
    }
}

export default PlnPascaBayarConfirm;
