import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, BackHandler } from 'react-native';
import { Container, Content, Button, Form,Item, Label } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import appStyle from '../../styles/AppStyle';
import ReceiptImage from '../../../supports/ReceiptImage';

class PlnPrabayarPayment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200'
        };

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentWillMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
      this.backToMenu();
      return true;
    }

    backToMenu() {
        this.setState({
            showSpinner: true,
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
            });

            this.props.navigation.navigate('Pln')
        }, 1000);
    }


    render() {
        const payRes = this.props.navigation.getParam('payRes')
        return (
            <Container>
                <Content>
                    <View style={appStyle.headerTrxSuccess}>
                        <Text style={appStyle.txtTrxSuccess}>
                            Transaksi Berhasil
                        </Text>
                    </View>
                   <View style={appStyle.checkedHeaderTrxSuccessContainer}>
                        <View style={appStyle.checkedHeaderTrxSuccessMasking}/>
                        <LottieView
                            style={{ height: 100, marginTop: -35 }}
                            autoSize={true}
                            resizeMode="cover"
                            source={require('../../../assets/animation/checked.json')}
                            autoPlay
                            loop={false}
                        />
                    </View>
                    <View style={appStyle.btnheaderTrxSuccessSection}>
                        <Button onPress={()=> this.receiptImage.share(this.props.navigation.getParam('phone'),payRes.reffNum, "BILLER", "PAYMENT_PLN_PRA_BAYAR")} style={appStyle.btnShareTrxSuccess}>
                            <Icon color="#17284c" active name='share' size={20} type="material-community" />
                            <Text style={appStyle.txtShareTrxSuccess}>Share</Text>
                        </Button>
                        <Button onPress={()=> this.receiptImage.download(this.props.navigation.getParam('phone'),payRes.reffNum, "BILLER", "PAYMENT_PLN_PRA_BAYAR")} style={appStyle.btnDownloadTrxSuccess}>
                            <Icon color="#17284c" active name='download' size={20} type="material-community" />
                            <Text style={appStyle.txtBtnDownloadTrxSuccess}>Download</Text>
                        </Button>
                    </View>

                    <View style={appStyle.titleContainerTrxSuccess}>
                        <Text style={appStyle.txtTitleContainerTrxSuccess}>
                            Detail Transaksi
                        </Text>
                    </View>
                    <View style={[appStyle.formBox, {marginTop:0, marginBottom:0}]}>
                      <Form>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Tanggal Transaksi</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.trxDate}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Waktu Transaksi</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.trxTime}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Jenis Layanan</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.jenisLayanan}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Nomor Reference</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.reff}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Nomor Meter</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.serialMeter}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Nama Pelanggan</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.nama}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>ID Pelanggan</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.idpel}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Tarif/Daya</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.tarifDayaFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Jumlah KWH</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.kwhFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Stroom/Token</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.token}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Materai</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.materaiFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>PPN</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.ppnFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>PPJ</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.ppjFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Angsuran</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.angsuranFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Rp Stroom/Token</Label>
                          <Text style={appStyle.labelTrxValue}>{payRes.stroomFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Nominal Token</Label>
                          <Text style={[appStyle.labelTrxValue, {fontWeight : 'bold'}]}>{payRes.denomFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Biaya Transaksi</Label>
                          <Text style={[appStyle.labelTrxValue, {fontWeight : 'bold'}]}>{payRes.adminFeeFormatted}</Text>
                        </Item>
                      </Form>
                    </View>
                    <View style={[appStyle.titleContainerTrxSuccess, {marginTop:0}]}>
                        <Text style={appStyle.txtTitleContainerTrxSuccess}>
                            Total Transaksi
                        </Text>
                    </View>
                    <View style={[appStyle.formBox, {marginTop:0}]}>
                      <Form>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Total Pembayaran</Label>
                          <Text style={[appStyle.labelTrxValue, {fontWeight : 'bold'}]}>{payRes.totalTagihanFormatted}</Text>
                        </Item>
                      </Form>
                    </View>
                    <View style={appStyle.btnDefaultBox}>
                        <Button onPress={() => this.backToMenu()} style={appStyle.btnDefault}>
                            <Text style={appStyle.btnDefaultText}>Kembali Ke Menu</Text>
                        </Button>
                    </View>
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                    <ReceiptImage
                      onRef={ ref => (this.receiptImage = ref) }
                    />

                </Content>
            </Container>
        );
    }
}

export default PlnPrabayarPayment;
