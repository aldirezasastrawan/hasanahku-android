import React, { Component } from 'react';
import { View, Text, StyleSheet, Image,ImageBackground, Dimensions, FlatList, LayoutAnimation, UIManager, TouchableOpacity  } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label, CheckBox, ListItem, Body } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import appStyle from '../../styles/AppStyle';
import SegmentedControlTab from "react-native-segmented-control-tab";
import { post, postWithToken } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import { getValue } from '../../../module/LocalData/Storage';
import {dialog} from '../../../supports/Dialog';
import {utils} from '../../../utils/Utils';

const size = Dimensions.get('window')

class Pln extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTabIndex : 0,
            selectedPrabayarPLNIndex : -1,
            showSpinner: false,
            dataPrabayarPLN: [],
            namaProduk: '',
            denom:'',
            isBtnDisable: true,
            isBtnDisablePra: true,
            isBtnDisablePasca: true,
            inputs: {
              noMeter: {
                type: 'noMeter',
                value: ''
              },
              idPelanggan: {
                type: 'idPelanggan',
                value: ''
              }
            }
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.validateInput = ValidationHelper.validateInput.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentDidMount() {
        this.getDataPrabayarPLN();
    }

    getDataPrabayarPLN() {

      this.setState({
        showSpinner: true
      });

      let params = {
          "kodeProduct": 'PLN_PREPAID'
      }

      post(Env.base_url + Api.getProduct, params).then(response => {
          if(response.code == ResponseCode.OK){
              this.setState({
                  showSpinner: false,
                  dataPrabayarPLN: response.okContent.produk
              });
          } else {
            this.setState({showSpinner: false});
            dialog.alertFail(response.failContent, this);
          }
      }).catch(err => {
        this.setState({showSpinner: false});
        dialog.alertException(err)
      });
    }

    handleTabIndexSelect = (index: number) => {
      this.setState(prevState => (
        {
          ...prevState,
          selectedTabIndex: index,
          isBtnDisable : index === 0 ? !(this.state.isBtnDisablePra == false && this.state.selectedPrabayarPLNIndex !== -1) : this.state.isBtnDisablePasca
         }
      ))
    }

    selectPrabayarPLN(item, index) {
        this.setState({
          namaProduk: item.namaProduk,
          denom: item.denom,
          selectedPrabayarPLNIndex : index,
          isBtnDisable : this.state.isBtnDisablePra
        });
    }

    renderPrabayarPLN = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => this.selectPrabayarPLN(item, index)} activeOpacity = {0.5} >
                    { this.state.selectedPrabayarPLNIndex === index ?
                      <View style={[styles.GridViewBlockStyle, {backgroundColor: '#FFF2DF'}]}>
                          <Text style={styles.GridViewInsideTextItemStyle}>{utils.currencyFormat(item.denom, 0)}</Text>
                      </View>
                      :
                      <View style={styles.GridViewBlockStyle}>
                          <Text style={styles.GridViewInsideTextItemStyle}> {utils.currencyFormat(item.denom, 0)} </Text>
                      </View>
                    }
            </TouchableOpacity>
        )
    }

    onChangeMeterNum(meterNum){

      if(this.validateInput({id: 'noMeter', value: meterNum}) == true){
        this.setState({
          isBtnDisablePra : false,
          isBtnDisable : this.state.selectedPrabayarPLNIndex === -1
        })
      }else{
        this.setState({
          isBtnDisablePra : true,
          isBtnDisable : true
        })
      }
    }

    onChangeIdPel(idPelanggan){

      if(this.validateInput({id: 'idPelanggan', value: idPelanggan}) == true){
        this.setState({
          isBtnDisablePasca : false,
          isBtnDisable : false
        })
      }else{
        this.setState({
          isBtnDisablePasca : true,
          isBtnDisable : true
        })
      }
    }

    inquiryPLN() {

        this.setState({
            showSpinner: true
        });

        if(this.state.selectedTabIndex === 0){

          let params = {
              "phone": this.props.navigation.getParam('phone'),
              "idpel": this.state.inputs.noMeter.value,
              "namaProduk": this.state.namaProduk
          }

          getValue('tokenLogin').then(tokenLogin => {
            postWithToken(Env.base_url + Api.inqPLNPrepaid, params, tokenLogin).then(response => {
              this.setState({showSpinner: false});
              if(response.code == ResponseCode.OK){
                  this.props.navigation.navigate('PlnPrabayarConfirm',{
                    "phone": this.props.navigation.getParam('phone'),
                    "inqRes": response.okContent,
                  });
              } else {
                dialog.alertFail(response.failContent, this);
              }

            }).catch(err => {
              this.setState({showSpinner: false});
              dialog.alertException(err);
            });
          });
        }else{
          let params = {
              "phone": this.props.navigation.getParam('phone'),
              "idpel": this.state.inputs.idPelanggan.value
          }

          getValue('tokenLogin').then(tokenLogin => {
            postWithToken(Env.base_url + Api.inqPLNPostpaid, params, tokenLogin).then(response => {
              this.setState({showSpinner: false});
              if(response.code == ResponseCode.OK){
                  this.props.navigation.navigate('PlnPascaBayarConfirm',{
                    "phone": this.props.navigation.getParam('phone'),
                    "inqRes": response.okContent
                  });
              } else {
                  dialog.alertFail(response.failContent, this);
              }
            }).catch(err => {
              this.setState({showSpinner: false});
              dialog.alertException(err);
            });
          });
        }
      }

    render() {
        return (
            <Container>
                <Content>
                    <View style={appStyle.navBox}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={appStyle.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={25} type='antdesign' />
                            <View style={appStyle.navBoxTitle}>
                              <Text style={appStyle.navTitle}>Listrik PLN</Text>
                            </View>
                        </Button>
                    </View>
                    <View style={appStyle.bannerBox}>
                      <View style={appStyle.bannerContent}>
                        <Image style={appStyle.bannerLogo} source={require('../../../assets/image/logo_container_pln.png')} />
                        <View style={appStyle.bannerInfo}>
                            <Text style={appStyle.bannerTitle}>Pembelian Listrik</Text>
                            <Text style={appStyle.bannerDesc}>Pembelian listrik dengan menginput{"\n"}nomor meter/ id pelanggan kemudian{"\n"}pilih nominal</Text>
                        </View>
                      </View>
                    </View>
                    <View>
                        <View style={appStyle.tabBox}>
                            <SegmentedControlTab
                                values={['Prabayar', 'Pascabayar']}
                                selectedIndex={this.state.selectedTabIndex}
                                onTabPress={this.handleTabIndexSelect}
                                borderRadius={20}
                                tabsContainerStyle={{ height: 40, backgroundColor: '#B6EACC', borderRadius : 20 }}
                                tabStyle={{ backgroundColor: '#B6EACC', borderWidth: 0, margin : 5, borderColor: 'transparent' }}
                                activeTabStyle={{ backgroundColor: '#FFFFFF', borderRadius : 20 }}
                                tabTextStyle={{ color: '#198E4B', fontWeight: 'bold' }}
                                activeTabTextStyle={{ color: '#888888' }}/>
                                {this.state.selectedTabIndex === 0 &&
                                    <View style={appStyle.tabContent}>
                                      <Form>
                                        <Item stackedLabel>
                                          <Label style={appStyle.label}>No Meter</Label>
                                          <Input style={appStyle.input} keyboardType='numeric' maxLength={12} minLength={11}
                                           value={this.state.inputs.noMeter.value}
                                           onChangeText={value => this.onChangeMeterNum(value)} />
                                        </Item>
                                        {this.validateError('noMeter')}
                                      </Form>
                                      <View style={styles.gridContainer}>
                                          <FlatList
                                              data={this.state.dataPrabayarPLN}
                                              renderItem={this.renderPrabayarPLN}
                                              extraData={this.state}
                                              numColumns={3}
                                          />
                                      </View>
                                    </View>
                                }
                                {this.state.selectedTabIndex === 1 &&
                                  <View style={appStyle.tabContent}>
                                    <Form>
                                      <Item stackedLabel>
                                        <Label style={appStyle.label}>ID Pelanggan</Label>
                                        <Input style={appStyle.input} keyboardType='numeric' maxLength={12} minLength={11}
                                         value={this.state.inputs.idPelanggan.value}
                                         onChangeText={value => this.onChangeIdPel(value)} />
                                      </Item>
                                    </Form>
                                  </View>
                                }
                        </View>
                        <View style={appStyle.btnDefaultBox}>
                            <Button disabled={this.state.isBtnDisable} onPress={() => this.inquiryPLN()} style={[appStyle.btnDefault, this.state.isBtnDisable == true && { backgroundColor: "#EAEAEA" }]}>
                                <Text style={appStyle.btnDefaultText}>Lanjutkan</Text>
                            </Button>
                        </View>
                    </View>
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    gridContainer: {
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'visible'
    },
    GridViewBlockStyle: {
      justifyContent: 'center',
      borderRadius: 10,
      alignItems: 'center',
      height: 50,
      width: size.width / 4,
      margin: 5,
      backgroundColor: '#FCFBFC'
    },
    GridViewInsideTextItemStyle: {
      color: '#131A22',
      padding: 5,
      fontSize: 12
    }
})

export default Pln;
