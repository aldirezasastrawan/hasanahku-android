import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Container, Content, Button, Form, Item, Label, Input } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import LinearGradient from 'react-native-linear-gradient';


class Tutupakun extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasHubungiKami: false,
            showSpinner: false,
            colorSpinner: '#e35200',
            email: '',
            noHp: '',
            tanggal: '',
            namaBank: '',
            namaPemilikRekening: '',
            nomorRekening: ''
        };
    }

    kirim() {
        this.setState({
            showSpinner: true,
        });

        if (this.state.namaPemilikRekening != '' && this.state.email != '' && this.state.noHp != '' && this.state.namaPemilikRekening != '' && this.state.tanggal != '' && this.state.nomorRekening != '') {
            setTimeout(() => {
                this.setState({
                    showSpinner: false,
                });
                setTimeout(() => {
                    alert('Terima kasih, data anda akan segera kami tutup.')
                    this.props.navigation.goBack()
                }, 500);

            }, 3000);
        } else {
            setTimeout(() => {
                this.setState({
                    showSpinner: false,
                });

                setTimeout(() => {
                    alert('Harap masukkkan data dengan benar')

                }, 500);
            }, 3000);
        }
    }

    render() {
        return (
            <Container>
                <Content>
                    <LinearGradient locations={[0, 1.0]} colors={['#2a8b40', '#1e622d']} style={styles.header}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color='#fff' size={30} type='antdesign' />
                        </Button>
                        <Text style={styles.title}>Tutup Akun</Text>
                    </LinearGradient>

                    {this.state.hasHubungiKami == false ?
                        <View>
                            <View style={styles.infoContainer}>
                                <Text style={styles.infoSubTitle}>Untuk penutupan akun dapat dilakukan melalui Customer Service HasanahKu(1111111) atau menghubungi via E-mail hasanahku@gmail.com atau menggunakan fitur Hubungi Kami</Text>
                            </View>

                            <View style={styles.btnContainer}>
                                <Button onPress={() => this.setState({ hasHubungiKami: true })} transparent style={styles.btnHubungi}>
                                    <Icon containerStyle={{ marginRight: 10 }} color="#385757" name='filetext1' type="antdesign" />
                                    <Text style={styles.txtHubungi}>Hubungi Kami</Text>
                                </Button>
                            </View>
                        </View>

                        :

                        <View>
                            <View style={styles.formContainer}>
                                <Text style={styles.infoFormTitle}>Form penutupan akun</Text>
                                <Text style={styles.infoFormSubTitle}>Silahkan isi form untuk penutupan akun</Text>
                            </View>

                            <View style={styles.inputContainer}>
                                <Form>
                                    <Item stackedLabel>
                                        <Label style={styles.label}>Email Terdaftar</Label>
                                        <Input value={this.state.email} onChangeText={(email) => this.setState({ email: email })} />
                                    </Item>

                                    <Item stackedLabel last>
                                        <Label style={styles.label}>Nomor HP Terdaftar</Label>
                                        <Input value={this.state.noHp} onChangeText={(noHp) => this.setState({ noHp: noHp })} />
                                    </Item>

                                    <Item stackedLabel last>
                                        <Label style={styles.label}>Tanggal Penutupan Akun</Label>
                                        <Input keyboardType='number-pad' value={this.state.tanggal} onChangeText={(tanggal) => this.setState({ tanggal: tanggal })} />
                                    </Item>
                                    <Item stackedLabel>
                                        <Label style={styles.label}>Nama Bank</Label>
                                        <Input value={this.state.namaBank} onChangeText={(namaBank) => this.setState({ namaBank: namaBank })} />
                                    </Item>

                                    <Item stackedLabel last>
                                        <Label style={styles.label}>Nama Pemilik Rekening</Label>
                                        <Input value={this.state.namaPemilikRekening} onChangeText={(namaPemilikRekening) => this.setState({ namaPemilikRekening: namaPemilikRekening })} />
                                    </Item>

                                    <Item stackedLabel last>
                                        <Label style={styles.label}>Nomor Rekening</Label>
                                        <Input keyboardType='number-pad' value={this.state.nomorRekening} onChangeText={(nomorRekening) => this.setState({ nomorRekening: nomorRekening })} />
                                    </Item>
                                </Form>
                            </View>

                            <View style={styles.btnRegisterContainer}>
                                <Button onPress={() => this.kirim()} style={styles.btnRegister}>
                                    <Text style={styles.RegisterText}>Kirim</Text>
                                </Button>
                            </View>


                        </View>

                    }

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    header: {
        backgroundColor: '#e35200',
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    title: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16,
        flex: 2
    },
    infoContainer: {
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    infoTitle: {
        color: '#e35200',
        fontSize: 16,
        fontWeight: 'bold'
    },
    infoSubTitle: {
        color: '#385757',
        fontSize: 13,
        marginTop: 20,
        fontWeight: '900',
        textAlign: 'center'
    },
    btnContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50
    },
    btnHubungi: {
        width: 100,
        marginRight: 40
    },
    txtHubungi: {
        color: '#385757',
        fontWeight: 'bold'
    },
    formContainer: {
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    infoFormTitle: {
        color: '#2a8b40',
        fontSize: 16,
        fontWeight: 'bold'
    },
    infoFormSubTitle: {
        color: '#385757',
        fontSize: 13,
        marginTop: 20,
        fontWeight: 'bold',
    },
    inputContainer: {
        margin: 20
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#2a8b40',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
})

export default Tutupakun;
