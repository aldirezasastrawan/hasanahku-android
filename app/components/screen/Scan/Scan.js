import React, { Component } from 'react';
import { Container, Content, Button, List, ListItem, Body } from 'native-base';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {Text, Linking, View, StyleSheet, Dimensions, Image} from 'react-native';
import { Icon } from 'react-native-elements';
import { withNavigationFocus } from 'react-navigation'


class Scan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scan: true,
            ScanResult: false,
            result: null,
            hasCameraPermission: false
        };
    }

    onSuccess = (e) => {
        const check = e.data.substring(0, 4);
        console.log('scanned data' + check);
        this.setState({
            result: e,
            scan: false,
            ScanResult: true
        })
        if (check === 'http') {
            Linking
                .canOpenURL(e.data)

            this.scanner.reactivate()

        } else {
            this.setState({
                result: e,
                scan: false,
                ScanResult: true
            })
        }

    }

    activeQR = () => {
        this.setState({
            scan: true
        })
    }
    scanAgain = () => {
        this.setState({
            scan: true,
            ScanResult: false
        })
    }

    paymentConfirm() {
        alert('Pembayaran Berhasil Dikonfirmasi')
        this.scanAgain()
    }

    render() {
        const { scan, ScanResult, result, hasCameraPermission } = this.state
        const desccription = ''
        const { isFocused } = this.props
        return (

            <Container>
                {/* <ImageBackground style={{ height: '100%', width: '100%' }} source={require('../../../assets/image/bg_image.png')}> */}
                <Content>

                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='close' color='#000' size={24} type='font-awesome' />
                            <Text></Text>
                        </Button>
                        <Image style={styles.imageLogo} source={require('../../../assets/image/bnisyariahlogo.png')} />
                    </View>

                    {scan && isFocused &&
                        <View style={styles.infoContainer}>
                            <Text style={styles.title}>Gerakan Kamera untuk melihat QR Code dan Konfirmasi Pembayaran</Text>
                        </View>
                    }
                    {scan && isFocused &&
                        <QRCodeScanner
                            reactivate={true}
                            showMarker={true}
                            ref={(node) => { this.scanner = node }}
                            onRead={this.onSuccess}
                            containerStyle={styles.scannerView}
                        />
                    }

                    {ScanResult && isFocused &&
                        <View style={styles.detailContainer}>
                            <List>
                                <ListItem button>
                                    <Body>
                                        <Text style={styles.result1}>Type : {result.type}</Text>
                                    </Body>
                                </ListItem>
                                <ListItem button>
                                    <Body>
                                        <Text style={styles.result1}>Result : {result.data}</Text>
                                    </Body>
                                </ListItem>
                                <ListItem button>
                                    <Body>
                                        <Text style={styles.result1}>Raw Data : {result.rawData}</Text>
                                    </Body>
                                </ListItem>
                            </List>
                        </View>
                    }

                    {ScanResult && isFocused &&
                        <View style={styles.btnContainer}>
                            <Button onPress={() => this.paymentConfirm()} style={styles.konfirmBtn}>
                                <Text style={styles.konfirmText}>Konfirmasi Pembayaran</Text>
                            </Button>
                        </View>}
                </Content>
                {/* </ImageBackground> */}
            </Container >

        );
    }
}


const styles = StyleSheet.create({
    container: {
    },
    title: {
        fontSize: 14,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    infoContainer: {
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10
    },
    scannerView: {
        marginTop: 100,
        height: Dimensions.get('window').height / 2
    },
    konfirmBtn: {
        backgroundColor: '#e35200',
        justifyContent: 'center',
        alignItems: 'center',
        width: 200,
    },
    konfirmText: {
        color: '#fff',
        fontWeight: 'bold'
    },
    btnContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50
    },
    detailContainer: {
        marginTop: 100,
        marginLeft: 20,
        marginRight: 20,
    },
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1,
    },
    imageLogo: {
        height: 60,
        width: 80,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        resizeMode: 'contain'
    },
});

export default withNavigationFocus(Scan);
