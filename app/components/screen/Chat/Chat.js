import React, { Component } from 'react';
import {
    View, Text, KeyboardAvoidingView, FlatList, StyleSheet, ScrollView,
    Image, TouchableOpacity, Dimensions, UIManager,
    Keyboard, Animated, Platform
} from 'react-native';
import { Dialogflow_V2 } from 'react-native-dialogflow';
import Env from '../../../module/Utils/Env';
import { Input, Button, Card } from 'native-base';
import Tts from 'react-native-tts';
import { Icon } from 'react-native-elements';
import moment from 'moment';
import LottieView from 'lottie-react-native';
import Voice from 'react-native-voice';

// get device OS
const OS = Platform.OS;
// get OS navigation bar height
const NAVIGATION_HEIGHT = OS === 'ios' ? 64 : 54;
// get window height
const WINDOW_HEIGHT = Dimensions.get('window').height;

class Chat extends Component {
    constructor(props) {
        super(props);

        const {
            chatVerticalOffset,
            fixNavBarOffset
        } = this.props;

        const navigationHeight = fixNavBarOffset ? NAVIGATION_HEIGHT : 0;
        // get FlatChat vertical offset to allow extra space on top
        const verticalOffset = (chatVerticalOffset || 0) + navigationHeight;

        this.state = {
            resultDialogFlow: '',
            dataMessage: [],
            dataQuickButton: [],
            menu: [],
            paket: [],
            paketList: [],
            messageFromMe: '',
            user: '',
            is_listening: false,
            onFocusChat: false,
            imagePaketList: '',
            titlePaketList: '',


            // flags based on 'scrollBottomOnKeyboardShow' prop type
            scrollOnKeyboardShowBottomStart: false,
            scrollOnKeyboardShowBottomEnd: true,

            /* -----------------------------------------------------
             * FlatChat layout settings
             */

            // manage keyboard status
            activeKeyboard: false,

            // manage FlatList scroll position on onScroll event
            scrollPosition: 0,

            // manage FlatList layout to get scroll max position (scrollLimit)
            contentSize: 0,
            layoutHeight: 0,


            // FlatChat layout variables
            flatListOffset: verticalOffset,
            flatListHeight: WINDOW_HEIGHT - verticalOffset,
            listHeight: new Animated.Value(WINDOW_HEIGHT - verticalOffset),
        };



        Voice.onSpeechStart = this.onSpeechStart;
        Voice.onSpeechEnd = this.onSpeechEnd;
        Voice.onSpeechError = this.onSpeechError;
        Voice.onSpeechResults = this.onSpeechResults;
        Voice.onSpeechPartialResults = this.onSpeechPartialResults;
        Voice.onSpeechVolumeChanged = this.onSpeechVolumeChanged;

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentWillMount() {
        this.keyboardWillShowListener = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow.bind(this));
        this.keyboardWillHideListener = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide.bind(this));
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide.bind(this));
    }

    componentWillUnmount() {
        Tts.stop()
        Voice.destroy().then(Voice.removeAllListeners);

        this.keyboardWillShowListener.remove();
        this.keyboardWillHideListener.remove();
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    componentDidUpdate(prevProps, prevState) {
        // if (prevState.dataMessage.length !== this.state.dataMessage.length && this.state.dataMessage.length > 0) {
        //     setTimeout(() => {
        this.scroller.scrollToEnd();
        // }, 250);
        // }
    }

    componentWillUpdate() {
        this.scroller.scrollToEnd();
    }

    componentDidMount() {
        Dialogflow_V2.setConfiguration(
            Env.client_email,
            Env.private_key,
            Dialogflow_V2.LANG_ENGLISH_US,
            Env.project_id
        );

        // this.sendData()
        this.getUserData();
        this.getDataBanner()
        this.getDataPaket()
        this.menu()
    }

    keyboardWillShow(e) {
        const { listHeight, flatListHeight, scrollOnKeyboardShowBottomStart } = this.state;
        const keyboardHeight = e.endCoordinates.height;

        if (scrollOnKeyboardShowBottomStart) {
            listHeight.addListener(() => this.scroller.scrollToEnd());
        } else


            Animated.timing(listHeight, {
                // animate the FlatList height to his initial minus the keyboard height
                toValue: flatListHeight - keyboardHeight,
                // uses the keyboard animation timing to be synchronized
                duration: e.duration

                // scroll to the end when animation is finished
            }).start(() => {
                /** remove all listHeight listeners if @prop @type scrollOnKeyboardShow = "bottom-start" */
                if (this.state.scrollOnKeyboardShowBottomStart) {
                    this.state.listHeight.removeAllListeners();
                }
            });
    }

    keyboardDidShow() {
        this.setState({ activeKeyboard: true });
        if (this.state.scrollOnKeyboardShowBottomEnd) {
            this.scroller.scrollToEnd();
        }
    }

    keyboardWillHide(e) {
        const { listHeight, flatListHeight, scrollPosition, contentSize, layoutHeight } = this.state;
        /*************************** FLATLIST ANIMATION  */

        Animated.timing(listHeight, {
            // animate the FlatList height to his initial height
            toValue: flatListHeight,
            // fixer to speed up the resizing animation on closing keyboard
            duration: e.duration - 150
        }).start();
    }


    keyboardDidHide() {
        this.setState({ activeKeyboard: false });
    }

    updateScrollPosition(e) {
        const scrollPosition = this.state.scrollPosition;
        const currentScrollPos = e.nativeEvent.contentOffset.y;

        // ignore negative scroll positions in case of iOS momentum or bugs
        if (currentScrollPos >= 0) {
            const sensitivity = 10;
            const scrollPositionOffset = (currentScrollPos - scrollPosition);

            if (Math.abs(scrollPositionOffset) > sensitivity) {
                this.setState({ scrollPosition: currentScrollPos });
            }
        }
    }

    updateLayoutHeight(e) {
        this.setState({ layoutHeight: e.nativeEvent.layout.height });
    }

    updateContentSize(w, h) {
        this.setState({ contentSize: h });
    }

    onSpeechError = e => {
        console.log('speech error: ', e);
        Voice.stop()
        this.setState({ is_listening: false })
    }

    onSpeechEnd(e) {
        console.log('end ');
        // console.log('endd ', e);

        // if (Platform.OS === 'ios') {
        //     this.sendMessage(this.state.messageFromMe);
        // }
    }


    onSpeechResults = e => {
        console.log('speech results: ', e);
        this.setState({
            is_listening: false,
            // messageFromMe: e.value[0],
        });

        if (Platform.OS === 'ios') {
            console.log('ios: ', e.value[0]);
            Voice.stop();
            this.sendMessage(e.value[0]);

        } else {
            if (e.value[0] != '') {
                Voice.stop();
                this.sendMessage(e.value[0]);
            }


            // setTimeout(() => {
            //     this.sendMessage(e.value[0])
            // }, 1500);
        }
    }

    listen = async () => {
        if (this.state.is_listening == false) {
            this.setState({
                is_listening: true
            });

            try {
                await Voice.start("id-ID");
            } catch (e) {
                console.error(e);
            }
        } else {
            this.setState({
                is_listening: false
            });

            try {
                await Voice.stop()
            } catch (e) {
                console.error(e);
            }
        }

    }

    startVoice(voice) {
        Tts.setDefaultPitch(1.3);
        Tts.setDefaultLanguage('id-ID');
        // let voice = [
        //     'Hai Inna Alvi',
        //     'Saldo kamu saat ini sebesar 200.000.000 Rupiah.',
        // ];

        // voice.map(y => {
        //     console.log(y);
        //     Tts.speak(y);
        // });
        Tts.speak(voice)
    }

    handleGoogleResponse(result) {
        let resultDialog = result
        console.log("respon ", resultDialog);
        alert(resultDialog.queryResult.fulfillmentMessages[0].text.text[0])
        this.setState({ resultDialogFlow: resultDialog.queryResult.fulfillmentMessages[0].text.text[0] })
    }

    sendDataToBot(newMessage) {
        this.scroller.scrollToEnd({ animated: true })
        this.giveLoading();
        console.log('called');
        Dialogflow_V2.requestQuery(
            newMessage,
            result => this.getDataChat(result),
            error => console.log(error)
        );
    }


    getUserData() {

    }

    getDataBanner() {
        let data = [
            { title: 'Kode Transfer BNI Syariah', menu: 'Transfer', image: require('../../../assets/image/transfer.jpg'), description: 'Mau transfer ke BNI Syariah? Gunakan kode transfer 427' },
            { title: 'Asuransi melalui HasanahKu', menu: 'Asuransi', image: require('../../../assets/image/donasi.jpg'), description: 'Hasanahku bekerja sama dengan berbagai Asuransi terbaik' },
            { title: 'Kemudahan Pembayaran ', menu: 'Paketdata', image: require('../../../assets/image/transfer1.jpg'), description: 'Pembayaran menjadi lebih mudah melalui HasanahKu Mobile App' },
            { title: 'Top Up HasanahKu', menu: 'TopUp', image: require('../../../assets/image/credit1.jpg'), description: 'Dapatkan berbagai Promo menarik di HanasahKu' },
        ]

        this.setState({
            menu: data
        })
    }

    getDataPaket() {
        let data = [
            {
                typeId: 1, type: 'Telkomsel', detail: 'Telkomsel', image: require('../../../assets/image/telkom.png'),
                dataNominal: [
                    { nama: 'Data 12 GB', detail: 'Data 12 GB + 2GB Videomax', nominal: '103.410', masaAktif: '30 hari' },
                    { nama: 'Data 25.000', detail: 'Kuota 300MB - 750MB + 1GB Videomax', nominal: '26.008', masaAktif: '30 hari' },
                    { nama: 'Data 4.5 GB', detail: 'Data 4.5 GB + 2GB Video', nominal: '70.319', masaAktif: '30 hari' },
                    { nama: 'Data 8 GB', detail: 'Data 8 GB + 2GB Video', nominal: '87.899', masaAktif: '30 hari' },
                    { nama: 'Data MAXstream Gala 30 GB', detail: '30GB MAXstream ', nominal: '152.788', masaAktif: '30 hari' },
                ]
            },
            {
                typeId: 2, type: 'XL Axiata', detail: 'XL Axiata', image: require('../../../assets/image/xl.png'),
                dataNominal: [
                    { nama: 'AnyNet 150 Menit 30 Hari', detail: 'Bebas bicara ke semua operator 150 menit', nominal: '103.410', masaAktif: '30 hari' },
                    { nama: 'Data 25.000', detail: 'Kuota 300MB - 750MB + 1GB Videomax', nominal: '26.008', masaAktif: '30 hari' },
                    { nama: 'Data 4.5 GB', detail: 'Data 4.5 GB + 2GB Video', nominal: '70.319', masaAktif: '30 hari' },
                    { nama: 'Data 8 GB', detail: 'Data 8 GB + 2GB Video', nominal: '87.899', masaAktif: '30 hari' },
                    { nama: 'Data MAXstream Gala 30 GB', detail: '30GB MAXstream ', nominal: '152.788', masaAktif: '30 hari' },
                ]
            },
            {
                typeId: 3, type: 'Indosat', detail: 'Indosat Oredoo', image: require('../../../assets/image/indosat.png'),
                dataNominal: [
                    { nama: 'Data 12 GB', detail: 'Data 12 GB + 2GB Videomax', nominal: '103.410', masaAktif: '30 hari' },
                    { nama: 'Data 25.000', detail: 'Kuota 300MB - 750MB + 1GB Videomax', nominal: '26.008', masaAktif: '30 hari' },
                    { nama: 'Data 4.5 GB', detail: 'Data 4.5 GB + 2GB Video', nominal: '70.319', masaAktif: '30 hari' },
                    { nama: 'Data 8 GB', detail: 'Data 8 GB + 2GB Video', nominal: '87.899', masaAktif: '30 hari' },
                    { nama: 'Data MAXstream Gala 30 GB', detail: '30GB MAXstream ', nominal: '152.788', masaAktif: '30 hari' },
                ]
            },
            {
                typeId: 4, type: '3', detail: '3', image: require('../../../assets/image/3.png'),
                dataNominal: [
                    { nama: 'Data 12 GB', detail: 'Data 12 GB + 2GB Videomax', nominal: '103.410', masaAktif: '30 hari' },
                    { nama: 'Data 25.000', detail: 'Kuota 300MB - 750MB + 1GB Videomax', nominal: '26.008', masaAktif: '30 hari' },
                    { nama: 'Data 4.5 GB', detail: 'Data 4.5 GB + 2GB Video', nominal: '70.319', masaAktif: '30 hari' },
                    { nama: 'Data 8 GB', detail: 'Data 8 GB + 2GB Video', nominal: '87.899', masaAktif: '30 hari' },
                    { nama: 'Data MAXstream Gala 30 GB', detail: '30GB MAXstream ', nominal: '152.788', masaAktif: '30 hari' },
                ]
            },
        ]

        this.setState({
            paket: data
        })
    }

    menu() {
        let dataMessage = this.state.dataMessage;

        let data = [
            {
                text: 'Assalamualaikum, Saya Fitri.',
                type: 'text',
            },
            {
                text: 'Sahabat HasanahKu ada perlu apa ?',
                type: 'text',
            },
            // {
            //     text: '',
            //     type: 'menu',
            // }
        ]

        // dataMessage.push(data);
        this.setState({ dataMessage: data, messageFromMe: '' });
        this.startVoice('Halo Saya fitri, Asisten Virtual Kamu. Apa yang bisa Saya bantu ?')
    }


    sendMessage(newMessage) {
        let dataMessage = this.state.dataMessage;

        let data = {
            text: newMessage,
            type: 'messageFromMe',
        };
        dataMessage.push(data);
        this.setState({ dataMessage: dataMessage, messageFromMe: '' });

        // this.getDataChat(newMessage);
        this.sendDataToBot(newMessage)
        // LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    }

    onEndEditingMessage(newMessage) {

    }

    giveLoading(newMessage) {
        let dataMessage = this.state.dataMessage;

        let data = {
            text: '...',
            type: 'loading',
        };
        dataMessage.push(data);
        this.setState({ dataMessage: dataMessage, messageFromMe: '' });
    }

    bubbleTouched(newMessage) {
        this.sendMessage(newMessage);
    }

    getDataChat(result) {
        let dataMessage = this.state.dataMessage;

        let resultBot = result.queryResult.fulfillmentMessages[1].payload.data

        console.log('result ', result);

        // let data = {
        //     text: resultBot.data.text,
        //     type: 'text',
        // };

        setTimeout(() => {

            dataMessage.pop();
            // this.setState({ dataQuickButton: response.result.quickbutton });
            resultBot.map(data => {
                dataMessage.push(data);
                if (data.type == 'text') {
                    this.startVoice(data.text)
                }
                this.setState({ dataMessage: dataMessage });
                // LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
            });

        }, 1000);
    }




    actionGrid(item) {
        this.sendMessage(item.defaultAction.data);
    }

    actionMovie(item) {
        if (item.type == 'postback') {
            this.sendMessage(item.data);
        } else if (item.type == 'uri') {
            Linking.openURL(item.uri);
        } else {
            const scheme = Platform.select({
                ios: 'maps:0,0?q=',
                android: 'geo:0,0?q=',
            });
            const latLng = `${'-6.202394'},${'106.652710'}`;
            const label = 'Lenna Maps';
            const url = Platform.select({
                ios: `${scheme}${label}@${latLng}`,
                android: `${scheme}${latLng}(${label})`,
            });

            Linking.openURL(url);
        }
    }

    actionCarousel(item) {
        if (item.type == 'postback') {
            this.sendMessage(item.data);
        } else if (item.type == 'uri') {
            Linking.openURL(item.uri);
        } else {
            const scheme = Platform.select({
                ios: 'maps:0,0?q=',
                android: 'geo:0,0?q=',
            });
            const latLng = `${'-6.202394'},${'106.652710'}`;
            const label = 'Lenna Maps';
            const url = Platform.select({
                ios: `${scheme}${label}@${latLng}`,
                android: `${scheme}${latLng}(${label})`,
            });

            Linking.openURL(url);
        }
    }

    actionMenu(menu) {
        this.props.navigation.navigate(menu)
    }

    actionPaket(paket) {
        let dataMessage = this.state.dataMessage;
        this.setState({ paketList: paket.dataNominal, imagePaketList: paket.image, titlePaketList: paket.type })
        let data = {
            text: '',
            type: 'paketList',
        };
        dataMessage.push(data);
    }

    actionPaketList(list) {
        this.RBSheet.open()
    }

    renderData = ({ item }) => {
        switch (item.type) {
            case 'text':
                return (
                    <View style={{ margin: 10 }}>
                        <View
                            style={{
                                alignSelf: 'flex-start',
                                padding: 12,
                                backgroundColor: '#f6f7f8',
                                borderRadius: 10,
                            }}>
                            <Text style={{ color: '#000', fontWeight: '500' }}>
                                {item.text}
                            </Text>
                        </View>
                    </View>
                );
            case 'loading':
                return (
                    <View style={{ margin: 10 }}>
                        <View
                            style={{
                                alignSelf: 'flex-start',
                                padding: 12,
                                width: 80,
                                backgroundColor: '#fff',
                                borderRadius: 10,
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                            {/* <Text style={{color: '#000', fontWeight: '500'}}>...</Text> */}
                            {/* <ActivityIndicator color="blue" /> */}
                            <LottieView
                                style={{ height: 20, transform: [{ scale: 1.5 }] }}
                                // autoSize={true}
                                resizeMode="cover"
                                source={require('../../../assets/animation/loading3.json')}
                                autoPlay
                            />
                        </View>
                    </View>
                );
            case 'messageFromMe':
                return (
                    <TouchableOpacity
                        onPress={() => this.bubbleTouched(item.text)}
                        activeOpacity={0.7}
                        style={{ margin: 10 }}>
                        <View
                            style={{
                                alignSelf: 'flex-end',
                                padding: 12,
                                backgroundColor: '#2a8b40',
                                borderRadius: 10,
                            }}>
                            <Text style={{ color: '#fff', fontWeight: '500' }}>
                                {item.text}
                            </Text>
                            <Text style={{ color: '#fff', fontSize: 10, textAlign: 'right' }}>
                                {moment().format('HH:mm')}
                            </Text>
                        </View>
                    </TouchableOpacity>
                );
            case 'image':
                return (
                    <View
                        style={{
                            margin: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        <View
                            style={{
                                height: 300,
                                width: Dimensions.get('window').width - 30,
                                borderRadius: 10,
                            }}>
                            <Image
                                style={{
                                    height: 300,
                                    width: Dimensions.get('window').width - 30,
                                    resizeMode: 'stretch',
                                    borderRadius: 10,
                                }}
                                source={{ uri: item.originalContentUrl }}
                            />
                        </View>
                    </View>
                );
            case 'html':
                return (
                    <ScrollView
                        nestedScrollEnabled={true}
                        contentContainerStyle={{
                            margin: 10,
                            height: Dimensions.get('window').height / 2 + 10,
                            flexGrow: 1,
                        }}>
                        <WebView
                            originWhitelist={['*']}
                            style={{
                                flex: 1,
                                height: Dimensions.get('window').height / 2 + 10,
                            }}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            source={{ html: item.html }}
                            startInLoadingState={true}
                            scalesPageToFit={true}
                        />
                    </ScrollView>
                );
            case 'carousel':
                return (
                    <View style={{ margin: 10 }}>
                        <ScrollView
                            horizontal={true}
                            style={{ overflow: 'visible' }}
                            keyboardShouldPersistTaps="handled"
                            showsHorizontalScrollIndicator={false}>
                            {this.renderCarousel(item)}
                        </ScrollView>
                    </View>
                );
            case 'menu':
                return (
                    <View style={{ margin: 10 }}>
                        <ScrollView
                            keyboardShouldPersistTaps={'always'}
                            horizontal={true}
                            style={{ overflow: 'visible' }}
                            keyboardShouldPersistTaps="handled"
                            showsHorizontalScrollIndicator={false}>
                            {this.renderMenu(this.state.menu)}
                        </ScrollView>
                    </View>
                );
            case 'list':
                return (
                    <View
                        style={{
                            margin: 10,
                            // height: Dimensions.get('window').height / 2 + 50,
                            borderWidth: 0.5,
                            borderColor: '#dedede',
                            borderRadius: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        <Image
                            source={{ uri: item.imageUrl }}
                            style={{
                                width: 150,
                                height: 100,
                                resizeMode: 'stretch',
                                margin: 10,
                            }}
                        />
                        <Text style={{ fontWeight: '500', marginBottom: 10 }}>
                            {item.subTitle}
                        </Text>
                        <ScrollView
                            nestedScrollEnabled={true}
                            keyboardShouldPersistTaps="handled"
                            showsHorizontalScrollIndicator={false}>
                            {this.renderList(item)}
                        </ScrollView>
                    </View>
                );
            case 'paketList':
                return (
                    <View
                        style={{
                            margin: 10,
                            // height: Dimensions.get('window').height / 2 + 50,
                            borderWidth: 0.5,
                            borderColor: '#dedede',
                            borderRadius: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        <Image
                            source={this.state.imagePaketList}
                            style={{
                                width: 150,
                                height: 100,
                                resizeMode: 'stretch',
                                margin: 10,
                            }}
                        />
                        <Text style={{ fontWeight: '500', marginBottom: 10 }}>
                            {this.state.titlePaketList}
                        </Text>
                        <ScrollView
                            nestedScrollEnabled={true}
                            keyboardShouldPersistTaps="handled"
                            showsHorizontalScrollIndicator={false}>
                            {this.renderPaketList(this.state.paketList)}
                        </ScrollView>
                    </View>
                );
            case 'movie':
                return (
                    <View style={{ margin: 10 }}>
                        <ScrollView
                            horizontal={true}
                            style={{ overflow: 'visible' }}
                            keyboardShouldPersistTaps="handled"
                            showsHorizontalScrollIndicator={false}>
                            {this.renderMovie(item)}
                        </ScrollView>
                    </View>
                );
            case 'summary':
                return (
                    <View
                        style={{
                            margin: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#fff',
                            borderWidth: 0.5,
                            borderColor: '#dedede',
                            borderRadius: 10,
                        }}>
                        <Text
                            style={{
                                textAlign: 'center',
                                color: '#0087d3',
                                fontSize: 16,
                                fontWeight: 'bold',
                                margin: 10,
                            }}>
                            {item.title}
                        </Text>
                        {/* <View style={{width: 150, height: 100}}> */}
                        <Image
                            style={{ height: 150, width: 100, resizeMode: 'stretch' }}
                            source={{ uri: item.imageUrl }}
                        />
                        {/* </View> */}
                        {this.renderSummary(item)}
                    </View>
                );
            case 'weather':
                return (
                    <View
                        style={{
                            margin: 10,
                            backgroundColor: '#0087d3',
                            borderRadius: 10,
                            padding: 10,
                        }}>
                        <Text style={{ color: '#fff', fontSize: 20 }}>
                            {item.area + ', ' + item.countryCode}
                        </Text>
                        <Text style={{ color: '#fff', fontSize: 14, marginTop: 5 }}>
                            {item.columns[0].longDate}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                marginLeft: 10,
                                marginRight: 10,
                                marginTop: 10,
                                marginBottom: 0,
                            }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text
                                    style={{
                                        color: '#ffe700',
                                        fontWeight: '700',
                                        textAlign: 'center',
                                        paddingLeft: 20,
                                    }}>
                                    Siang
                    </Text>
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text
                                    style={{
                                        color: '#ffe700',
                                        fontWeight: '700',
                                        textAlign: 'center',
                                        paddingRight: 10,
                                    }}>
                                    Suhu
                    </Text>
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text
                                    style={{
                                        color: '#ffe700',
                                        fontWeight: '700',
                                        textAlign: 'center',
                                        paddingRight: 20,
                                    }}>
                                    Siang
                    </Text>
                            </View>
                        </View>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                margin: 10,
                            }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image
                                    style={{ height: 80, width: 80, resizeMode: 'stretch' }}
                                    source={{ uri: item.columns[0].dayIconUrl }}
                                />
                                <Text style={{ color: '#fff', fontSize: 15, marginTop: 5 }}>
                                    {item.columns[0].dayWeather}
                                </Text>
                            </View>

                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 40, color: '#fff' }}>
                                    {item.columns[0].temperature + '˚'}
                                </Text>
                                <Text style={{ fontSize: 15, color: '#fff', marginTop: 10 }}>
                                    Min {item.columns[0].minTemperature + '˚'}
                                </Text>
                                <Text style={{ fontSize: 15, color: '#fff', marginTop: 10 }}>
                                    Max {item.columns[0].maxTemperature + '˚'}
                                </Text>
                            </View>

                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image
                                    style={{ height: 80, width: 80, resizeMode: 'stretch' }}
                                    source={{ uri: item.columns[0].nightIconUrl }}
                                />
                                <Text style={{ color: '#fff', fontSize: 15, marginTop: 5 }}>
                                    {item.columns[0].nightWeather}
                                </Text>
                            </View>
                        </View>
                    </View>
                );
            case 'grid':
                return (
                    <View
                        style={{
                            margin: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#fff',
                            borderWidth: 0.5,
                            borderColor: '#dedede',
                            borderRadius: 10,
                        }}>
                        {/* <View style={{width: 100, height: 100}}> */}
                        <Image
                            style={{
                                height: 100,
                                width: 180,
                                resizeMode: 'stretch',
                                margin: 10,
                            }}
                            source={{ uri: item.imageUrl }}
                        />
                        {/* </View> */}
                        <View style={{ marginTop: 10, marginBottom: 10 }}>
                            <FlatList
                                data={item.columns}
                                extraData={item.columns}
                                renderItem={this.renderGrid}
                                keyExtractor={(item, index) => index.toString()}
                                numColumns={3}
                            />
                        </View>
                    </View>
                );
            case 'paket':
                return (
                    <View style={{ margin: 10 }}>
                        <ScrollView
                            horizontal={true}
                            style={{ overflow: 'visible' }}
                            keyboardShouldPersistTaps="handled"
                            showsHorizontalScrollIndicator={false}>
                            {this.renderPaket(this.state.paket)}
                        </ScrollView>
                    </View>
                )
        }
    };

    renderSummary(item) {
        return item.columns.map((dataSummary, index) => {
            return (
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        // alignItems: 'center',
                        width: Dimensions.get('window').width - 50,
                        margin: 10,
                        // backgroundColor: '#fff',
                    }}>
                    <Text style={{ textAlign: 'left', color: '#4a4a4a', flex: 1 }}>
                        {dataSummary.field}
                    </Text>
                    <Text
                        style={{
                            textAlign: 'right',
                            fontWeight: 'bold',
                            color: '#4a4a4a',
                            flex: 1,
                        }}>
                        {dataSummary.value}
                    </Text>
                </View>
            );
        });
    }

    renderGrid = ({ item }) => {
        console.log('data grid', item);

        return (
            <Button
                onPress={() => this.actionGrid(item)}
                style={{
                    flexDirection: 'column',
                    margin: 5,
                    height: Dimensions.get('window').width / 3 - 50,
                    width: Dimensions.get('window').width / 3 - 20,
                    backgroundColor: '#fff',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                <Text style={{ fontWeight: '700', color: '#000', fontSize: 14 }}>
                    {item.text}
                </Text>
                <Text style={{ color: '#000', fontSize: 10, marginTop: 5 }}>
                    {item.subText}
                </Text>
            </Button>
        );
    };

    renderCarousel(item) {
        return item.columns.map((dataCarousel, index) => {
            return (
                <Card
                    style={{
                        borderRadius: 10,
                        width: 250,
                    }}>
                    <View style={{ height: 150, width: 250 }}>
                        <Image
                            style={{
                                height: 150,
                                width: 250,
                                resizeMode: 'cover',
                                borderTopRightRadius: 10,
                                borderTopLeftRadius: 10,
                            }}
                            source={{ uri: dataCarousel.thumbnailImageUrl }}
                        />
                    </View>

                    <Text
                        key={index}
                        style={{ maxWidth: 250, margin: 10, fontWeight: 'bold' }}>
                        {dataCarousel.title.split('<br>')}
                    </Text>

                    <Text key={index + 1} style={{ maxWidth: 250, margin: 10 }}>
                        {dataCarousel.text.split('<br>')}
                    </Text>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        {dataCarousel.actions.map((dataActions, index) => {
                            return (
                                <Button
                                    onPress={() => this.actionCarousel(dataActions)}
                                    transparent
                                    style={{ height: 30 }}>
                                    <Text
                                        key={index.toString()}
                                        style={{
                                            color: '#2a8b40',
                                            fontWeight: 'bold',
                                        }}>
                                        {dataActions.label}
                                    </Text>
                                </Button>
                            );
                        })}
                    </View>
                </Card>
            );
        });
    }

    renderMenu(item) {
        return item.map((dataCarousel, index) => {
            return (
                <Card
                    style={{
                        borderRadius: 10,
                        width: 250,
                    }}>
                    <View style={{ height: 150, width: 250 }}>
                        <Image
                            style={{
                                height: 150,
                                width: 250,
                                resizeMode: 'contain',
                                borderTopRightRadius: 10,
                                borderTopLeftRadius: 10,
                            }}
                            source={dataCarousel.image}
                        />
                    </View>

                    <Text
                        key={index}
                        style={{ maxWidth: 250, margin: 10, fontWeight: 'bold' }}>
                        {dataCarousel.title.split('<br>')}
                    </Text>

                    <Text key={index + 1} style={{ maxWidth: 250, margin: 10 }}>
                        {dataCarousel.description.split('<br>')}
                    </Text>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        {/* {dataCarousel.actions.map((dataActions, index) => { */}
                        {/* return ( */}
                        <Button
                            onPress={() => this.actionMenu(dataCarousel.menu)}
                            transparent
                            style={{ height: 30 }}>
                            <Text
                                key={index.toString()}
                                style={{
                                    color: '#2a8b40',
                                    fontWeight: 'bold',
                                }}>
                                {dataCarousel.menu}
                            </Text>
                        </Button>
                        {/* ); */}
                        {/* })} */}
                    </View>
                </Card>
            );
        });
    }

    renderPaket(item) {
        return item.map((dataCarousel, index) => {
            return (
                <Card
                    style={{
                        borderRadius: 10,
                        width: 250,
                    }}>
                    <View style={{ height: 150, width: 250 }}>
                        <Image
                            style={{
                                height: 150,
                                width: 250,
                                resizeMode: 'contain',
                                borderTopRightRadius: 10,
                                borderTopLeftRadius: 10,
                            }}
                            source={dataCarousel.image}
                        />
                    </View>

                    <Text
                        key={index}
                        style={{ maxWidth: 250, margin: 10, fontWeight: 'bold' }}>
                        {dataCarousel.type.split('<br>')}
                    </Text>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        {/* {dataCarousel.actions.map((dataActions, index) => { */}
                        {/* return ( */}
                        <Button
                            onPress={() => this.actionPaket(dataCarousel)}
                            transparent
                            style={{ height: 30 }}>
                            <Text
                                key={index.toString()}
                                style={{
                                    color: '#2a8b40',
                                    fontWeight: 'bold',
                                }}>
                                Cek Paket {dataCarousel.type}
                            </Text>
                        </Button>
                        {/* ); */}
                        {/* })} */}
                    </View>
                </Card>
            );
        });
    }

    renderList(item) {
        return item.columns.map((dataList, index) => {
            return (
                <Card
                    key={index}
                    style={{
                        borderRadius: 10,
                    }}>
                    <Button
                        transparent
                        onPress={() => this.sendMessage(dataList.defaultAction.data)}
                        style={{
                            flexDirection: 'column',
                            padding: 10,
                            height: 80,
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                        }}>
                        <Text style={{ fontWeight: 'bold', textAlign: 'left', flex: 1 }}>
                            {dataList.text}
                        </Text>
                        <Text style={{ textAlign: 'left' }}>{dataList.subText}</Text>
                    </Button>
                </Card>
            );
        });
    }

    renderPaketList(item) {
        return item.map((dataList, index) => {
            return (
                <Card
                    key={index}
                    style={{
                        borderRadius: 10,
                    }}>
                    <Button
                        transparent
                        onPress={() => this.actionPaketList(dataList)}
                        style={{
                            flexDirection: 'column',
                            padding: 10,
                            height: 100,
                            width: Dimensions.get('window').width - 50,
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                        }}>
                        <Text style={{ fontWeight: 'bold', textAlign: 'left', flex: 1 }}>
                            {dataList.nama}
                        </Text>
                        <Text style={{ textAlign: 'left' }}>{dataList.detail}</Text>
                        <Text style={{ textAlign: 'left', paddingTop: 5 }}>{dataList.nominal}</Text>
                        <Text style={{ textAlign: 'left', paddingTop: 5 }}>{dataList.masaAktif}</Text>
                    </Button>
                </Card>
            );
        });
    }

    renderMovie(item) {
        return item.columns.map((dataMovie, index) => {
            return (
                <Card
                    style={{
                        borderRadius: 10,
                        width: 250,
                    }}>
                    <View style={{ height: 200, width: 250 }}>
                        <Image
                            style={{
                                height: '100%',
                                width: 250,
                                resizeMode: 'cover',
                                borderTopRightRadius: 10,
                                borderTopLeftRadius: 10,
                            }}
                            source={{ uri: dataMovie.thumbnailImageUrl }}
                        />
                    </View>

                    <Text
                        key={index}
                        style={{ maxWidth: 250, margin: 10, fontWeight: 'bold' }}>
                        {dataMovie.title}
                    </Text>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        {dataMovie.actions.map((dataActions, index) => {
                            return (
                                <Button
                                    onPress={() => this.actionMovie(dataActions)}
                                    transparent
                                    style={{ height: 30 }}>
                                    <Text
                                        key={index.toString()}
                                        style={{
                                            color: '#2a8b40',
                                            fontWeight: 'bold',
                                        }}>
                                        {dataActions.label}
                                    </Text>
                                </Button>
                            );
                        })}
                    </View>
                </Card>
            );
        });
    }

    renderQuickButton = ({ item }) => {
        if (this.state.dataQuickButton.length == 0) {
            return null;
        } else {
            return (
                <Button
                    transparent
                    onPress={() => this.sendMessage(item)}
                    style={{
                        margin: 10,
                        borderWidth: 0.5,
                        borderColor: '#dedede',
                        borderRadius: 20,
                        minWidth: 50,
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: 40,
                    }}>
                    <View>
                        <Text
                            style={{
                                paddingTop: 5,
                                paddingBottom: 8,
                                paddingLeft: 12,
                                paddingRight: 12,
                            }}>
                            {item}
                        </Text>
                    </View>
                </Button>
            );
        }
    };

    render() {
        const offset = (Platform.OS === 'android') ? -200 : 0
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    bounces={false}
                    // FlatList reference to scroll it when necessary
                    ref={ref => this.scroller = ref}
                    // get data from props
                    data={this.state.dataMessage}
                    // render every message from data with <Bubble />
                    renderItem={this.renderData}
                    // keep trace of scroll position
                    // onScroll={e => this.updateScrollPosition(e)}
                    // // keep trace of layout height
                    // onLayout={e => this.updateLayoutHeight(e)}
                    // keep trace of contentSize
                    keyExtractor={(item, key) => key}
                    onContentSizeChange={(w, h) => this.updateContentSize(w, h)}
                    // style={{marginBottom:!isKeyboardShow?92.5:2.5,height:this.state.flatListHeight}} old style
                    style={{ marginBottom: 10, height: this.state.flatListHeight }}
                />
                <KeyboardAvoidingView keyboardVerticalOffset={offset} behavior="padding">
                    <View style={styles.footerRow}>
                        <Input
                            style={styles.input}
                            value={this.state.messageFromMe}
                            onFocus={() => this.setState({ onFocusChat: true })}
                            onEndEditing={() => this.setState({ onFocusChat: false })}
                            onChangeText={message => this.setState({ messageFromMe: message })}
                            placeholder="Enter your message"
                            autoCorrect={false}
                        />
                        <Button
                            transparent
                            style={{ marginRight: 10 }}
                            onPress={() => this.state.onFocusChat == true ? this.sendMessage(this.state.messageFromMe) : this.sendMessage(this.state.messageFromMe)}>
                            <Icon name={this.state.onFocusChat == true && this.state.messageFromMe != '' ? "send" : "send"} size={35} color={this.state.is_listening == false ? "#2a8b40" : "red"} />
                        </Button>
                    </View>
                </KeyboardAvoidingView>
            </View>
            // <Container>
            //     <KeyboardAvoidingView behavior="padding" style={{
            //         flex: 1,
            //     }}>
            //         {/* <View style={{ flex: 2 }}> */}
            //         <ScrollView
            //             nestedScrollEnabled={true}
            //             keyboardShouldPersistTaps={'always'}
            //             ref={ref => (this.scrollView = ref)}
            //             onContentSizeChange={(contentWidth, contentHeight) => {
            //                 this.scrollView.scrollToEnd({ animated: true });
            //             }}
            //             style={{ marginTop: 5 }}
            //             contentContainerStyle={styles.container}
            //         >
            //             <FlatList
            //                 style={styles.list}
            //                 data={this.state.dataMessage}
            //                 ref={ref => (this.scroller = ref)}
            //                 // getItemLayout={this.getItemLayout}
            //                 // onContentSizeChange={() =>
            //                 //   this.scroller.scrollToEnd({animated: true})
            //                 // }
            //                 // onLayout={() => this.scroller.scrollToEnd({animated: true})}
            //                 extraData={this.state}
            //                 renderItem={this.renderData}
            //                 keyExtractor={item => item.id}
            //                 showsVerticalScrollIndicator={false}
            //             />

            //             <View>
            //                 <FlatList
            //                     style={styles.quickbuttonContainer}
            //                     data={this.state.dataQuickButton}
            //                     extraData={this.state}
            //                     horizontal
            //                     renderItem={this.renderQuickButton}
            //                     keyExtractor={item => item.id}
            //                     showsHorizontalScrollIndicator={false}
            //                 />
            //                 <View style={styles.footerRow}>
            //                     <Input
            //                         style={styles.input}
            //                         value={this.state.messageFromMe}
            //                         onFocus={() => this.setState({ onFocusChat: true })}
            //                         onEndEditing={() => this.setState({ onFocusChat: false })}
            //                         onChangeText={message => this.setState({ messageFromMe: message })}
            //                         placeholder="Enter your message"
            //                         autoCorrect={false}
            //                     />
            //                     <Button
            //                         transparent
            //                         style={{ marginRight: 10 }}
            //                         onPress={() => this.state.onFocusChat == true ? this.sendMessage(this.state.messageFromMe) : this.listen()}>
            //                         <Icon name={this.state.onFocusChat == true && this.state.messageFromMe != '' ? "send" : "mic"} size={35} color={this.state.is_listening == false ? "#2a8b40" : "red"} />
            //                     </Button>
            //                 </View>
            //             </View>
            //         </ScrollView>
            //         {/* </View> */}

            //     </KeyboardAvoidingView>
            // </Container>
            // <Container style={styles.container}>
            //     <Header
            //         transparent
            //         androidStatusBarColor="#2a8b40"
            //         style={styles.header}
            //     >
            //         <Left style={{ flex: 1 }}>
            //             <Button transparent onPress={() => this.props.navigation.goBack()}>
            //                 <Icon name='arrowleft' size={35} type='antdesign' />
            //             </Button>
            //         </Left>
            //         <Body
            //             style={{
            //                 justifyContent: 'center',
            //                 alignItems: 'center',
            //                 flex: 1,
            //             }}>
            //             <Text style={styles.title}>HasanahKu Bot</Text>
            //         </Body>
            //         <Right style={{ flex: 1 }}>
            //             <Button transparent>
            //                 <Image
            //                     style={styles.profileImage}
            //                     source={require('../../../assets/image/man.png')}
            //                 />
            //             </Button>
            //         </Right>
            //     </Header>
            //     <Content contentContainerStyle={{ flexGrow: 1 }}>
            //         <ScrollView
            //             nestedScrollEnabled={true}
            //             keyboardShouldPersistTaps={'always'}
            //             ref={ref => (this.scrollView = ref)}
            //             onContentSizeChange={(contentWidth, contentHeight) => {
            //                 this.scrollView.scrollToEnd({ animated: true });
            //             }}
            //             style={{ marginTop: 5 }}
            //             contentContainerStyle={styles.container}
            //         >
            //             <FlatList
            //                 style={styles.list}
            //                 data={this.state.dataMessage}
            //                 ref={ref => (this.scroller = ref)}
            //                 // getItemLayout={this.getItemLayout}
            //                 // onContentSizeChange={() =>
            //                 //   this.scroller.scrollToEnd({animated: true})
            //                 // }
            //                 // onLayout={() => this.scroller.scrollToEnd({animated: true})}
            //                 extraData={this.state}
            //                 renderItem={this.renderData}
            //                 keyExtractor={item => item.id}
            //                 showsVerticalScrollIndicator={false}
            //             />
            //         </ScrollView>

            //         <KeyboardAvoidingView>
            //             <FlatList
            //                 style={styles.quickbuttonContainer}
            //                 data={this.state.dataQuickButton}
            //                 extraData={this.state}
            //                 horizontal
            //                 renderItem={this.renderQuickButton}
            //                 keyExtractor={item => item.id}
            //                 showsHorizontalScrollIndicator={false}
            //             />
            //             <View style={styles.footerRow}>
            //                 <Input
            //                     style={styles.input}
            //                     value={this.state.messageFromMe}
            //                     onFocus={() => this.setState({ onFocusChat: true })}
            //                     onEndEditing={() => this.setState({ onFocusChat: false })}
            //                     onChangeText={message => this.setState({ messageFromMe: message })}
            //                     placeholder="Enter your message"
            //                     autoCorrect={false}
            //                 />
            //                 <Button
            //                     transparent
            //                     style={{ marginRight: 10 }}
            //                     onPress={() => this.state.onFocusChat == true ? this.sendMessage(this.state.messageFromMe) : this.listen()}>
            //                     <Icon name={this.state.onFocusChat == true && this.state.messageFromMe != '' ? "send" : "mic"} size={35} color={this.state.is_listening == false ? "#2a8b40" : "red"} />
            //                 </Button>
            //             </View>
            //         </KeyboardAvoidingView>

            //     </Content>

            // <RBSheet
            //     ref={ref => {
            //         this.RBSheet = ref;
            //     }}
            //     closeOnDragDown
            //     height={Platform.OS == 'android' ? 220 : 300}
            //     // animationType="fade"
            //     duration={300}
            //     customStyles={{
            //         container: {
            //             borderTopRightRadius: 15,
            //             borderTopLeftRadius: 15,
            //         },
            //     }}>

            //     <View >
            //         <Text>aa</Text>
            //     </View>

            // </RBSheet>

            // </Container>
            // </View >
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#fff',
    },
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 10
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 40,
        // flex: 1
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#2a8b40'
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    profileImage: {
        height: 40,
        width: 40,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    container: {
        flex: 1,
        // backgroundColor: '#000',
    },
    footer: {
        height: 20,
        backgroundColor: '#fff',
    },
    list: {
        // height: Dimensions.get('window').height - 250,
        backgroundColor: '#fff',
        flex: 2,
    },
    quickbuttonContainer: {
        backgroundColor: '#fff',
    },
    input: {
        minHeight: 40, //tadinya 40
        fontSize: 13,
        width: 280,
        paddingHorizontal: 0,
        alignSelf: 'center',
        margin: 10,
        height: 20,
        borderRadius: 50,
        paddingVertical: 7,
        paddingLeft: 5,
        backgroundColor: '#fff',
    },
    footerRow: {
        flexDirection: 'row',
        backgroundColor: '#f6f7f8',
        minHeight: 40,
        height: 60,
        width: '100%',
        alignItems: 'center',
    },
});

export default Chat;
