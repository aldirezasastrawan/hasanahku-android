import React, { Component } from 'react';
import { View, Text, StyleSheet, Platform, FlatList, ScrollView, TouchableOpacity, Image, Dimensions } from 'react-native';
import { Container, Content, Button, Input, Form, Item, Label } from 'native-base';
import { Icon } from 'react-native-elements';
import RBSheet from 'react-native-raw-bottom-sheet';
import { getValue } from '../../../module/LocalData/Storage';
import { post, postWithToken, get} from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {Error, ResponseCode} from '../../../supports/Constants';
import {LogMe} from '../../../supports/LogMe';
import ImagePicker from 'react-native-image-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import ImageResizer from 'react-native-image-resizer';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';


const options = {
    title: 'Select Avatar',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};

const size = Dimensions.get('window')

class UpgradeAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataIdentity: [],
            selectedIdentity: 'KTP',
            showSpinner: false,
            filepath: {
                data: '',
                uri: ''
            },
            inputs: {
              identityNumber: {
                type: 'nik',
                value: ''
              }
            },
            fileData: '',
            fileDataSelfie: '',
            fileUri: '',
            hasSuccessUpgrade: false
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
    }

    componentDidMount() {
        this.getListIdentity()
    }

    openModalIdentity() {
        this.RBSheet.open()
    }

    getListIdentity() {
      get(Env.base_url + Api.getIdentity, null).then(response => {
          if(response.code == ResponseCode.OK){
              this.setState({
                  dataIdentity: response.okContent.identities,
              })
          } else {
            dialog.alertFail(response.failContent, this);
          }
      }).catch(err => {
        });
    }

    launchCamera = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images'
            },
        };
        ImagePicker.launchCamera(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                ImageResizer.createResizedImage(`data:image/jpeg;base64,${response.data}`, 800, 600, 'JPEG', 80).then(dataBase64 => {

                    var RNFS = require('react-native-fs');

                    RNFS.readFile(
                        dataBase64.path,
                        "base64"
                    ).then(base64Data => {
                        console.log("response database 64 ", base64Data);

                        const source = { uri: response.uri };
                        console.log('response', JSON.stringify(response));
                        this.setState({
                            filePath: response,
                            fileData: base64Data,
                            fileUri: response.uri
                        });
                    })
                }).catch(err => alert(JSON.stringify(err)))
            }
        });
    }

    launchCameraSelfie = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, (response) => {
          //  console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                ImageResizer.createResizedImage(`data:image/jpeg;base64,${response.data}`, 800, 600, 'JPEG', 80).then(dataBase64 => {

                    var RNFS = require('react-native-fs');

                    RNFS.readFile(
                        dataBase64.path,
                        "base64"
                    ).then(base64Data => {
                        console.log("response database 64 ", base64Data);

                        const source = { uri: response.uri };
                        console.log('response', JSON.stringify(response));
                        this.setState({
                            filePath: response,
                            fileDataSelfie: base64Data,
                            fileUri: response.uri
                        });
                    })
                }).catch(err => alert(JSON.stringify(err)))
            }
        });
    }

    renderFileData() {
        if (this.state.fileData) {
            return <Image source={{ uri: 'data:image/jpeg;base64,' + this.state.fileData }}
                style={styles.images}
            />
        } else {
            return <Image source={require('../../../assets/image/dummy.png')}
                style={styles.images}
            />
        }
    }

    renderFileDataSelfie() {
        if (this.state.fileDataSelfie) {
            return <Image source={{ uri: 'data:image/jpeg;base64,' + this.state.fileDataSelfie }}
                style={styles.images}
            />
        } else {
            return <Image source={require('../../../assets/image/dummy.png')}
                style={styles.images}
            />
        }
    }

    renderFileUri() {
        if (this.state.fileUri) {
            return <Image
                source={{ uri: this.state.fileUri }}
                style={styles.images}
            />
        } else {
            return <Image
                source={require('../../../assets/image/galeryImages.jpg')}
                style={styles.images}
            />
        }
    }

    selectIdentity(id) {
        this.setState({
            selectedIdentity: id
        })

        this.RBSheet.close()
    }

    upgrade() {

      if(this.isValidate() === false) return;

      this.setState({showSpinner: true});

        getValue('tokenLogin').then(tokenLogin => {
            let params = {
                "phone": this.props.navigation.getParam('phone'),
                "identityId": this.state.selectedIdentity,
                "identityNumber" : this.state.inputs.identityNumber.value,
                "identityPhoto": this.state.fileData,
                "identitySwafoto": this.state.fileData
            }

            console.log("params :: "+JSON.stringify(params))

            postWithToken(Env.base_url + Api.upgradeAccount, params, tokenLogin).then(response => {

                if(response.code == ResponseCode.OK){
                    setTimeout(() => {
                        this.setState({
                            hasSuccessUpgrade: true,
                            showSpinner: false
                        })

                        dialog.alertCallback(response.okContent.description, () => {
                          this.props.navigation.goBack()
                        });
                    }, 1000);
                } else {
                  this.setState({showSpinner: false})
                  dialog.alertFail(response.failContent, this);
                }
            }).catch(err => {
              this.setState({showSpinner: false})
              dialog.alertException(err);
        });
      })
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>Upgrade Akun</Text>
                        </View>
                    </View>

                    <View style={styles.identityContainer}>
                        <Text style={styles.jenisIdentity}>
                            Jenis Identitas
                        </Text>

                        <View style={styles.IDSelectedContainer}>
                            <Text style={styles.selectedIdentity}>{this.state.selectedIdentity}</Text>
                            <Button style={styles.btnOpenModal} onPress={() => this.openModalIdentity()} transparent>
                                <Icon name name="ellipsis-v" type="font-awesome" />
                            </Button>
                        </View>
                    </View>

                    <View style={styles.identityNUmberContainer}>
                        <Form>
                            <Item stackedLabel last>
                                <Label style={styles.labelNIK}>NIK</Label>
                                <Input style={styles.input} maxLength={16} value={this.state.inputs.identityNumber.value} onChangeText={value => this.onInputChange({id: 'identityNumber', value})} />
                            </Item>
                            {this.validateError('identityNumber')}
                        </Form>
                    </View>
                    <View style={styles.identytyPhoto}>

                        {this.renderFileData()}
                        <Button style={styles.btnTakePhoto} onPress={this.launchCamera}>
                            <Text style={styles.txtUpgrade}>Ambil Foto {this.state.selectedIdentity}</Text>
                        </Button>
                    </View>

                    <View style={styles.identytySelfiePhoto}>

                        {this.renderFileDataSelfie()}
                        <Button style={styles.btnTakePhoto} onPress={this.launchCameraSelfie}>
                            <Text style={styles.txtUpgrade}>Selfie dengan {this.state.selectedIdentity}</Text>
                        </Button>
                    </View>

                    {this.state.fileData && this.state.fileUri ?
                        <View>
                            <Button onPress={() => this.upgrade()} style={styles.btnUpgrade}>
                                <Text style={styles.txtUpgrade}>Upgrade</Text>
                            </Button>
                        </View>
                        :
                        null
                    }

                    <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown
                        height={Platform.OS == 'android' ? size.height / 2 : size.height / 2 + 100}
                        // animationType="fade"
                        duration={300}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 15,
                                borderTopLeftRadius: 15,
                            },
                        }}>
                        <View>
                            <View style={styles.sheetHeader}>
                                <Text style={styles.sheetTitle}>
                                    Ganti Jenis Identitas
                                </Text>
                                <Text style={styles.sheetSubTitle}>
                                    Silahkan pilih jenis identitas di bawah ini
                                </Text>
                            </View>

                            <ScrollView>
                                <FlatList
                                    style={{ margin: 20 }}
                                    data={this.state.dataIdentity}
                                    renderItem={({ item }) => (
                                        <TouchableOpacity onPress={() => this.selectIdentity(item.id)} style={styles.listIdentityContainer}>
                                            <Text style={styles.identityDescription}>{item.description}</Text>
                                        </TouchableOpacity>
                                    )}
                                    extraData={this.state}
                                />
                            </ScrollView>

                        </View>
                    </RBSheet>

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />

                </Content>
            </Container>
        );
    }
}

// const size = Dimensions.get('window')

const styles = StyleSheet.create({
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 26
    },
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    identityContainer: {
        margin: 20
    },
    identityNUmberContainer: {
        marginTop: 0,
        marginLeft: 20,
        marginRight: 20,
        marginBottom : 20
    },
    jenisIdentity: {
        fontSize: 14
    },
    identityNumber: {
        fontSize: 14
    },
    IDSelectedContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    selectedIdentity: {
        flex: 2,
        fontWeight: 'bold',
        fontSize: 21
    },
    btnOpenModal: {
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    scrollView: {
        backgroundColor: '#fff',
    },

    body: {
        backgroundColor: "#fff",
        justifyContent: 'center',
        borderColor: 'black',
        borderWidth: 1,
        height: Dimensions.get('screen').height - 20,
        width: Dimensions.get('screen').width
    },
    ImageSections: {
        display: 'flex',
        flexDirection: 'row',
        paddingHorizontal: 8,
        paddingVertical: 8,
        justifyContent: 'center'
    },
    images: {
        width: size.width - 40,
        height: size.height / 4,
        borderColor: 'black',
        borderWidth: 1,
        marginHorizontal: 3
    },
    btnParentSection: {
        alignItems: 'center',
        marginTop: 10
    },
    btnSection: {
        width: 225,
        height: 50,
        backgroundColor: '#DCDCDC',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 3,
        marginBottom: 10
    },
    btnText: {
        textAlign: 'center',
        color: 'gray',
        fontSize: 14,
        fontWeight: 'bold'
    },
    listIdentityContainer: {
        height: 50,
        backgroundColor: '#dedede',
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    sheetTitle: {
        fontWeight: 'bold',
        fontSize: 24,
        paddingLeft: 20
    },
    sheetSubTitle: {
        paddingLeft: 20,
        paddingTop: 10,

    },
    identityDescription: {
        fontWeight: 'bold'
    },
    btnUpgrade: {
        backgroundColor: '#26C165',
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtUpgrade: {
        fontWeight: 'bold',
        color: '#fff'
    },
    identytyPhoto: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    identytySelfiePhoto: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnTakePhoto: {
        backgroundColor: '#26C165',
        margin: 5,
        width: 200,
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        fontSize: 14,
        fontWeight: 'bold',
        textTransform: 'lowercase'
    },
    labelNIK: {
        fontSize: 14,
        marginLeft: -10
    },
})

export default UpgradeAccount;
