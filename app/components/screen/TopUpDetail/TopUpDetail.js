import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import LinearGradient from 'react-native-linear-gradient';


class TopUpDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasHubungiKami: false,
            showSpinner: false,
            colorSpinner: '#e35200',
            detail: {},
        };
    }

    componentDidMount() {
        this.getDataTopUp()
    }

    getDataTopUp() {
        let data = this.props.navigation.getParam('item')

        this.setState({
            detail: data
        })
    }

    renderDetail = ({ item }) => {
        return (
            <View onPress={() => this.selectType(item)} style={styles.ListContainer}>
                <Text style={styles.pageTxt}>{item.step}.</Text>
                <Text style={styles.txtGrid}>{item.nama}</Text>
            </View>
        )
    }

    render() {
        const title = this.state.detail.type
        return (
            <Container>
                <Content>
                    <LinearGradient locations={[0, 1.0]} colors={['#2a8b40', '#1e622d']} style={styles.header}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color='#fff' size={30} type='antdesign' />
                        </Button>
                        <Text style={styles.title}>Top Up Via {title}</Text>
                    </LinearGradient>

                    <View>
                        <View style={styles.formContainer}>
                            <Text style={styles.infoFormTitle}>Top Up Hasanahku via {title}</Text>
                            <Text style={styles.infoFormSubTitle}>Silahkan ikuti langkah - langkah berikut:</Text>
                        </View>

                        <View style={styles.inputContainer}>
                            <FlatList
                                data={this.state.detail.detail}
                                renderItem={this.renderDetail}
                                extraData={this.state}
                            />
                        </View>

                        <View style={styles.btnRegisterContainer}>
                            <Button onPress={() => this.props.navigation.goBack()} style={styles.btnRegister}>
                                <Text style={styles.RegisterText}>Kembali</Text>
                            </Button>
                        </View>


                    </View>

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    header: {
        backgroundColor: '#e35200',
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    title: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16,
        flex: 2,
        marginLeft: -20
    },
    infoContainer: {
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    infoTitle: {
        color: '#e35200',
        fontSize: 16,
        fontWeight: 'bold'
    },
    infoSubTitle: {
        color: '#385757',
        fontSize: 13,
        marginTop: 20,
        fontWeight: '900',
        textAlign: 'center'
    },
    btnContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50
    },
    btnHubungi: {
        width: 100,
        marginRight: 40
    },
    txtHubungi: {
        color: '#385757',
        fontWeight: 'bold'
    },
    formContainer: {
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    infoFormTitle: {
        color: '#2a8b40',
        fontSize: 16,
        fontWeight: 'bold'
    },
    infoFormSubTitle: {
        color: '#385757',
        fontSize: 13,
        marginTop: 20,
        fontWeight: 'bold',
    },
    inputContainer: {
        margin: 20
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#2a8b40',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    ListContainer: {
        flexDirection: 'row',
        margin: 5
    },
    pageTxt: {
        marginRight: 5
    }
})

export default TopUpDetail;
