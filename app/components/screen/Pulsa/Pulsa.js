import React, { Component } from 'react';
import { View, Text, StyleSheet, Image,ImageBackground, Dimensions, FlatList, LayoutAnimation, UIManager, TouchableOpacity,TouchableHighlight, ScrollView, Modal, PermissionsAndroid  } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label, CheckBox, ListItem, Body } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import appStyle from '../../styles/AppStyle';
import SegmentedControlTab from "react-native-segmented-control-tab";
import { post, postWithToken } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import {IconProvider} from '../../../supports/IconProvider';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import { getValue } from '../../../module/LocalData/Storage';
import Contacts from 'react-native-contacts';
import {dialog} from '../../../supports/Dialog';
import {utils} from '../../../utils/Utils';

const size = Dimensions.get('window');
const prepaid = 'prepaid';
const postpaid = 'postpaid';

class Pulsa extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedTabIndex : 0,
            selectedIndexPrabayarPulsa : -1,
            showSpinner: false,
            dataPrabayarPulsa: [],
            namaProdukPra: '',
            namaProdukPasca:'',
            isPhoneContactOpen: false,
            PhoneContactList: [],
            logoProviderPrabayar: '',
            logoProviderPasca: '',
            providerPraPrefix: '',
            providerPascaPrefix: '',
            isBtnDisable: true,
            isBtnDisablePra: true,
            isBtnDisablePasca: true,
            phoneNumberSearch:'',
            inputs: {
              noHandphonePra: {
                type: 'phoneNumber',
                value: this.props.navigation.getParam('phone')
              },
              noHandphonePasca: {
                type: 'phoneNumber',
                value: ''
              }
            }
        };

        this.arrayholderPhoneContact = [];
        this.validateInput = ValidationHelper.validateInput.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentDidMount() {
        this.getPulsaProduct(prepaid, this.props.navigation.getParam('phone'));
        this.getPhoneContact();
    }

    getPhoneContact() {
        this.setState({
            showSpinner: true
        })
        if (Platform.OS == 'android') {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                    'title': 'Contacts',
                    'message': 'Hasanahku membutuhkan akses kontak untuk proses pemilihan nomor handphone.'
                }
            ).then(() => {
                Contacts.getAll((err, contacts) => {
                    if (err === 'denied') {
                        this.setState({
                            showSpinner: false
                        })
                    } else {
                        let contactList = contacts.filter(contacts => contacts.phoneNumbers.length != 0)
                        this.setState(
                            {
                                PhoneContactList: contactList,
                                showSpinner: false
                            },
                            function () {
                                this.arrayholderPhoneContact = contactList;
                            },
                        )
                    }
                })
            })
        } else {
            Contacts.getAll((err, contacts) => {
                if (err) {
                    this.setState({
                        showSpinner: false
                    })
                    throw err;
                }
                let contactList = contacts.filter(contacts => contacts.phoneNumbers.length != 0)
                this.setState(
                    {
                        PhoneContactList: contactList,
                        showSpinner: false
                    },
                    function () {
                        this.arrayholderPhoneContact = contactList;
                    },
                )
            })
        }
    }

    getPulsaProduct(category, noHP) {

      let params = {
          "jenis": this.props.navigation.getParam('jenis'),
          "kategori": category,
          "phoneNo": noHP
      }

      post(Env.base_url + Api.getProductOperator, params).then(response => {
        const { inputs } = this.state;
          if(response.code == ResponseCode.OK){
              if(this.state.selectedTabIndex === 0){
                this.setState({
                    selectedIndexPrabayarPulsa: -1,
                    dataPrabayarPulsa: response.okContent.produk,
                    logoProviderPrabayar : response.okContent.logo,
                    providerPraPrefix : this.state.inputs.noHandphonePra.value.substring(0,4),
                    isBtnDisablePra : true,
                    isBtnDisable : true,
                    inputs: {
                      ...inputs,
                      ['noHandphonePra']: {
                        ...inputs['noHandphonePra'],
                        value : this.state.inputs.noHandphonePra.value,
                        error:  null
                      }
                    }
                });
              }else{
                this.setState({
                    namaProdukPasca: response.okContent.produk[0].namaProduk,
                    logoProviderPasca : response.okContent.logo,
                    providerPascaPrefix : this.state.inputs.noHandphonePasca.value.substring(0,4),
                    isBtnDisablePasca : false,
                    isBtnDisable : false
                });
              }
          } else {

              if(this.state.selectedTabIndex === 0){
                this.setState({
                  selectedIndexPrabayarPulsa: -1,
                  dataPrabayarPulsa: [],
                  logoProviderPrabayar : '',
                  providerPraPrefix : '',
                  isBtnDisablePra : true,
                  isBtnDisable : true,
                  inputs: {
                    ...inputs,
                    ['noHandphonePra']: {
                      ...inputs['noHandphonePra'],
                      value : this.state.inputs.noHandphonePra.value,
                      error:  response.failContent.failDescription
                    }
                  }
                });
              }else{
                this.setState({
                  namaProdukPasca:'',
                  logoProviderPasca : '',
                  providerPascaPrefix : '',
                  isBtnDisablePasca : true,
                  isBtnDisable : true,
                  inputs: {
                    ...inputs,
                    ['noHandphonePasca']: {
                      ...inputs['noHandphonePasca'],
                      value : this.state.inputs.noHandphonePasca.value,
                      error:  response.failContent.failDescription
                    }
                  }
                });
              }
          }
      }).catch(err => {
        dialog.alertException(err)
      });
    }

    handleTabIndexSelect = (index: number) => {
      this.setState(
        prevState => ({
          ...prevState,
          selectedTabIndex: index,
          isBtnDisable : index === 0 ? this.state.isBtnDisablePra : this.state.isBtnDisablePasca
         })
      )
    }

    selectPrabayarPulsa(item, index) {
      this.setState({
        namaProdukPra: item.namaProduk,
        selectedIndexPrabayarPulsa : index,
        isBtnDisablePra : false,
        isBtnDisable : false
      });
    }

    renderPrabayarPulsa = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => this.selectPrabayarPulsa(item, index)} activeOpacity = {0.5} >
                    { this.state.selectedIndexPrabayarPulsa === index ?
                      <View style={[styles.GridViewBlockStyle, {backgroundColor: '#FFF2DF'}]}>
                          <Text style={styles.GridViewInsideTextItemStyle}> {utils.currencyFormat(item.denom, 0)} </Text>
                      </View>
                      :
                      <View style={styles.GridViewBlockStyle}>
                          <Text style={styles.GridViewInsideTextItemStyle}> {utils.currencyFormat(item.denom, 0)} </Text>
                      </View>
                    }
            </TouchableOpacity>
        )
    }

    inquiryPulsa() {

        if(this.state.selectedTabIndex === 0){
          this.setState({showSpinner: true});

          let params = {
              "phone": this.props.navigation.getParam('phone'),
              "namaProduk": this.state.namaProdukPra,
              "phoneNo": this.state.inputs.noHandphonePra.value
          }

          getValue('tokenLogin').then(tokenLogin => {
            postWithToken(Env.base_url + Api.inqPulsaPrepaid, params, tokenLogin).then(response => {
              if(response.code == ResponseCode.OK){
                  setTimeout(() => {
                      this.setState({
                          showSpinner: false
                      });
                  }, 1000);

                  this.props.navigation.navigate('PulsaPrabayarConfirm',{
                    "phone": this.props.navigation.getParam('phone'),
                    "inqRes": response.okContent
                  });

              } else {
                this.setState({showSpinner: false});
                dialog.alertFail(response.failContent, this);
              }
            }).catch(err => {
              this.setState({showSpinner: false});
              dialog.alertException(err);
            });
          });
        }else{
          this.setState({showSpinner: true});

          let params = {
              "phone": this.props.navigation.getParam('phone'),
              "namaProduk": this.state.namaProdukPasca,
              "phoneNo": this.state.inputs.noHandphonePasca.value
          }

          getValue('tokenLogin').then(tokenLogin => {
            postWithToken(Env.base_url + Api.inqPulsaPostpaid, params, tokenLogin).then(response => {
                if(response.code == ResponseCode.OK){
                    setTimeout(() => {
                        this.setState({
                            showSpinner: false,
                        });
                    }, 1000);

                    this.props.navigation.navigate('PulsaPascaBayarConfirm',{
                      "phone": this.props.navigation.getParam('phone'),
                      "inqRes": response.okContent
                    });

                } else {
                  this.setState({showSpinner: false});
                  dialog.alertFail(response.failContent, this);
                }
              }).catch(err => {
                this.setState({showSpinner: false});
                dialog.alertException(err);
              });
            });
          }
    }

    loadPhoneContact(){
      this.setState({
          isPhoneContactOpen: true
      });
    }

    closePhoneContact() {
        this.setState({
            isPhoneContactOpen: false
        })
    }

    searchPhoneContact(phoneNumberSearch) {
        const newDataContact = this.arrayholderPhoneContact.filter(function (item) {
            const itemData = item.displayName
                ? item.displayName.toUpperCase()
                : ''.toUpperCase();
            const textData = phoneNumberSearch.toUpperCase();

            return itemData.indexOf(textData) > -1;
        });

        this.setState({
            PhoneContactList: newDataContact,
            phoneNumberSearch: phoneNumberSearch
        });
    }

    selectPhoneContact(phoneNumber) {

      const { inputs } = this.state;
      phoneNumber = phoneNumber.replace(/\s|-/g,'');
      let phoneLength = phoneNumber.length;

      if(phoneLength >= 3 && phoneNumber.substring(0,3) ==='+62'){
        phoneNumber = "0"+phoneNumber.substring(3, phoneLength);
      }

      if(this.state.selectedTabIndex === 0){
          this.setState({
              isPhoneContactOpen: false
          });

        if(this.validateInput({id: 'noHandphonePra', value: phoneNumber}) == true){
          this.getPulsaProduct(prepaid, phoneNumber);
        }

      }else{
        this.setState({
            isPhoneContactOpen: false
        });

        if(this.validateInput({id: 'noHandphonePasca', value: phoneNumber}) == true){
          this.getPulsaProduct(postpaid, phoneNumber);
        }

      }
    }

    onChangePhone(typeTrx, id, phoneNumber, tmpPrefixPhone){

      let reg = /^\d+$/;

      if(reg.test(phoneNumber) || phoneNumber ==""){
        if(this.validateInput({id: id, value: phoneNumber}) == true){
          if(phoneNumber.length >= 10 && phoneNumber.substring(0, 4) !== tmpPrefixPhone){
            this.getPulsaProduct(typeTrx, phoneNumber);
          }
        }else{
          this.doResetData();
        }
      }
    }

    doResetData(){
      if(this.state.selectedTabIndex === 0){
        this.setState({
          selectedIndexPrabayarPulsa: -1,
          dataPrabayarPulsa: [],
          logoProviderPrabayar : '',
          providerPraPrefix : '',
          isBtnDisablePra : true,
          isBtnDisable : true
        });
      }else{
        this.setState({
          namaProdukPasca:'',
          logoProviderPasca : '',
          providerPascaPrefix : '',
          isBtnDisablePasca : true,
          isBtnDisable : true
        });
      }
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={appStyle.navBox}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={appStyle.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={25} type='antdesign' />
                            <View style={appStyle.navBoxTitle}>
                              <Text style={appStyle.navTitle}>Pulsa</Text>
                            </View>
                        </Button>
                    </View>
                    <View style={appStyle.bannerBox}>
                      <View style={[appStyle.bannerContent, {backgroundColor : '#B9E2CA'}]}>
                        <Image style={[appStyle.bannerLogo, {height : 70, width: 60, margin : 10}]} source={require('../../../assets/image/logo_container_pulsa.png')} />
                        <View style={appStyle.bannerInfo}>
                            <Text style={[appStyle.bannerTitle, {color:'#FFFFFF'}]}>Pembelian dan{"\n"}Pembayaran Pulsa</Text>
                            <Text style={[appStyle.bannerDesc, {color:'#FFFFFF'}]}>Cukup masukkan nomor telepon dan input nominal</Text>
                        </View>
                      </View>
                    </View>
                    <View>
                        <View style={appStyle.tabBox}>
                            <SegmentedControlTab
                                values={['Prabayar', 'Pascabayar']}
                                selectedIndex={this.state.selectedTabIndex}
                                onTabPress={this.handleTabIndexSelect}
                                borderRadius={20}
                                tabsContainerStyle={{ height: 40, backgroundColor: '#B6EACC', borderRadius : 20 }}
                                tabStyle={{ backgroundColor: '#B6EACC', borderWidth: 0, margin : 5, borderColor: 'transparent' }}
                                activeTabStyle={{ backgroundColor: '#FFFFFF', borderRadius : 20 }}
                                tabTextStyle={{ color: '#198E4B', fontWeight: 'bold' }}
                                activeTabTextStyle={{ color: '#888888' }}/>
                                {this.state.selectedTabIndex === 0 &&
                                    <View style={appStyle.tabContent}>
                                      <Form>
                                        <Item stackedLabel>
                                          <Label style={appStyle.label}>Nomor Handphone</Label>
                                          <View style={{ flexDirection: 'row' }}>
                                            {this.state.logoProviderPrabayar !=='' &&
                                              <Image style={{height : 20, width: 21, marginTop:15}} source={IconProvider.selected(this.state.logoProviderPrabayar)} />
                                            }
                                              <Input returnKeyType="search" style={appStyle.input}
                                                onEndEditing={() => this.getPulsaProduct(prepaid, this.state.inputs.noHandphonePra.value)} keyboardType='numeric'
                                                maxLength={14} minLength={10} value={this.state.inputs.noHandphonePra.value}
                                                onChangeText={value => this.onChangePhone(prepaid, 'noHandphonePra', value, this.state.providerPraPrefix)} />
                                              <TouchableOpacity onPress={()=>this.loadPhoneContact()} activeOpacity = {0.5}>
                                                <Image style={{height : 20, width: 21, marginTop:15}} source={require('../../../assets/image/icon_contact.png')} />
                                              </TouchableOpacity>
                                          </View>
                                        </Item>
                                        {this.validateError('noHandphonePra')}
                                      </Form>
                                      <View style={styles.gridContainer}>
                                          <FlatList
                                              data={this.state.dataPrabayarPulsa}
                                              renderItem={this.renderPrabayarPulsa}
                                              extraData={this.state}
                                              numColumns={3}
                                          />
                                      </View>
                                    </View>
                                }
                                {this.state.selectedTabIndex === 1 &&
                                  <View style={appStyle.tabContent}>
                                    <Form>
                                      <Item stackedLabel>
                                          <Label style={appStyle.label}>Nomor Handphone</Label>
                                          <View style={{ flexDirection: 'row' }}>
                                            {this.state.logoProviderPasca !=='' &&
                                              <Image style={{height : 20, width: 21, marginTop:15}} source={IconProvider.selected(this.state.logoProviderPasca)} />
                                            }
                                              <Input returnKeyType="search" style={appStyle.input}
                                                onEndEditing={() => this.getPulsaProduct(postpaid, this.state.inputs.noHandphonePasca.value)} keyboardType='numeric'
                                                maxLength={14} minLength={10} value={this.state.inputs.noHandphonePasca.value}
                                                onChangeText={value => this.onChangePhone(postpaid, 'noHandphonePasca', value, this.state.providerPascaPrefix)} />
                                              <TouchableOpacity onPress={()=>this.loadPhoneContact()} activeOpacity = {0.5}>
                                                <Image style={{height : 20, width: 21, marginTop:15}} source={require('../../../assets/image/icon_contact.png')} />
                                              </TouchableOpacity>
                                          </View>
                                      </Item>
                                      {this.validateError('noHandphonePasca')}
                                    </Form>
                                  </View>
                                }
                        </View>
                        <View style={appStyle.btnDefaultBox}>
                            <Button disabled={this.state.isBtnDisable} onPress={() => this.inquiryPulsa()} style={[appStyle.btnDefault, this.state.isBtnDisable == true && { backgroundColor: "#EAEAEA" }]}>
                                <Text style={appStyle.btnDefaultText}>Lanjutkan</Text>
                            </Button>
                        </View>
                    </View>
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                     <Modal animationType={'slide'} transparent={false}
                        visible={this.state.isPhoneContactOpen}
                        onRequestClose={() => this.closePhoneContact()}>
                        <Container style={appStyle.phoneContactContainer}>
                            <Content style={{ height: Dimensions.get('window').height + 10 }}>
                                <View style={appStyle.btnBackContainer}>
                                    <Button onPress={() => this.closePhoneContact()} androidRippleColor='#000' transparent style={[appStyle.btnBack, {flex: 2}]}>
                                        <Icon name='arrowleft' size={30} type='antdesign' />
                                    </Button>
                                    <Image style={appStyle.profileImage} source={require('../../../assets/image/man.png')} />
                                </View>
                                <View style={appStyle.phoneContactTitleContainer}>
                                    <Text style={appStyle.phoneContactTitle}>Sahabat Hasanah</Text>
                                    <Text>{this.state.PhoneContactList.length} available</Text>
                                </View>
                                <View style={appStyle.phoneContactHeader}>
                                    <View style={appStyle.phoneContactViewHeader}>
                                        <Icon
                                            name="search"
                                            type="font-awesome"
                                            size={20}
                                            color="#02aced"
                                            containerStyle={appStyle.phoneContactInputIcon}
                                        />
                                        <Input
                                            style={appStyle.phoneContactInputHeader}
                                            placeholder="Search by name"
                                            autoCapitalize="none"
                                            onChangeText={text => this.searchPhoneContact(text)}
                                            underlineColorAndroid="transparent"
                                            value={this.state.phoneNumberSearch}
                                        />
                                    </View>
                                </View>
                                <View style={appStyle.phoneContactBody}>
                                    <ScrollView>
                                        <FlatList
                                            data={this.state.PhoneContactList}
                                            renderItem={({ item }) => (
                                                <View style={appStyle.phoneContactListContainer}>
                                                    <View style={appStyle.phoneContactAvaContainer}>
                                                        <Text style={appStyle.phoneContactAvaTxt}>{item.displayName == undefined ? "Z" : item.displayName == null ? "Z" : item.displayName.slice(0, 1)}</Text>
                                                    </View>
                                                    <View style={appStyle.phoneContactInfo}>
                                                        <Text style={appStyle.phoneContactName}>{item.displayName} {item.middleName} {item.familyName}</Text>
                                                        {item.phoneNumbers.map(number => {
                                                            return (
                                                                <View style={appStyle.phoneContactItem}>
                                                                    <Text style={appStyle.phoneContactNumber}>{number.number}</Text>
                                                                    <View style={appStyle.phoneContactChooseItem}>
                                                                        <Button onPress={() => this.selectPhoneContact(number.number)} style={appStyle.phoneContactbtnItem}>
                                                                            <Text style={appStyle.phoneContactTxtItem}>pilih</Text>
                                                                        </Button>
                                                                    </View>
                                                                </View>
                                                            )
                                                        })}
                                                    </View>

                                                </View>
                                            )}
                                            extraData={this.state}
                                        />
                                    </ScrollView>
                                </View>
                            </Content>
                        </Container>
                    </Modal>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    gridContainer: {
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'visible'
    },
    GridViewBlockStyle: {
      justifyContent: 'center',
      borderRadius: 10,
      alignItems: 'center',
      height: 50,
      width: size.width / 4,
      margin: 5,
      backgroundColor: '#FCFBFC'
    },
    GridViewInsideTextItemStyle: {
      color: '#131A22',
      padding: 5,
      fontSize: 12
    },
    GridViewInsideSubTextItemStyle: {
      color: '#26C165',
      fontSize: 10
    }
})

export default Pulsa;
