import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import { Container, Content, Button, Form,Item, Label, Input } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import { PinInput, PinKeyboard   } from 'react-native-awesome-pin';
import appStyle from '../../styles/AppStyle';
import PinScreen from '../../../supports/PinScreen';
import { postWithToken } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {Error, ResponseCode} from '../../../supports/Constants';
import { getValue } from '../../../module/LocalData/Storage';
import {dialog} from '../../../supports/Dialog';

const size = Dimensions.get('window');

class PulsaPrabayarConfirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            inqRes: this.props.navigation.getParam('inqRes'),
            colorSpinner: '#e35200',
        };
    }

    paymentConfirmation() {
      this.RBSheet.open();
    }

    pinReset(){
      this.RBSheet.close();
      setTimeout(() => {
          this.props.navigation.navigate('VerifikasiLupaPin', {
              phone: this.props.navigation.getParam('phone'),
              screenCallback : "PulsaPrabayarConfirm"
          });
      }, 500);
    }

    doPayment(pin){
      this.setState({
        showSpinner: true
      });

      let params = {
        "phone": this.props.navigation.getParam('phone'),
        "pin": pin,
        "namaProduk": this.state.inqRes.namaProduk,
        "phoneNo": this.state.inqRes.phoneNo
      }

    getValue('tokenLogin').then(tokenLogin => {
      postWithToken(Env.base_url + Api.payPulsaPrepaid, params, tokenLogin).then(response => {
          if(response.code == ResponseCode.OK){
              setTimeout(() => {
                  this.setState({showSpinner: false});
                  this.RBSheet.close();
                  this.props.navigation.navigate('PulsaPrabayarPayment',{
                    "payRes": response.okContent,
                    "phone": this.props.navigation.getParam('phone')
                  });
              }, 1000);

          } else {
            let failCode = response.failContent.failCode;
            if(failCode ==="USER_ERR_06"){
              setTimeout(() => {
                  this.setState({showSpinner: false});
                  this.pinScreen.clearError();
                  this.pinScreen.clearPin();
                  this.pinScreen.throwError(response.failContent.failDescription);
              }, 1000);
            }else{
              if(failCode === "USER_ERR_14" || failCode === "USER_ERR_15"){
                this.pinScreen.clearError();
                this.pinScreen.clearPin();
              }
              this.setState({showSpinner: false});
              this.pinScreen.clearPin();
              dialog.alertFail(response.failContent, this);
            }
          }
      }).catch(err => {
        this.setState({showSpinner: false});
        this.pinScreen.clearPin();
        dialog.alertException(err);
      });
    });
  }
    render() {
        return (
            <Container>
                <Content>
                    <View style={appStyle.navBox}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={appStyle.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={25} type='antdesign' />
                            <View style={appStyle.navBoxTitle}>
                              <Text style={appStyle.navTitle}>Pulsa Prabayar</Text>
                            </View>
                        </Button>
                    </View>
                    <View style={appStyle.formBox}>
                      <Form>
                        <Item fixedLabel style={appStyle.itemHeaderTrx}>
                            <Label style={appStyle.labelHeaderTrx}>Detail Transaksi</Label>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Jenis Layanan</Label>
                          <Text style={appStyle.labelTrxValue}>{this.state.inqRes.jenisLayanan}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>No Telepon</Label>
                          <Text style={appStyle.labelTrxValue}>{this.state.inqRes.phoneNo}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Provider</Label>
                          <Text style={appStyle.labelTrxValue}>{this.state.inqRes.providerDescription}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemHeaderTrx}>
                            <Label style={appStyle.labelHeaderTrx}>Detail Pembayaran</Label>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Nominal</Label>
                          <Text style={[appStyle.labelTrxValue, {fontWeight : 'bold'}]}>{this.state.inqRes.nominalFormatted}</Text>
                        </Item>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Biaya Transaksi</Label>
                          <Text style={[appStyle.labelTrxValue, {fontWeight : 'bold'}]}>{this.state.inqRes.biayaAdminFormatted}</Text>
                        </Item>
                        <Item/>
                      </Form>
                    </View>
                    <View style={appStyle.boxTotalConfirm}>
                      <View style={appStyle.formBox}>
                        <Form>
                        <Item fixedLabel style={appStyle.itemTrx}>
                          <Label style={appStyle.labelTrx}>Total Pembayaran</Label>
                          <Text style={[appStyle.labelTrxValue, {fontWeight : 'bold'}]}>{this.state.inqRes.totalFormatted}</Text>
                        </Item>
                        </Form>
                      </View>
                    </View>
                    <View style={appStyle.btnDefaultBox}>
                        <Button onPress={() => this.paymentConfirmation()} style={appStyle.btnDefault}>
                            <Text style={appStyle.btnDefaultText}>Konfirmasi</Text>
                        </Button>
                    </View>
                    <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown
                        height={Platform.OS == 'android' ? size.height / 1.5 : (size.height / 1.5) + 100}
                        // animationType="fade"
                        duration={300}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 10,
                                borderTopLeftRadius: 10,
                            },
                        }}>
                          <PinScreen
                            onRef={ ref => (this.pinScreen = ref) }
                            tagline='Masukkan PIN HasanahKu'
                            numberOfPins={6}
                            keyDown={ this.doPayment.bind(this) }
                            pinReset={this.pinReset.bind(this)}
                            />
                      </RBSheet>
                      <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                      />
                </Content>
            </Container>
        );
    }
}

export default PulsaPrabayarConfirm;
