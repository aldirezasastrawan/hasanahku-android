import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';

class PulsaConfirmation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
        };
    }

    konfirmasi() {
        this.setState({
            showSpinner: true,
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
            });

            this.props.navigation.navigate('Home')
        }, 3000);
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' size={35} type='antdesign' />
                        </Button>
                        <Image style={styles.profileImage} source={require('../../../assets/image/man.png')} />
                    </View>

                    <View styles={styles.titleContainer}>
                        <Text style={styles.title}>
                            Detail Transaksi
                        </Text>
                    </View>

                    <View style={styles.detailSlipContainer}>
                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>ID Transaksi</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>0089888822225555</Text>
                        </View>

                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Tanggal Transaksi</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>26-12-2019</Text>
                        </View>

                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Nama Pemilik</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>Inna Alvi</Text>
                        </View>

                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Id Pelanggan</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>00773837387383</Text>
                        </View>

                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Nominal</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>Rp. 500.000</Text>
                        </View>

                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Biaya Admin</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>Rp. 6.500</Text>
                        </View>

                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Total</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>Rp. 500.6500</Text>
                        </View>

                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Keterangan</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>Beli Pulsa</Text>
                        </View>
                    </View>

                    <View style={styles.btnRegisterContainer}>
                        <Button onPress={() => this.konfirmasi()} style={styles.btnRegister}>
                            <Text style={styles.RegisterText}>Konfirmasi</Text>
                        </Button>
                    </View>
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    titleContainer: {
        marginTop: 5,
    },
    title: {
        fontSize: 24,
        color: '#385757',
        paddingTop: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    detailSlipContainer: {
        margin: 20,
        borderWidth: 0.5,
        borderRadius: 10
    },
    rowDetail: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        padding: 10
    },
    labelDetail: {
        flex: 1,
        paddingLeft: 10
    },
    labelValue: {
        fontWeight: 'bold',
        flex: 1,
        paddingLeft: 10
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#2a8b40',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
})

export default PulsaConfirmation;
