import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, FlatList, TouchableOpacity } from 'react-native';
import { Container, Content, Button, Card } from 'native-base';
import { Icon } from 'react-native-elements';
import Tts from 'react-native-tts';
import LinearGradient from 'react-native-linear-gradient';
import LottieView from 'lottie-react-native';
import { postData } from '../../../module/Rest/Rest';
import Env from '../../../module/Utils/Env';
import { getValue } from '../../../module/LocalData/Storage';
import Spinner from 'react-native-loading-spinner-overlay';


class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataUser: {},
            dataGrid: [],
            dataFeature: [],
            dataBanner: [],
            indexBanner: 0,
            showSpinner: false,
        };
    }

    componentDidMount() {
        this.getDataUser()
        this.getDataGrid()
        this.getDataBanner()
        this.getDataLayanan()
    }

    componentWillUnmount() {
        Tts.stop();
    }

    startVoice(name, saldo) {
        Tts.setDefaultPitch(1);
        Tts.setDefaultLanguage('id-ID');
        let voice = [
            'Hai ' + name,
            'Saldo kamu saat ini sebesar ' + saldo + ' Rupiah.',
        ];

        voice.map(y => {
            console.log(y);
            Tts.speak(y);
        });
    }

    getDataUser() {
        this.setState({
            showSpinner: true,
        });
        getValue('token').then(token => {
            let params = {
                "access_token": token,
                "phone": this.props.navigation.getParam('phone'),
            }
            postData(Env.base_url + '/accountInfo', params).then(response => {
                console.log(response);
                if (response.data.code == 'OK') {
                    this.setState({
                        dataUser: response.data.okContent,
                        showSpinner: false,
                    })

                    setTimeout(() => {
                        this.startVoice(response.data.okContent.namaDepan, response.data.okContent.balance)
                    }, 500);
                } else {

                    setTimeout(() => {
                        this.setState({
                            showSpinner: false,
                        });

                        setTimeout(() => {
                            alert('Server sedang bermasalah. Coba lagi nanti yaa')

                        }, 500);
                    }, 2000);
                }

            })
        })

    }

    getDataGrid() {
        let data = [
            { menu: 'PLN', icon: 'light-bulb', type: 'entypo', color: '#1198f8' },
            { menu: 'Paket Data', icon: 'signal', type: 'entypo', color: '#e85a59' },
            { menu: 'Pulsa', icon: 'perm-phone-msg', type: 'material', color: '#fea818' },
            { menu: 'Asuransi', icon: 'security', type: 'material', color: '#5335db' },
            { menu: 'Air', icon: 'waves', type: 'material-community', color: '#5335db' },
            { menu: 'Top Up UE', icon: 'wallet-outline', type: 'material-community', color: '#fea818' },
            { menu: 'Zakat & Wakaf', icon: 'wallet-giftcard', type: 'material-community', color: '#e85a59' },
            { menu: 'Lainnya', icon: 'ellipsis1', type: 'antdesign', color: '#1198f8' },
        ]

        this.setState({
            dataGrid: data
        })
    }

    getDataBanner() {
        let data = [
            { title: 'Kode Transfer BNI Syariah', image: require('../../../assets/image/transfer.jpg'), description: 'Mau transfer ke BNI Syariah? Gunakan kode transfer 427' },
            { title: 'Asuransi melalui HasanahKu', image: require('../../../assets/image/donasi.jpg'), description: 'Hasanahku bekerja sama dengan berbagai Asuransi terbaik' },
            { title: 'Kemudahan Pembayaran ', image: require('../../../assets/image/transfer1.jpg'), description: 'Pembayaran menjadi lebih mudah melalui HasanahKu Mobile App' },
            { title: 'Top Up HasanahKu', image: require('../../../assets/image/credit1.jpg'), description: 'Dapatkan berbagai Promo menarik di HanasahKu' },
        ]

        this.setState({
            dataBanner: data
        })
    }

    getDataLayanan() {
        let data = [
            { title: 'Kode Transfer BNI Syariah', menu: 'Transfer', image: require('../../../assets/image/transfer.jpg'), description: 'Mau transfer ke BNI Syariah? Gunakan kode transfer 427' },
            { title: 'Asuransi melalui HasanahKu', menu: 'Asuransi', image: require('../../../assets/image/donasi.jpg'), description: 'Hasanahku bekerja sama dengan berbagai Asuransi terbaik' },
            { title: 'Kemudahan Pembayaran ', menu: 'Paketdata', image: require('../../../assets/image/transfer1.jpg'), description: 'Pembayaran menjadi lebih mudah melalui HasanahKu Mobile App' },
            { title: 'Top Up HasanahKu', menu: 'TopUp', image: require('../../../assets/image/credit1.jpg'), description: 'Dapatkan berbagai Promo menarik di HanasahKu' },
        ]

        this.setState({
            dataFeature: data
        })
    }

    transfer() {
        this.props.navigation.navigate('Transfer', {
            "phone": this.state.dataUser.noHP
        })
    }

    tarikSaldo() {
        this.props.navigation.navigate('Tariksaldo', {
            "phone": this.state.dataUser.noHP
        })
    }

    riwayat() {
        this.props.navigation.navigate('Riwayat', {
            "phone": this.state.dataUser.noHP
        })
    }

    topUp() {
        this.props.navigation.navigate('TopUp')
    }

    notif() {
        this.props.navigation.navigate('Notification')
    }

    chat() {
        this.props.navigation.navigate('Chat')
    }

    bannerPressed(item) {
        alert(item.title)
    }

    gotoGridMenu(menu) {
        switch (menu) {
            case 'PLN':

                this.props.navigation.navigate('Pln')

                break;

            case 'Paket Data':

                this.props.navigation.navigate('Paketdata')

                break;

            case 'Pulsa':

                this.props.navigation.navigate('Pulsa')

                break;

            case 'Asuransi':

                this.props.navigation.navigate('Asuransi')

                break;

            case 'Air':

                this.props.navigation.navigate('Air')

                break;

            case 'Top Up UE':

                this.props.navigation.navigate('Topupue')

                break;

            case 'Zakat & Wakaf':

                this.props.navigation.navigate('Zakatwakaf')

                break;

            default:

                alert(menu)

                break;
        }
    }

    renderGrid = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => this.gotoGridMenu(item.menu)} activeOpacity={0.7} style={styles.gridIconContainer}>
                {/* <View style={[styles.iconContainer, {backgroundColor: item.color}]}> */}
                <View style={[styles.iconContainer]}>
                    <Icon size={30} name={item.icon} type={item.type} color='#fff' />
                </View>
                <Text style={styles.txtGrid}>{item.menu}</Text>
            </TouchableOpacity>
        )
    }

    renderBanner = ({ item }) => {
        return (
            <Card
                style={styles.bannerWrapper}>
                <View style={styles.bniLogoContainer}>
                    <Image style={styles.bniLogo} source={require('../../../assets/image/bnisyariahlogo.png')} />
                </View>
                <TouchableOpacity onPress={() => this.bannerPressed(item)} activeOpacity={0.7} style={styles.bannerInfo}>
                    <View style={styles.bannerImageContainer}>
                        <Image style={styles.bannerImage} source={item.image} />
                    </View>
                    <View style={styles.detailBannerContainer}>
                        <Text numberOfLines={2} style={styles.titleBanner}>{item.title}</Text>
                        <Text numberOfLines={3} style={styles.descriptionBanner}>{item.description}</Text>
                    </View>
                </TouchableOpacity>

            </Card>
        );
    }

    renderFeature = ({ item, index }) => {
        return (
            <View style={{ margin: 3.5, }}>

                <Card
                    style={styles.cardFeature}>
                    <Image
                        style={styles.featureImage}
                        source={item.image}
                    />

                    <View style={{ backgroundColor: 'rgba(42, 137, 16, 0.2)', flex: 1 }}>
                        <Text numberOfLines={2} key={index + 1} style={styles.titleFeature}>
                            {item.title}
                        </Text>

                        <Text numberOfLines={3} key={index + 1} style={styles.detailFeature}>
                            {item.description}
                        </Text>
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Button
                            // transparent
                            onPress={() => this.props.navigation.navigate(item.menu)}
                            style={styles.btnDetail}>
                            <Text
                                key={index.toString()}
                                style={styles.btnDetailFeature}>
                                Lihat Detail
                        </Text>
                        </Button>
                    </View>
                </Card>
            </View>

        );
    }

    render() {

        const dataUser = this.state.dataUser;

        return (
            <Container>
                {/* <ImageBackground style={{ height: '100%', width: '100%', flex: 1 }} source={require('../../../assets/image/bg_image.png')}> */}

                <Content>
                    <LinearGradient colors={['#2a8b10', '#1e622d']} style={styles.header}>
                        <View style={styles.iconHeaderContainer}>
                            <View style={styles.leftIconHeaderContainer}>
                                <Button onPress={() => this.notif()} transparent>
                                    <Icon color='#fff' name='notifications-none' />
                                </Button>
                                <Button style={{ marginRight: 10 }} onPress={() => this.chat()} transparent>
                                    <Text style={styles.TextHeaderLeft}>HasanahkU</Text>
                                </Button>
                            </View>
                            <View style={styles.rightIconHeaderContainer}>
                                <Button style={{ marginRight: 10 }} onPress={() => this.chat()} transparent>
                                    <Icon color='#fff' name='message' />
                                </Button>
                                <Button onPress={() => this.notif()} transparent>
                                    <Icon color='#fff' name='notifications-none' />
                                </Button>
                            </View>
                        </View>
                        <View style={styles.infoContainer}>
                            <Image style={styles.profileImage} source={require('../../../assets/image/man.png')} />
                            {/* <LottieView
                                    style={styles.profileImage}
                                    // autoSize={true}
                                    resizeMode="cover"
                                    source={require('../../../assets/animation/person.json')}
                                    autoPlay
                                /> */}
                            <View style={styles.userInfoContainer}>
                                <Text style={styles.name}>{dataUser.namaDepan}</Text>
                                <Text style={styles.amount}>Rp. {dataUser.balance}</Text>
                            </View>
                            <Button onPress={() => this.topUp()} style={styles.btnTopup}>
                                <Text style={styles.txtTopUp}>Top Up</Text>
                            </Button>
                        </View>
                    </LinearGradient>
                    <View style={styles.shortcutContainer}>
                        <Card style={styles.shortcutBtnContainer}>
                            <Button onPress={() => this.transfer()} style={styles.btnShortcut} transparent>
                                <Icon size={35} color='#2a8b40' name="logout" type="material-community" />
                                <Text style={styles.txtShrtcut}>
                                    Transfer
                                </Text>
                            </Button>
                            <Button onPress={() => this.tarikSaldo()} style={styles.btnShortcut} transparent>
                                <Icon size={35} color='#2a8b40' name="credit-card" type="entypo" />
                                <Text style={styles.txtShrtcut}>
                                    Tarik Tunai
                                </Text>
                            </Button>
                            <Button onPress={() => this.riwayat()} style={styles.btnShortcut} transparent>
                                <Icon size={35} color='#2a8b40' containerStyle={{ marginLeft: 7 }} name="checklist" type="octicon" />
                                <Text style={styles.txtShrtcut}>
                                    Riwayat
                                </Text>
                            </Button>
                        </Card>
                    </View>

                    <View style={styles.gridContainer}>
                        <FlatList
                            data={this.state.dataGrid}
                            renderItem={this.renderGrid}
                            extraData={this.state}
                            numColumns={4}
                        />
                    </View>

                    <View style={styles.featureContainer}>
                        <View style={styles.titleContainer}>
                            <Text style={styles.titleBold}>
                                Layanan HasanahKu Favorit
                            </Text>
                        </View>

                        <FlatList
                            horizontal
                            style={styles.featureWrapper}
                            showsHorizontalScrollIndicator={false}
                            data={this.state.dataFeature}
                            renderItem={this.renderFeature}
                            extraData={this.state}
                        />
                    </View>

                    {/* <View style={styles.titleContainer}>
                            <Text style={styles.titleBold}>
                                Promo
                            </Text>
                        </View>
                        <View style={styles.bannerContainer}>
                            <Carousel
                                layout={'default'}
                                ref={c => (this._slider1Ref = c)}
                                data={this.state.dataBanner}
                                sliderWidth={Dimensions.get('window').width - 50}
                                itemWidth={Dimensions.get('window').width - 50}
                                loop={true}
                                loopClonesPerSide={2}
                                autoplay={true}
                                autoplayDelay={500}
                                autoplayInterval={3000}
                                firstItem={1}
                                initialNumToRender={this.state.dataBanner.length}
                                inactiveSlideScale={1}
                                inactiveSlideOpacity={1}
                                onSnapToItem={index => this.setState({ indexBanner: index })}
                                hasParallaxImages={false}
                                containerCustomStyle={styles.slider}
                                contentContainerCustomStyle={styles.sliderContentContainer}
                                renderItem={this.renderBanner}
                            />
                            <Pagination
                                dotsLength={this.state.dataBanner.length}
                                activeDotIndex={this.state.indexBanner}
                                containerStyle={styles.paginationContainer}
                                dotColor="#385757"
                                dotStyle={styles.paginationDot}
                                inactiveDotColor="#5E929E"
                                inactiveDotOpacity={0.4}
                                inactiveDotScale={0.6}
                                carouselRef={this._slider1Ref}
                                tappableDots={true}
                            />

                        </View> */}


                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />

                </Content>
                {/* </ImageBackground> */}

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {

    },
    header: {
        height: 200,
        backgroundColor: '#e35200',
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30
    },
    iconHeaderContainer: {
        flexDirection: 'row',
        // justifyContent: 'flex-end',
        // alignItems: 'flex-end',
        backgroundColor: '#1e622d',
        marginBottom: 10
    },
    TextHeaderLeft: {
        color: '#fff',
        fontWeight: 'bold'
    },
    leftIconHeaderContainer: {
        flex: 2,
        flexDirection: 'row',
        marginLeft: 15
    },
    rightIconHeaderContainer: {
        flexDirection: 'row',
        marginRight: 15
    },
    profileImage: {
        height: 80,
        width: 80,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede',
    },
    infoContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    userInfoContainer: {
        flex: 2,
        marginLeft: 20,
        marginRight: 20
    },
    btnTopup: {
        backgroundColor: '#fff',
        width: 80,
        height: 35,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    txtTopUp: {
        color: '#2a8b40',
        fontSize: 13,
        fontWeight: 'bold'
    },
    name: {
        color: '#fff',
        fontWeight: 'bold',
        // fontFamily: 'Varela',
        fontSize: 16
    },
    amount: {
        fontWeight: 'bold',
        color: '#E0E4E9'
    },
    shortcutContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -60
    },
    shortcutBtnContainer: {
        width: Dimensions.get('window').width - 40,
        height: 90,
        backgroundColor: '#fff',
        borderRadius: 20,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    btnShortcut: {
        width: 80,
        height: 80,
        flexDirection: 'column',
    },
    txtShrtcut: {
        fontWeight: '500',
        color: '#000',
        fontSize: 12,
        //color: '#385757'
    },
    gridContainer: {
        margin: 20,
    },
    gridIconContainer: {
        flexDirection: 'column',
        height: Dimensions.get('window').width / 4,
        width: Dimensions.get('window').width / 4 - 20,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
    },
    iconContainer: {
        height: Dimensions.get('window').width / 4 - 40,
        width: Dimensions.get('window').width / 4 - 40,
        backgroundColor: '#2a8b10',
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 5,
    },
    txtGrid: {
        marginTop: 5,
        fontSize: 11,
        fontWeight: 'bold',
        color: '#385757'
    },
    bannerContainer: {
        // margin: 20,
        marginBottom: 20,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'visible',
    },
    bannerWrapper: {
        height: 150,
        width: Dimensions.get('window').width - 55,
        flex: 1,
        // margin: 1,
        // borderWidth: 0.5,
        borderRadius: 20,
        overflow: 'visible'

    },
    bannerImageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    bannerImage: {
        height: 100,
        width: 120,
        margin: 5,
        marginTop: -30,
        resizeMode: 'cover'
    },
    bniLogoContainer: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginTop: 5,
        marginRight: 10,
    },
    bniLogo: {
        height: 50,
        width: 50
    },
    bannerInfo: {
        flexDirection: 'row',
    },
    detailBannerContainer: {
        margin: 10,
        marginTop: -10,
        flex: 1.5,
    },
    titleBanner: {
        paddingRight: 10,
        paddingBottom: 10,
        fontSize: 12,
        fontWeight: '600'
    },
    descriptionBanner: {
        paddingBottom: 10,
       // fontSize: 13,
        fontSize: 11,
    },
    paginationContainer: {
        paddingVertical: -10,
        marginTop: -30,
        marginLeft: 200
    },
    paginationDot: {
        width: 15,
        height: 8,
        borderRadius: 5,
        backgroundColor: 'rgba(255, 255, 255, 0.92)'
    },
    slider: {
        // overflow: 'visible'
    },
    titleContainer: {
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginLeft: 25,
        width: '100%',
        marginTop: 5,
        marginBottom: 1
    },
    titleBold: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'left',
        color: '#385757'
    },
    featureContainer: {

    },
    featureWrapper: {
        margin: 20, overflow: 'visible'
    },
    cardFeature: {
        borderRadius: 10,
        width: 250,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 5,
    },
    featureImage: {
        height: 150,
        width: 250,
        resizeMode: 'contain',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
    },
    titleFeature: {
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 14,
        fontWeight: 'bold'
    },
    detailFeature: {
        marginTop: 7,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 13,
        marginBottom: 7
    },
    btnDetailFeature: {
        fontWeight: 'bold',
        // color: '#e35200'
        color: '#fff',
        textAlign: 'center'
    },
    btnDetail: {
        backgroundColor: '#2a8b40',
        height: 30,
        width: '100%',
        borderTopRightRadius: 0,
        borderTopLeftRadius: 0,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default Home;
