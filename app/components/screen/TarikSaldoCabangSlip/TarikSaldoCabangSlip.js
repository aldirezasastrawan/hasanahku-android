import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Container, Content, Button } from 'native-base';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';

class TarikSaldoCabangSlip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            dataSlip: this.props.navigation.getParam('dataSlip')
        };
    }

    konfirmasi() {
        this.setState({
            showSpinner: true,
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
            });

            this.props.navigation.navigate('Home2')
        }, 3000);
    }

    render() {
        const dataSlipDetail = this.state.dataSlip
        return (
            <Container>
                <Content>
                    <View style={styles.header}>
                        <Text style={styles.saldoTitle}>
                            Kode Transaksi
                        </Text>
                        <Text style={styles.saldo}>
                            Xce44556672828dd
                        </Text>

                        <Button onPress={() => alert('copied')} transparent>
                            <Text>Salin Code</Text>
                        </Button>
                    </View>

                    <View style={styles.checkedHeaderContainer}>
                        <View style={styles.checkedHeaderMasking}>
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 2 }] }}
                                autoSize={true}
                                resizeMode="cover"
                                source={require('../../../assets/animation/checked.json')}
                                autoPlay
                                loop={false}
                            />
                        </View>

                    </View>

                    {/* <View style={styles.btnheaderSection}>
                        <Button style={styles.btnSender}>
                            <Icon color="#17284c" active name='calendar-today' size={20} type="material-community" />
                            <Text style={styles.txtSender}>Rp {dataSlipDetail.prevBalance}</Text>
                        </Button>
                        <Button style={styles.btnReceiver}>
                            <Icon color="#07c79d" active name='calendar-today' size={20} type="material-community" />
                            <Text style={styles.txtBtnReceiver}>Rp {dataSlipDetail.newBalance}</Text>
                        </Button>
                    </View> */}

                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>
                            Rincian Transaksi
                        </Text>
                    </View>

                    <View style={styles.detailSlipContainer}>
                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Waktu</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>12 : 00 PM</Text>
                        </View>

                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Tanggal</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>12 November 2020</Text>
                        </View>

                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>No Transaksi</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>125566777</Text>
                        </View>

                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Keterangan</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>Hehehehehe</Text>
                        </View>

                    </View>

                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>
                            Rincian Nominal
                        </Text>
                    </View>

                    <View style={styles.detailSlipContainer}>
                        <View style={styles.rowDetail}>
                            <Text style={styles.labelDetail}>Jumlah Tarik Tunai</Text>
                            <Text>:</Text>
                            <Text style={styles.labelValue}>Rp 300.000</Text>
                        </View>

                    </View>

                    <View style={styles.narasiContainer}>
                        <Text style={styles.narasi}>
                            Tunjukan Kode Transaksi ke Teller Cabang terdekat untuk mengambil Uang Anda ya
                        </Text>
                    </View>

                    <View style={styles.btnRegisterContainer}>
                        <Button onPress={() => this.konfirmasi()} style={styles.btnRegister}>
                            <Text style={styles.RegisterText}>Kembali Ke Beranda</Text>
                        </Button>
                    </View>
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        height: 200,
        backgroundColor: '#e7fcfa',
        borderBottomRightRadius: 20,
        borderBottomLeftRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkedHeaderContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkedHeaderMasking: {
        marginTop: -20,
        borderWidth: 15,
        borderColor: '#fff',
        borderRadius: 50,
        backgroundColor: '#fff'
    },
    btnheaderSection: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    btnSender: {
        backgroundColor: '#f7f8fa',
        padding: 10,
        borderRadius: 20,
        marginRight: 10
    },
    txtSender: {
        marginLeft: 5,
        color: '#273554'
    },
    btnReceiver: {
        backgroundColor: '#e7fcfa',
        padding: 10,
        borderRadius: 20
    },
    txtBtnReceiver: {
        marginLeft: 5,
        color: '#21c4a0'
    },
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    titleContainer: {
        margin: 10,
        backgroundColor: '#f9f8ff',
        borderRadius: 10,
        justifyContent: 'center'
    },
    title: {
        fontSize: 18,
        color: '#283757',
        padding: 10,
        fontWeight: 'bold',
    },
    detailSlipContainer: {
        margin: 10,
    },
    rowDetail: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        padding: 10
    },
    labelDetail: {
        flex: 1,
        paddingLeft: 10,
        color: '#8493b3'
    },
    labelValue: {
        fontWeight: 'bold',
        flex: 1,
        paddingLeft: 10,
        color: '#273554'
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#4caf50',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    saldoTitle: {
        fontWeight: 'bold'
    },
    saldo: {
        fontWeight: 'bold',
        fontSize: 25,
        marginTop: 15
    },
    narasiContainer: {
        margin: 20
    }
})

export default TarikSaldoCabangSlip;
