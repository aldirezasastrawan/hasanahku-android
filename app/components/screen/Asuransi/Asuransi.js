import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, FlatList, LayoutAnimation, UIManager } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label, CheckBox, ListItem, Body } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';

class Asuransi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            dataPaket: [],
            dataNominal: [],
            tipe: '',
            nominal: '',
            detail: '',
            bulan: ''
        };

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentDidMount() {
        this.getDataPaket();
    }

    componentWillUpdate() {
        // LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    }

    getDataPaket() {
        let data = [
            {
                typeId: 1, type: 'BPJS', detail: 'Pembyaran BPJS', image: require('../../../assets/image/telkom.png')
            },
            {
                typeId: 2, type: 'Prudential', detail: 'Pembayaran Prudential', image: require('../../../assets/image/telkom.png')
            },
            {
                typeId: 3, type: 'Alianz', detail: 'Pembayaran Alianz', image: require('../../../assets/image/telkom.png')
            },
        ]

        this.setState({
            dataPaket: data
        })
    }

    selectType(item) {
        this.setState({ tipe: item.type, detail: item.detail })
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);

    }



    renderPaket = ({ item }) => {
        return (
            <Button onPress={() => this.selectType(item)} style={styles.gridListContainer}>
                <Image style={styles.imageGrid} source={item.image} />
                <Text numberOfLines={1} style={styles.txtGrid}>{item.type}</Text>
            </Button>
        )
    }

    proses() {
        this.setState({
            showSpinner: true,
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
            });

            this.props.navigation.navigate('AsuransiConfirmation')

        }, 3000);
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' size={35} type='antdesign' />
                        </Button>
                        <Image style={styles.profileImage} source={require('../../../assets/image/man.png')} />
                    </View>

                    <View>
                        <View style={styles.formContainer}>
                            <Text style={styles.infoFormTitle}>Lakukan Pembayaran Asuransi Anda melalui aplikasi HasanahKu</Text>
                            <Text style={styles.infoFormSubTitle}>Silahkan pilih Asuransi Anda</Text>
                        </View>

                        <View style={styles.gridContainer}>
                            <FlatList
                                data={this.state.dataPaket}
                                renderItem={this.renderPaket}
                                extraData={this.state}
                                numColumns={3}
                            />
                        </View>

                        <View style={styles.inputContainer}>
                            <Form>
                                <Item stackedLabel>
                                    <Label style={styles.label}>Asuransi</Label>
                                    <Input style={styles.labelInput} keyboardType='default' value={this.state.tipe} disabled />
                                </Item>

                                {this.state.tipe != "" ?
                                    <Item stackedLabel>
                                        <Label style={styles.label}>Deskripsi</Label>
                                        <Input style={styles.labelInput} keyboardType='default' value={this.state.detail} disabled />
                                    </Item>
                                    : null
                                }

                                {this.state.tipe != "" ?
                                    <Item stackedLabel last>
                                        <Label style={styles.label}>Jumlah Bulan</Label>
                                        <Input style={styles.labelInput} keyboardType='number-pad' value={this.state.bulan} onChangeText={(bulan) => this.setState({ bulan: bulan })} />
                                    </Item>
                                    : null
                                }

                                {this.state.tipe != "" ?
                                    <Item stackedLabel last>
                                        <Label style={styles.label}>Nominal</Label>
                                        <Input style={styles.labelInput} keyboardType='number-pad' value={this.state.nominal} onChangeText={(nominal) => this.setState({ nominal: nominal })} />
                                    </Item>
                                    : null
                                }

                            </Form>
                        </View>

                        <View style={styles.checkContainer}>
                            <ListItem noBorder>
                                <CheckBox onPress={() => this.setState({ checkedFav: !this.state.checkedFav })} checked={this.state.checkedFav} />
                                <Body style={styles.checkTextContainer}>
                                    <Text style={styles.txtSetuju}> Simpan ke</Text>
                                    <Button style={{ marginLeft: 5, marginTop: -1, marginRight: 10 }} transparent>
                                        <Text style={styles.txtSyarat}>Favorit</Text>
                                    </Button>
                                </Body>
                            </ListItem>
                        </View>

                        <View style={styles.checkContainer}>
                            <ListItem noBorder>
                                <CheckBox onPress={() => this.setState({ checked: !this.state.checked })} checked={this.state.checked} />
                                <Body style={styles.checkTextContainer}>
                                    <Text style={styles.txtSetuju}> Saya menyetujui</Text>
                                    <Button style={{ marginLeft: 5, marginTop: -1, marginRight: 10 }} transparent>
                                        <Text style={styles.txtSyarat}>Syarat dan Ketentuan HasanahKu</Text>
                                    </Button>
                                </Body>
                            </ListItem>
                        </View>

                        <View style={styles.btnRegisterContainer}>
                            <Button onPress={() => this.proses()} style={styles.btnRegister}>
                                <Text style={styles.RegisterText}>Proses</Text>
                            </Button>
                        </View>


                    </View>

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    titleContainer: {
        marginTop: 10,
    },
    title: {
        fontSize: 24,
        color: '#e35200',
        paddingLeft: 30,
        paddingTop: 20,
        fontWeight: 'bold'
    },
    formContainer: {
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    infoFormTitle: {
        color: '#2a8b40',
        fontSize: 16,
        fontWeight: 'bold'
    },
    infoFormSubTitle: {
        color: '#385757',
        fontSize: 13,
        marginTop: 20,
        fontWeight: 'bold',
    },
    inputContainer: {
        margin: 20
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    labelInput: {
        fontWeight: 'bold',
        fontSize: 13,
        paddingLeft: 10
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#2a8b40',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    gridContainer: {
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    gridListContainer: {
        height: Dimensions.get('window').width / 3 - 25,
        width: Dimensions.get('window').width / 3 - 25,
        backgroundColor: '#fff',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation: 5,
        flexDirection: 'column'
    },
    gridNominalContainer: {
        height: 50,
        // width: Dimensions.get('window').width / 3 - 40,
        backgroundColor: '#fff',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 3,
        elevation: 5,
        flexDirection: 'column'
    },
    imageGrid: {
        height: Dimensions.get('window').width / 3 - 80,
        width: Dimensions.get('window').width / 3 - 80,
        resizeMode: 'contain',
        flex: 2
    },
    txtGrid: {
        // color: '#fff',
        fontSize: 13,
        fontWeight: 'bold',
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        // paddingBottom: 5,
        flex: 1,
        textAlign: 'center'
    },
    checkContainer: {
        marginTop: -20,
    },
    checkTextContainer: {
        flexDirection: 'row',
        marginRight: 20
    },
    txtSetuju: {
        marginTop: 14,
        fontSize: 12
    },
    txtSyarat: {
        fontSize: 12,
        marginTop: 2,
        color: '#385757',
        fontWeight: 'bold'
    },
})

export default Asuransi;
