import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView, ActivityIndicator } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import WebView from 'react-native-webview';

class Bantuan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bantuan: "",
            checkedSK: false,
            showSpinnerWebView: true
        };
    }

    componentDidMount() {
        this.getDataBantuan()
    }

    getDataBantuan() {
        this.setState({
            bantuan: "https://hpay.bnisyariah.co.id/mobile/faq/faqHasanahKu.html"
        })
    }

    hideSpinner() {
        this.setState({ showSpinnerWebView: false });
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>Pusat Bantuan</Text>
                        </View>
                    </View>

                    {/* <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <View style={styles.searchContainer}>
                            <Icon name="search" type="font-awesome" size={20} color="#26C165" />
                            <Input placeholder="Cari .." style={styles.input} />
                        </View>
                    </View> */}

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <ScrollView style={styles.listContainer}>
                            <WebView
                                onLoad={() => this.hideSpinner()}
                                style={styles.webView}
                                source={{ uri: this.state.bantuan }}
                                automaticallyAdjustContentInsets={false}
                            />

                            {this.state.showSpinnerWebView && (
                                <ActivityIndicator
                                    style={{ position: "absolute", top: 30, left: 0, right: 0 }}
                                    size="large"
                                />
                            )}
                        </ScrollView>
                    </View>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    Container: {
        height: '100%',
    },
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 26
    },
    searchContainer: {
        marginTop: 20,
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#F1F1F1',
        borderRadius: 30,
        width: Dimensions.get('window').width - 50,
        height: 45,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center',

        shadowColor: '#808080',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    input: {
        height: 45,
        marginLeft: 5,
        borderBottomColor: '#FFFFFF',
        flex: 1,
        color: '#000',
        fontWeight: '500',
        fontSize: 13
    },
    listContainer: {
        margin: 10
    },
    webView: {
        height: Dimensions.get('window').height - 50,
        width: Dimensions.get('window').width - 20,
        borderRadius: 20,
    },
})

export default Bantuan;
