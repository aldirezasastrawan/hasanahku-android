import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView, ActivityIndicator } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import WebView from 'react-native-webview';
import appStyle from '../../styles/AppStyle';

class WebViewRekeningOnline extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wvData: "",
            showSpinnerWebView: true
        };
    }

    componentDidMount() {
        this.getWebViewData()
    }

    getWebViewData() {
        this.setState({
            wvData: "https://bro.bnisyariah.co.id/"
        })
    }

    hideSpinner() {
        this.setState({ showSpinnerWebView: false });
    }

    render() {
        return (
            <Container>
                <Content>
                  <View style={appStyle.navBox}>
                    <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={appStyle.btnBack}>
                       <Icon name='arrowleft' color="#26C165" size={25} type='antdesign' />
                       <View style={appStyle.navBoxTitle}>
                         <Text style={appStyle.navTitle}>Buka Rekening</Text>
                       </View>
                       </Button>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <ScrollView>
                            <WebView
                                onLoad={() => this.hideSpinner()}
                                style={styles.webView}
                                source={{ uri: this.state.wvData }}
                                automaticallyAdjustContentInsets={false}
                            />

                            {this.state.showSpinnerWebView && (
                                <ActivityIndicator
                                    style={{ position: "absolute", top: 30, left: 0, right: 0 }}
                                    size="large"
                                />
                            )}
                        </ScrollView>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    webView: {
        height: Dimensions.get('window').height - 100,
        width: Dimensions.get('window').width
    },
})

export default WebViewRekeningOnline;
