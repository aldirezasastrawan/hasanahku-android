import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';
import { Container, Content, Button, Body, ListItem, List } from 'native-base';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

class Hubungikami extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <Container>
                <Content style={styles.Container}>
                    <LinearGradient locations={[0, 1.0]} colors={['#2a8b40', '#1e622d']} style={styles.header}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color='#fff' size={30} type='antdesign' />
                        </Button>
                        <Text style={styles.title}>Akun Blokir</Text>
                    </LinearGradient>
                    <List>
                        <ListItem style={styles.listHeight} button onPress={() => alert('profile')} icon>
                            <Body>
                                <Text>Bagaimana cara membuka blokir ?</Text>
                            </Body>
                        </ListItem>
                        <ListItem style={styles.listHeight} button onPress={() => this.changePin()} icon>
                            <Body>
                                <Text>Mengapa akun bisa terblokir ?</Text>
                            </Body>
                        </ListItem>
                        <ListItem style={styles.listHeight} button onPress={() => this.props.navigation.navigate('Tutupakun')} icon>
                            <Body>
                                <Text>Bagaimana cara tutup akun ?</Text>
                            </Body>
                        </ListItem>
                        <ListItem style={styles.listHeight} button onPress={() => alert('bantuan')} icon>
                            <Body>
                                <Text>Lainnya</Text>
                            </Body>
                        </ListItem>
                    </List>

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    Container: {
        height: '100%',
    },
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    header: {
        backgroundColor: '#e35200',
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    title: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 16,
        flex: 2
    },
    btnSignOut: {
        position: 'absolute',
        backgroundColor: '#e35200',
        width: 100,
        height: 35,
        marginLeft: 20,
        marginTop: 400,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtSignOut: {
        color: '#fff',
        fontWeight: '600'
    },
    listHeight: {
        height: 60
    }
})

export default Hubungikami;
