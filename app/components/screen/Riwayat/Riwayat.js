import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native';
import { Container, Content, Button, Form, Item, Label, DatePicker } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { postWithToken} from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import { getValue } from '../../../module/LocalData/Storage';
import {dialog} from '../../../supports/Dialog';
import appStyle from '../../styles/AppStyle';
import moment from 'moment';


class Riwayat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            fromDate: '',
            toDate: '',
        };
    }

    riwayat() {
        this.setState({
            showSpinner: true,
        });

        getValue('tokenLogin').then(tokenLogin => {
            let params = {
                "phone": this.props.navigation.getParam('phone'),
                "fromDate": this.state.fromDate,
                "toDate": this.state.toDate
            }

            console.log(params);

            postWithToken(Env.base_url + Api.sakuHistoryTransaction, params, tokenLogin).then(response => {
                if(response.code == ResponseCode.OK){
                    setTimeout(() => {
                        this.setState({showSpinner: false});

                        this.props.navigation.navigate('RiwayatList', {
                            "dataRiwayat": response.okContent.histories
                        })
                    }, 1000);

                } else {
                  this.setState({showSpinner: false});
                  dialog.alertFail(response.failContent, this);
                }
            }).catch(err => {
              this.setState({showSpinner: false})
              dialog.alertException(err);
            })
        })
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={appStyle.navBox}>
                      <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={appStyle.btnBack}>
                          <Icon name='arrowleft' color="#26C165" size={25} type='antdesign' />
                          <View style={appStyle.navBoxTitle}>
                            <Text style={appStyle.navTitle}>Riwayat</Text>
                          </View>
                      </Button>
                    </View>
                    <View style={styles.inputContainer}>
                        <Form>
                            <Item stackedLabel>
                                <Label style={styles.label}>Tanggal Awal</Label>
                                {/* <Input value={this.state.nama} onChangeText={(nama) => this.setState({ nama: nama })} /> */}
                                <DatePicker
                                    defaultDate={new Date()}
                                    // minimumDate={new Date(1999 - 1 - 1)}
                                    // maximumDate={new Date(2070 - 1 - 1)}
                                    locale={"en"}
                                    formatChosenDate={date => { return moment(date).format('YYYY-MM-DD') }}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText="Pilih tanggal awal"
                                    textStyle={{ color: "green" }}
                                    placeHolderTextStyle={{ color: "#d3d3d3", fontSize: 13 }}
                                    onDateChange={fromDate => this.setState({ fromDate: moment(fromDate).format('YYYY-MM-DD') })}
                                    disabled={false}
                                />
                            </Item>

                            <Item stackedLabel>
                                <Label style={styles.label}>Tanggal Akhir</Label>
                                <DatePicker
                                    defaultDate={new Date()}
                                    // minimumDate={new Date(2018, 1, 1)}
                                    // maximumDate={new Date(2018, 12, 31)}
                                    locale={"en"}
                                    formatChosenDate={date => { return moment(date).format('YYYY-MM-DD') }}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText="Pilih tanggal akhir"
                                    textStyle={{ color: "green" }}
                                    placeHolderTextStyle={{ color: "#d3d3d3", fontSize: 13 }}
                                    onDateChange={toDate => this.setState({ toDate: moment(toDate).format('YYYY-MM-DD') })}
                                    disabled={false}
                                />
                            </Item>
                        </Form>
                    </View>

                    <View style={styles.btnRegisterContainer}>
                        <Button onPress={() => this.riwayat()} style={styles.btnRegister}>
                            <Text style={styles.RegisterText}>Tampilkan Mutasi</Text>
                            {this.state.showSpinner == false ?
                                <LottieView
                                    style={{ height: 40, marginLeft: 5, transform: [{ scale: 1.5 }] }}
                                    autoSize={true}
                                    resizeMode="cover"
                                    source={require('../../../assets/animation/searching1.json')}
                                    autoPlay
                                />
                                : null}
                        </Button>
                    </View>
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/searching1.json')}
                                autoPlay
                            />
                        }
                    />

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    titleContainer: {
        marginTop: 10,
    },
    title: {
        fontSize: 24,
        color: '#2a8b40',
        paddingLeft: 30,
        paddingTop: 20,
        fontWeight: 'bold'
    },
    cardContainer: {
        margin: 20
    },
    cardWrapper: {
        height: 200,
        borderRadius: 10,
        // backgroundColor: '#e35200'
        backgroundColor: '#2a8b40'
    },
    cardTitleContainer: {
        margin: 20
    },
    cardTitle: {
        fontSize: 24,
        color: '#fff'
    },
    cardChipContainer: {
        height: 40,
        width: 70,
        borderWidth: 0.5,
        borderColor: '#dedede',
        marginLeft: 20,
        borderRadius: 5,
        backgroundColor: '#f5cf1a'
    },
    cardNumberContainer: {
        margin: 10,
        flexDirection: 'row'
    },
    cardNumber: {
        color: '#fff',
        padding: 10,
        fontSize: 20
    },
    cardOwnerContainer: {
        marginLeft: 25
    },
    cardOwner: {
        color: '#fff'
    },
    inputContainer: {
        margin: 20
    },
    checkContainer: {
        marginTop: -20,
    },
    checkTextContainer: {
        flexDirection: 'row',
        marginRight: 20
    },
    txtSetuju: {
        marginTop: 14,
        fontSize: 12
    },
    txtSyarat: {
        fontSize: 12,
        marginTop: 2,
        color: '#385757',
        fontWeight: 'bold'
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#2a8b40',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    }
})

export default Riwayat;
