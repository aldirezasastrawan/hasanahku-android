import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import moment from 'moment';
import appStyle from '../../styles/AppStyle';
import {utils} from '../../../utils/Utils';

class RiwayatList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            dataRiwayat: []
        };
    }

    componentDidMount() {
        this.getDataTransaksi()
    }

    getDataTransaksi() {
        this.setState({
            dataRiwayat: this.props.navigation.getParam('dataRiwayat')
        })
    }

    renderRiwayat = ({ item }) => {
        return (
            <View>
                <View style={{ flexDirection: 'row', width: '100%' }}>
                    {/*<View style={styles.iconContainer}>
                        <LottieView
                            style={{ height: 40, transform: [{ scale: 1.3 }] }}
                            autoSize={true}
                            resizeMode="cover"
                            source={item.transactionType.type == "TRANSFER" ? require('../../../assets/animation/topup.json') : require('../../../assets/animation/internet.json')}
                            autoPlay
                        />
                    </View>*/}

                    <TouchableOpacity activeOpacity={0.7} style={styles.listItemContainer}>
                        <View style={styles.listTitleContainer}>
                            <Text style={styles.listType}>{item.transactionType.type}</Text>
                            <Text style={styles.listDate}>{utils.dateToDDmmYYYY(item.trxdate)}</Text>
                        </View>

                        {/* <Text style={styles.listTitle}>{item.narative}</Text> */}
                        <Text style={styles.listDetail}>{item.description}</Text>
                        <View style={styles.listTitleContainer}>
                            <Text style={styles.listNominal}>Rp{utils.currencyFormat(item.nominalTransaction, 0)}</Text>
                            <Text style={[styles.listStatus, { color: item.statusTransaction.id == 1 ? "#2a8b40" : "red" }]}>{item.statusTransaction.name}</Text>
                        </View>
                    </TouchableOpacity>
                </View>


                <View
                    style={{
                        height: StyleSheet.hairlineWidth,
                        width: '100%',
                        backgroundColor: '#ebebeb',
                    }}
                />
            </View >

        )
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={appStyle.navBox}>
                      <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={appStyle.btnBack}>
                        <Icon name='arrowleft' color="#26C165" size={25} type='antdesign' />
                        <View style={appStyle.navBoxTitle}>
                          <Text style={appStyle.navTitle}>Daftar Transaksi</Text>
                        </View>
                      </Button>
                    </View>
                    <View style={styles.listContainer}>
                        <FlatList
                            data={this.state.dataRiwayat}
                            renderItem={this.renderRiwayat}
                            extraData={this.state}
                        />
                    </View>
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 20, transform: [{ scale: 3.5 }] }}
                                // autoSize={true}
                                resizeMode="cover"
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    listContainer: {
        margin: 20
    },
    listItemContainer: {
        marginLeft: 20,
        marginRight: 5,
        marginBottom: 20,
        marginTop: 20,
        flex: 2
    },
    listTitleContainer: {
        flexDirection: 'row',
    },
    listType: {
        fontSize: 15,
        flex: 2,
        fontWeight: 'bold',
        margin: 1
    },
    listDate: {
        margin: 1,
        fontSize: 13
    },
    listTitle: {
        paddingTop: 5,
        fontSize: 14,
        textTransform: 'capitalize'
    },
    listDetail: {
        fontSize: 14,
        paddingTop: 5
    },
    listNominal: {
        paddingTop: 10,
        fontSize: 13,
        fontWeight: 'bold',
        flex: 2,
    },
    iconContainer: {
        width: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    listStatus: {
        paddingTop: 10,
        fontSize: 13,
        fontWeight: 'bold',
    },
})

export default RiwayatList;
