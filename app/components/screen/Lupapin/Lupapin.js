import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, BackHandler } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { post } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {Error, ResponseCode} from '../../../supports/Constants';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';


class Lupapin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            hasVerify: false,
            hasConfirmation: false,
            confirmationNewCode: '',
            wrongCodeCount: 0,
            currentExpDate: "",
            isBanned: false,
            inputs: {
              newCode: {
                type: 'pin',
                value: ''
              },
              confirmationNewCode: {
                type: 'pin',
                value: ''
              }
            }
        };

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
        this.validate = ValidationHelper.validate;
    }

    componentWillMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
      this.finish();
      return true;
    }

    resetInputNewPinConfirmation(){
      const { inputs } = this.state;
        this.setState({
            inputs: {
              ...inputs,
              ['confirmationNewCode']: {
                ...inputs['confirmationNewCode'],
                value : '',
                error:  null
              }
            }
        });
    }

    securityCode(newPin) {

      const valid =  this.validate({ type: 'pin', value : newPin });

      if(valid !== null) return;

        this.setState({
            showSpinner: true,
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
                hasConfirmation: true
            });

        }, 1000);
    }

    securityCodeConfirm(newPinConfirmation) {

      const valid =  this.validate({ type: 'pin', value : newPinConfirmation });

      if(valid !== null) return;

      this.setState({
        showSpinner: true
      });

      let params = {
          "phone": this.props.navigation.getParam('phone'),
          "otp": this.props.navigation.getParam('otp'),
          "newPin": this.state.inputs.newCode.value,
          "reEnterNewPin": newPinConfirmation
      }

      post(Env.base_url + Api.sakuPinReset, params).then(response => {
          if(response.code == ResponseCode.OK){
              this.setState({
                  showSpinner: false,
                  hasVerify: true
              });
          } else {
              setTimeout(() => {
                this.setState({showSpinner: false});
                this.resetInputNewPinConfirmation();
                dialog.alertFail(response.failContent, this);
              }, 1000);
          }
      }).catch(err => {
            setTimeout(() => {
              this.setState({showSpinner: false});
              this.resetInputNewPinConfirmation();
              dialog.alertException(err);
            }, 1000);
      });
    }

    finish() {
      this.props.navigation.navigate(this.props.navigation.getParam('screenCallback'),{
            phone: this.props.navigation.getParam('phone')
      })
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.finish()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>PIN</Text>
                        </View>
                    </View>

                    {this.state.hasVerify == false ?

                        <View>
                            <View style={styles.infoContainer}>
                                <Text style={styles.infoTitle}>{this.state.hasConfirmation == false ? 'Masukkan PIN baru Anda' : 'Konfirmasi PIN baru Anda'}</Text>
                                <Text style={styles.infoSubTitle}>PIN digunakan untuk login ke akun Anda dan digunakan untuk bertransaksi</Text>

                            </View>
                            {this.state.hasConfirmation == false ?
                                    <View style={styles.inputContainer}>
                                        <OTPInputView
                                            style={{ width: '80%', height: 200, }}
                                            pinCount={6}
                                            code={this.state.inputs.newCode.value} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                            onCodeChanged={value => { this.onInputChange({id: 'newCode', value})}}
                                            autoFocusOnLoad
                                            secureTextEntry
                                            codeInputFieldStyle={styles.underlineStyleBase}
                                            codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                            onCodeFilled={(code => {this.securityCode(code)})}
                                        />
                                        <Text style={styles.jagaPIN}>Jaga kerahasiaan PIN Anda</Text>
                                        {this.validateError('newCode')}
                                    </View>
                                :
                                    <View style={styles.inputContainer}>
                                        <OTPInputView
                                            style={{ width: '80%', height: 200, }}
                                            pinCount={6}
                                            code={this.state.inputs.confirmationNewCode.value} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                            onCodeChanged={value => { this.onInputChange({id: 'confirmationNewCode', value})}}
                                            autoFocusOnLoad
                                            secureTextEntry
                                            codeInputFieldStyle={styles.underlineStyleBase}
                                            codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                            onCodeFilled={(code => {this.securityCodeConfirm(code)})}
                                        />
                                        <Text style={styles.jagaPIN}>Jaga kerahasiaan PIN Anda</Text>
                                        {this.validateError('confirmationNewCode')}
                                    </View>
                            }
                        </View>
                        :
                        <View>
                            <View style={styles.titleContainer}>
                                <Text style={styles.infoTitle}>Selamat yaa ...</Text>
                                <Text style={styles.infoSubTitle1}>PIN baru Anda berhasil dibuat, tetap jaga kerahasiaan PIN anda.</Text>
                                <Image style={styles.imageBig} source={require('../../../assets/image/Credit-card.jpg')} />
                            </View>

                            <View style={styles.btnRegisterContainer}>
                                <Button onPress={() => this.finish()} style={styles.btnRegister}>
                                    <Text style={styles.RegisterText}>Selesai</Text>
                                </Button>
                            </View>
                        </View>
                    }
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    infoContainer: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    infoTitle: {
        color: '#000',
        fontSize: 24,
        fontWeight: 'bold',
    },
    infoSubTitle: {
        color: '#B8B8B8',
        fontSize: 13,
        marginTop: 20,
        fontWeight: '500',
        marginRight: 5,
    },
    infoSubTitle1: {
        color: '#385757',
        fontSize: 13,
        marginTop: 10,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    borderStyleHighLighted: {
        // borderColor: "#2a8b40",
    },
    underlineStyleBase: {
        width: Dimensions.get('window').width / 8,
        height: Dimensions.get('window').width / 8,
        borderWidth: 0,
        borderBottomWidth: 2,
        backgroundColor: '#F9F9F9',
        borderRadius: 20,
        fontSize: 24
    },
    underlineStyleHighLighted: {
        // borderColor: "#2a8b40",
    },
    borderStyleBase: {
        // width: 30,
        // height: 45
    },
    titleContainer: {
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    title: {
        // fontFamily: 'Varela',
        fontSize: 16,
        fontWeight: 'bold'
    },
    imageBig: {
        height: Dimensions.get('window').height / 2 - 50,
        width: Dimensions.get('window').width - 50
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#26C165',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    infoBottomContainer: {
        margin: 20
    },
    infoBtmText: {
        fontSize: 12,
        color: '#385757'
    },
    jagaPIN: {
        textAlign: 'left',
        marginTop: -50,
        fontWeight: '800',
        fontSize: 13
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 26
    },
})

export default Lupapin;
