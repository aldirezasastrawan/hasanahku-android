import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, FlatList, TouchableOpacity, LayoutAnimation, UIManager, Platform } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { getValue } from '../../../module/LocalData/Storage';
import appStyle from '../../styles/AppStyle';

class Notification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            dataRiwayat: [],
            dataNotification: []
        };


        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }

        this.arrayholder = [];
    }

    componentDidMount() {
      //  this.getDataTransaksi()
        this.getDataNotification()
    }

    getDataTransaksi() {
        let data = [
            { type: 'Top Up', typeId: 1, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Sukses' },
            { type: 'Paket Data', typeId: 2, tanggal: '24-10-2019', title: 'Isi Paket Data Telkomsel', detail: 'Isi Saldo Telkomsel Via HasanahKu Payment', nominal: 'Rp 200.000', status: 'Sukses' },
            { type: 'Pulsa', typeId: 2, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Sukses' },
            { type: 'Transfer', typeId: 1, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Gagal' },
            { type: 'Top Up', typeId: 1, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Sukses' },
            { type: 'Top Up', typeId: 2, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Gagal' },
            { type: 'Top Up', typeId: 2, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Sukses' },
            { type: 'Top Up', typeId: 1, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Sukses' },
            { type: 'Top Up', typeId: 1, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Gagal' },
            { type: 'Top Up', typeId: 2, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Sukses' },
            { type: 'Top Up', typeId: 1, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Gagal' },
            { type: 'Top Up', typeId: 2, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Sukses' },
            { type: 'Top Up', typeId: 1, tanggal: '24-10-2019', title: 'Top Up saldo HasanahKu', detail: 'Isi Saldo HasanahKu Via Bank BNI Syariah', nominal: 'Rp 1000.000', status: 'Sukses' },
        ]

        this.setState({
            dataRiwayat: data
        })
    }

    detail(item) {

    }

    getDataNotification() {
        getValue('dataNotif').then(dataNotif => {
            console.log('data notif ', dataNotif);

            if (dataNotif == null) {
                this.setState(
                    {
                        isLoading: false,
                        dataNotification: [],
                        isFetching: false,
                    },
                    function () {
                        this.arrayholder = [];
                    },
                );
            } else {
                this.setState(
                    {
                        isLoading: false,
                        dataNotification: dataNotif,
                        isFetching: false,
                    },
                    function () {
                        this.arrayholder = dataNotif;
                    },
                );
            }
            LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        });
    }

    renderRiwayat = ({ item }) => {
        return (
            <View style={styles.listWrapper}>
                <View style={{ flexDirection: 'row', width: '100%' }}>
                  { /* <View style={styles.iconContainer}>
                        <LottieView
                            style={{ height: 40, transform: [{ scale: 1.3 }] }}
                            autoSize={true}
                            resizeMode="cover"
                            source={require('../../../assets/animation/topup.json')}
                            autoPlay
                        />
                    </View> */}

                    <TouchableOpacity activeOpacity={0.7} style={styles.listItemContainer}>
                        <View style={styles.listTitleContainer}>
                            <Text style={styles.listType}>{item.title}</Text>
                        </View>

                        <Text style={styles.listDetail}>{item.body == null ? item.message : item.body}</Text>
                    </TouchableOpacity>
                </View>

                <View
                    style={{
                        height: StyleSheet.hairlineWidth,
                        width: '100%',
                        backgroundColor: '#ebebeb',
                    }}
                />
            </View >

        )
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={appStyle.navBox}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={appStyle.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={25} type='antdesign' />
                            <View style={appStyle.navBoxTitle}>
                              <Text style={appStyle.navTitle}>Notifikasi</Text>
                            </View>
                        </Button>
                    </View>
                    <View style={styles.listContainer}>
                        <FlatList
                            data={this.state.dataNotification.reverse()}
                            renderItem={this.renderRiwayat}
                            extraData={this.state}
                        />
                    </View>
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 20, transform: [{ scale: 3.5 }] }}
                                // autoSize={true}
                                resizeMode="cover"
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    listWrapper: {

    },
    listContainer: {
        margin: 20
    },
    listItemContainer: {
        marginLeft: 20,
        marginRight: 5,
        marginBottom: 20,
        marginTop: 20,
        flex: 2
    },
    listTitleContainer: {
        flexDirection: 'row',
    },
    listType: {
        fontSize: 15,
        flex: 2,
        fontWeight: 'bold',
        margin: 1
    },
    listDetail: {
        fontSize: 14,
        paddingTop: 5
    },
    iconContainer: {
        width: 40,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default Notification;
