import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Container, Content, Button, Form, Item, Picker } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { post} from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {Error, ResponseCode} from '../../../supports/Constants';
import {DialogBox} from '../../../supports/DialogBox';
import {LogMe} from '../../../supports/LogMe';

class TopUpChannel extends Component {
    constructor(props) {
        super(props);
        this.state = {
          nominal : 0
        };

        this.dialogShow = DialogBox.dialogShow.bind(this);
        this.onlyDialogShow = DialogBox.onlyDialogShow;
    }

    proses() {

      if(this.state.nominal < 1){
        this.onlyDialogShow("Silakan pilih nominal top up!");
        return;
      }

      this.setState({
        showSpinner: true,
      });

      let params = {
        "phone": this.props.navigation.getParam('phone'),
        "providerVa": "BNI",
        "trxAmount": this.state.nominal
      }

      post(Env.base_url + Api.createBilling, params).then(response => {
        if(response.code == ResponseCode.OK){
          this.setState({
            showSpinner: false
          });
          this.props.navigation.navigate('TopUp',{
            "phone": this.props.navigation.getParam('phone'),
            "va" : response.okContent.va
          });
        } else {
          this.dialogShow(response.failContent.failDescription, false);
        }
      }).catch(error => {
        LogMe.error("createBilling", JSON.stringify(error));
        this.dialogShow(Error.timeout, false);
      });
    }

    render() {
        return (
            <Container>
                <Content>
                <View style={styles.imageLogoContainer}>
                    <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                        <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                    </Button>
                    <View style={styles.titleNavContainer}>
                        <Text style={styles.titleNav}>HasanahKu</Text>
                    </View>
                </View>
                <View>
                <View style={styles.inputContainer}>
                  <Form>
                    <Item>
                      <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      placeholder="Pilih Nominal"
                      selectedValue={this.state.nominal}
                      onValueChange={(itemValue, itemIndex) => this.setState({nominal: itemValue})}>
                      <Picker.Item label="Pilih Nominal" value="0" />
                      <Picker.Item label="10.000" value="10000" />
                      <Picker.Item label="50.000" value="50000" />
                      <Picker.Item label="100.000" value="100000" />
                      <Picker.Item label="200.000" value="200000" />
                      <Picker.Item label="500.000" value="500000" />
                      <Picker.Item label="1.000.000" value="1000000" />
                      <Picker.Item label="5.000.000" value="5000000" />
                      <Picker.Item label="10.000.000" value="10000000" />
                      </Picker>
                    </Item>
                  </Form>
                </View>
                <View style={styles.btnRegisterContainer}>
                  <Button onPress={() => this.proses()} style={styles.btnRegister}>
                    <Text style={styles.RegisterText}>Proses</Text>
                    </Button>
                </View>
                </View>
                <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
  titleNavContainer: {
      flex: 7,
      justifyContent: 'center',
      alignItems: 'flex-start',
  },
  titleNav: {
      color: '#26C165',
      fontWeight: 'bold',
      fontSize: 26
  },
  btnBack: {
      flex: 1,
      marginLeft: 20,
      marginTop: 5
  },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    title: {
        fontSize: 24,
        color: '#e35200',
        paddingLeft: 30,
        paddingTop: 20,
        fontWeight: 'bold'
    },
    formContainer: {
        marginTop: 20,
        marginLeft: 20,
        marginRight: 20,
    },
    infoFormTitle: {
        color: '#2a8b40',
        fontSize: 16,
        fontWeight: 'bold'
    },
    infoFormSubTitle: {
        color: '#385757',
        fontSize: 13,
        marginTop: 20,
        fontWeight: 'bold',
    },
    inputContainer: {
        margin: 20
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    labelInput: {
        fontWeight: 'bold',
        fontSize: 13,
        paddingLeft: 10
    },
    labelBr : {
      marginBottom : 20
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#2a8b40',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    }
})

export default TopUpChannel;
