import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Clipboard, FlatList, Image } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import TopUpAccordion from '../../common/TopUpAccordion/TopUpAccordion';
import {toast} from '../../../supports/Toast';

class TopUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasHubungiKami: false,
            showSpinner: false,
            colorSpinner: '#e35200',
            dataTopUp: [],
            dataTopUpLainnya: [],
            expanded: false,
            dataUser: {},
            dataVa: ''
        };
    }

    componentDidMount() {
        this.getDataTopUplainnya()
    }

    copyVa(va) {
        Clipboard.setString(va);   
         toast.show('Nomor VA berhasil disalin')
      //  alert('Nomor VA berhasil disalin')
    }

    getDataTopUplainnya() {
        let data = [
            {
                typeId: 1, name: 'Mobile Banking',
                icon: 'tablet', iconType: 'font-awesome', iconSize: 40,
                expanded: false,
                detail: [
                    { nama: 'Akses BNI Mobile Banking dari handphone kemudian masukkan userID dan password', step: '1' },
                    { nama: 'Pilih Menu Transfer', step: '2' },
                    { nama: 'Pilih "Antar Rekening BNI" kemudian "Input Rekening Baru"', step: '3' },
                    { nama: 'Masukkan Nomor Rekening Debit dan nomor Virtual Account Tujuan (contoh: 082390819xxxxxx)', step: '4' },
                    { nama: 'Masukkan nominal transfer sesuai dengan keinginan Anda', step: '5' },
                    { nama: 'Konfirmasi, apabila telah sesuai, lanjutkan transaksi', step: '6' },
                    { nama: 'Transfer Anda Telah Berhasil', step: '7' },
                ]
            },
            {
                typeId: 2, name: 'ATM',
                icon: 'credit-card', iconType: 'font-awesome', iconSize: 30,
                expanded: false,
                detail: [
                    { nama: 'Masukkan Kartu Anda', step: '1' },
                    { nama: 'Pilih Bahasa', step: '2' },
                    { nama: 'Masukkan PIN ATM Anda', step: '3' },
                    { nama: 'Pilih "Menu Lainnya" ', step: '4' },
                    { nama: 'Pilih "Transfer" ', step: '5' },
                    { nama: 'Pilih "Rekening Tabungan" ', step: '6' },
                    { nama: 'Pilih "Ke rekening BNI" ', step: '7' },
                    { nama: 'Masukkan nomor rekening tujuan dengan 16 digit Nomor Virtual Account (contoh: 082390819xxxxxx)', step: '8' },
                    { nama: 'Masukkan nominal transfer sesuai dengan keinginan Anda', step: '9' },
                    { nama: 'Konfirmasi, apabila telah sesuai, lanjutkan transaksi', step: '10' },
                    { nama: 'Transaksi telah selesai', step: '11' },
                ]
            },
            {
                typeId: 3, name: 'SMS Banking',
                icon: 'message-text-outline', iconType: 'material-community', iconSize: 30,
                expanded: false,
                detail: [
                    { nama: 'Buka aplikasi SMS Banking BNI', step: '1' },
                    { nama: 'Pilih menu Transfer', step: '2' },
                    { nama: 'Pilih menu Trf rekening BNI', step: '3' },
                    { nama: 'Masukkan nomor rekening tujuan dengan 16 digit Nomor Virtual Account (contoh: 082390819xxxxxx)', step: '4' },
                    { nama: 'Masukkan nominal transfer sesuai dengan keinginan Anda', step: '5' },
                    { nama: 'Konfirmasi, apabila telah sesuai, lanjutkan transaksi', step: '6' },
                    { nama: 'Transaksi telah selesai', step: '7' },
                ]
            },
            {
                typeId: 4, name: 'Internet Banking',
                icon: 'web', iconType: 'material-community', iconSize: 30,
                expanded: false,
                detail: [
                    { nama: 'Ketik alamat https://ibank.bni.co.id kemudian klik "Enter"', step: '1' },
                    { nama: 'Masukkan User ID dan Password', step: '2' },
                    { nama: 'Pilih menu "Transfer" kemudian pilih "TAMBAH REKENING FAVORIT". Jika menggunakan desktop untuk menambah rekening, pada menu "Transaksi" lalu pilih "Info & Administrasi Transfer" kemudian "Atur Rekening Tujuan" lalu "Tambah Rekening Tujuan"', step: '3' },
                    { nama: 'Masukkan nomor rekening tujuan dengan 16 digit Nomor Virtual Account (contoh: 8239081911xxxxx)', step: '4' },
                    { nama: 'Masukkan Kode Otentikasi Token. Nomor rekening tujuan berhasil ditambahkan', step: '5' },
                    { nama: 'Kembali ke menu "TRANSFER". Pilih "TRANSFER ANTAR REKENING BNI", kemudian pilih rekening tujuan', step: '6' },
                    { nama: 'Pilih Rekening Debit dan ketik nominal transfer sesuai dengan keinginan Anda', step: '7' },
                    { nama: 'Lalu Masukkan kode otentikasi token', step: '8' },
                    { nama: 'Transfer Anda telah selesai', step: '9' },
                ]
            },
            {
                typeId: 5, name: 'Teller BNI Syariah',
                icon: 'institution', iconType: 'font-awesome', iconSize: 30,
                expanded: false,
                detail: [
                    { nama: 'Masukkan Kartu Anda', step: '1' },
                    { nama: 'Pilih Bahasa', step: '2' },
                    { nama: 'Masukkan PIN ATM Anda', step: '3' },
                    { nama: 'Pilih "Menu Lainnya" ', step: '4' },
                    { nama: 'Pilih "Transfer" ', step: '5' },
                    { nama: 'Pilih "Rekening Tabungan" ', step: '6' },
                    { nama: 'Pilih "Ke rekening BNI" ', step: '7' },
                    { nama: 'Masukkan nomor rekening tujuan dengan 16 digit Nomor Virtual Account (contoh: 8239081911xxxxx)', step: '8' },
                    { nama: 'Masukkan nominal transfer sesuai dengan keinginan Anda', step: '9' },
                    { nama: 'Konfirmasi, apabila telah sesuai, lanjutkan transaksi', step: '10' },
                    { nama: 'Transaksi telah selesai', step: '11' },
                ]
            }
        ]

        this.setState({
            dataTopUpLainnya: data
        })
    }

    // getDataTopUp() {
    //     let data = [
    //         {
    //             typeId: 1, name: 'Kartu Debit', type: 'menu', page: 'TopUpDebit',
    //             subTitle: 'Lakukan Top Up saldo HasanahKu melalui Kartu Debit milikmu',
    //             image: require('../../../assets/image/payment-method.png'),
    //         },
    //         {
    //             typeId: 2, name: 'Kartu Kredit', type: 'menu', page: 'TopUpKredit',
    //             subTitle: 'Lakukan Top Up saldo HasanahKu melalui Kartu Kredit milikmu',
    //             image: require('../../../assets/image/credit-card.png'),
    //         },
    //     ]

    //     this.setState({
    //         dataTopUp: data
    //     })
    // }

    selectType(item) {
        this.props.navigation.navigate('TopUpDetail', {
            item: item
        })
    }

    lihatDetail(item) {
        this.props.navigation.navigate(item)
    }

    renderTopUp = ({ item }) => {
        return (
            <View onPress={() => this.selectType(item)} style={styles.gridListContainer}>
                <View style={styles.imageGridContainer}>
                    <Image style={styles.imageGrid} source={item.image} />
                </View>
                <Text style={styles.txtGrid}>{item.name}</Text>
                <Text style={styles.txtSubGrid}>{item.subTitle}</Text>
                <Button onPress={() => this.lihatDetail(item.page)} style={styles.btnLihatDetail}>
                    <Text style={styles.txtLihat}>Lihat caranya</Text>
                </Button>
            </View>
        )
    }

    renderTopUpLainnya = ({ item }) => {
        return (
            <TopUpAccordion icon={item.icon} iconType={item.iconType} name={item.name} data={item.detail} iconSize={item.iconSize} />
        )
    }

    toggleExpand = () => {
        this.setState({ expanded: !this.state.expanded })
    }



    render() {
      //  const dataUser = this.state.dataUser
      //  const va = this.state.dataVa
        const va = this.props.navigation.getParam('vaBNI')
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>Top Up Saldo</Text>
                        </View>
                    </View>

                    <View style={styles.imageTitleContainer}>
                        <Image style={styles.imageTitle} source={require('../../../assets/image/pay.jpeg')} />
                    </View>

                    {/* <View style={styles.cardTitleContainer}>
                        <View style={styles.infoCardContainer}>
                            <Text style={styles.infoCardTitle}>
                                Balance
                        </Text>
                            <Text style={styles.infoCardAmount}>
                                15.000.000
                        </Text>
                        </View>
                        <View style={styles.infoCardSubContainer}>
                            <View style={styles.infoSubContainer}>
                                <Text style={styles.infoCardSub}>Rupiah</Text>

                            </View>
                        </View>
                    </View> */}

                    <View style={styles.VaContainer}>
                        <Text style={styles.VaTitle}>Nomor Virtual Account</Text>
                        <View style={styles.salinContainer}>
                            <Text style={styles.VaNumber}>{va == null ? "Va belum tersedia" : va}
                            </Text>
                            {va != null ?
                                <Button onPress={() => this.copyVa(va)} style={styles.btnSalin}>
                                    <Text style={styles.txtSalin}>Salin</Text>
                                </Button>
                                :
                                null}
                        </View>
                        <Text style={styles.VaDetail}>Maksimum Saldo adalah Rp. 10.000.000 Akumulasi mutasi saldo setiap bulan adalah Rp. 20.000.000</Text>
                    </View>

                    {/* <View style={styles.CaraContainer}>
                        <Text style={styles.cara}>Cara Top Up Via Aplikasi</Text>
                    </View>

                    <View style={styles.listContainer}>
                        <FlatList
                            data={this.state.dataTopUp}
                            renderItem={this.renderTopUp}
                            extraData={this.state}
                            horizontal
                        />
                    </View> */}

                    {/* <View style={styles.CaraContainer}>
                        <Text style={styles.cara}>Cara Top Up</Text>
                    </View> */}

                    <View style={styles.listTopLainnyaContainer}>
                        <FlatList
                            data={this.state.dataTopUpLainnya}
                            renderItem={this.renderTopUpLainnya}
                            extraData={this.state}
                        />
                    </View>

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const size = Dimensions.get('window')

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    imageTitleContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageTitle: {
        height: size.height / 4,
        width: size.width - 50,
        resizeMode: 'contain'
    },
    cardTitleContainer: {
        margin: 20,
        backgroundColor: '#F5F3F5',
        borderRadius: 20,
        height: size.height / 8,
        flexDirection: 'row',
        alignItems: 'center'
    },
    infoCardContainer: {
        padding: 20,
        flex: 2,
    },
    infoCardSubContainer: {
        flex: 2,
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoCardTitle: {
        fontSize: 18
    },
    infoCardAmount: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    infoSubContainer: {
        backgroundColor: '#6B3BED',
        borderRadius: 25,
        marginTop: 20,
    },
    infoCardSub: {
        padding: 5,
        color: '#fff',
        width: 100,
        textAlign: 'center'
    },
    VaContainer: {
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FCFBFC',
        padding: 20,
        borderRadius: 10
    },
    VaTitle: {
        fontSize: 18,
        fontWeight: '500',
    },
    VaDetail: {
        color: '#B8B8B8',
        paddingTop: 15,
        textAlign: 'center'
    },
    VaNumber: {
        paddingTop: 20,
        paddingLeft: 10,
        fontWeight: '500',
        color: '#26C165',
        paddingRight: 10,
        fontSize: 18
    },
    salinContainer: {
        flexDirection: 'row'
    },
    btnSalin: {
        backgroundColor: '#6B3BED',
        borderRadius: 25,
        marginTop: 18,
        height: 30,
        width: 80,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtSalin: {
        color: '#fff',
    },
    CaraContainer: {
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10
    },
    cara: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    listContainer: {
        margin: 20
    },
    listTopLainnyaContainer: {
        margin: 20
    },
    gridListContainer: {
        height: size.height / 3.5,
        width: size.width - (size.width / 3),
        backgroundColor: '#FBFAFB',
        borderRadius: 10,
        margin: 5,
        flexDirection: 'column'
    },
    listItemContainer: {
        width: size.width - 50,
        // backgroundColor: '#dedede',
        margin: 5
    },
    row: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row'
    },
    listItemName: {
        flex: 1,
        color: '#000'
    },
    imageGridContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageGrid: {
        height: 100,
        width: 100,
        resizeMode: 'cover',
    },
    txtGrid: {
        fontSize: 18,
        fontWeight: 'bold',
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 5,
        textAlign: 'center',
    },
    txtSubGrid: {
        fontWeight: '500',
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 5,
        flex: 2,
    },
    btnLihatDetail: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#6B3BED',
        borderTopRightRadius: 0,
        borderTopLeftRadius: 0
    },
    txtLihat: {
        color: '#fff'
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 21
    },
})

export default TopUp;
