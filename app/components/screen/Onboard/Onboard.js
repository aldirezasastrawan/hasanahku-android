import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Content, Button } from 'native-base';
import Swiper from 'react-native-swiper'
import { Dimensions } from 'react-native';
import { saveData, getValue } from '../../../module/LocalData/Storage';

class Onboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentIndex: 0
        };
    }

    componentWillMount() {
        getValue('onBoard').then(onBoard => {
            if (onBoard == false) {
                this.props.navigation.replace('Welcome')
            }
        })
    }


    renderItem = ({ item }) => {
        return (
            <View>
                <Text>aa</Text>
            </View>
        )
    }

    btnPressed() {
        if (this.state.currentIndex != 2) {
            this.swiper.scrollBy(2)
        } else {
            this.props.navigation.replace('Welcome')
            saveData('onBoard', false)
        }
    }

    render() {
        return (
            <Content>
                <View style={styles.carouselSection}>
                    <Swiper
                        paginationStyle={{ position: 'absolute', bottom: -40 }}
                        ref={ref => { this.swiper = ref }}
                        onIndexChanged={(index) => this.setState({ currentIndex: index })}
                        style={styles.wrapper}
                        showsButtons={false}
                        loop={false}>
                        <View style={styles.slide1}>
                            <Image style={styles.imgBoard} source={require('../../../assets/image/onb1.jpg')} />
                            <View style={styles.infoBoardContainer}>
                                <Text style={styles.text}>Easy & Fast Transaction</Text>
                                <Text style={styles.subtext}>HasanahKu memberikan <Text style={styles.highlightText}>kemudahan</Text> untuk bertransaksi dengan mengutamakan kecepatan <Text style={styles.highlightText}>proses</Text></Text>

                            </View>
                        </View>
                        <View style={styles.slide1}>
                            <Image style={styles.imgBoard} source={require('../../../assets/image/onb2.jpg')} />
                            <View style={styles.infoBoardContainer}>
                                <Text style={styles.text}>Trusted</Text>
                                <Text style={styles.subtext}>HasanahKu bekerja sama dengan berbagai merchant di Indonesia dan diakui sebagai Dompet Online yang <Text style={styles.highlightText}>terpercaya</Text></Text>

                            </View>
                        </View>
                        <View style={styles.slide1}>
                            <Image style={styles.imgBoard} source={require('../../../assets/image/onb3.jpg')} />
                            <View style={styles.infoBoardContainer}>
                                <Text style={styles.text}>Safe & Secure</Text>
                                <Text style={styles.subtext}>Dengan didukung Sistem Bank BNI Syariah, HasanahKu memiliki sistem yang sangat baik dan tentunya <Text style={styles.highlightText}>aman</Text></Text>

                            </View>
                        </View>
                    </Swiper>

                </View>
                <View style={styles.btnMulaiContainer}>
                    <Button style={styles.btnMulai} onPress={() => this.btnPressed()}>
                        <Text style={styles.txtMulai}>{this.state.currentIndex == 2 ? "Start" : "Skip"}</Text>
                    </Button>
                </View>
            </Content>
        );
    }
}

const size = Dimensions.get('window');

const styles = StyleSheet.create({
    btnMulaiContainer: {
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnMulai: {
        backgroundColor: '#26C165',
        justifyContent: 'center',
        alignItems: 'center',
        width: 200,
        borderRadius: 50,
        marginTop: 100
    },
    txtMulai: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 14
    },
    wrapper: {
        height: size.height / 2 + 100
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff'
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5'
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9'
    },
    text: {
        color: '#000',
        fontSize: 27,
        fontWeight: 'bold'
    },
    subtext: {
        fontSize: 15,
        marginTop: 30,
    },
    highlightText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#32ce32',
        textTransform: 'capitalize'
    },
    imgBoard: {
        flex: 2,
        width: '100%',
        resizeMode: 'contain'
    },
    infoBoardContainer: {
        flex: 1,
        margin: 20
    }
})

export default Onboard;
