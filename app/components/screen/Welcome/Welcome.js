import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import { Container, Content, Button, Input, Item } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import { post } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import { ResponseCode} from '../../../supports/Constants';
import { getValue } from '../../../module/LocalData/Storage';
import LottieView from 'lottie-react-native';
import DeviceInfo from 'react-native-device-info';
import { pushNotifications } from '../../../module/Notification';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';
import moment from 'moment';

class Welcome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            deviceId: "",
            versionName: "",
            inputs: {
              noHp: {
                type: 'phoneNumber',
                value: ''
              }
            }
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
    }

    componentWillMount() {
        getValue('hasLogin').then(hasLogin => {
            if (hasLogin == true) {
                this.props.navigation.navigate('Home2')
            }
        })
    }

    componentDidMount() {
        // this.startVoice();
        this.getdeviceId()
        this.getVersionName()
    }

    getVersionName(){
      this.setState({
          versionName: DeviceInfo.getVersion()
      })
    }

    getdeviceId() {
        //Getting the Unique Id from here
        var id = DeviceInfo.getUniqueId();
        this.setState({ deviceId: id });
    };

    //showAlert = (title, message) => {
  //      pushNotifications.localNotification(title, message, title, message);
        // pushNotifications.localNotificationIOS(title, message, title, message);
  //  };

    login() {
      if(this.isValidate() === false) return;

      this.setState({
        showSpinner: true
      });

      let params = {
        phone: this.state.inputs.noHp.value,
        device: this.state.deviceId,
        versionName : this.state.versionName
      }

      post(Env.base_url + Api.loginDevice, params).then(response => {

        this.setState({showSpinner: false});

        if(response.code == ResponseCode.OK){
          if(response.okContent.option == "V"){
            this.props.navigation.navigate('Login', {
                "phone": this.state.inputs.noHp.value
            })
          }else{
            this.props.navigation.navigate('VerificationUserExist', {
                "phone": this.state.inputs.noHp.value
            });
          }
        }else{
          if(response.failContent.failCode == "app.device.other.error"){
            dialog.alertCallback(response.failContent.failDescription, () =>
              this.props.navigation.navigate('VerifyLoginOtherDevice', {
                  "phone": this.state.inputs.noHp.value,
                  "device": this.state.deviceId
              })
            )
          }else{
            dialog.alertFail(response.failContent, this);
          }
        }
      }).catch(err => {
        this.setState({showSpinner: false});
        dialog.alertException(err);
      });
    }

    register() {
      this.props.navigation.navigate('Register', {
          phone: this.state.inputs.noHp.value
      });
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Image style={styles.imageBig} source={require('../../../assets/image/welcome.jpg')} />
                    </View>
                    <View>
                        <View style={styles.titleContainer}>
                            <Text style={styles.title}>Selamat datang</Text>
                            <Text style={styles.subTitle}>HasanahKu membuat transaksi kamu jadi lebih mudah, lebih aman, dan lebih cepat</Text>
                        </View>

                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <View style={styles.phoneinfoContainer}>
                                <Item style={styles.phoneinfo}>
                                    <Icon containerStyle={styles.phoneIcon} name="phone" type="material-community" active />
                                    <Input keyboardType='numeric' maxLength={14} minLength={10} onChangeText={value => this.onInputChange({id: 'noHp', value})} placeholder='Masukkan Nomor HP Anda' />
                                </Item>
                                {this.validateError('noHp')}
                            </View>
                        </View>

                        <View style={styles.btnLoginContainer}>
                            <Button onPress={() => this.login()} style={styles.btnLogin}>
                                <Text style={styles.loginText}>Login</Text>
                            </Button>
                        </View>

                        <View style={styles.atauContainer}>
                            <View style={styles.divider}></View>
                            <Text>Belum punya akun?</Text>
                            <View style={styles.divider}></View>
                        </View>

                        <View style={styles.btnRegisterContainer}>
                            <Button onPress={() => this.register()} style={styles.btnRegister}>
                                <Text style={styles.RegisterText}>Register</Text>
                            </Button>
                        </View>
                    </View>

                    <Spinner
                        cancelable={true}
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />

                </Content>
            </Container>
        );
    }
}

const size = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageLogoContainer: {
        // flex: 1
        height: size.height / 2
    },
    titleContainer: {
        marginLeft: 20,
        marginRight: 20
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    subTitle: {
        paddingTop: 10
    },
    imageBig: {
        resizeMode: 'stretch',
        height: size.height / 2,
        width: size.width
    },
    phoneinfoContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: Dimensions.get('window').width - 80,
        marginTop: 30
    },
    phoneinfo: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    phoneIcon: {
        marginTop: 3,
        marginRight: 10
    },
    phone: {
        fontWeight: 'bold',
        fontSize: 14,
        // fontFamily: 'Varela',
    },
    btnLoginContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnLogin: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        // backgroundColor: '#e35200',
        backgroundColor: '#15c159',
        justifyContent: 'center'
    },
    loginText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#fff',
        justifyContent: 'center',
        borderColor: '#15c159',
        borderWidth: 1
    },
    RegisterText: {
        color: '#15c159',
        fontWeight: 'bold'
    },
    atauContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    divider: {
        height: 1,
        backgroundColor: '#dedede',
        width: 80,
        margin: 10
    }
})

export default Welcome;
