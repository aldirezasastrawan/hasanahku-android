import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, FlatList, TouchableOpacity, Image, ImageBackground, RefreshControl } from 'react-native';
import { Content, Container, Button, Card, Footer, FooterTab, Text } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { post, postWithToken, get } from '../../../client/RestClient';
import {ResponseCode} from '../../../supports/Constants';
import { getValue, saveData } from '../../../module/LocalData/Storage';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {IconMenu} from '../../../supports/IconMenu';
import {dialog} from '../../../supports/Dialog';

class Home2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataUser: {},
            dataFeature: [],
            dataCarousel: [],
            isRefreshing: false,
            isLoadingFeature: true,
            showSpinner: false,
            connection_Status: ''
        };
    }

    componentDidMount() {
      console.log("componentDidMount is call...")
        this.getDataFeature()
        this.getDataCarousel()
        this.getDataUser()
    }

    onRefresh() {
      console.log("on refresh is call...")
        //Clear old data of the list
        this.setState({
            dataFeature: [],
            dataCarousel: [],
            isRefreshing: true
        });
        //Call the Service to get the latest data
        this.getDataFeature()
        this.getDataCarousel()
        this.getDataUser()
    }

    getDataUser() {
      getValue('userLogin').then(userLogin => {
        getValue('tokenLogin').then(tokenLogin => {
          this.setState({isRefreshing: true});

          let params = {
              "phone": userLogin.noHP
          }

          postWithToken(Env.base_url + Api.accountInfo, params, tokenLogin).then(response => {
            if(response.code == ResponseCode.OK){
              this.setState({
                  dataUser: response.okContent,
                  isRefreshing: false
              })

              saveData('userLogin', response.okContent);//refresh userLogin data
            }else{
              //init state user data based on user login
              this.onChangeStateUserData(() => {
                this.setState({
                  isRefreshing: false
                });
                dialog.alertFail(response.failContent, this)
              })
            }
          }).catch(err => {
            this.onChangeStateUserData(() => {
              this.setState({
                isRefreshing: false
              });
              dialog.alertException(err)
            })
          })
        })
      })
    }

    getDataFeature() {
      this.setState({
          isLoadingFeature: true
      })
      get(Env.base_url + Api.getAllMenu).then(response => {
          if(response.code == ResponseCode.OK){

              this.setState({
                  dataFeature: response.okContent.data.slice(0, 8),
                  isLoadingFeature: false
              })
          } else {
              this.setState({
                  dataFeature: [],
                  isLoadingFeature: false
              })
          }
      }).catch(err => {
        this.setState({
            dataFeature: [],
            isLoadingFeature: false
        })
      });

    }

    getDataCarousel() {
        let data = [
            { title: 'Kode Transfer BNI Syariah', menu: 'Transfer', image: require('../../../assets/image/transfer.jpg'), description: 'Mau transfer ke BNI Syariah? Gunakan kode transfer 427' },
            { title: 'Asuransi melalui HasanahKu', menu: 'Asuransi', image: require('../../../assets/image/donasi.jpg'), description: 'Hasanahku bekerja sama dengan berbagai Asuransi terbaik' },
            { title: 'Kemudahan Pembayaran ', menu: 'Paketdata', image: require('../../../assets/image/transfer1.jpg'), description: 'Pembayaran menjadi lebih mudah melalui HasanahKu Mobile App' },
            { title: 'Top Up HasanahKu', menu: 'TopUp', image: require('../../../assets/image/credit1.jpg'), description: 'Dapatkan berbagai Promo menarik di HanasahKu' },
        ]

        this.setState({
            dataCarousel: data
        })
    }

    onChangeStateUserData(callback){
      if(JSON.stringify(this.state.dataUser) === JSON.stringify({})){
        getValue('userLogin').then(userLogin => {
          this.setState({
              dataUser: userLogin
          })
          callback();
        })
      }else{
        callback();
      }
    }

    transfer() {
      this.onChangeStateUserData(() => {
        this.props.navigation.navigate('Transfer', {
            "userLogin": this.state.dataUser
        })
      })
    }

    tarikTunai() {
      //alert("Menu not available");
      this.onChangeStateUserData(() => {
        this.props.navigation.navigate('Tariksaldo', {
            "phone": this.state.dataUser.noHP
        })
      })
    }

    riwayat() {
      this.onChangeStateUserData(() => {
        this.props.navigation.navigate('Riwayat', {
            "phone": this.state.dataUser.noHP
        })
      })
    }

    generateVA(callback){
      this.onChangeStateUserData(() => {
        getValue('tokenLogin').then(tokenLogin => {
          this.setState({showSpinner: true});

          let params = {
              "phone": this.state.dataUser.noHP
          }

          postWithToken(Env.base_url + Api.generateUserVA, params, tokenLogin).then(response => {
            this.setState({showSpinner: false})
            if(response.code == ResponseCode.OK){
              callback(response.okContent)
            }else{
              dialog.alertFail(response.failContent, this)
            }
          }).catch(err => {
            this.setState({showSpinner: false});
            dialog.alertException(err)
          })
        })
      })
    }

    topUp() {

      const vaContents = this.state.dataUser.vaContents;

      if(vaContents.length > 0){
        if(vaContents[0].companyPublisherVA.publisherVaId =="BNI"){
          if(vaContents[0].vaStatus == "CREATED"){
            this.props.navigation.navigate('TopUp', {
              "vaBNI": vaContents[0].va
            });
          }else{
            this.generateVA((response) => {
              this.props.navigation.navigate('TopUp', {
                "vaBNI": response.va
              });
            })
          }
        }
      }else{
        this.generateVA((response) => {
          this.props.navigation.navigate('TopUp', {
            "vaBNI": response.va
          });
        })
      }
        //this.props.navigation.navigate('TopUpChannel',{
        //"phone": this.state.dataUser.noHP
    //  }); */
    }

    notif() {
      this.props.navigation.navigate('Notification')
    }

    chat() {
        alert("Menu not available");
        //this.props.navigation.navigate('Chat');
    }

    scan() {
        alert("Menu not available");
        //this.props.navigation.navigate('Scan');
    }

    akun() {
      this.onChangeStateUserData(() =>{
        this.props.navigation.navigate('Akun', {
          "phone": this.state.dataUser.noHP
        })
      })
      //
    }

    pln() {
      this.props.navigation.navigate('Pln', {
          "phone": this.state.dataUser.noHP
      })
    }

    paketData() {
        alert("Menu not available");
        //this.props.navigation.navigate('Paketdata');
    }

    pulsa() {
      this.props.navigation.navigate('Pulsa',{
        'phone' : this.state.dataUser.noHP,
        'jenis' : 'pulsa'
      })
    }

    asuransi() {
        alert("Menu not available");
        //this.props.navigation.navigate('Asuransi')
    }

    air() {
        alert("Menu not available");
        //this.props.navigation.navigate('Air')
    }

    topUpUe() {
        alert("Menu not available");
        //this.props.navigation.navigate('Topupue');
    }

    zakatWakaf() {
      alert("Menu not available");
      //this.props.navigation.navigate('Zakatwakaf')
    }

    listMenu() {
      alert("Menu not available");
      //.props.navigation.navigate('ListMenu')
    }

    gotoFeatureMenu(menu) {
        switch (menu) {
            case 'topup':

                this.topUp()

                break;
            case 'transfer':

                this.transfer()

                break;
            case 'tariktunai':

                this.tarikTunai()

                break;
            case 'pln':

                this.pln()
                break;

            case 'paketdata':

                this.paketData()
                break;

            case 'pulsa':

                this.pulsa()
                break;

            case 'asuransi':

                this.asuransi()
                break;

            case 'air':

                this.air()
                break;

            case 'wallet':

                this.topUpUe()
                break;

            case 'zakat':

                this.zakatWakaf()
                break;

            default:

                // alert(menu)
                this.listMenu()
                break;
        }
    }

    renderPlaceholderFeature() {
        return (
            <SkeletonPlaceholder>
                <View style={{
                    height: 40,
                    width: '100%'
                }}>

                </View>
            </SkeletonPlaceholder>
        )
    }

    renderFeature = ({ item, index }) => {
        if (index != 7) {
            return (
                <TouchableOpacity onPress={() => this.gotoFeatureMenu(item.menuId)} activeOpacity={0.7} style={styles.featureIconContainer}>
                    <View style={[styles.iconContainer]}>
                        <Image style={{height : 50, width: 50}} source={IconMenu.selected(item.icon)}/>
                    </View>
                    <Text style={styles.txtFeature}>{item.menuName}</Text>
                </TouchableOpacity>
            )
        } else {
            return (
                <TouchableOpacity onPress={() => this.gotoFeatureMenu("lainnya")} activeOpacity={0.7} style={styles.featureIconContainer}>
                    <View style={[styles.iconContainer]}>
                        <Image style={{height : 50, width: 50}} source={IconMenu.selected('lainnya')}/>
                    </View>
                    <Text style={styles.txtFeature}>Lainnya</Text>
                </TouchableOpacity>
            )
        }
    }

    renderCarousel = ({ item, index }) => {
        return (
            <TouchableOpacity activeOpacity={0.9} style={{ margin: 3.5 }}
                onPress={() => this.props.navigation.navigate(item.menu, {
                    "phone": this.state.dataUser.noHP
                })}
            >

                <Card
                    style={styles.cardCarousel}>
                    <Image
                        style={styles.carouselImage}
                        source={item.image}
                    />

                    <View style={{ backgroundColor: 'rgba(42, 137, 16, 0.2)', flex: 1, borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}>
                        <Text numberOfLines={2} key={index + 1} style={styles.titleCarousel}>
                            {item.title}
                        </Text>

                        <Text numberOfLines={3} key={index + 1} style={styles.detailCarousel}>
                            {item.description}
                        </Text>
                    </View>

                    {/* <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Button
                            // transparent
                            onPress={() => this.props.navigation.navigate(item.menu)}
                            style={styles.btnDetail}>
                            <Text
                                key={index.toString()}
                                style={styles.btnDetailCarousel}>
                                Lihat Detail
                            </Text>
                        </Button>
                    </View> */}
                </Card>
            </TouchableOpacity>

        );
    }

    render() {
        return (
            <Container>
                <Content
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isRefreshing}
                            onRefresh={() => this.onRefresh()}
                            title="Loading..."
                        />
                    }
                >

                    <View style={styles.headerContainer}>
                        <View style={styles.infoContainer}>
                            <Text style={styles.greeting}>Hello!</Text>
                            <Text style={styles.name}>{this.state.dataUser.nama}</Text>
                        </View>
                        <View style={styles.iconHeaderContainer}>
                            <Button transparent onPress={() => this.riwayat()} badge style={styles.btnNotifContainer}>
                                <Icon name="history" type="font-awesome" color="#27a3ec" />
                            </Button>
                            <Button transparent onPress={() => this.chat()} badge style={styles.btnNotifContainer}>
                                <Icon name="chat" type="entypo" color="#27a3ec" />
                            </Button>
                            <Button transparent onPress={() => this.notif()} badge style={styles.btnNotifContainer}>
                                <Icon name="bell" type="font-awesome" color="#27a3ec" />
                                {/* <Badge style={styles.badge}><Text>2</Text></Badge> */}
                            </Button>
                        </View>
                    </View>

                    <View style={styles.saldoContainer}>
                        <Text style={styles.saldoLabel}>Saldo HasanahKu</Text>
                        <Text style={styles.saldo}>{this.state.dataUser.balanceFormat}</Text>
                    </View>

                    <View style={styles.bannerContainer}>
                        <ImageBackground imageStyle={{ borderRadius: 20 }} source={require('../../../assets/image/bannerbg.jpg')} style={styles.banner}>

                        </ImageBackground>
                    </View>

                    <View style={styles.featureContainer}>
                        <Text style={styles.titleSection}>Features</Text>
                        <View style={styles.featureItemContainer}>
                            <FlatList
                                data={this.state.dataFeature}
                                renderItem={this.renderFeature}
                                extraData={this.state}
                                numColumns={4}
                            />
                        </View>
                    </View>

                    {/*<View style={styles.carouselContainer}>
                        <Text style={styles.titleSection}>Special Promo</Text>
                        <View style={styles.carouselItemContainer}>
                            <FlatList
                                horizontal
                                style={styles.carouselWrapper}
                                showsHorizontalScrollIndicator={false}
                                data={this.state.dataCarousel}
                                renderItem={this.renderCarousel}
                                extraData={this.state}
                            />
                        </View>
                    </View>*/}

                    <View style={styles.footerCustom}>

                    </View>
                    <Spinner
                        cancelable={true}
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
                <Footer style={styles.footer}>
                    <FooterTab style={styles.footerTab}>
                        <Button badge vertical>
                            {/* <Badge><Text>2</Text></Badge> */}
                            <Icon size={35} color="#385757" name="apps" />
                        </Button>
                        <Button onPress={() => this.scan()}>
                            <View style={styles.centerTab}>
                                <Icon style={styles.iconTab} color="#fff" size={30} name="scan1" type="antdesign" />
                            </View>
                        </Button>
                        <Button onPress={() => this.akun()} badge vertical>
                            <Icon containerStyle={{ marginTop: 10 }} color="#385757" size={35} name="person" />
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const size = Dimensions.get('window')

const styles = StyleSheet.create({
    headerContainer: {
        flexDirection: 'row',
    },
    infoContainer: {
        flex: 1,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        marginBottom: 10
    },
    iconHeaderContainer: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        flex: 1
    },
    greeting: {
        fontWeight: 'bold',
        fontSize: 25
    },
    name: {
        marginTop: 10,
        textTransform: 'capitalize'
    },
    saldoContainer: {
        marginLeft: 20,
        marginRight: 20,
        flexDirection: 'row'
    },
    saldoLabel: {
        flex: 2
    },
    saldo: {
        fontSize: 18,
        fontWeight: 'bold',
        // color: '#32ce32',
        textTransform: 'capitalize',
        marginTop: -5
    },
    bannerContainer: {
        margin: 20
    },
    banner: {
        // borderWidth: 0.5,
        height: size.height / 7,
        width: '100%',
        borderRadius: 20
    },
    featureContainer: {
        margin: 20
    },
    featureIconContainer: {
        flexDirection: 'column',
        height: size.width / 4,
        width: size.width / 4 - 20,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
    },
    iconContainer: {
        height: size.width / 4 - 40,
        width: size.width / 4 - 40,
        borderRadius: 18,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        borderWidth : 1,
        borderColor: '#F1F3F4',
      /*  shadowColor: '#666',
         shadowOffset: { width: 0, height: 0.8 },
         shadowOpacity: 0.8,
         shadowRadius: 5,
         elevation: 5,*/
    },
    txtFeature: {
        marginTop: 5,
        fontSize: 10
    },
    carouselContainer: {
        margin: 20
    },
    carouselItemContainer: {
        marginTop: 15,
        marginBottom: 15
    },
    carouselWrapper: {
        overflow: 'visible'
    },
    cardCarousel: {
        borderRadius: 10,
        width: 250,
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 5,
    },
    carouselImage: {
        height: 150,
        width: 250,
        resizeMode: 'contain',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
    },
    titleCarousel: {
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 16,
        fontWeight: 'bold'
    },
    detailCarousel: {
        marginTop: 7,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 13,
        marginBottom: 7
    },
    btnDetailCarousel: {
        fontWeight: 'bold',
        // color: '#e35200'
        color: '#fff',
        textAlign: 'center'
    },
    btnDetail: {
        backgroundColor: '#2a8b40',
        height: 30,
        width: '100%',
        borderTopRightRadius: 0,
        borderTopLeftRadius: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleSection: {
        fontWeight: 'bold',
        fontSize: 16
    },
    btnNotifContainer: {
        backgroundColor: '#fcfbfc',
        justifyContent: 'center',
        alignItems: 'center',
        width: 50
    },
    badge: {
        position: 'absolute',
        top: -15,
        right: -5,
    },
    centerTab: {
        marginTop: -50,
        backgroundColor: '#26c165',
        borderRadius: size.width / 6,
        width: size.width / 6,
        height: size.width / 6,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 5,
        borderColor: '#fff',
        shadowColor: '#26c165',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 5,
    },
    iconTab: {

    },
    footer: {
        borderTopWidth: 0,
        backgroundColor: '#F8F8F8'
    },
    footerTab: {
        backgroundColor: '#F8F8F8'
    }
})

export default Home2;
