import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, BackHandler } from 'react-native';
import { Container, Content, Button} from 'native-base';
import { Icon } from 'react-native-elements';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { post } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {Error, ResponseCode} from '../../../supports/Constants';
import { getValue } from '../../../module/LocalData/Storage';
import DeviceInfo from 'react-native-device-info';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';

class RegisterUserExistChannelUnreg3 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            hasVerify: false,
            hasConfirmation: false,
            hasRegisterCore: false,
            deviceId: '',
            fcm: '',
            inputs: {
              code: {
                type: 'pin',
                value: ''
              },
              confirmationCode: {
                type: 'pin',
                value: ''
              }
            }
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
        this.handleBackButton = this.handleBackButton.bind(this);
    }

    componentDidMount() {
        this.getdeviceId()
        this.getFcmKey()
    }

    componentWillMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
      this.goBack();
      return true;
    }

    onFilledPin(pin) {
      const valid = ValidationHelper.validate({ type: 'pin', value : pin });

      if(valid !== null) return;

        this.setState({
            showSpinner: true,
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
                hasConfirmation: true,
            });

        }, 1000);
    }

    getdeviceId() {
        //Getting the Unique Id from here
        var id = DeviceInfo.getUniqueId();
        this.setState({ deviceId: id, });
    };

    getFcmKey() {
        getValue('fcm').then(fcmKey => {
            this.setState({
                fcm: fcmKey
            })
        })
    }

    resetInputPinConfirmation(){
      const { inputs } = this.state;
        this.setState({
            inputs: {
              ...inputs,
              ['confirmationCode']: {
                ...inputs['confirmationCode'],
                value : '',
                error:  null
              }
            }
        });
    }

    doRegister(pinConfirmation) {

      if(this.isValidate() === false) return;

      if(this.state.inputs.code.value !== pinConfirmation){
        setTimeout(() => {
          this.resetInputPinConfirmation();
          dialog.alert(Error.new_pin_confirmation_pin_not_match);
        }, 1000);
        return;
      }

      this.setState({
        showSpinner: true
      });

      let params =
      {
        "phone": this.props.navigation.getParam('phone'),
        "password": this.props.navigation.getParam('password'),
        "fcmKey": this.state.fcm,
        "device": this.state.deviceId,
        "email": this.props.navigation.getParam('email'),
        "pin": this.state.inputs.code.value
      }

      post(Env.base_url + Api.addUserLinkChannel, params).then(response => {
        if(response.code == ResponseCode.OK){
          this.setState({
              showSpinner: false,
              hasVerify: true
          });
        }else{
          this.setState({
              showSpinner: false,
              hasVerify: false
          });
          dialog.alertFail(response.failContent, this);
        }
      }).catch(err => {
        this.setState({
            showSpinner: false,
            hasVerify: false
        });
        dialog.alertException(err);
      });
    }

    goBack() {
      if(this.state.hasVerify == true){
        this.setState({
            showSpinner: true,
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false,
            });

            this.props.navigation.navigate('Welcome', {
                "phone": this.props.navigation.getParam("phone")
            })

        }, 1000);
      }else{
        this.props.navigation.goBack();
      }
    }

    render() {
        return (
            <Container>
                <Content>
                {this.state.hasVerify == false &&
                  <View style={styles.imageLogoContainer}>
                      <Button onPress={() => this.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                          <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                      </Button>
                      <View style={styles.titleNavContainer}>
                        <Text style={styles.titleNav}>PIN</Text>
                      </View>
                   </View>
                 }
                    {this.state.hasVerify == false ?
                        (
                            <View>
                                <View style={styles.infoContainer}>
                                    <Text style={styles.infoTitle}>{this.state.hasConfirmation == false ? 'Buat PIN Transaksi Anda' : ' Konfirmasi PIN Anda'}</Text>
                                    <Text style={styles.infoSubTitle}>PIN digunakan untuk melakukan transaksi</Text>

                                </View>

                                {this.state.hasConfirmation == false ? (

                                    <View style={styles.inputContainer}>
                                        <OTPInputView
                                            style={{ width: '80%', height: 200, }}
                                            pinCount={6}
                                            code={this.state.inputs.code.value} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                            onCodeChanged={value => { this.onInputChange({id: 'code', value})}}
                                            autoFocusOnLoad
                                            secureTextEntry
                                            codeInputFieldStyle={styles.underlineStyleBase}
                                            codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                            onCodeFilled={(code => {this.onFilledPin(code)})}
                                        />
                                        <Text style={styles.jagaPIN}>Jaga kerahasiaan PIN Anda</Text>
                                        {this.validateError('code')}
                                    </View>

                                ) :

                                    <View style={styles.inputContainer}>
                                        <OTPInputView
                                            style={{ width: '80%', height: 200, }}
                                            pinCount={6}
                                            code={this.state.inputs.confirmationCode.value} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                            onCodeChanged={value => { this.onInputChange({id: 'confirmationCode', value})}}
                                            autoFocusOnLoad
                                            secureTextEntry
                                            codeInputFieldStyle={styles.underlineStyleBase}
                                            codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                            onCodeFilled={code => this.doRegister(code)}
                                        />
                                        <Text style={styles.jagaPIN}>Jaga kerahasiaan PIN Anda</Text>
                                        {this.validateError('confirmationCode')}
                                    </View>

                                }

                            </View>
                        ) :
                        <View>
                            <View style={styles.titleContainer}>
                                <Text style={[styles.infoTitle, {textAlign: 'center'}]}>Alhamdulillah akun berhasil dibuat</Text>
                                <Text style={styles.infoSubTitle1}>PIN kamu akan digunakan setiap transaksi, jaga baik-baik ya...</Text>
                                <Image style={styles.imageBig} source={require('../../../assets/image/Credit-card.jpg')} />
                            </View>

                            <View style={styles.btnRegisterContainer}>
                                <Button onPress={() => this.goBack()} style={styles.btnRegister}>
                                    <Text style={styles.RegisterText}>Mulai</Text>
                                </Button>
                            </View>
                        </View>

                    }

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    infoContainer: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    infoTitle: {
        color: '#000',
        fontSize: 24,
        fontWeight: 'bold',
    },
    infoSubTitle: {
        color: '#B8B8B8',
        fontSize: 13,
        marginTop: 20,
        fontWeight: '500',
        marginRight: 5,
    },
    infoSubTitle1: {
        color: '#385757',
        fontSize: 13,
        marginTop: 10,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    borderStyleHighLighted: {
        // borderColor: "#2a8b40",
    },
    underlineStyleBase: {
        width: Dimensions.get('window').width / 8,
        height: Dimensions.get('window').width / 8,
        borderWidth: 0,
        borderBottomWidth: 2,
        backgroundColor: '#F9F9F9',
        borderRadius: 20,
        fontSize: 24
    },
    underlineStyleHighLighted: {
        // borderColor: "#2a8b40",
    },
    borderStyleBase: {
        // width: 30,
        // height: 45
    },
    titleContainer: {
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    title: {
        // fontFamily: 'Varela',
        fontSize: 16,
        fontWeight: 'bold'
    },
    imageBig: {
        height: Dimensions.get('window').height / 2 - 50,
        width: Dimensions.get('window').width - 50
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#26C165',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    infoBottomContainer: {
        margin: 20
    },
    infoBtmText: {
        fontSize: 12,
        color: '#385757'
    },
    jagaPIN: {
        textAlign: 'left',
        marginTop: -50,
        fontWeight: '800',
        fontSize: 13
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 26
    },
})

export default RegisterUserExistChannelUnreg3;
