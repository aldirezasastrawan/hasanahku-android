import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, ScrollView, ActivityIndicator } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { post, get } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import Modal from 'react-native-modalbox';
import AutoHeightWebView from 'react-native-autoheight-webview';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            isModalOpen: false,
            sk: "",
            checkedSK: false,
            showSpinnerWebView: true,
            endScroll: false,
            disabled: true,
            inputs: {
              noHp: {
                type: 'phoneNumber',
                value: this.props.navigation.getParam('phone')
              }
            }
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
      //  this.onRegister = this.onRegister.bind(this);
    }

    componentDidMount() {
        // this.startVoice();
        this.getTermsAndConditions();
    }

    componentWillUnmount() {
    }

    hideSpinner() {
        this.setState({ showSpinnerWebView: false });
    }

    getTermsAndConditions() {

      this.setState({ showSpinner: true, disabled: true });

      get(Env.base_url + Api.termCondition).then(response => {

        this.setState({ showSpinner: false});

        if(response.code == ResponseCode.OK){
          this.setState({
              sk: response.okContent.termConditions.content
          });
        }
      }).catch(err => {
        this.setState({ showSpinner: false});
      //  dialog.alertException(err)
      });
    }

    SK() {
        this.setState({
            isModalOpen: true,
            disabled: true
        })
    }

    closeModal() {
        this.setState({
            isModalOpen: false,
            disabled: true
        })
    }

    onRegister() {
      if(this.isValidate() === false) return;

      if(this.state.sk == ""){
        this.setState({ showSpinner: true, disabled: true });

        get(Env.base_url + Api.termCondition).then(response => {

          this.setState({ showSpinner: false});

          if(response.code == ResponseCode.OK){
            this.setState({
                sk: response.okContent.termConditions.content
            });

            setTimeout(() => {
              this.SK();
            }, 500);

          }else {
            dialog.alertFail(response.failContent, this)
          }
        }).catch(err => {
          this.setState({ showSpinner: false});
          dialog.alertException(err)
        });
      }else{
        this.SK();
      }
    }

    accepted() {

        this.closeModal()
        this.setState({showSpinner: true});

        let params = {
            phone: this.state.inputs.noHp.value
        }

        post(Env.base_url + Api.checkUserExist, params).then(response => {
          this.setState({showSpinner: false});

          if(response.code === ResponseCode.OK){
            this.props.navigation.navigate('RegisterNewUser1', {
                phone: this.state.inputs.noHp.value
            });
          }else{
            if(response.failContent.failCode === "USER_ERR_01"){
              this.props.navigation.navigate('RegisterUserExist1', {
                  phone: this.state.inputs.noHp.value
              });
            }else{
              dialog.alertFail(response.failContent, this)
            }
          }

        }).catch(err => {
          this.setState({showSpinner: false});
          dialog.alertException(err)
        });
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>Registrasi Nomor</Text>
                        </View>
                    </View>

                    <View style={styles.infoContainer}>
                        <Text style={styles.infoTitle}>Masukkan Nomor Handphone kamu</Text>
                        <Text style={styles.infoSubTitle}>Nomor Handphone akan digunakan untuk dapat mengakses Aplikasi HasanahKu</Text>
                    </View>

                    <View style={styles.inputContainer}>
                        <Form>
                            <Item stackedLabel last>
                                <Label style={styles.label}>Nomor Handphone</Label>
                              <Input style={styles.input} keyboardType='numeric' maxLength={14} minLength={10}   onChangeText={value => this.onInputChange({id: 'noHp', value})} />
                            </Item>
                            {this.validateError('noHp')}
                        </Form>

                    </View>
                    {/* <View style={styles.checkContainer}>
                        <ListItem noBorder>
                            <CheckBox onPress={() => alert('Silahkan baca dan setujui Syarat dan Ketentuan HasanahKu untuk melanjutkan')} checked={this.state.checkedSK} />
                            <Body style={styles.checkTextContainer}>
                                <Text style={styles.txtSetuju}> Saya menyetujui</Text>
                                <Button onPress={() => this.SK()} style={{ marginLeft: 5, marginTop: -1, marginRight: 10 }} transparent>
                                    <Text style={styles.txtSyarat}>Syarat dan Ketentuan HasanahKu</Text>
                                </Button>
                            </Body>
                        </ListItem>
                    </View> */}

                    <View style={styles.btnRegisterContainer}>
                        <Button onPress={() => this.onRegister()} style={styles.btnRegister}>
                            <Text style={styles.RegisterText}>Daftar</Text>
                        </Button>
                    </View>

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        cancelable={true}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
                <Modal animationType={"slide"}
                    isOpen={this.state.isModalOpen}
                    swipeArea={20}
                    onClosed={() => this.closeModal()}
                    style={[styles.modal, styles.modal4]}
                    position={'center'}>

                    <ScrollView
                        onScroll={(e) => {
                            let paddingToBottom = 10;
                            paddingToBottom += e.nativeEvent.layoutMeasurement.height;
                            if (e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height - paddingToBottom) {
                                // make something...
                                // Toast.show({
                                //     text: "end",
                                //     buttonStyle: 'Okay'
                                // })
                                alert('Pastikan Kamu sudah membaca seluruh Syarat dan Ketentuan, tekan SETUJU untuk melanjutkan')
                                this.setState({
                                    disabled: false
                                })
                            }
                        }}
                        style={styles.listContainer}>
                        {/* <WebView
                            onLoad={() => this.hideSpinner()}
                            style={styles.webView}
                            source={{ html: this.state.sk }}
                            automaticallyAdjustContentInsets={false}
                        /> */}

                        <AutoHeightWebView
                            style={{ width: Dimensions.get('window').width - 50 }}
                            onSizeUpdated={(size) => console.log(size.height)}

                            customStyle={`
                                           p {
                                                 font-size: 16px;
                                                 text-align: justify
                                             }
                                         `}

                            source={{ html: this.state.sk }}
                            onLoad={() => this.hideSpinner()}
                        />

                        {this.state.showSpinnerWebView && (
                            <ActivityIndicator
                                style={{ position: "absolute", top: 30, left: 0, right: 0 }}
                                size="large"
                            />
                        )}

                    </ScrollView>

                    <View style={styles.btnRegisterContainer}>
                        <Button disabled={this.state.disabled} onPress={() => this.accepted()} style={[styles.btnSetuju, this.state.disabled == false ? { backgroundColor: "#4caf50" } : null]}>
                            <Text style={styles.RegisterText}>Setuju</Text>
                        </Button>
                    </View>

                </Modal>
            </Container >
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        marginLeft: 10,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    infoContainer: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginLeft: 35,
        marginRight: 20
    },
    infoTitle: {
        color: '#000',
        fontSize: 24,
        fontWeight: 'bold',
    },
    infoSubTitle: {
        color: '#B8B8B8',
        fontSize: 13,
        marginTop: 20,
        fontWeight: '500',
        marginRight: 5,
    },
    inputContainer: {
        margin: 20
    },
    checkContainer: {
        marginTop: -20,
    },
    checkTextContainer: {
        flexDirection: 'row',
        marginRight: 20
    },
    btnSetuju: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        // backgroundColor: '#26C165',
        justifyContent: 'center'
    },
    txtSetuju: {
        marginTop: 14,
        fontSize: 12
    },
    txtSyarat: {
        fontSize: 12,
        marginTop: 2,
        color: '#385757',
        fontWeight: 'bold'
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#26C165',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    input: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    modal: {
        alignItems: 'center',
    },
    modal4: {
        height: Dimensions.get('window').height - 50,
        width: '95%',
        borderRadius: 20,
    },
    listContainer: {
        margin: 10,
        flex: 1
    },
    webView: {
        height: Dimensions.get('window').height - 150,
        width: Dimensions.get('window').width - 50,
        borderRadius: 20,
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 26
    }
})

export default Register;
