import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { post } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import { getValue, saveData } from '../../../module/LocalData/Storage';
import moment from 'moment';
import CountDown from 'react-native-countdown-component';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';

class RegisterUserExist1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            code: '',
            wrongCodeCount: 0,
            currentExpDate: "",
            isBanned: false,
            deviceId: '',
            inputs: {
              code: {
                type: 'pin',
                value: ''
              }
            }
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
    }

    componentDidMount() {
        // saveData("timeWrongCode", "2020-02-13 01:22:30")
        this.checkIsBanned()
    }

    checkIsBanned() {
        const currentDate = moment().format("YYYY-MM-DD hh:mm:ss")

        getValue('timeWrongCode').then(response => {
            if (response != null) {
                if (response < currentDate) {
                    this.setState({
                        currentExpDate: moment(response).add(1, "hours").format('YYYY-MM-DD hh:mm:ss'),
                        isBanned: true
                    })
                } else {
                    saveData("timeWrongCode", null)
                    this.setState({
                        currentExpDate: "",
                        isBanned: false
                    })
                }
            }
        })
    }

    resetInputPIN(){
      const { inputs } = this.state;
        this.setState({
            inputs: {
              ...inputs,
              ['code']: {
                ...inputs['code'],
                value : '',
                error:  null
              }
            }
        });
    }

    onFilledPin(pin) {

      const valid = ValidationHelper.validate({ type: 'pin', value : pin });

      if(valid !== null) return;

        this.setState({
            showSpinner: true,
        });

        let params =
        {
            "phone": this.props.navigation.getParam('phone'),
            "pin": pin
        }

        post(Env.base_url +Api.registerPinMatch, params).then(response => {
            if(response.code == ResponseCode.OK){
                setTimeout(() => {
                    this.setState({
                        showSpinner: false,
                    });

                    this.props.navigation.navigate('RegisterUserExist2', {
                        phone: this.props.navigation.getParam('phone')
                    })

                }, 500);
            } else {
                setTimeout(() => {
                    this.setState({
                        showSpinner: false,
                        wrongCodeCount: this.state.wrongCodeCount + 1
                    });

                    this.resetInputPIN();
                    dialog.alertFail(response.failContent, this);

                    if (this.state.wrongCodeCount == 3) {
                        const currentDate = moment().format("YYYY-MM-DD hh:mm:ss")

                        saveData('timeWrongCode', currentDate)
                        this.setState({
                            currentExpDate: currentDate
                        })
                    }

                }, 2000);
            }
        }).catch(err => {
          setTimeout(() => {
            this.setState({showSpinner: false});
            this.resetInputPIN();
            dialog.alertException(err);
          }, 1000);
        });
    }

    lupapin() {
        this.props.navigation.navigate('VerifikasiLupaPin', {
            phone: this.props.navigation.getParam('phone'),
            screenCallback : "RegisterUserExist1"
        })
    }

    finishBanned() {
        saveData('timeWrongCode', null)
        this.setState({
            currentExpDate: "",
            isBanned: false
        })
    }

    render() {
        var now = moment().format("YYYY-MM-DD hh:mm:ss"); //todays date
        var end = moment(this.state.currentExpDate); // another date
        var duration = moment.duration(moment(end).diff(now));
        var second = duration.asSeconds();

        if (second < 0) {
            this.setState({
                isBanned: false,
                currentExpDate: ""
            })
        }
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>PIN</Text>
                        </View>
                    </View>

                    {this.state.wrongCodeCount == 3 || this.state.currentExpDate != "" ?
                        null
                        :
                        <View>

                            <View style={styles.infoContainer}>
                                <Text style={styles.infoTitle}>Masukkan PIN Transaksi</Text>
                                <Text style={styles.infoSubTitle}>PIN digunakan untuk melakukan transaksi</Text>

                            </View>

                            <View style={styles.inputContainer}>
                                <OTPInputView
                                    style={{ width: '80%', height: 200, }}
                                    pinCount={6}
                                    code={this.state.inputs.code.value} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                    onCodeChanged={value => { this.onInputChange({id: 'code', value})}}
                                    autoFocusOnLoad
                                    secureTextEntry
                                    codeInputFieldStyle={styles.underlineStyleBase}
                                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                    onCodeFilled={(code => {this.onFilledPin(code)})}
                                />
                                <Text style={styles.jagaPIN}>Jaga kerahasiaan PIN Anda</Text>

                                <Button onPress={() => this.lupapin()} transparent style={styles.btnLupaPin}>
                                    <Text style={styles.txtLupaPin}>Lupa PIN</Text>
                                </Button>
                            </View>
                            {/*
                            <View style={styles.inputContainer}>

                                <Form>
                                    <Item stackedLabel last>
                                        <Input style={styles.input} keyboardType='number-pad' value={this.state.inputs.code.value} onChangeText={(code) => this.setState({ code: code })} />
                                    </Item>
                                </Form>
                            </View> */}
                        </View>
                    }

                    {this.state.wrongCodeCount == 3 || this.state.currentExpDate != "" ?
                        <View style={styles.batasContainer}>
                            <Text style={styles.batas}>
                                anda telah mencapai batas memasukkan PIN
                        </Text>
                            <Text style={styles.bataspin}>
                                Lakukan input PIN kembali pada
                            </Text>
                            {/* <Text style={styles.batas}>
                                {this.state.currentExpDate}
                            </Text> */}
                            <CountDown
                                style={{ marginTop: 10 }}
                                until={second}
                                onFinish={() => this.finishBanned()}
                                size={16}
                            />
                        </View>

                        :

                        null

                    }

                    {/* {this.state.wrongCodeCount == 3 || this.state.currentExpDate != "" ?
                        null

                        :

                        <View style={styles.btnRegisterContainer}>
                            <Button onPress={() => this.checkPin()} style={styles.btnRegister}>
                                <Text style={styles.RegisterText}>Submit PIN</Text>
                            </Button>
                        </View>

                    } */}



                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    infoContainer: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    infoTitle: {
        color: '#000',
        fontSize: 24,
        fontWeight: 'bold',
    },
    infoSubTitle: {
        color: '#B8B8B8',
        fontSize: 13,
        marginTop: 20,
        fontWeight: '500',
        marginRight: 5,
    },
    infoSubTitle1: {
        color: '#385757',
        fontSize: 13,
        marginTop: 10,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    borderStyleHighLighted: {
        // borderColor: "#2a8b40",
    },
    underlineStyleBase: {
        width: Dimensions.get('window').width / 8,
        height: Dimensions.get('window').width / 8,
        borderWidth: 0,
        borderBottomWidth: 2,
        backgroundColor: '#F9F9F9',
        borderRadius: 20,
        fontSize: 24
    },
    underlineStyleHighLighted: {
        // borderColor: "#2a8b40",
    },
    borderStyleBase: {
        // width: 30,
        // height: 45
    },
    titleContainer: {
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    title: {
        // fontFamily: 'Varela',
        fontSize: 16,
        fontWeight: 'bold'
    },
    imageBig: {
        height: Dimensions.get('window').height / 2 - 50,
        width: Dimensions.get('window').width - 50
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#26C165',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    infoBottomContainer: {
        margin: 20
    },
    infoBtmText: {
        fontSize: 12,
        color: '#385757'
    },
    jagaPIN: {
        textAlign: 'left',
        marginTop: -50,
        fontWeight: '800',
        fontSize: 13
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 26
    },
    batasContainer: {
        marginTop: 100,
        justifyContent: 'center',
        alignItems: 'center'
    },
    bataspin: {
        marginTop: 10
    },
    btnLupaPin: {
        marginTop: 10
    },
    txtLupaPin: {
        color: '#26C165',
        fontWeight: 'bold'
    }
})

export default RegisterUserExist1;
