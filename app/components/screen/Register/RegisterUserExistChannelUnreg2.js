import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Platform, UIManager } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import  {BoxPasswordStrengthDisplay} from 'react-native-password-strength-meter';


class RegisterUserExistChannelUnreg2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            canConfirmation: false,
            showPassword: false,
            showConfirmationPassword: false,
            hasInputConfirmation: false,
            passwordMatching: false,
            inputs: {
              password: {
                type: 'password',
                value: ''
              },
              confirmationPassword: {
                type: 'password',
                value: ''
              }
            }
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.validateInput = ValidationHelper.validateInput.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    componentDidMount() {
        // this.startVoice();
    }

    componentWillUnmount() {
    }

    checkPassword() {

      if(!this.validateInput({id : "password", value : this.state.inputs.password.value})){
        this.setState({hasInputConfirmation: false, passwordMatching: false})
        return;
      }

    //  this.setState({canConfirmation: true })
    //  LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);

      this.checkConfirmationPassword();
    }

    checkConfirmationPassword() {

      if(!this.validateInput({id : "confirmationPassword", value : this.state.inputs.confirmationPassword.value})){
        this.setState({ hasInputConfirmation: false, passwordMatching: false });
        return;
      }

    //  LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);

      if (this.state.inputs.password.value != this.state.inputs.confirmationPassword.value) {
          this.setState({ hasInputConfirmation: true, passwordMatching: false })
      } else {
          this.setState({ hasInputConfirmation: true, passwordMatching: true })
      }
    }

    createPassword() {

      if(this.isValidate() === false) return;

      if (this.state.inputs.password.value != this.state.inputs.confirmationPassword.value) {
      //  LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        this.setState({ hasInputConfirmation: true, passwordMatching: false });
        return;
      }

        this.setState({
            showSpinner: true
        });

        setTimeout(() => {
            this.setState({
                showSpinner: false
            });

            this.createPin();

        }, 1000);
    }

    createPin() {
        this.props.navigation.navigate('RegisterUserExistChannelUnreg3', {
            email: this.props.navigation.getParam('email'),
            phone: this.props.navigation.getParam('phone'),
            password: this.state.inputs.password.value
        })
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>Password</Text>
                        </View>
                    </View>
                    <View style={{ margin: 20 }}>
                        <View style={styles.infoContainer}>
                            <Text style={styles.infoTitle}>Buat Password Anda</Text>
                            <Text style={styles.infoSubTitle}>Password digunakan untuk login ke akun Anda dan digunakan setiap masuk ke Aplikasi</Text>
                        </View>
                        <Form>
                            <Item stackedLabel>
                                <Label style={styles.label}>Password</Label>
                                <View style={{ flexDirection: 'row' }}>
                                    <Input
                                        secureTextEntry={this.state.showPassword == false ? true : false}
                                        style={styles.labelInput} keyboardType='default'
                                        value={this.state.inputs.password.value} maxLength={12} onChangeText={value => { this.onInputChange({id: 'password', value})}}
                                        onEndEditing={() => this.checkPassword()} />
                                    <Icon
                                        onPress={() => this.setState({ showPassword: !this.state.showPassword })}
                                        active
                                        name={this.state.showPassword == true ? "eye" : "eye-slash"} type="font-awesome" />
                                </View>
                            </Item>
                            <View style={{ marginRight: 20 }}>
                                <BoxPasswordStrengthDisplay
                                    minLength={1}
                                    width={200}
                                    password={this.state.inputs.password.value}/>
                            </View>
                            <View style={{marginTop: 20}}>
                              {this.validateError('password')}
                            </View>
                            <Item stackedLabel last>
                            <Label style={styles.label}>Konfirmasi Password</Label>
                            <View style={{ flexDirection: 'row' }}>
                                <Input
                                    secureTextEntry={this.state.showConfirmationPassword == false ? true : false}
                                    style={styles.labelInput} keyboardType='default'
                                    value={this.state.inputs.confirmationPassword.value} maxLength={12}
                                    onChangeText={value => { this.onInputChange({id: 'confirmationPassword', value})}}
                                    onEndEditing={() => this.checkConfirmationPassword()} />
                                <Icon
                                    onPress={() => this.setState({ showConfirmationPassword: !this.state.showConfirmationPassword })}
                                    active
                                    name={this.state.showConfirmationPassword == true ? "eye" : "eye-slash"} type="font-awesome" />
                            </View>
                            </Item>
                            {this.validateError('confirmationPassword')}

                          {this.state.hasInputConfirmation == true &&
                            <View style={{ margin: 10, justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                {this.state.passwordMatching == true ?

                                    <Text style={styles.txtMatch}>Password berhasil dikonfirmasi</Text>
                                    :
                                    <Text style={styles.txtNoMatch}>Password tidak sama</Text>
                                }
                            </View>
                          }
                        </Form>
                        <View style={styles.btnRegisterContainer}>
                              <Button onPress={() => this.createPassword()} style={styles.btnRegister}>
                                  <Text style={styles.RegisterText}>Buat Password</Text>
                              </Button>
                        </View>
                    </View>

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    infoContainer: {
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginLeft: 20,
        marginRight: 20
    },
    infoTitle: {
        color: '#000',
        fontSize: 24,
        fontWeight: 'bold',
    },
    infoSubTitle: {
        color: '#B8B8B8',
        fontSize: 13,
        marginTop: 20,
        fontWeight: '500',
        marginRight: 5,
    },
    infoSubTitle1: {
        color: '#385757',
        fontSize: 13,
        marginTop: 10,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    borderStyleHighLighted: {
        borderColor: "#2a8b40",
    },
    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 2,
    },
    underlineStyleHighLighted: {
        borderColor: "#2a8b40",
    },
    borderStyleBase: {
        width: 30,
        height: 45
    },
    titleContainer: {
        marginTop: 50,
        marginLeft: 20,
        marginRight: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        // fontFamily: 'Varela',
        fontSize: 16,
        fontWeight: 'bold'
    },
    imageBig: {
        height: Dimensions.get('window').height / 2 - 50,
        width: Dimensions.get('window').width - 50
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#26C165',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    infoBottomContainer: {
        margin: 20
    },
    infoBtmText: {
        fontSize: 12,
        color: '#385757'
    },
    jagaPIN: {
        textAlign: 'left',
        marginTop: 10,
        fontWeight: '800',
        fontSize: 13
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    labelInput: {
        fontWeight: 'bold',
        fontSize: 13,
        paddingLeft: 10
    },
    txtMatch: {
        color: '#2a8b40',
        fontWeight: 'bold',
        fontSize: 12
    },
    txtNoMatch: {
        color: 'red',
        fontWeight: 'bold',
        fontSize: 12
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 26
    },
})

export default RegisterUserExistChannelUnreg2;
