import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, BackHandler  } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import { post } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import {dialog} from '../../../supports/Dialog';

class RegisterNewUser2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            noHp: this.props.navigation.getParam('phone'),
            disable: true,
            inputs: {
              nama: {
                type: 'name',
                value: ''
              },
              email: {
                type: 'email',
                value: ''
              }
            }
        };

        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);

    }

    componentWillMount() {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
      this.goBack();
      return true;
    }

    goBack(){
      this.props.navigation.navigate('Register');
    }

    doRegister() {

      if(this.isValidate() === false) return;

      this.setState({
          showSpinner: true
      });

      let params = {
          "phone": this.props.navigation.getParam('phone'),
          "email": this.state.inputs.email.value
      }

      post(Env.base_url + Api.userEmailExist, params).then(response => {
        this.setState({showSpinner: false});

        if(response.code == ResponseCode.FAIL){
          if(response.failContent.failCode == "app.email.not.found.error"){
            this.props.navigation.navigate('RegisterNewUser3', {
                nama: this.state.inputs.nama.value,
                email: this.state.inputs.email.value,
                phone: this.props.navigation.getParam('phone')
            })
          }else{
            dialog.alertFail(response.failContent, this);
          }
        }

      }).catch(err => {
        this.setState({showSpinner: false});
        dialog.alertException(err);
      });
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>Input Data</Text>
                        </View>
                    </View>

                    <View style={styles.infoContainer}>
                        <Text style={styles.infoTitle}>Masukkan Nama Lengkap, dan Email</Text>
                        <Text style={styles.infoSubTitle}>Email dan Nomor Handphone akan otomatis terhubung dengan akun HasanahKu</Text>
                    </View>

                    <View style={styles.inputContainer}>
                        <Form>
                            <Item stackedLabel>
                                <Label style={styles.label}>Nama Lengkap</Label>
                                <Input style={styles.input} onChangeText={value => this.onInputChange({id: 'nama', value})} />
                            </Item>
                            {this.validateError('nama')}

                            <Item stackedLabel last>
                                <Label style={styles.label}>Email</Label>
                                <Input style={styles.input} onChangeText={value => this.onInputChange({id: 'email', value})} />
                            </Item>
                            {this.validateError('email')}
                        </Form>
                    </View>

                    <View style={styles.btnRegisterContainer}>
                        <Button onPress={() => this.doRegister()} style={styles.btnRegister}>
                            <Text style={styles.RegisterText}>Daftar</Text>
                        </Button>
                    </View>
                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container >
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    infoContainer: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 20,
        marginRight: 20
    },
    infoTitle: {
        color: '#000',
        fontSize: 24,
        fontWeight: 'bold',
    },
    infoSubTitle: {
        color: '#B8B8B8',
        fontSize: 13,
        marginTop: 20,
        fontWeight: '500',
        marginRight: 5,
    },
    inputContainer: {
        margin: 20
    },
    checkContainer: {
        marginTop: -20,
    },
    checkTextContainer: {
        flexDirection: 'row',
        marginRight: 20
    },
    txtSetuju: {
        marginTop: 14,
        fontSize: 12
    },
    txtSyarat: {
        fontSize: 12,
        marginTop: 2,
        color: '#385757',
        fontWeight: 'bold'
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#4caf50',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 26
    },
    input: {
        fontSize: 14,
        fontWeight: 'bold',
        textTransform: 'lowercase'
    }
})

export default RegisterNewUser2;
