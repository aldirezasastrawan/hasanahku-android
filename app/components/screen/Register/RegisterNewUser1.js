import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { Container, Content, Button } from 'native-base';
import { Icon } from 'react-native-elements';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { post } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import { saveData } from '../../../module/LocalData/Storage';
import CountDown from 'react-native-countdown-component';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';

class RegisterNewUser1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            noHp: this.props.navigation.getParam('phone'),
            showSpinner: false,
            colorSpinner: '#e35200',
            timer: 60,
            canResend: false,
            inputs: {
              code: {
                type: 'otp',
                value: ''
              }
            }
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
    }

    componentDidMount() {
    }

    sendOtp() {

      setTimeout(() => {
          this.resetInput();
      }, 500);

        this.setState({
            showSpinner: true
        });

        let params = {
            "phone": this.props.navigation.getParam('phone')
        }

        post(Env.base_url + Api.getOTP, params).then(response => {
            if(response.code == ResponseCode.OK){
                setTimeout(() => {
                    this.setState({
                        showSpinner: false,
                        canResend: false,
                        timer: 60
                    });
                }, 500);

                saveData('timeCountdown', 60);

            } else {
              this.setState({showSpinner: false});
              dialog.alertFail(response.failContent, this);
            }
        }).catch(err => {
          this.setState({showSpinner: false});
          dialog.alertException(err);
        });
    }

    finishCountdown() {
        this.setState({
            canResend: true,
            timer: 0
        })
    }

    resetInput(){
      const { inputs } = this.state;
        this.setState({
            inputs: {
              ...inputs,
              ['code']: {
                ...inputs['code'],
                value : '',
                error:  null
              }
            }
        });
    }

    onFilledOtp(otp) {

        if(this.isValidate() === false) return;

        this.setState({
            showSpinner: true
        });

        let params = {
            "phone": this.props.navigation.getParam('phone'),
            "otp": otp
        }

        post(Env.base_url + Api.validateOTP, params).then(response => {
          this.setState({showSpinner: false});

          if(response.code == ResponseCode.OK){
            this.props.navigation.navigate('RegisterNewUser2', {
                phone: this.props.navigation.getParam('phone'),
            })
          } else {
            setTimeout(() => {
              this.resetInput();
              dialog.alertFail(response.failContent, this);
            }, 1000);
          }
        }).catch(err => {
          setTimeout(() => {
            this.setState({
                showSpinner: false
            });
            this.resetInput();
            dialog.alertException(err);
          }, 1000);
        });
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>Verifikasi OTP</Text>
                        </View>
                    </View>

                    <View>
                        <View style={styles.infoContainer}>
                            <Text style={styles.infoTitle}>Masukkan Kode Verifikasi</Text>
                            <Text style={styles.infoSubTitle}>Kode telah dikirim ke <Text>{this.state.noHp}</Text></Text>
                            <View style={styles.countdownContainer}>
                                <Text style={styles.infoSubTitle1}>Kode ini akan expired dalam</Text>
                                <CountDown
                                    style={{ marginTop: -1 }}
                                    until={this.state.timer}
                                    onFinish={() => this.finishCountdown()}
                                    size={16}
                                    digitStyle={{ backgroundColor: '#fff' }}
                                    digitTxtStyle={{ color: '#FF3B30' }}
                                    timeToShow={['M', 'S']}
                                    timeLabels={{ d: 'Hari', h: 'Jam', m: 'Menit', s: 'Detik' }}
                                />
                            </View>
                        </View>

                        <View style={styles.inputContainer}>
                            <OTPInputView
                                style={{ width: '80%', height: 200, }}
                                pinCount={6}
                                code={this.state.inputs.code.value} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                onCodeChanged={value => { this.onInputChange({id: 'code', value})}}
                                autoFocusOnLoad
                                secureTextEntry
                                codeInputFieldStyle={styles.underlineStyleBase}
                                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                onCodeFilled={(code => {this.onFilledOtp(code)})}
                            />
                            {this.validateError('code')}

                            {this.state.canResend == true ?
                                <Button onPress={() => this.sendOtp()} transparent>
                                    <Text style={styles.txtKirimUlang}>Kirim Ulang</Text>
                                </Button>
                                :
                                null
                            }
                        </View>
                    </View>

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    infoContainer: {
        marginTop: 30,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginLeft: 35,
        marginRight: 20
    },
    infoTitle: {
        color: '#000',
        fontSize: 24,
        fontWeight: 'bold',
    },
    infoSubTitle: {
        color: '#B8B8B8',
        fontSize: 13,
        marginTop: 20,
        fontWeight: '500',
        marginRight: 5,
    },
    infoSubTitle1: {
        color: '#B8B8B8',
        fontSize: 13,
        marginTop: 10,
        fontWeight: '500',
        textAlign: 'center'
    },
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    borderStyleHighLighted: {
        borderColor: "#2a8b40",
    },
    underlineStyleBase: {
        width: Dimensions.get('window').width / 8,
        height: Dimensions.get('window').width / 8,
        borderWidth: 0,
        borderBottomWidth: 2,
        backgroundColor: '#F9F9F9',
        borderRadius: 20,
        fontSize: 24
    },
    underlineStyleHighLighted: {
        borderColor: "#2a8b40",
    },
    borderStyleBase: {
        width: 30,
        height: 45
    },
    titleContainer: {
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        // fontFamily: 'Varela',
        fontSize: 16,
        fontWeight: 'bold'
    },
    imageBig: {
        height: Dimensions.get('window').height / 2 - 50,
        width: Dimensions.get('window').width - 50
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#26C165',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    infoBottomContainer: {
        margin: 20
    },
    infoBtmText: {
        fontSize: 12,
        color: '#385757'
    },
    txtKirimUlang: {
        color: '#06B3BA',
        fontWeight: 'bold'
    },
    kirimCode: {
        fontWeight: 'bold',
        fontSize: 16,
        marginBottom: 5
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 26
    },
    countdownContainer: {
        flexDirection: 'row'
    }
})

export default RegisterNewUser1;
