import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, Image, FlatList, TouchableOpacity, ScrollView, ActivityIndicator } from 'react-native';
import { Container, Content, Button, Switch } from 'native-base';
import { Icon } from 'react-native-elements';
import LottieView from 'lottie-react-native';
import { getValue, removeValue } from '../../../module/LocalData/Storage';
import { postWithToken, get } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import Modal from 'react-native-modalbox';
import firebase from 'react-native-firebase';
import Spinner from 'react-native-loading-spinner-overlay';
import AutoHeightWebView from 'react-native-autoheight-webview'
import {dialog} from '../../../supports/Dialog';

const size = Dimensions.get('window')

class Akun extends Component {
    constructor(props) {
        super(props);
        this.state = {
            receiveNotif: true,
            dataList: [],
            dataUser: {},
            isModalOpen: false,
            sk: "",
            checkedSK: false,
            showSpinnerWebView: true,
            showSpinner: false,
            colorSpinner: '#e35200',
            type: '',
            disabled: true
        };
    }

    componentDidMount() {
        this.getDatalist()
        this.getDataUser()
    }

    profile() {
        this.props.navigation.navigate('Profile')
    }

    hideSpinner() {
        this.setState({ showSpinnerWebView: false });
    }

    upgradeAccount() {
      getValue('tokenLogin').then(tokenLogin => {
        this.setState({showSpinner: true});

        let params = {
            "phone": this.props.navigation.getParam('phone')
        }

        postWithToken(Env.base_url + Api.userUpgradeInfo, params, tokenLogin).then(response => {
          this.setState({showSpinner: false})

          if(response.code == ResponseCode.OK){
            this.props.navigation.navigate('UpgradeAccount', {
                "phone": this.state.dataUser.noHP
            })
          }else{
            dialog.alertFail(response.failContent, this)
          }
        }).catch(err => {
          this.setState({showSpinner: false})
          dialog.alertException(err)
        })
      })
    }

    updateProfile() {
        // Toast.show({
        //     text: 'yaaah menu nya belum siap, coba lagi nanti yaa',
        //     buttonStyle: 'Okay'
        // })
        alert('Menu not available')
    }

    SK() {

      this.setState({
          showSpinner: false
      });

      get(Env.base_url + Api.termCondition, null).then(response => {
        if(response.code == ResponseCode.OK){
          this.setState({
            sk: response.okContent.termConditions.content
          });

          setTimeout(() => {
            this.setState({
              showSpinner: false,
              isModalOpen: true
            })
          }, 1000);
        }else{
          this.setState({
              showSpinner: false
          });
          dialog.alertFail(response.alertFail, this);
        }
      }).catch(err => {
        this.setState({
          showSpinner: false
        });
        dialog.alertException(err);
      });
    }

    closeModal() {
        this.setState({
            isModalOpen: false
        })
    }

    getDataUser(){
      getValue('userLogin').then(userLogin => {
        this.setState({
            dataUser: userLogin,
            type: userLogin.registerType.type
        });
      })
    }

    gotoMenu(menu) {
      switch (menu) {
          case 'Ubah Password':
              this.changePassword();
              break;
          case 'Ubah PIN Transaksi':
              this.changePin();
              break;
          case 'Syarat & Ketentuan':
              this.SK()
              break;
          case 'Bantuan':
              this.bantuan();
              break;
          case 'Kebijakan Privasi':
              setTimeout(() => {
                //  alert(menu);
                alert("Menu not available");
              }, 500);
              break;
          case 'Buka Blokir dan Tutup Akun':
              this.bukaAkun();
              break;
          default:
              // alert(menu)
              // this.props.navigation.goBack()
              break;
      }
    }

    getDatalist() {
        let data = [
            { menu: 'Notifikasi', menuType: 'switch', icon: 'bell', iconType: 'font-awesome', color: '#26c165' },
            { menu: 'Ubah Password', menuType: 'page', icon: 'shield-account', iconType: 'material-community', color: '#26c165' },
            { menu: 'Ubah PIN Transaksi', menuType: 'page', icon: 'security', iconType: 'material-community', color: '#26c165' },
            { menu: 'Syarat & Ketentuan', menuType: 'page', icon: 'flag', iconType: 'font-awesome', color: '#26c165' },
            { menu: 'Bantuan', menuType: 'page', icon: 'child', iconType: 'font-awesome', color: '#26c165', },
            // { menu: 'Kebijakan Privasi', menuType: 'page', icon: 'warning', iconType: 'font-awesome', color: '#26c165' },
            { menu: 'Buka Blokir dan Tutup Akun', menuType: 'page', icon: 'wrench', iconType: 'font-awesome', color: '#26c165' },
        ]

        this.setState({
            dataList: data
        })
    }

    changeNotif() {
        this.setState({
            receiveNotif: !this.state.receiveNotif
        })

        if (this.state.receiveNotif == true) {
            dialog.alert("Notif Off")
            firebase.messaging().unsubscribeFromTopic('all')
        } else {
            dialog.alert("Notif On")
            firebase.messaging().subscribeToTopic('all');
        }
    }

    logout() {
      removeValue('userLogin');
      removeValue('hasLogin');
      removeValue('tokenLogin');
      this.props.navigation.navigate('Welcome')
    }

    changePin() {
        this.props.navigation.navigate('Ubahpin', {
            "phone": this.state.dataUser.noHP
        })
    }

    changePassword() {
        this.props.navigation.navigate('UbahPassword', {
            "phone": this.state.dataUser.noHP
        })
    }

    bantuan() {
      alert("menu not available");
        /*this.props.navigation.navigate('Bantuan', {
            "phone": this.state.dataUser.noHP
        })*/
    }

    bukaAkun() {
      dialog.alert("Silahkan hubungi Layanan BNI Call Center di 1500046")
      //alert("menu not available");
        // this.props.navigation.navigate('Hubungikami', {
        //     "phone": this.state.dataUser.noHP
        // })
        // Toast.show({
        //     text: 'Yah menu nya belum siap nih, coba lagi nanti yaa ...',
        //     buttonStyle: 'Okay'
        // })
      //  alert('yaaah menu nya belum siap, coba lagi nanti yaa')

    }

    goBack(){
      this.props.navigation.navigate('Home2')
    }

    renderList = ({ item }) => {
        return (
            <TouchableOpacity style={styles.listItem} activeOpacity={0.8} onPress={() => this.gotoMenu(item.menu)}>
                <View style={styles.listIconContainer}>
                    <Icon name={item.icon} color={item.color} type={item.iconType} />
                </View>
                <Text style={styles.listMenuName}>{item.menu}</Text>
                <View>
                    {
                        item.menuType == "page" ?

                            <Icon containerStyle={{ paddingRight: 10 }} size={16} name="chevron-right" color="#c4c4c4" type="font-awesome" />
                            :
                            <Switch onValueChange={() => this.changeNotif()} value={this.state.receiveNotif} />
                    }
                </View>
            </TouchableOpacity>
        )
    }


    render() {
        const dataUser = this.state.dataUser
        const type = this.state.type
        return (
            <Container>
                <Content style={styles.Container}>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' color="#26C165" size={30} type='antdesign' />
                        </Button>
                        <View style={styles.titleNavContainer}>
                            <Text style={styles.titleNav}>Pengaturan Akun</Text>
                        </View>
                    </View>

                    <View style={styles.bannerContainer}>
                        <ImageBackground imageStyle={{ borderRadius: 20 }} source={require('../../../assets/image/bgProfile.jpg')} style={styles.banner}>
                            <Image style={styles.profileImage} source={require('../../../assets/image/man.png')} />
                            <View style={styles.infoContainer}>
                                <Text style={styles.name}>{dataUser.nama}</Text>
                                <Text allowFontScaling={false} style={styles.phone}>{dataUser.noHP}</Text>

                                <View style={styles.btnHeaderContainer}>
                                    <Button onPress={() => this.updateProfile()} style={styles.btnProfile}>
                                        <Text style={styles.txtProfile}>Ubah Profile</Text>
                                    </Button>
                                    {type != 'REGISTER' ?
                                        <Button onPress={() => this.upgradeAccount()} style={styles.btnUpgrade}>
                                            <Text style={styles.txtProfile}>Upgrade Akun</Text>
                                        </Button>
                                        : null
                                    }
                                </View>

                            </View>
                        </ImageBackground>
                    </View>

                    <View style={styles.listContainer}>
                        <View style={styles.listItemContainer}>
                            <FlatList
                                data={this.state.dataList}
                                renderItem={this.renderList}
                                extraData={this.state}
                            />
                        </View>
                    </View>

                    <View style={styles.signoutWrapper}>
                        <TouchableOpacity onPress={() => this.logout()} activeOpacity={0.8} style={styles.signoutContainer}>
                            <Text style={styles.txtSignout}>Sign Out</Text>
                            <View style={styles.signoutIconContainer}>
                                <Icon name="sign-out" color="#fff" type="font-awesome" />
                            </View>
                        </TouchableOpacity>
                    </View>

                    <Spinner
                        cancelable={true}
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />

                </Content>
                <Modal animationType={"slide"}
                    isOpen={this.state.isModalOpen}
                    swipeArea={20}
                    onClosed={() => this.closeModal()}
                    style={[styles.modal, styles.modal4]}
                    position={'center'}>

                    <ScrollView
                        onScroll={(e) => {
                            let paddingToBottom = 10;
                            paddingToBottom += e.nativeEvent.layoutMeasurement.height;
                            if (e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height - paddingToBottom) {
                                // make something...
                                // Toast.show({
                                //     text: "end",
                                //     buttonStyle: 'Okay'
                                // })
                                alert('Pastikan Kamu sudah membaca seluruh Syarat dan Ketentuan')
                                this.setState({
                                    disabled: false
                                })
                            }
                        }}
                        style={styles.listContainer}
                    >
                        {/* <WebView
                            onLoad={() => this.hideSpinner()}
                            style={styles.webView}
                            source={{ html: this.state.sk }}
                            automaticallyAdjustContentInsets={false}
                        /> */}

                        <AutoHeightWebView
                            style={{ width: Dimensions.get('window').width - 50 }}
                            onSizeUpdated={(size) => console.log(size.height)}

                            customStyle={`
                                          p {
                                                font-size: 16px;
                                                text-align: justify
                                            }
                                        `}

                            source={{ html: this.state.sk }}
                            // scalesPageToFit={true}
                            onLoad={() => this.hideSpinner()}
                            viewportContent={'width=device-width, user-scalable=no'}
                        />

                        {this.state.showSpinnerWebView && (
                            <ActivityIndicator
                                style={{ position: "absolute", top: 30, left: 0, right: 0 }}
                                size="large"
                            />
                        )}
                    </ScrollView>
                    <View style={styles.btnRegisterContainer}>
                        <Button disabled={this.state.disabled} onPress={() => this.closeModal()} style={[styles.btnSetuju, this.state.disabled == false ? { backgroundColor: "#4caf50" } : null]}>
                            <Text style={styles.txtSetuju}>Tutup</Text>
                        </Button>
                    </View>

                </Modal>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    Container: {
        height: '100%',
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnBack: {
        flex: 1,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    profileImage: {
        height: size.width / 6,
        width: size.width / 6,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#c0c4db',
        marginLeft: 10,
        resizeMode: 'cover',
        backgroundColor: '#fff',
    },
    title: {
        color: '#000',
        fontWeight: 'bold',
        fontSize: 16,
        flex: 1,
        marginTop: 5,
        textAlign: 'left'
    },
    bannerContainer: {
        margin: 20,
    },
    banner: {
        height: size.height / 7,
        width: '100%',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    infoContainer: {
        flex: 1,
        marginLeft: 10
    },
    name: {
        fontSize: 14,
        fontWeight: '600',
        color: '#fff',
        textTransform: 'capitalize'
    },
    phone: {
        marginTop: 5,
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 18
    },
    btnProfile: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 100,
        height: 30,
        marginTop: 10,
        marginRight: 10,
        backgroundColor: '#006699'
    },
    btnUpgrade: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 100,
        height: 30,
        marginTop: 10,
        backgroundColor: '#006699'
    },
    txtProfile: {
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 12
    },

    listItem: {
        flexDirection: 'row',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    },
    listMenuName: {
        flex: 1,
        marginLeft: 20,
        color: '#000',
        fontWeight: '700'
    },
    listIconContainer: {
        backgroundColor: '#fcfbfc',
        justifyContent: 'center',
        alignItems: 'center',
        width: 50,
        height: 50,
        borderRadius: 10
    },
    signoutContainer: {
        margin: 20,
        height: 50,
        width: 150,
        flexDirection: 'row',
        backgroundColor: '#e8fff1',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20
    },
    signoutIconContainer: {
        width: 50,
        backgroundColor: '#26c165',
        height: 40,
        marginRight: 15,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    txtSignout: {
        color: '#26c165',
        fontWeight: 'bold',
        flex: 2,
        textAlign: 'right',
        marginRight: 10
    },
    signoutWrapper: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    titleNavContainer: {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    titleNav: {
        color: '#26C165',
        fontWeight: 'bold',
        fontSize: 26
    },
    modal: {
        alignItems: 'center',
    },
    modal4: {
        height: Dimensions.get('window').height - 50,
        width: '95%',
        borderRadius: 20,
    },
    listContainer: {
        margin: 10,
        flex: 1
    },
    webView: {
        height: Dimensions.get('window').height - 150,
        width: Dimensions.get('window').width - 50,
        borderRadius: 20,
    },
    btnHeaderContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    btnSetuju: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        // backgroundColor: '#26C165',
        justifyContent: 'center',
        marginBottom: 20
    },
    txtSetuju: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
})

export default Akun;
