import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Container, Content, Button } from 'native-base';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { getValue } from '../../../module/LocalData/Storage';
import { post, postWithToken } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {ResponseCode} from '../../../supports/Constants';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import moment from 'moment';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import {dialog} from '../../../supports/Dialog';

class TransferConfirm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: [],
            inputs: {
              pin: {
                type: 'pin',
                value: ''
              }
            },
            dataTransfer: {},
            showSpinner: false,
            wrongCodeCount: 0,
            currentExpDate: ""
        };

        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
    }

    resetInputPin(){
      const { inputs } = this.state;
        this.setState({
            inputs: {
              ...inputs,
              ['pin']: {
                ...inputs['pin'],
                value : '',
                error:  null
              }
            }
        });
    }

    doTransfer() {
      this.setState({showSpinner: true});
      getValue('tokenLogin').then(tokenLogin => {
          let params = {
              "phone": this.props.navigation.getParam('phone'),
              "receiverPhone": this.props.navigation.getParam('receiverPhone'),
              "nominal": this.props.navigation.getParam('nominal'),
              "pin": this.state.inputs.pin.value,
              "note": this.props.navigation.getParam('note'),
              "fee": this.props.navigation.getParam('fee')
          }

          postWithToken(Env.base_url + Api.sakuTransferMtoM, params, tokenLogin).then(response => {
              if(response.code == ResponseCode.OK){
                this.setState({showSpinner: false});
                this.props.navigation.navigate('TransferPayment', {
                    payRes: response.okContent,
                    senderName: this.props.navigation.getParam('senderName'),
                    receiverName: this.props.navigation.getParam('receiverName')
                })
              } else {
                setTimeout(() => {
                  this.resetInputPin()
                  this.setState({showSpinner: false})
                  dialog.alertFail(response.failContent, this)
                }, 1000);
              }
          }).catch(err => {
            setTimeout(() => {
              this.resetInputPin()
              this.setState({showSpinner: false})
              dialog.alertException(err)
            }, 1000);
          });
        })
    }

    lupapin() {
        this.props.navigation.navigate('VerifikasiLupaPin', {
            phone: this.props.navigation.getParam('phone'),
            screenCallback : "TransferConfirm"
        })
    }

    render() {
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Image style={styles.imageLogo} source={require('../../../assets/image/shield.png')} />
                    </View>
                    <View style={styles.inputContainer}>
                        <Text style={styles.txtInfo1}>Masukkan PIN Transaksi Anda</Text>
                        <OTPInputView
                            style={{ width: '80%', height: 100, }}
                            pinCount={6}
                            code={this.state.inputs.pin.value} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                            onCodeChanged={value => { this.onInputChange({id: 'pin', value})}}
                            autoFocusOnLoad
                            secureTextEntry
                            codeInputFieldStyle={styles.underlineStyleBase}
                            codeInputHighlightStyle={styles.underlineStyleHighLighted}
                            onCodeFilled={(pin => {this.doTransfer(pin)})}
                        />
                        {this.validateError('pin')}
                    </View>

                    <View style={styles.btncontainer}>
                        <Button onPress={() => this.lupapin()} transparent style={styles.btnLupaPin}>
                            <Text style={styles.txtLupaPin}>Lupa PIN</Text>
                        </Button>
                        <View style={styles.btnBerandaContainer}>
                            <Text>Ingin membatalkan ? Silahkan kembali ke</Text>
                            <Button onPress={() => this.props.navigation.navigate('Home2')} transparent>
                                <Text style={styles.berandaTxt}>Beranda</Text>
                            </Button>
                        </View>
                    </View>

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    imageLogoContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40
    },
    imageLogo: {
        width: 150,
        height: 150
    },
    btncontainer: {
        margin: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    btnProses: {
        padding: 20,
        borderRadius: 25,
        width: 150,
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtProses: {
        color: '#fff',
        fontSize: 14,
        fontWeight: 'bold'
    },
    btnBerandaContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    berandaTxt: {
        color: '#2a8b40',
        paddingLeft: 5,
        fontWeight: 'bold'
    },
    txtInfo1: {
        fontWeight: 'bold'
    },
    txtInfo2: {
        fontSize: 13,
        paddingTop: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    txtInfo3: {
        fontSize: 13,
        paddingTop: 5,
        color: 'red',
        paddingLeft: 10,
        paddingRight: 10
    },
    btnLupaPin: {
        marginTop: 10
    },
    txtLupaPin: {
        color: '#2a8b40',
        fontWeight: 'bold'
    }
})

export default TransferConfirm;
