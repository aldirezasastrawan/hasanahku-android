import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, Platform, PermissionsAndroid, Modal, ScrollView, FlatList } from 'react-native';
import { Container, Content, Button, Form, Input, Item, Label, Card } from 'native-base';
import { Icon } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import { getValue } from '../../../module/LocalData/Storage';
import { postWithToken, post } from '../../../client/RestClient';
import Env from '../../../supports/Env';
import Api from '../../../supports/Api';
import {Error, ResponseCode} from '../../../supports/Constants';
import {requestReadContactsPermission} from '../../../supports/AccessPermission';
import {ValidationHelper} from '../../../validation/ValidationHelper';
import Contacts from 'react-native-contacts';
import RBSheet from "react-native-raw-bottom-sheet";
import {dialog} from '../../../supports/Dialog';
import { TextInputMask } from 'react-native-masked-text'

class Transfer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            colorSpinner: '#e35200',
            inputs: {
              receiverPhone: {
                type: 'phoneNumber',
                value: ''
              },
              nominal: {
                type: 'currencyIDR',
                value: ''
              }
            },
            receiverName: '',
            keterangan: '',
            checked: true,
            checkedFav: true,
            dataUser: {},
            Contact: [],
            isPhoneContactOpen: false,
            userInput: '',
            dataInquiryTransfer: {},
            hasConfirmation: false,
            hasSuccessConfirm: false
        };

        this.arrayholderContact = [];
        this.onInputChange = ValidationHelper.onInputChange.bind(this);
        this.validateInput = ValidationHelper.validateInput.bind(this);
        this.isValidate = ValidationHelper.isValidate.bind(this);
        this.validateError = ValidationHelper.validateError.bind(this);
    }

    componentDidMount() {
        this.getDataUser()
    }

    getDataUser() {
      this.setState({dataUser: this.props.navigation.getParam('userLogin')})
    }

    openPhoneContact() {
      requestReadContactsPermission().then(isPermission => {
        if(isPermission){
          this.setState({showSpinner: true})
          if(this.arrayholderContact.length > 0){
            setTimeout(() => {
              this.setState({
                showSpinner: false,
                isPhoneContactOpen: true
              })
            }, 1000);
          }else{
            Contacts.getAll((err, contacts) => {
                if (err === 'denied' || (err)) {
                    console.log(err);
                    this.setState({
                        showSpinner: false
                    })
                } else {
                    let contactss = contacts.filter(contacts => contacts.phoneNumbers.length != 0)
                    this.setState(
                        {
                            Contact: contactss,
                            showSpinner: false,
                            isPhoneContactOpen: true
                        },
                        function () {
                            this.arrayholderContact = contactss;
                        },
                    )
                }
            })
          }
        }
      })
    }

    closePhoneContact() {
        this.setState({
            isPhoneContactOpen: false
        })
    }

    selectPhoneContact(phoneNumber) {

      phoneNumber = phoneNumber.replace(/\s|-/g,'');
      let phoneLength = phoneNumber.length;

      if(phoneLength >= 3 && phoneNumber.substring(0,3) ==='+62'){
        phoneNumber = "0"+phoneNumber.substring(3, phoneLength);
      }

      this.setState({
          isPhoneContactOpen: false
      });

      if(this.validateInput({id: 'receiverPhone', value: phoneNumber}) == true){
        this.getDataReceiver(phoneNumber);
      }
    }

    getDataReceiver(phone) {
        this.setState({
            showSpinner: true
        });

        let params = {
            "phone": phone
        }
        post(Env.base_url + Api.inquiryDestinationAccount, params).then(response => {
            if(response.code == ResponseCode.OK){
                this.setState({
                    receiverName: response.okContent.nama,
                    showSpinner: false
                })
            } else {
              this.setState({showSpinner: false})
              dialog.alertFail(response.failContent, this)
            }
        }).catch(err => {
          this.setState({showSpinner: false})
          dialog.alertException(err)
        });
    }

    searching(userInput) {
        const newDataContact = this.arrayholderContact.filter(function (item) {
            const itemData = item.displayName
                ? item.displayName.toUpperCase()
                : ''.toUpperCase();
            const textData = userInput.toUpperCase();

            return itemData.indexOf(textData) > -1;
        });


        this.setState({
            Contact: newDataContact,
            userInput: userInput
        });
    }

    onChangeNominal(nominal){
      const { inputs } = this.state;
      this.setState({
          inputs: {
            ...inputs,
            ['nominal']: {
              ...inputs['nominal'],
              value : nominal,
              error:  null
            }
          }
      });
    }

    renderGrid = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => this.gotoGridMenu(item.menu)} activeOpacity={0.7} style={styles.gridIconContainer}>
                {/* <View style={[styles.iconContainer, {backgroundColor: item.color}]}> */}
                <View style={[styles.iconContainer]}>
                    <Icon size={30} name={item.icon} type={item.type} color='#fff' />
                </View>
                <Text style={styles.txtGrid}>{item.menu}</Text>
            </TouchableOpacity>
        )
    }

    inquiryTransfer() {

      if(this.isValidate() === false) return;

      this.setState({showSpinner: true})

      getValue('tokenLogin').then(tokenLogin => {
          let params = {
              "phone": this.state.dataUser.noHP,
              "receiverPhone": this.state.inputs.receiverPhone.value
          }

          postWithToken(Env.base_url + Api.sakuInquiryTransferMtoM, params, tokenLogin).then(response => {
               if(response.code == ResponseCode.OK){
                  this.setState({
                    showSpinner: false,
                    dataInquiryTransfer: response.okContent
                  })
                  this.RBSheet.open();
              } else {
                this.setState({showSpinner: false})
                dialog.alertFail(response.failContent, this)
              }
          }).catch(err => {
            this.setState({showSpinner: false})
            dialog.alertException(err)
          });
      })
    }

    transferConfirmation() {
        this.setState({
            hasConfirmation: true
        })

        setTimeout(() => {
            this.processTransfer()
        }, 500);
    }

    processTransfer() {
        this.setState({
            // hasConfirmation: false,
            hasSuccessConfirm: true
        })

        setTimeout(() => {
            this.RBSheet.close();
        }, 600);

        setTimeout(() => {
            const dataInquiry = this.state.dataInquiryTransfer;
            const nominal = this.moneyField.getRawValue();
            this.props.navigation.navigate('TransferConfirm', {
                "phone": dataInquiry.senderPhone,
                "receiverPhone": this.state.inputs.receiverPhone.value,
                "nominal": nominal,
                "note": this.state.keterangan,
                "senderName": dataInquiry.senderName,
                "receiverName": dataInquiry.receiverName,
                "fee": dataInquiry.fee
            })
        }, 1000);


        setTimeout(() => {
          this.setState({
              hasConfirmation: false,
              hasSuccessConfirm: false
          })
        }, 1200);

      /*  console.log("after dimari...")

        const dataInquiry = this.state.dataInquiryTransfer;
        this.props.navigation.navigate('TransferConfirm', {
            "phone": dataInquiry.senderPhone,
            "receiverPhone": this.state.inputs.receiverPhone.value,
            "nominal": this.state.inputs.nominal.value,
            "note": this.state.keterangan,
            "fee": "0",
            "senderName": dataInquiry.senderName,
            "receiverName": dataInquiry.receiverName
        }) */

    }

    render() {
        const datauser = this.state.dataUser
        const dataInquiry = this.state.dataInquiryTransfer
        const size = Dimensions.get('window')
        return (
            <Container>
                <Content>
                    <View style={styles.imageLogoContainer}>
                        <Button onPress={() => this.props.navigation.goBack()} androidRippleColor='#000' transparent style={styles.btnBack}>
                            <Icon name='arrowleft' size={35} type='antdesign' />
                        </Button>
                        <Image style={styles.profileImage} source={require('../../../assets/image/man.png')} />
                    </View>

                    <View styles={styles.titleContainer}>
                        <Text style={styles.title}>Info Kartu</Text>
                    </View>

                    <View style={styles.cardContainer}>
                        <Card style={styles.cardWrapper}>
                            <View style={styles.cardTitleContainer}>
                                <Text style={styles.cardTitle}>HasanahKu</Text>
                            </View>
                            <View style={styles.cardChipContainer}>
                                <LottieView
                                    style={{ height: 40, marginLeft: 3, transform: [{ scale: 1.5 }] }}
                                    autoSize={true}
                                    resizeMode="cover"
                                    source={require('../../../assets/animation/cc.json')}
                                    autoPlay
                                />
                            </View>
                            <View style={styles.cardNumberContainer}>
                                <Text style={styles.cardNumber}>{datauser.noHP}</Text>
                                {/* <Text style={styles.cardNumber}>5678</Text>
                                <Text style={styles.cardNumber}>9101</Text>
                                <Text style={styles.cardNumber}>1121</Text> */}
                            </View>

                            <View style={styles.cardOwnerContainer}>
                                <Text style={styles.cardOwner}>Mr. / Ms. {datauser.nama}</Text>
                            </View>
                        </Card>
                    </View>

                    <View styles={styles.titleContainer}>
                        <Text style={styles.title}>
                            Transfer
                        </Text>
                    </View>

                    <View style={styles.inputContainer}>
                        <Form>
                            <Item stackedLabel>
                                <Label style={styles.label}>Nomor Handphone</Label>
                                <View style={{ flexDirection: 'row' }}>
                                    <Input returnKeyType="search" style={styles.input}
                                    onEndEditing={() => this.getDataReceiver(this.state.inputs.receiverPhone.value)}
                                    keyboardType='numeric' value={this.state.inputs.receiverPhone.value}
                                    onChangeText={value => this.onInputChange({id: 'receiverPhone', value})}
                                    />
                                    <Icon onPress={() => this.openPhoneContact()} active name='contacts' type="antdesign" />
                                </View>
                            </Item>
                            {this.validateError('receiverPhone')}

                            <Item stackedLabel>
                                <Label style={styles.label}>Nama Penerima</Label>
                                <Input style={styles.input} disabled value={this.state.receiverName} onChangeText={(nama) => this.setState({ nama: nama })} />
                            </Item>
                            <View style={{marginLeft: 15}}>
                              <Label style={{fontWeight: 'bold', fontSize: 13, marginTop:10, color: '#575757'}}>Nominal</Label>
                              <TextInputMask
                                type={'money'}
                                options={{
                                  precision: 0,
                                  separator: ',',
                                  delimiter: '.',
                                  unit: 'Rp',
                                  suffixUnit: ''
                                }}
                                value={this.state.inputs.nominal.value}
                                onChangeText={value => this.onChangeNominal(value)}
                                ref={(ref) => this.moneyField = ref}
                              />
                            <View style={{borderBottomColor: '#D9D5DC',borderBottomWidth: 1,}}/>
                            </View>
                            {this.validateError('nominal')}
                            <Item stackedLabel>
                                <Label style={styles.label}>Keterangan</Label>
                                <Input style={styles.input} value={this.state.keterangan} onChangeText={(keterangan) => this.setState({ keterangan: keterangan })} />
                            </Item>
                        </Form>
                    </View>
                    <View style={styles.btnRegisterContainer}>
                        <Button onPress={() => this.inquiryTransfer()} style={styles.btnRegister}>
                            <Text style={styles.RegisterText}>Transfer</Text>
                        </Button>
                    </View>

                    <Spinner
                        visible={this.state.showSpinner}
                        color={this.state.colorSpinner}
                        textStyle={styles.spinnerTextStyle}
                        customIndicator={
                            <LottieView
                                style={{ height: 40, transform: [{ scale: 1.6 }] }}
                                autoSize={true}
                                source={require('../../../assets/animation/loading2.json')}
                                autoPlay
                            />
                        }
                    />

                    <Modal animationType={'slide'} transparent={false}
                        visible={this.state.isPhoneContactOpen}
                        // presentationStyle='pageSheet'
                        onRequestClose={() => this.closePhoneContact()}>

                        <Container style={styles.modal}>
                            <Content style={{ height: Dimensions.get('window').height + 10 }}>

                                <View style={styles.backModalContainer}>
                                    <Button onPress={() => this.closePhoneContact()} androidRippleColor='#000' transparent style={styles.btnBack}>
                                        <Icon name='arrowleft' size={30} type='antdesign' />
                                    </Button>
                                    <Image style={styles.profileImage} source={require('../../../assets/image/man.png')} />
                                </View>

                                <View style={styles.modalTitleContainer}>
                                    <Text style={styles.titleModal}>Sahabat Hasanah</Text>
                                    <Text>{this.state.Contact.length} available</Text>
                                </View>

                                <View style={styles.modalHeader}>

                                    <View style={styles.modalInputHeader}>
                                        <Icon
                                            name="search"
                                            type="font-awesome"
                                            size={20}
                                            color="#02aced"
                                            containerStyle={styles.inputIcon}
                                        />
                                        <Input
                                            style={styles.inputHeader}
                                            placeholder="Search by name"
                                            autoCapitalize="none"
                                            onChangeText={text => this.searching(text)}
                                            onClear={text => this.SearchFilterFunction('')}
                                            underlineColorAndroid="transparent"
                                            value={this.state.userInput}
                                        />
                                    </View>
                                    {/*
                                    <Button onPress={() => this.closePhoneContact()} style={styles.btnCloseHeader}>
                                        <Text style={styles.btnCloseText}>Tutup</Text>
                                    </Button> */}

                                </View>

                                <View style={styles.listContainer}>
                                    <View style={styles.kirimKeContainer}>
                                        <Text>Kirim transfer ke: </Text>
                                    </View>
                                    <ScrollView >

                                        <FlatList
                                            data={this.state.Contact}
                                            renderItem={({ item }) => (
                                                <View style={styles.contactListContainer}>
                                                    <View style={styles.avaContainer}>
                                                        <Text style={styles.txtAva}>{item.displayName == undefined ? "Z" : item.displayName == null ? "Z" : item.displayName.slice(0, 1)}</Text>
                                                    </View>
                                                    <View style={styles.infoContact}>
                                                        <Text style={styles.contactName}>{item.displayName} {item.middleName} {item.familyName}</Text>
                                                        {item.phoneNumbers.map(number => {
                                                            return (
                                                                <View style={styles.contactContainer}>
                                                                    <Text style={styles.contactNumber}>{number.number}</Text>
                                                                    <View style={styles.btnPilihContainer}>
                                                                        <Button onPress={() => this.selectPhoneContact(number.number)} style={styles.btnPilih}>
                                                                            <Text style={styles.txtPilih}>pilih</Text>
                                                                        </Button>
                                                                    </View>
                                                                </View>
                                                            )
                                                        })}
                                                    </View>

                                                </View>
                                            )}
                                            extraData={this.state}
                                        />


                                        {/* {this.state.Contact.map(contact => {
                                            return (
                                                <View style={styles.contactListContainer}>
                                                    <View style={styles.avaContainer}>
                                                        <Text style={styles.txtAva}>{contact.displayName == undefined ? "Z" : contact.displayName == null ? "Z" : contact.displayName.slice(0, 1)}</Text>
                                                    </View>
                                                    <View style={styles.infoContact}>
                                                        <Text style={styles.contactName}>{contact.displayName} {contact.middleName} {contact.familyName}</Text>
                                                        {contact.phoneNumbers.map(number => {
                                                            return (
                                                                <View style={styles.contactContainer}>
                                                                    <Text style={styles.contactNumber}>{number.number}</Text>
                                                                    <View style={styles.btnPilihContainer}>
                                                                        <Button onPress={() => this.selectPhoneContact(number.number)} style={styles.btnPilih}>
                                                                            <Text style={styles.txtPilih}>pilih</Text>
                                                                        </Button>
                                                                    </View>
                                                                </View>
                                                            )
                                                        })}
                                                    </View>

                                                </View>
                                            )
                                        })} */}


                                    </ScrollView>
                                </View>

                            </Content>

                        </Container>
                    </Modal>

                    <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown
                        height={Platform.OS == 'android' ? size.height - 50 : size.height - 50}
                        // animationType="fade"
                        duration={300}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 15,
                                borderTopLeftRadius: 15,
                            },
                        }}>
                        <View>
                            <View style={styles.sheetHeader}>
                                <Text style={styles.sheetTitle}>
                                    Konfirmasi Transfer
                                </Text>
                            </View>
                            <View style={styles.inputContainer}>

                                <Form>
                                    <Item stackedLabel>
                                        <Label style={styles.label}>Nama Pengirim</Label>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Input style={styles.input1} disabled value={dataInquiry.senderName} />
                                        </View>
                                    </Item>

                                    <Item stackedLabel>
                                        <Label style={styles.label}>Nama Penerima</Label>
                                        <Input style={styles.input1} disabled value={dataInquiry.receiverName} />
                                    </Item>

                                    <Item stackedLabel>
                                        <Label style={styles.label}>Nominal</Label>
                                        <Input style={styles.input1} value={this.state.inputs.nominal.value} disabled />
                                    </Item>
                                    <Item stackedLabel>
                                        <Label style={styles.label}>Keterangan</Label>
                                        <Input numberOfLines={1} style={styles.input1} value={this.state.keterangan} disabled />
                                    </Item>
                                </Form>

                            </View>

                            <View style={styles.btnConfirmationContainer}>
                                {this.state.hasConfirmation == false ?
                                    <Button onPress={() => this.transferConfirmation()} style={styles.btnConfirmation}>
                                        <Text style={styles.txtConfirmation}>
                                            Konfirmasi Transfer
                                </Text>
                                    </Button>

                                    :
                                    this.state.hasSuccessConfirm == false ?
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                                            <LottieView
                                                style={{ height: 40, transform: [{ scale: 1.5 }] }}
                                                autoSize={true}
                                                resizeMode="cover"
                                                source={require('../../../assets/animation/searching1.json')}
                                                autoPlay
                                            />

                                            <Text style={styles.txtOnProcess}>On process ...</Text>

                                        </View>

                                        :

                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                                            <LottieView
                                                style={{ height: 40, transform: [{ scale: 1.5 }] }}
                                                autoSize={true}
                                                resizeMode="cover"
                                                source={require('../../../assets/animation/checked.json')}
                                                autoPlay
                                            />

                                            <Text style={styles.txtOnProcess}>Data terverifikasi ...</Text>

                                        </View>
                                }
                            </View>

                        </View>
                    </RBSheet>

                </Content>
            </Container >
        );
    }
}

const styles = StyleSheet.create({
    btnBack: {
        flex: 2,
        marginLeft: 20,
        marginTop: 5
    },
    imageLogoContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    imageLogo: {
        height: 80,
        width: 100,
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    profileImage: {
        height: 60,
        width: 60,
        borderRadius: 50,
        backgroundColor: '#fff',
        borderWidth: 0.5,
        borderColor: '#dedede'
    },
    titleContainer: {
        marginTop: 10,
    },
    title: {
        fontSize: 24,
        color: '#2a8b40',
        paddingLeft: 30,
        paddingTop: 20,
        fontWeight: 'bold'
    },
    cardContainer: {
        margin: 20
    },
    cardWrapper: {
        height: 200,
        borderRadius: 10,
        // backgroundColor: '#e35200'
        backgroundColor: '#2a8b40'
    },
    cardTitleContainer: {
        margin: 20
    },
    cardTitle: {
        fontSize: 24,
        color: '#fff'
    },
    cardChipContainer: {
        height: 40,
        width: 70,
        borderWidth: 0.5,
        borderColor: '#dedede',
        marginLeft: 20,
        borderRadius: 5,
        backgroundColor: '#f5cf1a'
    },
    cardNumberContainer: {
        margin: 10,
        flexDirection: 'row'
    },
    cardNumber: {
        color: '#fff',
        padding: 10,
        fontSize: 20
    },
    cardOwnerContainer: {
        marginLeft: 25
    },
    cardOwner: {
        color: '#fff',
        textTransform: 'capitalize'
    },
    inputContainer: {
        margin: 20
    },
    checkContainer: {
        marginTop: -20,
    },
    checkTextContainer: {
        flexDirection: 'row',
        marginRight: 20
    },
    txtSetuju: {
        marginTop: 14,
        fontSize: 12
    },
    txtSyarat: {
        fontSize: 12,
        marginTop: 2,
        color: '#385757',
        fontWeight: 'bold'
    },
    btnRegisterContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20
    },
    btnRegister: {
        width: Dimensions.get('window').width - 150,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#2a8b40',
        justifyContent: 'center'
    },
    RegisterText: {
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    label: {
        fontWeight: 'bold',
        fontSize: 13
    },
    input: {
        fontSize: 13
    },
    input1: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    modal: {
        flex: 1,
    },
    modalHeader: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    modalInputHeader: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#f7f7f7',
        borderRadius: 30,
        width: Dimensions.get('window').width - 50,
        height: 40,
        flexDirection: 'row',
        alignItems: 'center',
        shadowColor: '#666',
        shadowOffset: { width: 0, height: 0.8 },
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 5,
    },
    inputIcon: {
        marginLeft: 15
    },
    inputHeader: {
        height: 40,
        marginLeft: 5,
        borderBottomColor: '#FFFFFF',
        flex: 1,
        fontSize: 13
    },
    btnCloseHeader: {
        backgroundColor: '#02aced',
        marginLeft: 10,
        marginRight: 10,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnCloseText: {
        color: '#fff',
        paddingLeft: 10,
        paddingRight: 10,
        fontWeight: 'bold'
    },
    listContainer: {
        flex: 2,
        margin: 20
    },
    contactListContainer: {
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        margin: 5,
    },
    backModalContainer: {
        flexDirection: 'row',
        marginRight: 20,
        marginTop: 20,
        flex: 1
    },
    contactName: {
        fontWeight: 'bold',
        fontSize: 18
    },
    avaContainer: {
        backgroundColor: '#fff',
        borderWidth: 2,
        borderColor: '#dedede',
        borderRadius: 50,
        width: 60,
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10
    },
    txtAva: {
        fontSize: 24
    },
    infoContact: {
        marginBottom: 10,
        marginLeft: 15,
        width: '70%'
        // justifyContent: 'center',
    },
    contactNumber: {
        color: '#666',
        fontSize: 13,
        padding: 3,
        flex: 2
    },
    contactContainer: {
        marginTop: 5,
        marginBottom: 5,
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f7f7f7',
        width: '100%'
    },
    modalTitleContainer: {
        marginLeft: 25
    },
    titleModal: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    kirimKeContainer: {
        margin: 10,
        justifyContent: 'center',
    },
    btnPilih: {
        height: 30,
        padding: 5,
        backgroundColor: '#02aced',
    },
    txtPilih: {
        color: '#fff',
        fontWeight: 'bold'
    },
    btnPilihContainer: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    sheetHeader: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    sheetTitle: {
        flex: 2,
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    todayContainer: {
        flexDirection: 'row',
        backgroundColor: '#eee',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        marginBottom: 10
    },
    btnConfirmationContainer: {
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnConfirmation: {
        backgroundColor: '#2a8b40',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20,
        width: 300
    },
    txtConfirmation: {
        fontWeight: 'bold',
        color: '#fff',
        fontSize: 14
    },
    txtOnProcess: {
        textAlign: 'center',
        paddingTop: 15,
        color: '#666'
    }

})

export default Transfer;
