import axios from 'axios';

export const postData = (url, data, token) => {
    var headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
    };

    return new Promise((resolve, reject) => {
        axios
            .post(url, data, { headers: headers })
            .then(res => {
              console.log('data resolve di rest : ', res.status);
                resolve(res.data);
              //  console.log('data resolve di rest', res);
            })
            .catch(err => {
              console.log('catch reject is execute : ', err.status);
                reject(err);
              //  console.log('data reject di rest', err.response);
            });
    });
};

export const getData = (url, token) => {
    var headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
    };
    return new Promise((resolve, reject) => {
        axios
            .get(url, { headers: headers })
            .then(res => {
              console.log("resolve getData : "+JSON.stringify(res))
                resolve(res);
            //    console.log('data resolve di rest', res);
            })
            .catch(err => {
              console.log("reject getData : "+JSON.stringify(err.response))
                reject(err);
              //  console.log('data reject di rest', err);
            });
    });
};

export const patchData = (url, data, token) => {
    var headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
    };
    return new Promise((resolve, reject) => {
        axios
            .put(url, data, { headers: headers })
            .then(res => {
                resolve(res.data);
              //  console.log('data resolve di rest', res);
            })
            .catch(err => {
                reject(err);
            //    console.log('data reject di rest', err.response);
            });
    });
};
