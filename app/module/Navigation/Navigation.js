import React from 'react';
import { View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import { createStackNavigator } from 'react-navigation-stack';
import Home from '../../components/screen/Homepage/Home';
import Akun from '../../components/screen/Akun/Akun';
import Scan from '../../components/screen/Scan/Scan';
import { Icon } from 'react-native-elements';
import Lupapin from '../../components/screen/Lupapin/Lupapin';
import Tutupakun from '../../components/screen/Tutupakun/Tutupakun';
import Bukaakun from '../../components/screen/Bukaakun/Bukaakun';
import Login from '../../components/screen/Login/Login';
import VerifyLoginOtherDevice from '../../components/screen/Login/VerifyLoginOtherDevice';
import Ubahpin from '../../components/screen/Ubahpin/Ubahpin';
import Welcome from '../../components/screen/Welcome/Welcome';
import Register from '../../components/screen/Register/Register';
import RegisterNewUser1 from '../../components/screen/Register/RegisterNewUser1';
import RegisterNewUser2 from '../../components/screen/Register/RegisterNewUser2';
import RegisterNewUser3 from '../../components/screen/Register/RegisterNewUser3';
import RegisterNewUser4 from '../../components/screen/Register/RegisterNewUser4';
import RegisterUserExist1 from '../../components/screen/Register/RegisterUserExist1';
import RegisterUserExist2 from '../../components/screen/Register/RegisterUserExist2';
import RegisterUserExist3 from '../../components/screen/Register/RegisterUserExist3';
import VerificationUserExist from '../../components/screen/Register/VerificationUserExist';
import RegisterUserExistChannelReg1 from '../../components/screen/Register/RegisterUserExistChannelReg1';
import RegisterUserExistChannelReg2 from '../../components/screen/Register/RegisterUserExistChannelReg2';
import RegisterUserExistChannelUnreg1 from '../../components/screen/Register/RegisterUserExistChannelUnreg1';
import RegisterUserExistChannelUnreg2 from '../../components/screen/Register/RegisterUserExistChannelUnreg2';
import RegisterUserExistChannelUnreg3 from '../../components/screen/Register/RegisterUserExistChannelUnreg3';
import Hubungikami from '../../components/screen/Hubungikami/Hubungikami';
import Transfer from '../../components/screen/Transfer/Transfer';
import Tariksaldo from '../../components/screen/Tariksaldo/Tariksaldo';
import TarikSaldoViaTransfer from '../../components/screen/Tariksaldo/TarikSaldoViaTransfer';
import TarikSaldoViaTransferConfirm from '../../components/screen/Tariksaldo/TarikSaldoViaTransferConfirm';
import TarikSaldoViaTransferPayment from '../../components/screen/Tariksaldo/TarikSaldoViaTransferPayment';
import Riwayat from '../../components/screen/Riwayat/Riwayat';
import RiwayatList from '../../components/screen/Riwayat/RiwayatList';
import Paketdata from '../../components/screen/Paketdata/Paketdata';
import Asuransi from '../../components/screen/Asuransi/Asuransi';
import Air from '../../components/screen/Air/Air';
import Topupue from '../../components/screen/Topupue/Topupue';
import Zakatwakaf from '../../components/screen/Zakatwakaf/Zakatwakaf';
import PaketdataConfirmation from '../../components/screen/PaketdataConfirmation/PaketdataConfirmation';
import AsuransiConfirmation from '../../components/screen/AsuransiConfirmation/AsuransiConfirmation';
import AirConfirmation from '../../components/screen/AirConfirmation/AirConfirmation';
import TopupueConfirmation from '../../components/screen/TopupueConfirmation/TopupueConfirmation';
import ZakatwakafConfirmation from '../../components/screen/ZakatwakafConfirmation/ZakatwakafConfirmation';
import TopUp from '../../components/screen/TopUp/TopUp';
import TopUpDebit from '../../components/screen/TopUpDebit/TopUpDebit';
import TopUpKredit from '../../components/screen/TopUpKredit/TopUpKredit';
import TopUpChannel from '../../components/screen/TopUpChannel/TopUpChannel';
import TopUpDetail from '../../components/screen/TopUpDetail/TopUpDetail';
import Notification from '../../components/screen/Notification/Notification';
import NotificationDetail from '../../components/screen/NotificationDetail/NotificationDetail';
import Chat from '../../components/screen/Chat/Chat';
import TransferConfirm from '../../components/screen/Transfer/TransferConfirm';
import TransferPayment from '../../components/screen/Transfer/TransferPayment';
import Onboard from '../../components/screen/Onboard/Onboard';
import Home2 from '../../components/screen/Home2/Home2';
import ListMenu from '../../components/screen/Listmenu/ListMenu';
import Profile from '../../components/screen/Profile/Profile';
import UbahPassword from '../../components/screen/UbahPassword/UbahPassword'
import LupaPassword from '../../components/screen/LupaPassword/LupaPassword'
import VerifikasiLupaPassword from '../../components/screen/VerifikasiLupaPassword/VerifikasiLupaPassword';
import VerifikasiLupaPin from '../../components/screen/Lupapin/VerifikasiLupaPin'
import TarikSaldoCabangSlip from '../../components/screen/TarikSaldoCabangSlip/TarikSaldoCabangSlip';
import Bantuan from '../../components/screen/Bantuan/Bantuan';
import WebViewRekeningOnline from '../../components/screen/Others/WebViewRekeningOnline';
import UpgradeAccount from '../../components/screen/UpgradeAccount/UpgradeAccount';
import Pln from '../../components/screen/Pln/Pln';
import PlnPrabayarConfirm from '../../components/screen/Pln/PlnPrabayarConfirm';
import PlnPrabayarPayment from '../../components/screen/Pln/PlnPrabayarPayment';
import PlnPascaBayarConfirm from '../../components/screen/Pln/PlnPascaBayarConfirm';
import PlnPascaBayarPayment from '../../components/screen/Pln/PlnPascaBayarPayment';
import Pulsa from '../../components/screen/Pulsa/Pulsa';
import PulsaPrabayarConfirm from '../../components/screen/Pulsa/PulsaPrabayarConfirm';
import PulsaPrabayarPayment from '../../components/screen/Pulsa/PulsaPrabayarPayment';
import PulsaPascaBayarConfirm from '../../components/screen/Pulsa/PulsaPascaBayarConfirm';
import PulsaPascaBayarPayment from '../../components/screen/Pulsa/PulsaPascaBayarPayment';

const TABS = createMaterialTopTabNavigator(
    {
        Home: {
            screen: Home,
            path: 'home',
            navigationOptions: {
                tabBarIcon: ({ focused }) =>
                    focused ? (
                        <View>
                            <Icon
                                type="ionicon"
                                color='#2a8b40'
                                size={25}
                                name='ios-home'
                            />
                        </View>
                    ) : (
                            <View>
                                <Icon
                                    type="ionicon"
                                    color='#385757'
                                    size={25}
                                    name='ios-home'
                                />
                            </View>
                        ),
            },
        },
        Scan: {
            screen: Scan,
            path: 'scan',
            navigationOptions: {
                tabBarIcon: ({ focused }) =>
                    focused ?
                        (
                            <View>
                                <Icon
                                    type="antdesign"
                                    color='#2a8b40'
                                    size={25}
                                    name='scan1'
                                />
                            </View>

                        ) : (
                            <View>
                                <Icon
                                    type="antdesign"
                                    color='#385757'
                                    size={25}
                                    name='scan1'
                                />
                            </View>
                        ),
            },
        },
        Akun: {
            screen: Akun,
            path: 'akun',
            navigationOptions: {
                tabBarIcon: ({ focused }) =>
                    focused ? (
                        <View>
                            <Icon
                                type="octicon"
                                color='#2a8b40'
                                size={25}
                                name='person'
                            />
                        </View>
                    ) : (
                            <View>
                                <Icon
                                    type="octicon"
                                    color='#385757'
                                    size={25}
                                    name='person'
                                />
                            </View>
                        ),
            },
        },
    },
    {
        swipeEnabled: false,
        lazy: true,
        tabBarOptions: {
            activeTintColor: '#2a8b40',
            inactiveTintColor: '#385757',
            showIcon: true,
            showLabel: false,
            style: {
                backgroundColor: '#fff',
                elevation: 5,
                paddingBottom: 2,
                height: Platform.OS == 'android' ? 55 : 65,
            },
            labelStyle: {
                fontSize: 9,
                // color: '#e35200',
                marginTop: -2,
            },
            indicatorStyle: {
                backgroundColor: '#fff',
            },
            pressColor: '#2a8b40',
        },
        initialRouteName: 'Home',
        tabBarPosition: 'bottom',
    },
);

const AppStackNavigator = createStackNavigator({
    Onboard: {
        screen: Onboard,
        navigationOptions: {
            header: null
        }
    },
    Welcome: {
        screen: Welcome,
        navigationOptions: {
            header: null
        }
    },
    Login: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    },
    VerifyLoginOtherDevice: {
        screen: VerifyLoginOtherDevice,
        navigationOptions: {
            header: null
        }
    },
    Home2: {
        screen: Home2,
        navigationOptions: {
            header: null,
            gesturesEnabled: false,
        },
    },
    Scan: {
        screen: Scan,
        navigationOptions: {
            header: null,
        },
    },
    Akun: {
        screen: Akun,
        navigationOptions: {
            header: null,
        },
    },
    Register: {
        screen: Register,
        navigationOptions: {
            header: null
        }
    },
    RegisterNewUser1: {
        screen: RegisterNewUser1,
        navigationOptions: {
            header: null
        }
    },
    RegisterNewUser2: {
        screen: RegisterNewUser2,
        navigationOptions: {
            header: null
        }
    },
    RegisterNewUser3: {
        screen: RegisterNewUser3,
        navigationOptions: {
            header: null
        }
    },
    RegisterNewUser4: {
        screen: RegisterNewUser4,
        navigationOptions: {
            header: null
        }
    },
    RegisterUserExist1: {
        screen: RegisterUserExist1,
        navigationOptions: {
            header: null
        }
    },
    RegisterUserExist2: {
        screen: RegisterUserExist2,
        navigationOptions: {
            header: null
        }
    },
    RegisterUserExist3: {
        screen: RegisterUserExist3,
        navigationOptions: {
            header: null
        }
    },
    VerificationUserExist: {
        screen: VerificationUserExist,
        navigationOptions: {
            header: null
        }
    },
    RegisterUserExistChannelReg1: {
        screen: RegisterUserExistChannelReg1,
        navigationOptions: {
            header: null
        }
    },
    RegisterUserExistChannelReg2: {
        screen: RegisterUserExistChannelReg2,
        navigationOptions: {
            header: null
        }
    },
    RegisterUserExistChannelUnreg1: {
        screen: RegisterUserExistChannelUnreg1,
        navigationOptions: {
            header: null
        }
    },
    RegisterUserExistChannelUnreg2: {
        screen: RegisterUserExistChannelUnreg2,
        navigationOptions: {
            header: null
        }
    },
    RegisterUserExistChannelUnreg3: {
        screen: RegisterUserExistChannelUnreg3,
        navigationOptions: {
            header: null
        }
    },
    Lupapin: {
        screen: Lupapin,
        navigationOptions: {
            header: null
        }
    },
    LupaPassword: {
        screen: LupaPassword,
        navigationOptions: {
            header: null
        }
    },
    ListMenu: {
        screen: ListMenu,
        navigationOptions: {
            header: null
        }
    },
    Bantuan: {
        screen: Bantuan,
        navigationOptions: {
            header: null
        }
    },
    WebViewRekeningOnline: {
        screen: WebViewRekeningOnline,
        navigationOptions: {
            header: null
        }
    },
    Profile: {
        screen: Profile,
        navigationOptions: {
            header: null
        }
    },
    Tutupakun: {
        screen: Tutupakun,
        navigationOptions: {
            header: null
        }
    },
    VerifikasiLupaPassword: {
        screen: VerifikasiLupaPassword,
        navigationOptions: {
            header: null
        }
    },
    VerifikasiLupaPin: {
        screen: VerifikasiLupaPin,
        navigationOptions: {
            header: null
        }
    },
    Bukaakun: {
        screen: Bukaakun,
        navigationOptions: {
            header: null
        }
    },
    Ubahpin: {
        screen: Ubahpin,
        navigationOptions: {
            header: null
        }
    },
    UbahPassword: {
        screen: UbahPassword,
        navigationOptions: {
            header: null
        }
    },
    Hubungikami: {
        screen: Hubungikami,
        navigationOptions: {
            header: null
        }
    },
    Transfer: {
        screen: Transfer,
        navigationOptions: {
            header: null
        }
    },
    TransferConfirm: {
        screen: TransferConfirm,
        navigationOptions: {
            header: null,
        }
    },
    TransferPayment: {
        screen: TransferPayment,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Tariksaldo: {
        screen: Tariksaldo,
        navigationOptions: {
            header: null
        }
    },
    TarikSaldoViaTransfer: {
        screen: TarikSaldoViaTransfer,
        navigationOptions: {
            header: null
        }
    },
    TarikSaldoViaTransferConfirm: {
        screen: TarikSaldoViaTransferConfirm,
        navigationOptions: {
            header: null,
        }
    },
    TarikSaldoViaTransferPayment: {
        screen: TarikSaldoViaTransferPayment,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    TarikSaldoCabangSlip: {
        screen: TarikSaldoCabangSlip,
        navigationOptions: {
            header: null,
            gesturesEnabled: false
        }
    },
    Riwayat: {
        screen: Riwayat,
        navigationOptions: {
            header: null
        }
    },
    RiwayatList: {
        screen: RiwayatList,
        navigationOptions: {
            header: null
        }
    },
    Pln: {
        screen: Pln,
        navigationOptions: {
            header: null
        }
    },
    PlnPrabayarConfirm: {
        screen: PlnPrabayarConfirm,
        navigationOptions: {
            header: null
        }
    },
    PlnPrabayarPayment: {
        screen: PlnPrabayarPayment,
        navigationOptions: {
            header: null
        }
    },
    PlnPascaBayarConfirm: {
        screen: PlnPascaBayarConfirm,
        navigationOptions: {
            header: null
        }
    },
    PlnPascaBayarPayment: {
        screen: PlnPascaBayarPayment,
        navigationOptions: {
            header: null
        }
    },
    Paketdata: {
        screen: Paketdata,
        navigationOptions: {
            header: null
        }
    },
    PaketdataConfirmation: {
        screen: PaketdataConfirmation,
        navigationOptions: {
            header: null
        }
    },
    Pulsa: {
        screen: Pulsa,
        navigationOptions: {
            header: null
        }
    },
    PulsaPrabayarConfirm: {
        screen: PulsaPrabayarConfirm,
        navigationOptions: {
            header: null
        }
    },
    PulsaPrabayarPayment: {
        screen: PulsaPrabayarPayment,
        navigationOptions: {
            header: null
        }
    },
    PulsaPascaBayarConfirm: {
        screen: PulsaPascaBayarConfirm,
        navigationOptions: {
            header: null
        }
    },
    PulsaPascaBayarPayment: {
        screen: PulsaPascaBayarPayment,
        navigationOptions: {
            header: null
        }
    },
    Asuransi: {
        screen: Asuransi,
        navigationOptions: {
            header: null
        }
    },
    AsuransiConfirmation: {
        screen: AsuransiConfirmation,
        navigationOptions: {
            header: null
        }
    },
    Air: {
        screen: Air,
        navigationOptions: {
            header: null
        }
    },
    AirConfirmation: {
        screen: AirConfirmation,
        navigationOptions: {
            header: null
        }
    },
    TopUp: {
        screen: TopUp,
        navigationOptions: {
            header: null
        }
    },
    TopUpDebit: {
        screen: TopUpDebit,
        navigationOptions: {
            header: null
        }
    },
    TopUpKredit: {
        screen: TopUpKredit,
        navigationOptions: {
            header: null
        }
    },
    TopUpChannel: {
        screen: TopUpChannel,
        navigationOptions: {
            header: null
        }
    },
    TopUpDetail: {
        screen: TopUpDetail,
        navigationOptions: {
            header: null
        }
    },
    Topupue: {
        screen: Topupue,
        navigationOptions: {
            header: null
        }
    },
    TopupueConfirmation: {
        screen: TopupueConfirmation,
        navigationOptions: {
            header: null
        }
    },
    Zakatwakaf: {
        screen: Zakatwakaf,
        navigationOptions: {
            header: null
        }
    },
    ZakatwakafConfirmation: {
        screen: ZakatwakafConfirmation,
        navigationOptions: {
            header: null
        }
    },
    Notification: {
        screen: Notification,
        navigationOptions: {
            header: null
        }
    },
    NotificationDetail: {
        screen: NotificationDetail,
        navigationOptions: {
            header: null
        }
    },
    Chat: {
        screen: Chat,
        navigationOptions: {
            header: null
        }
    },
    UpgradeAccount: {
        screen: UpgradeAccount,
        navigationOptions: {
            header: null
        }
    }
});

export const AppContainer = createAppContainer(AppStackNavigator);
