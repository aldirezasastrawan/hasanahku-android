import moment from 'moment';

const decimalDelimeter = ",";
const sectionDelimeter = ".";

function currencyFormat(amount, decimalCount){
  var result = amount;
    try{
        var nominal = parseFloat(amount);
        decimalCount = isNaN(decimalCount = Math.abs(decimalCount)) ? 2 : decimalCount;
        var negativeSign = nominal < 0 ? "-" : "";
        var i = String(parseInt(nominal = Math.abs(Number(nominal) || 0).toFixed(decimalCount)));
        var j = (j = i.length) > 3 ? j % 3 : 0;

        result = negativeSign +(j ? i.substr(0, j) + sectionDelimeter : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + sectionDelimeter) + (decimalCount ? decimalDelimeter + Math.abs(nominal - i).toFixed(decimalCount).slice(2) : "");

    }catch (e){
        result = amount;
    }

    return result;
};

function dateToDDmmYYYY(strDate){
  var result = strDate;
  try{
    let dateList = moment(strDate).format('DD-MMMM-YYYY').split("-");
    let dd = parseInt(dateList[0]);
    result = dd+" "+dateList[1]+" "+dateList[2];

  }catch (e){
    //handling error here
  }
  return result;
}

export const utils = {
	currencyFormat, dateToDDmmYYYY
}
