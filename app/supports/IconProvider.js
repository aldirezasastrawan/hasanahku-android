
import {Icon} from './Images';

selected = iconName => {
  const iconList = {
    'axis' : Icon.logos.axis,
    'fren' : Icon.logos.fren,
    'halo' : Icon.logos.halo,
    'indosat' : Icon.logos.indosat,
    'smartfren' : Icon.logos.smartfren,
    'telkomsel' : Icon.logos.telkomsel,
    'tri' : Icon.logos.tri,
    'xl' : Icon.logos.xl,
    '' : Icon.logos.default
  };

  return iconList[iconName];
};

export const IconProvider = {
  selected
}
