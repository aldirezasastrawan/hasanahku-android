
export const Error = {
  timeout: 'Maaf, Gagal terhubung ke server. Periksa koneksi jaringan dan coba lagi.',
  new_pin_confirmation_pin_not_match : 'PIN Konfirmasi tidak sama dengan PIN',
  cannot_connect_to_server : 'Maaf, Gagal terhubung ke server. Silakan coba beberapa saat lagi.',
}

export const ResponseCode = {
  OK: 'OK',
  FAIL: 'FAIL'
}
