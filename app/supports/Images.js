export const Icon = {
  logos : {
    axis : require('../assets/image/axis.png'),
    fren : require('../assets/image/fren.png'),
    halo : require('../assets/image/halo.png'),
    indosat : require('../assets/image/indosat.png'),
    smartfren : require('../assets/image/smartfren.png'),
    telkomsel : require('../assets/image/telkomsel.png'),
    tri : require('../assets/image/tri.png'),
    xl : require('../assets/image/xl.png'),
    default : require('../assets/image/icon_provider_default.png')
  },
  menus : {
    asuransi : require('../assets/image/menu/asuransi.png'),
    topup : require('../assets/image/menu/top-up.png'),
    wallet : require('../assets/image/menu/wallet.png'),
    paketdata : require('../assets/image/menu/paket-data.png'),
    pdam : require('../assets/image/menu/pdam.png'),
    pln : require('../assets/image/menu/pln.png'),
    pulsa : require('../assets/image/menu/pulsa.png'),
    tariktunai : require('../assets/image/menu/tarik-tunai.png'),
    telkompay : require('../assets/image/menu/telkom-pay.png'),
    transfer : require('../assets/image/menu/transfer.png'),
    tvcable : require('../assets/image/menu/tv-cable.png'),
    zakat : require('../assets/image/menu/zakat.png'),
    lainnya : require('../assets/image/menu/lainnya.png')
  }
};
