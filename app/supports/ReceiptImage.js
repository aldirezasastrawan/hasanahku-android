import React, { Component } from "react";
import { ToastAndroid, Dimensions } from "react-native";
import Spinner from 'react-native-loading-spinner-overlay';
import LottieView from 'lottie-react-native';
import PropTypes from "prop-types";
import {Error, ResponseCode} from './Constants';
import RNFetchBlob from 'rn-fetch-blob';
import Share from "react-native-share";
import {post } from '../client/RestClient';
import {requestWriteExternalStoragePermission} from './AccessPermission';
import { getValue } from '../module/LocalData/Storage';
import Api from './Api';
import Env from './Env';

const showToast = (msg) => {
  setTimeout(() => {
      ToastAndroid.show(msg, ToastAndroid.SHORT);
  }, 500);
};

const size = Dimensions.get('window')

class ReceiptImage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      spinnerVisible: false
    };
  }

  componentDidMount() {
    this.props.onRef(this);
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
  }

  share(phone, reffNum, trxType, trxTypeDetail){
    requestWriteExternalStoragePermission().then(isPermission => {

      if(isPermission){
        this.setState({
            spinnerVisible: true
        });

        let params = {
            "phone": phone,
            "reffNum": reffNum,
            "trxType" : trxType,
            "trxTypeDetail" : trxTypeDetail,
            "screenHeight" : size.height,
            "screenWidth" : size.width
        }

        post(Env.base_url + Api.getReceiptByParams, params).then(response => {
          setTimeout(() => {
            if(response.code == ResponseCode.OK){

              this.setState({spinnerVisible: false});
              let options = {
                title: 'Share Receipt',
                url: 'data:image/png;base64,'+response.okContent.receipt
              };
              Share.open(options).then((res) => {
                //is open share receipt
               }).catch((err) => {
                //cancel share receipt
               });

            } else {
              this.setState({spinnerVisible: false});
              showToast(response.failContent.failDescription);
            }
          }, 1000);

        }).catch(error => {
          setTimeout(() => {
            this.setState({spinnerVisible: false});
            showToast(Error.timeout);
          }, 1000);
        });
      }
    });
  }

  download(phone, reffNum, trxType, trxTypeDetail){
    requestWriteExternalStoragePermission().then(isPermission => {

      if(isPermission){
        this.setState({
            spinnerVisible: true
        });

        let params = {
            "phone": phone,
            "reffNum": reffNum,
            "trxType" : trxType,
            "trxTypeDetail" : trxTypeDetail,
            "screenHeight" : size.height,
            "screenWidth" : size.width
        }

        post(Env.base_url + Api.getReceiptByParams, params).then(response => {
          setTimeout(() => {
            if(response.code == ResponseCode.OK){
              //path for androids
              let path = RNFetchBlob.fs.dirs.PictureDir + '/'+response.okContent.fileName;

              RNFetchBlob.fs.writeFile(path, response.okContent.receipt, 'base64').then(res =>{
                this.setState({spinnerVisible: false});
                showToast("Receipt transaksi berhasil disimpan!");
              }).catch(error => {
                this.setState({spinnerVisible: false});
                showToast("Receipt transaksi gagal disimpan!");
              });
            } else {
              this.setState({spinnerVisible: false});
              showToast(response.failContent.failDescription);
            }
          }, 1000);
        }).catch(error => {
          setTimeout(() => {
            this.setState({spinnerVisible: false});
            showToast(Error.timeout);
          }, 1000);
        });
      }
    });
  }

  render() {
    return (
      <Spinner
          visible={this.state.spinnerVisible}
          color='#e35200'
          customIndicator={
              <LottieView
                  style={{ height: 40, transform: [{ scale: 1.6 }] }}
                  autoSize={true}
                  source={require('../assets/animation/loading2.json')}
                  autoPlay
              />
          }
      />
    );
  }
}

ReceiptImage.propTypes = {
  onRef: PropTypes.any.isRequired
};

export default ReceiptImage;
