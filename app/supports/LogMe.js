
const isDebug = true;

function info(tag, message){
    if(isDebug) console.log("INFO:: "+tag+" : "+message);
};

function error(tag, message){
    if(isDebug) console.log("ERROR:: "+tag+" : "+message);
};

export const LogMe = {
	info,
  error
}
