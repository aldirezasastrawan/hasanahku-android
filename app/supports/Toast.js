
import { ToastAndroid } from "react-native";

function show(msg){
  setTimeout(() => {
      ToastAndroid.show(msg, ToastAndroid.SHORT);
  }, 500);
}

export const toast = {
  show
}
