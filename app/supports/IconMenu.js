
import {Icon} from './Images';

selected = iconName => {
  const iconList = {
    'asuransi' : Icon.menus.asuransi,
    'topup' : Icon.menus.topup,
    'wallet' : Icon.menus.wallet,
    'paketdata' : Icon.menus.paketdata,
    'pdam' : Icon.menus.pdam,
    'pln' : Icon.menus.pln,
    'pulsa' : Icon.menus.pulsa,
    'tariktunai' : Icon.menus.tariktunai,
    'telkompay' : Icon.menus.telkompay,
    'transfer' : Icon.menus.transfer,
    'tvcable' : Icon.menus.tvcable,
    'zakat' : Icon.menus.zakat,
    'lainnya' : Icon.menus.lainnya
  };

  return iconList[iconName];
};

export const IconMenu = {
  selected
}
