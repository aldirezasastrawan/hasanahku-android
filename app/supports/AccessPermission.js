import { PermissionsAndroid, ToastAndroid } from "react-native";

const showToast = (msg) => {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
};

export const requestWriteExternalStoragePermission = async () => {

  var isPermission = false;

    try {

      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
        isPermission = (granted === PermissionsAndroid.RESULTS.GRANTED);

        if(isPermission === false){
          showToast("Access denied!");
        }
      }
    } catch (err) {
         console.warn(err);
    }

    return isPermission;
};

export const requestReadContactsPermission = async () => {

  var isPermission = false;

    try {

      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS);
        isPermission = (granted === PermissionsAndroid.RESULTS.GRANTED);

        if(isPermission === false){
          showToast("Access denied!");
        }
      }
    } catch (err) {
         console.warn(err);
    }

    return isPermission;
};
