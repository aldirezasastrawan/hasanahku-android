import { Alert } from 'react-native'
import {LogMe} from './LogMe'
import {Error} from './Constants';
import {removeValue} from '../module/LocalData/Storage';

function alert(strMessage){
    Alert.alert(
      '',
      strMessage,
      [
        {text: 'OK'}
      ],
      {cancelable: false}
    )
}

function alertFail(failContent, context){
    Alert.alert(
      '',
      failContent.failDescription,
      [
        {
          text: 'OK',
          onPress: () => {
            if(failContent.failCode === "463" || failContent.failCode === "401" || failContent.failCode === "402" || failContent.failCode === "USER_ERR_02")
            {
              removeValue('userLogin');
              removeValue('hasLogin');
              removeValue('tokenLogin');
              context.props.navigation.navigate('Welcome')
            }
          }
        }
      ],
      {cancelable: false}
    )
}

function alertException(exception){
  LogMe.error("Exception", JSON.stringify(exception));

  Alert.alert(
    '',
    Error.cannot_connect_to_server,
    [
      {text: 'OK'}
    ],
    {cancelable: false}
  )
}

function alertCallback(strMessage, callback){
  Alert.alert(
    '',
    strMessage,
    [
      {text: 'OK', onPress: () => callback()}
    ],
    {cancelable: false}
  )
}

export const dialog = {
  alert, alertFail, alertException, alertCallback
}
