
function dialogShow(message, showSpinner){
  setTimeout(() => {
      this.setState({
          showSpinner: showSpinner
      });

      setTimeout(() => {
          alert(message);
      }, 500);

  }, 1000);
};

function onlyDialogShow(message){
  setTimeout(() => {
      alert(message);
  }, 500);
};

function show(object){
  setTimeout(() => {
    console.log(JSON.stringify(object));
    //  alert(message);
  }, 1000);
}

export const DialogBox = {
	dialogShow,
  onlyDialogShow,
  show
}
