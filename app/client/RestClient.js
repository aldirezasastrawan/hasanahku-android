import axios from 'axios';

export const post = (url, data) => {
    var headers = {
        'Content-Type': 'application/json'
    };

    return new Promise((resolve, reject) => {
        axios
            .post(url, data, { headers: headers })
            .then(response => {
                resolve(response.data);
            })
            .catch(error => {
              reject(error);
            });
    });
};

export const get = (url) => {
    var headers = {
        'Content-Type': 'application/json'
    };
    return new Promise((resolve, reject) => {
        axios
            .get(url, { headers: headers })
            .then(response => {
                resolve(response.data);
            })
            .catch(error => {
                reject(error);
            });
    });
};

export const postWithToken = (url, data, token) => {
    var headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
    };

    return new Promise((resolve, reject) => {
        axios
            .post(url, data, { headers: headers })
            .then(response => {
                resolve(response.data);
            })
            .catch(error => {
                reject(error);
            });
    });
};

export const getWithToken = (url, token) => {
    var headers = {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
    };
    return new Promise((resolve, reject) => {
        axios
            .get(url, { headers: headers })
            .then(response => {
                resolve(response.data);
            })
            .catch(error => {
                reject(error);
            });
    });
};
