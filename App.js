/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';

import { AppContainer } from './app/module/Navigation/Navigation';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import { pushNotifications } from './app/module/Notification';
import firebase from 'react-native-firebase';
import { saveData, getValue } from './app/module/LocalData/Storage';
import { Root } from 'native-base';


class App extends Component {

  componentDidMount() {
    console.log('app started');
    pushNotifications.configure(this.onNotif.bind(this));
    this.checkPermission();
    this.messageListener();
    firebase.messaging().subscribeToTopic('all');
  }

  checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getFcmToken();
    } else {
      this.requestPermission();
    }
  };

  getFcmToken = async () => {
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      console.log('fcm ', fcmToken);
      // this.showAlert('Sukses fcm', fcmToken);

      saveData('fcm', fcmToken);
      // Clipboard.setString(fcmToken);
    } else {
      this.showAlert('Failed', 'No token received');
    }
  };

  requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
    } catch (error) {
      // User has rejected permissionsx
    }
  };

  messageListener = async () => {
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification1 => {
        const { title, body } = notification1;
        //let notifData = { title: title, body: body };
        this.showAlert(title, body);
      });

    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const { title, body } = notificationOpen.notification;
        // this.showAlert(title, body);
        // this.storeData(notificationOpen.notification);
      });

    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      //const { title, body } = notificationOpen.notification;
      // this.showAlert(title, body);
      // this.storeData(notificationOpen.notification);
    }

    this.messageListener = firebase.messaging().onMessage(message => {
      let messageNotif = message._data;

      this.showAlert(messageNotif.title, messageNotif.body);
    });
  };

  showAlert = (title, message) => {
    pushNotifications.localNotification(title, message, title, message);
    // pushNotifications.localNotificationIOS(title, message, title, message);
    let notifData = {
      title: title,
      body: message,
    };
    this.storeData(notifData);
  };

  onNotif(notif) {
    pushNotifications.cancelAll();
    notif.finish(PushNotificationIOS.FetchResult.NoData);
    console.log('on notif ', notif);
    this.storeData(notif);
    //getValue('dataNotif');dicomment
  }

  storeData = async data => {
    var arrayNotif = [];
    getValue('dataNotif').then(response => {

      if (response == null) {
        arrayNotif.push(data);

        saveData('dataNotif', arrayNotif);
      } else {
        response.push(data);

        saveData('dataNotif', response);
      }
    });
  };

  render() {
    return (
      <Root>
        <AppContainer></AppContainer>
      </Root>
    );
  }

};


export default App;
